<!-- retain these comments for translator revision tracking -->
<!-- original version: 43693 -->


  <sect2 arch="arm"><title>Suport de CPU, plaques base i vídeo</title>

<para>

Cada arquitectura ARM diferent necessita del seu propi nucli. Per açò
la distribució de Debian estàndard tan sols suporta la instal·lació a
uns quants dels sistemes més comuns. El mode d'usuari de Debian però,
es pot utilitzar per <emphasis>qualsevol</emphasis> CPU ARM, incloent
xscale.

</para>

<para>

Quasi totes les CPU ARM poden funcionar amb els dos modes endian (big o
little). Tanmateix, la majoria de les implementacions de sistemes
actuals utilitzen el mode little endian. En aquest moment, Debian tan
sols suporta sistemes ARM en mode little endian.

</para>

<para>

Els sistemes suportats són:

<variablelist>

<varlistentry>
<term>Netwinder</term>
<listitem><para>

Aquest és el nom que rep el grup de màquines basades en la CPU StrongARM
110 i l'Intel 21285 Northbridge (també coneguda com Footbridge). Açò
comprèn màquines com: Netwinder (possiblement una de les més comunes
basades en ARM), CATS (també coneguda com EB110ATX), EBSA 285 i el
servidor personal de Compaq (cps, àlies skiff).

</para></listitem>
</varlistentry>

<varlistentry>
<term>IOP32x</term>
<listitem><para>

La línia de processadors d'E/S d'Intel (IOP) és en nombrosos productes
relacionats amb l'emmagatzemament i processament de dades. Debian
suporta actualment la plataforma IOP32x, representada pels xips IOP
80219 i 32x localitzats normalment als dispositius Network Attached
Storage (NAS). Debian suporta explícitament dos d'aquests dispositius:
el <ulink url="&url-arm-cyrius-glantank;">GLAN Tank</ulink>
d'IO-Data i el <ulink url="&url-arm-cyrius-n2100;">Thecus N2100</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>IXP4xx</term>
<listitem><para>

La plataforma IXP4xx està basada en el nucli XScale ARM d'Intel. Actualment,
només un sistema basat en IXP4xx està suportat: el Linksys NSLU2.
El Linksys NSLU2 (Network Storage Link per a dispositius de disc USB 2.0)
és un petit dispositiu que permet fàcilment l'emmagatzemament utilitzant
la xarxa. Ve amb una connexió Ethernet i dos ports USB per a connectar
els discs durs. Hi ha una pàgina externa amb les <ulink
url="&url-arm-cyrius-nslu2;">instruccions d'instal·lació</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>RiscPC</term>
<listitem><para>

Aquesta màquina és la que té el maquinari més antic suportat, però
aquest suport és incomplet per part del nou instal·lador.
Té un SO RISC en ROM, Linux pot arrencar-se des d'aquest
SO utilitzant linloader. El RiscPC té una targeta CPU modular i
típicament integra una CPU que té un 610 a 30MHz, un 710 a 40MHz o un
Strongarm110 a 233MHz. La placa base integra una IDE, vídeo SVGA, port
paral·lel, un port sèrie, teclat PS/2 i un port de ratolí propietari.
El mòdul d'ampliació del bus propietari permet més de 8 targetes d'expansió
depenent de la configuració, i alguns d'aquests tenen controladors Linux.

</para></listitem>
</varlistentry>

</variablelist>
</para>
  </sect2>
