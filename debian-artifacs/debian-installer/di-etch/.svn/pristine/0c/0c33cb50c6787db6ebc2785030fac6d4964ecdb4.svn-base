<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43558 -->

  <sect2 id="dhcpd">
   <title>DHCP サーバの設定</title>
<para>

<!--
One free software DHCP server is ISC <command>dhcpd</command>.
For &debian;, the <classname>dhcp3-server</classname> package is
recommended.  Here is a sample configuration file for it (see
<filename>/etc/dhcp3/dhcpd.conf</filename>):
-->
フリーソフトウェアの DHCP サーバのひとつに、
ISC の <command>dhcpd</command> があります。
&debian; では、<classname>dhcp3-server</classname> パッケージをお奨めします。
以下に、設定ファイルの例を示します。
(<filename>/etc/dhcpd.conf</filename> を参照)

<informalexample><screen>
option domain-name "example.com";
option domain-name-servers ns1.example.com;
option subnet-mask 255.255.255.0;
default-lease-time 600;
max-lease-time 7200;
server-name "servername";

subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option routers 192.168.1.1;
}

host clientname {
  filename "/tftpboot/tftpboot.img";
  server-name "servername";
  next-server servername;
  hardware ethernet 01:23:45:67:89:AB;
  fixed-address 192.168.1.90;
}
</screen></informalexample>

</para><para>

<!--
In this example, there is one server
<replaceable>servername</replaceable> which performs all of the work
of DHCP server, TFTP server, and network gateway.  You will almost
certainly need to change the domain-name options, as well as the
server name and client hardware address.  The
<replaceable>filename</replaceable> option should be the name of the
file which will be retrieved via TFTP.
-->
この例では、<replaceable>servername</replaceable> というサーバがひとつあり、
DHCP サーバ, TFTP サーバ, ネットワークゲートウェイの仕事をすべて行っています。
domain-name オプション、サーバ名、クライアントのハードウェアアドレスは、
必ず変更する必要があります。
<replaceable>filename</replaceable> オプションは TFTP 経由で取得するファイルの名前です。

</para><para>

<!--
After you have edited the <command>dhcpd</command> configuration file,
restart it with <userinput>/etc/init.d/dhcpd3-server restart</userinput>.
-->
<command>dhcpd</command> の設定ファイルの編集を終えたら、
<userinput>/etc/init.d/dhcpd3-server restart</userinput> で
<command>dhcpd</command> を再起動してください。

</para>

   <sect3 arch="x86">
   <title>DHCP 設定での PXE 起動の有効化</title>
<para>
<!--
Here is another example for a <filename>dhcp.conf</filename> using the
Pre-boot Execution Environment (PXE) method of TFTP.
-->
ここでは TFTP の Pre-boot Execution Environment (PXE) 法を用いた、
<filename>dhcp.conf</filename> の例を示します。

<informalexample><screen>
option domain-name "example.com";

default-lease-time 600;
max-lease-time 7200;

allow booting;
allow bootp;

# The next paragraph needs to be modified to fit your case
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option broadcast-address 192.168.1.255;
# the gateway address which can be different
# (access to the internet for instance)
  option routers 192.168.1.1;
# indicate the dns you want to use
  option domain-name-servers 192.168.1.3;
}

group {
  next-server 192.168.1.3;
  host tftpclient {
# tftp client hardware address
  hardware ethernet  00:10:DC:27:6C:15;
  filename "pxelinux.0";
 }
}
</screen></informalexample>

<!--
Note that for PXE booting, the client filename <filename>pxelinux.0</filename>
is a boot loader, not a kernel image (see <xref linkend="tftp-images"/>
below).
-->
PXE ブートでは、クライアントのファイル名はカーネルイメージではなく、
ブートローダであることに注意してください。
(以下 <xref linkend="tftp-images"/> 参照)

</para>
   </sect3>
  </sect2>
