<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 18767 -->
<!-- Piarres Beobidek egina 2004-ko Azaroaren 26 -->

 <sect2 id="install-packages">
 <title>Package Installation</title>

<para>

Gero debian baturiko eta aurre-sailkaturiko software bildumak
eskeiniko zaizkizu. Zuk beti paketeak banaka kudeatzeko aukera
izango duzu, horrela makinan instalatzen dezuna gehiago ezagutu
eta kontrolatzeko. Hori da geroago argibieak emango diren
<command>aptitude</command> programaren zeregina. Baina horrela
egiteak asko zail dezake erabiltzaile berrientzat debianek
dituen pakete zenbakiagatik (&num-of-distrib-pkgs;).

</para><para>

Hala ere, zuk <emphasis>atazak</emphasis> aukeratu ondoren 
banakako paketeak gehitu ahal izango dituzu beranduago.
Ataza hauek oso orokorrean gure ustetan ordenagailuak bete ditzaketen 
funtzio eta erabilera ezberdinetan pentsaturik aukeratu dira,
adibidez <quote>idazmahai ingurunea</quote>,
<quote>web zerbitzaria</quote>, edo <quote>inprimagailu zerbitzaria</quote>.

<footnote>
<para>

Jakin behar da zerrenda hau bistarazteko
<command>base-config</command>-ek <command>tasksel</command> programa 
abiarzte duela. Eskuzko pakete aukeraketarako
<command>aptitude</command> abiarziko da. Hauetako edozein
instalazioa amaitu ondoren edozein momentutan erabili daiteke
paketteak instalatu edo kentzeko. Pakete berezi ba instalatu nahi
baduzu sinpleki <userinput>apt-get install 
<replaceable>paketea</replaceable></userinput> erabili non 
<replaceable>paketea</replaceable> instalatu nahi duzun paketea
den.

</para>
</footnote>

</para><para>

Paketeak banaka aukeratu nahi izan ezkero <command>tasksel</command> menuan
<quote>eskuzko paketea aukeraketa</quote> aukeratu.

</para><para>

Bein atazka aukeratu dituzula sakatu 
<guibutton>Ados</guibutton> botoia. Puntu honetan
<command>aptitude</command>k aukeratu dituzun paketeak isntalatuko
ditu. Kontutan eduki nahiz ez atazik aukeratu lehenespen beharrezko, 
garrantsitsu edo estansarra duten eta sisteman instalaturik ez dauden
paketeak instalatu egingo direla. Funztio hau komando lerroan
<userinput>tasksel -s</userinput> egitearen berdina da eta 
momentu honetan 37MB inguru fitxategi deskargatzen ditu. Instalaturko
diren pakete zenbakia, Kb kopurua eta deskargatu behar izan ezkero
deskargatu behar de Kb kopurua erakutsiko du.

</para><para>

Debian sistema  erabilgarri dauden &num-of-distrib-pkgs; paketeetatik
gutxi batzu ez ditu kudeatzen ataza instalatzaileak. Pakete gehiagori
buruzko argibideak ikusteko erabili <userinput>apt-cache
search <replaceable>bilaketa-katea</replaceable></userinput>, non bilaketa
katea bilatu nahi duzun pakete edo betebeharra den (ikusi 
<citerefentry><refentrytitle>apt-cache</refentrytitle> <manvolnum>8</manvolnum>
</citerefentry> man orrialdea) edo 
<command>aptitude</command> aerabili.

</para>

 <sect3 id="aptitude">
<title><command>aptitude</command> erabiliaz pakete aukeraketa aurreratua</title>

<para>

<command>Aptitude</command> paketeak kudeatzeko program eguneratuaa
da. <command>aptitude</command>k paketeak banakam  emandako irizpide
batzuekin (erabiltzaile aurreratuentzat) edo atazak hautatzeko aukera 
ematen du. 

</para><para>

Laster-tekla oinarrizkoenak:

<informaltable>
<tgroup cols="2">
<thead>
<row>
  <entry>Tekla</entry><entry>Ekintza</entry>
</row>
</thead>

<tbody>
<row>
  <entry><keycap>Gora</keycap>, <keycap>Behera</keycap></entry>
  <entry>Aueka gora edo behera mugitu.</entry>
</row><row>
  <entry>&enterkey;</entry>
  <entry>Aukera Gaitu/Zabaldu/itxi.</entry>
</row><row>
  <entry><keycap>+</keycap></entry>
  <entry>Paketea instalaziorako markatu.</entry>
</row><row>
  <entry><keycap>-</keycap></entry>
  <entry>Paketea ezabatzeko markatu.</entry>
</row><row>
  <entry><keycap>d</keycap></entry>
  <entry>Pakete dependentziak erakutsi.</entry>
</row><row>
  <entry><keycap>g</keycap></entry>
  <entry>Paketeak orain deskargatu/instalatu/ezabaturemove.</entry>
</row><row>
  <entry><keycap>q</keycap></entry>
  <entry>Uneko ikuspegia utzi.</entry>
</row><row>
  <entry><keycap>F10</keycap></entry>
  <entry>Menua Gaitu.</entry>
</row>
</tbody></tgroup></informaltable>

Komando gehiagorako begiratu lineako laguntza  <keycap>?</keycap> tekla 
erabiliaz.

</para>
  </sect3>
 </sect2>
