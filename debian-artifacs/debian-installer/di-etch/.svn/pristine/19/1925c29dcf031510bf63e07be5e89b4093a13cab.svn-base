<!-- retain these comments for translator revision tracking -->
<!-- original version: 36639 untranslated -->

  <sect2 arch="arm" id="boot-tftp"><title>Booting from TFTP</title>

&boot-installer-intro-net.xml;

   <sect3 arch="arm"><title>Booting from TFTP on Netwinder</title>
<para>

Netwinders have two network interfaces: A 10Mbps NE2000-compatible
card (which is generally referred to as <literal>eth0</literal>) and
a 100Mbps Tulip card.  There may be problems loading the image via TFTP
using the 100Mbps card so it is recommended that you use the 10Mbps
interface (the one labeled with <literal>10 Base-T</literal>).

</para>
<note><para>

You need NeTTrom 2.2.1 or later to boot the installation system, and
version 2.3.3 is recommended.  Unfortunately, firmware files are currently
not available for download because of license issues.  If this situation
changes, you may find new images at <ulink url="http//www.netwinder.org/"></ulink>.

</para></note>
<para>

When you boot your Netwinder you have to interrupt the boot process during the
countdown.  This allows you to set a number of firmware settings needed in
order to boot the installer.  First of all, start by loading the default
settings:

<informalexample><screen>
    NeTTrom command-&gt; load-defaults
</screen></informalexample>

Furthermore, you must configure the network, either with a static address:

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 flash
    NeTTrom command-&gt; setenv eth0_ip 192.168.0.10/24
</screen></informalexample>

where 24 is the number of set bits in the netmask, or a dynamic address:

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 dhcp
</screen></informalexample>

You may also need to configure the <userinput>route1</userinput>
settings if the TFTP server is not on the local subnet.

Following these settings, you have to specify the TFTP server and the
location of the image.  You can then store your settings to flash.

<informalexample><screen>
    NeTTrom command-&gt; setenv kerntftpserver 192.168.0.1
    NeTTrom command-&gt; setenv kerntftpfile boot.img
    NeTTrom command-&gt; save-all
</screen></informalexample>

Now you have to tell the firmware that the TFTP image should be booted:

<informalexample><screen>
    NeTTrom command-&gt; setenv kernconfig tftp
    NeTTrom command-&gt; setenv rootdev /dev/ram
</screen></informalexample>

If you use a serial console to install your Netwinder, you need to add the
following setting:

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram console=ttyS0,115200
</screen></informalexample>

Alternatively, for installations using a keyboard and monitor you have to
set:

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram
</screen></informalexample>

You can use the <command>printenv</command> command to review your
environment settings.  After you have verified that the settings are
correct, you can load the image:

<informalexample><screen>
    NeTTrom command-&gt; boot
</screen></informalexample>

In case you run into any problems, a <ulink
url="http://www.netwinder.org/howto/Firmware-HOWTO.html">detailed
HOWTO</ulink> is available.

</para>
   </sect3>

   <sect3 arch="arm"><title>Booting from TFTP on CATS</title>
<para>

On CATS machines, use <command>boot de0:</command> or similar at the
Cyclone prompt.

</para>
   </sect3>
  </sect2>


  <sect2 arch="arm"><title>Booting from CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

To boot a CD-ROM from the Cyclone console prompt, use the command
<command>boot cd0:cats.bin</command>

</para>
  </sect2>


  <sect2 arch="arm" id="boot-firmware"><title>Booting from Firmware</title>

&boot-installer-intro-firmware.xml;

   <sect3 arch="arm" id="boot-firmware-nslu2"><title>Booting the NSLU2</title>
<para>

There are three ways how to put the installer firmware into flash:

</para>

    <sect4 arch="arm"><title>Using the NSLU2 web interface</title>
<para>

Go to the administration section and choose the menu item
<literal>Upgrade</literal>.  You can then browse your disk for the
installer image you have previously downloaded.  Then press the
<literal>Start Upgrade</literal> button, confirm, wait for a few minutes
and confirm again.  The system will then boot straight into the installer.

</para>
    </sect4>

    <sect4 arch="arm"><title>Via the network using Linux/Unix</title>
<para>

You can use <command>upslug2</command> from any Linux or Unix machine to
upgrade the machine via the network.  This software is packaged for
Debian.

First, you have to put your NSLU2 in upgrade mode:

<orderedlist>
<listitem><para>

Disconnect any disks and/or devices from the USB ports.

</para></listitem>
<listitem><para>

Power off the NSLU2

</para></listitem>
<listitem><para>

Press and hold the reset button (accessible through the small hole on the
back just above the power input).

</para></listitem>
<listitem><para>

Press and release the power button to power on the NSLU2.

</para></listitem>
<listitem><para>

Wait for 10 seconds watching the ready/status LED. After 10 seconds it
will change from amber to red. Immediately release the reset button.

</para></listitem>
<listitem><para>

The NSLU2 ready/status LED will flash alternately red/green (there is a 1
second delay before the first green). The NSLU2 is now in upgrade mode.

</para></listitem>
</orderedlist>

See the <ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">NSLU2-Linux
pages</ulink> if you have problems with this.

Once your NSLU2 is in upgrade mode, you can flash the new image:

<informalexample><screen>
sudo upslug2 -i di-nslu2.bin
</screen></informalexample>

Note that the tool also shows the MAC address of your NSLU2, which may come
in handy to configure your DHCP server.  After the whole image has been
written and verified, the system will automatically reboot.  Make sure you
connect your USB disk again now, otherwise the installer won't be able to
find it.

</para>
    </sect4>

    <sect4 arch="arm"><title>Via the network using Windows</title>
<para>

There is <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">a
tool</ulink> for Windows to upgrade the firmware via the network.

</para>
    </sect4>
   </sect3>
  </sect2>
