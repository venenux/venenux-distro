<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43841 -->
<!-- revisado y actualizado rudy, 5 diciembre 2004 -->

  <sect2 arch="sparc" id="boot-tftp"><title>Arrancar desde TFTP</title>

&boot-installer-intro-net.xml;

<para>

En m�quinas con OpenBoot, simplemente acceda al monitor de arranque
en la m�quina que est� instalando (vea la <xref linkend="invoking-openboot"/>).
Use la orden <userinput>�boot net�</userinput> para arrancar desde un
servidor con TFTP y RARP, o intente <userinput>�boot net:bootp�</userinput>
o <userinput>�boot net:dhcp�</userinput> para arrancar desde un servidor con TFTP,
BOOTP o DHCP. 

</para>
  </sect2>


  <sect2 arch="sparc"><title>Arrancar desde un CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

La mayor�a de versiones de OpenBoot soportan la orden
<userinput>�boot cdrom�</userinput>, la cual simplemente es un alias
para arrancar desde el dispositivo SCSI con ID 6 (o el maestro
secundario para sistemas basados en IDE).

</para>
  </sect2>


  <sect2 arch="sparc" condition="supports-floppy-boot">
  <title>Arrancar desde disquetes</title>
<para>

De momento s�lo se disponen im�genes de disquete para sparc32 pero no
para las publicaciones oficiales por razones t�cnicas (la raz�n es que
s�lo se pueden crear como root, algo que no pueden hacer nuestros
demonios de compilaci�n). Para encontrar las im�genes de disquete
para sparc32 mire los enlaces bajo <quote>im�genes compiladas
diariamente</quote> en el <ulink url="&url-d-i;">sitio web del proyecto del
instalador de Debian</ulink>.

</para><para>

Para arrancar desde disquete en una Sparc, use

<informalexample><screen>
Stop-A -> OpenBoot: "boot floppy"
</screen></informalexample>

Tenga en cuenta que la nueva arquitectura Sun4u (ultra) no soporta
arranque desde disquetes. Un t�pico mensaje de fallo es
<computeroutput>Bad magic number in disk label - Can't open disk label package</computeroutput>.

</para><para>

Algunas Sparcs (como la Ultra 10) tienen un fallo de OBP que les impide
arrancar (en lugar de simplemente no soportar esta caracter�stica).
Puede descargar la actualizaci�n de OBP apropiada con ID de producto
106121 desde <ulink url="http://sunsolve.sun.com"></ulink>.

</para><para>

Si est� arrancando desde disquetes y observa mensajes similares a

<informalexample><screen>
Fatal error: Cannot read partition
Illegal or malformed device name
</screen></informalexample>

entonces es posible que el arranque desde disquetes simplemente no
est� soportado en su m�quina.

</para>
  </sect2>

  <sect2 arch="sparc"><title>Mensajes de IDPROM</title>
<para>

Si no puede arrancar debido a que obtiene mensajes sobre un
problema con <quote>IDPROM</quote>, entonces es posible que la bater�a de su NVRAM,
la cual almacena la informaci�n de configuraci�n de su firmware, se
haya agotado. Vea el
<ulink url="&url-sun-nvram-faq;">PUF de NVRAM de Sun</ulink>
para mayor informaci�n.

</para>
  </sect2>
