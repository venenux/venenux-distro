<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 35613 -->


  <sect2 arch="mips"><title>CPU・マザーボード・ビデオのサポート</title>
<para>

<!--
Debian on &arch-title; supports the following platforms:
-->
&arch-title; 上で Debian は以下のプラットフォームをサポートしています。

<itemizedlist>
<listitem><para>

<!--
SGI IP22: this platform includes the SGI machines Indy, Indigo 2 and
Challenge S.  Since these machines are very similar, whenever this document
refers to the SGI Indy, the Indigo 2 and Challenge S are meant as well.
-->
SGI IP22: このプラットフォームには、Indy, Indigo 2, Challenge S といった
SGI マシンを含む。
これらのマシンはとても似通っているため、
本文書では、SGI Indy, Indigo 2, Challenge S についても同様に参照できる。

</para></listitem>
<listitem><para>

<!--
SGI IP32: this platform is generally known as SGI O2.
-->
SGI IP32: このプラットフォームは一般的に SGI O2 として知られています。

</para></listitem>
<listitem><para>

<!--
Broadcom BCM91250A (SWARM): this is an ATX form factor evaluation board
from Broadcom based on the dual-core SB1 1250 CPU.
-->
Broadcom BCM91250A (SWARM): デュアルコアの SB1 1250 CPU をベースにした、
Broadcom 製 ATX フォームファクタ評価ボード。

</para></listitem>
<listitem><para>

<!--
Broadcom BCM91480B (BigSur): this is an ATX form factor evaluation board
from Broadcom based on the quad-core SB1A 1480 CPU.
-->
Broadcom BCM91480B (BigSur): クアッドコアの SB1A 1480 CPU をベースにした、
Broadcom 製 ATX フォームファクタ評価ボード。

</para></listitem>
</itemizedlist>

<!--
Complete information regarding supported mips/mipsel machines can be found
at the <ulink url="&url-linux-mips;">Linux-MIPS homepage</ulink>.  In the
following, only the systems supported by the Debian installer will be
covered.  If you are looking for support for other subarchitectures, please
contact the <ulink url="&url-list-subscribe;">
debian-&arch-listname; mailing list</ulink>.
-->
mips/mipsel マシンサポートについての完全な情報は、
<ulink url="&url-linux-mips;">Linux-MIPS homepage</ulink> で見つかります。
以下では、Debian インストーラでサポートされているシステムについてのみ
対象にしています。
その他のサブアーキテクチャのサポートが必要な場合は、
<ulink url="&url-list-subscribe;">
debian-&arch-listname; メーリングリスト</ulink>に連絡してください。

</para>

   <sect3><title>CPU</title>
<para>

<!--
On SGI IP22, SGI Indy, Indigo 2 and Challenge S with R4000, R4400, R4600 and R5000
processors are supported by the Debian installation system on big endian
MIPS.  On SGI IP32, currently only systems based on the R5000 are supported.
The Broadcom BCM91250A evaluation board comes with an SB1 1250 chip with
two cores which are supported in SMP mode by this installer.  Similarly,
the BCM91480B evaluation board contains an SB1A 1480 chip with four cores
which are supported in SMP mode.
-->
R4000, R4400, R4600, R5000 プロセッサを搭載した SGI IP22, SGI Indy, Indigo 2, 
Challenge S は、
ビッグエンディアン MIPS 用 Debian インストールシステムでサポートしています。
SGI IP32 では現在、R5000 ベースのシステムのみサポートしています。
2 コアの SB1 1250 チップに付属する Broadcom BCM91250A 評価ボードは、
このインストーラの SMP モードでサポートしています。
同様に、4 コアの SB1A 1480 チップを含む BCM91480B 評価ボードは、
SMP モードでサポートしています。

</para><para>

<!--
Some MIPS machines can be operated in both big and little endian mode.  For
little endian MIPS, please read the documentation for the mipsel
architecture.
-->
MIPS マシンの中には、
ビッグエンディアンとリトルエンディアンの両方のモードで動作するものがあります。
リトルエンディアン用 MIPS については、
mipsel アーキテクチャの文書をご覧ください。

</para>
   </sect3>
  </sect2>

