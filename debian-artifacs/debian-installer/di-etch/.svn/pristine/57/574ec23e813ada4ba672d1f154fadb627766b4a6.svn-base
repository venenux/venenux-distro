<!-- retain these comments for translator revision tracking -->
<!-- original version: 43789 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.09.15 -->
<!-- updated 40542:43789 by Felipe Augusto van de Wiel (faw) 2007.01.20 -->

 <sect2 arch="alpha" id="alpha-firmware">
 <title>Firmware do Console Alpha</title>
<para>

O firmware do console é armazenado em uma flash ROM e inicializado
quando o sistema Alpha é ligado ou reiniciado. Existem dois tipos
diferentes de especificações de consoles usados nos sistemas Alpha,
e duas classes de firmwares de consoles disponíveis:

</para>

<itemizedlist>
<listitem><para>

  <emphasis>console SRM</emphasis>, baseado no subsistema de console
  da especificação Alpha, que oferece um ambiente operacional para o
  OpenVMS, UNIX Tru64 e sistema operacional Linux.

</para></listitem>
<listitem><para>

  <emphasis>ARC, AlphaBIOS, ou console ARCSBIOS </emphasis>, baseado na
  especificação Avançada de computação RISC (ARC), que oferece um ambiente
  operacional para o Windows NT.

</para></listitem>
</itemizedlist>

<para>

Da perspectiva do usuário, a diferença mais importante entre o
SRM e o ARC é que a escolha do console se restringem a possibilidade
do esquema de particionamento do disco rígido a partir do qual
inicializou.

</para><para>

O ARC requer que utilize uma tabela de partição MS-DOS (como
criada pelo comando <command>cfdisk</command>) para o disco de inicialização.
As tabelas de partição MS-DOS são partições no formato <quote>nativo</quote>
quando inicializa o sistema através do ARC. De fato, pois o AlphaBIOS contém um
utilitário de particionamento de disco, você pode preferir particionar seus
discos através dos menus da firmware antes de instalar o Linux.

</para><para>

De modo oposto, o SRM é <emphasis>incompatível</emphasis><footnote>

<para>
Especificamente, o formato do setor de inicialização requerida pela
Especificação do Sub-sistema Console, conflita com a posição da tabela de
partição do DOS.
</para>

</footnote> com tabelas de partição MS-DOS. Pois sistemas Unix Tru64
usa o formato BSD como volume de disco, este é o formato <quote>nativo</quote>
de partições para as instalações SRM.

</para><para>

O GNU/Linux é o único sistema operacional na Alpha que pode ser
inicializado através de ambos os tipos de console, mas a &debian;
&release; somente suporta a inicialização em sistemas baseados
em SRM. Se tiver um Alpha em que nenhuma versão do SRM esteja
disponível, se estiver fazendo dupla inicialização do sistema com
o Windows NT ou se seu dispositivo de inicialização requer o suporte
a console ARC para a inicialização da BIOS, não será possível
usar o programa de instalação da &debian; &release;. Você ainda
poderá executar a &debian; &release; em tal sistema usando outra
mídia de instalação; por exemplo, poderá instalar a Debian woody com
o MILO e realizar a atualização.

</para><para>

Devido ao <command>MILO</command> não estar disponível para
qualquer sistema Alpha em produção (em Fevereiro 2000) e porque
não é mais necessário comprar uma licença do OpenVMS ou Unix Tru64 para
ter uma firmware SRM em seu Alpha antigo, é recomendado que utilize uma
firmware SRM quando possível.

</para><para>

A seguinte tabela resume os sistemas disponíveis e suportados
entre as combinações de tipo/console (veja <xref linkend="alpha-cpus"/>
para os nomes dos tipos de sistema). A palavra <quote>ARC</quote> abaixo denota
qualquer um dos tipos de consoles compatíveis com o ARC.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry>Tipo de Sistema</entry>
  <entry>Tipo de Console Suportado</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>somente SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry>ARC (veja o manual da placa mãe) ou SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>somente o ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>somente o SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>somente o ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>somente o ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

Geralmente, nenhum destes consoles podem inicializar o Linux
diretamente assim é necessária a assistência de um gerenciador
de inicialização intermediário. Para o console SRM, é utilizado o
<command>aboot</command>, um gerenciador de inicialização pequeno e
independente de plataforma. Veja o (infelizmente desatualizado) <ulink
url="&url-srm-howto;">SRM HOWTO</ulink> para mais informações
sobre o <command>aboot</command>.

</para><para condition="FIXME">

Os seguintes parágrafos foram retirados do manual de instalação da
Woody e estão incluídos aqui para referência; eles podem ser úteis para
alguém em um estágio mais avançado quando a Debian suportará novamente
instalações baseados no MILO.

</para><para condition="FIXME">

Geralmente nenhum destes consoles podem ser iniciados diretamente no
Linux, assim é requerida a assistência de um gerenciador
de inicialização intermediário. Existem dois gerenciadores principais
no Linux: o <command>MILO</command> e o <command>aboot</command>.

</para><para condition="FIXME">

O <command>MILO</command> por si próprio é um console, que substitui
o ARC ou SRM na memória. O <command>MILO</command> pode ser diretamente
inicializado a partir do ARC e SRM e é o único método de inicializar o
Linux através do console ARC.
O <command>MILO</command> é específico de plataforma (um <command>MILO</command>
diferente é necessário para cada tipo de sistema) e existe somente para
estes sistemas, no qual o suporte ao ARC é mostrado na tabela acima.
Veja também o (infelizmente desatualizado)
<ulink url="&url-milo-howto;">MILO HOWTO</ulink>.

</para><para condition="FIXME">

O <command>aboot</command> é um gerenciador de inicialização pequeno e
independente de plataforma que somente roda a partir do SRM.
Veja o (também infelizmente desatualizado) <ulink
url="&url-srm-howto;">SRM HOWTO</ulink> para mais informações sobre o
<command>aboot</command>.

</para><para condition="FIXME">

Assim, três cenários são possíveis, dependendo do firmware do
console do sistema e se o <command>MILO</command> estiver ou não
disponível:

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

Por causa que o <command>MILO</command> não está disponível para qualquer
um dos sistemas alpha atualmente em produção (em Fevereiro de 2000) e
porque não é mais necessária a compra de licenças OpenVMS ou
Unix Tru64 para ter uma firmware SRM em seu antigo Alpha, é recomendado
que você use o SRM e o <command>aboot</command> em novas instalações do
GNU/Linux, a não ser que deseje fazer dupla inicialização com o
Windows NT.

</para><para>

A maioria dos servidores Alpha e todos os produtos servidores e estações
contém ambos o SRM e o AlphaBIOS em sua firmware. Para máquinas
<quote>half-flash</quote> tal como as várias placas de avaliação. É possível
mudar de uma versão para outra regravando a firmware. Também,
assim que o SRM estiver instalado, é possível executar o
ARC/AlphaBIOS a partir de um disquete (usando o comando <command>arc</command>).
Pelas razões mencionadas acima, nós recomendamos mudar para o SRM antes
de instalar a &debian;.

</para><para>

Como em outras arquiteturas, você deverá instalar a revisão mais
nova disponível de sua firmware<footnote>

<para>
Com exceção das Jensen, onde o Linux não é suportado nas versões de firmwares
mais novas que a 1.7 &mdash; Veja <ulink url="&url-jensen-howto;"></ulink> para
mais informações.
</para>

</footnote> antes de instalar a &debian;.
Para a Alpha, as atualizações de firmware podem ser obtidas de
<ulink url="&url-alpha-firmware;">Atualizações de Firmwares do Alpha</ulink>.

</para>
 </sect2>


  <sect2 arch="alpha"><title>Inicializando com o TFTP</title>
<para>

No SRM, as interfaces Ethernet são nomeadas com o prefixo
<userinput>ewa</userinput>, e será listada na saída do comando
<userinput>show dev</userinput>, como este (levemente editado):

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

Você precisará ajustar o protocolo de inicialização:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

Então verificar se o tipo de mídia está correto:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>

Você poderá obter uma listagem de modos válidos com <userinput>&gt;&gt;&gt;set ewa0_mode</userinput>.

</para><para>

Então, para inicializar através da primeira interface Ethernet, você poderá
digitar:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags ""
</screen></informalexample>

Isto fará a inicialização usando os parâmetros padrões do kernel como incluídas
na imagem de inicialização netboot.

</para><para>

Se deseja usar uma console serial, você <emphasis>deve</emphasis>
passar o argumento <userinput>console=</userinput> ao kernel.
Isto pode ser feito usando o argumento <userinput>-flags</userinput>
ao comando <userinput>boot</userinput> do console SRM. As portas
seriais tem o mesmo nome dos seus arquivos correspondentes em
<userinput>/dev</userinput>. Também, quando especificar parâmetros
adicionais de kernel, você deverá repetir certas opções adicionais
que são necessárias pelas imagens do &d-i;. Por exemplo, para inicializar
através de <userinput>ewa0</userinput> e usar uma console na
primeira porta serial, você deverá digitar:

<informalexample><screen>
&gt;&gt;&gt;boot ewa0 -flags console=ttyS0
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha"><title>Inicializando através de um CD-ROM com o console SRM</title>
<para>

Os CDs de instalação do &debian; incluem várias opções de inicialização
pré-configuradas para VGA e consoles seriais. Digite

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

para inicializar usando console VGA, onde onde <replaceable>xxxx</replaceable>
é sua unidade de CD-ROM na notação do SRM. Para usar um console serial no
primeiro dispositivo serial, digite

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

e para console na segunda porta serial, digite

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <title>Inicializando através de um CD-ROM com o ARC ou console AlphaBIOS</title>
<para>

Para inicializar através de um CD-ROM a partir do console ARC, encontre
seu nome de código de sub-arquitetura (veja <xref linkend="alpha-cpus"/>),
então digite <filename>\milo\linload.exe</filename> como gerenciador de
inicialização e <filename>\milo\<replaceable>subarch</replaceable></filename>
(aonde <replaceable>subarch</replaceable> é o nome de sub-arquitetura
apropriada) como caminho do OS no menu <quote>OS Selection Setup</quote>.
Ruffians fez uma exceção: você precisará usar
<filename>\milo\ldmilo.exe</filename> como gerenciador de inicialização.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Inicialização através de Disquetes com o Console SRM</title>
<para>

No aviso de comando do SRM (<prompt>&gt;&gt;&gt;</prompt>), digite o seguinte
comando:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

Possivelmente substituindo <filename>dva0</filename> com o nome de
dispositivo atual. Normalmente <filename>dva0</filename> é o disquete; digite

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

para ver a lista de dispositivos (e.g., caso deseje inicializar a partir de
uma unidade de CD).
Note que se se estiver inicializando através do MILO, o argumento
<command>-flags</command> será ignorado, assim você poderá só digitar
<command>boot dva0</command>.
Se tudo funcionar bem, você verá o kernel do Linux iniciando.

</para><para>

Se quiser especificar parâmetros de inicialização do
kernel via <command>aboot</command>, use o seguinte comando:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 argumentos"
</screen></informalexample>

(digite um por linha), substituindo, se necessário, o nome atual
do dispositivo de inicialização SRM por <filename>dva0</filename>,
o nome do dispositivo de inicialização do Linux por
<filename>fd0</filename> e os parâmetros designados do kernel por
<filename>argumentos</filename>.

</para><para>

Se desejar especificar parâmetros do kernel quando inicializar via
<command>MILO</command>, você terá que interromper o processo de
inicialização assim que entrar no MILO. Veja
<xref linkend="booting-from-milo"/>.
</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Inicializando a partir de Disquetes com o Console ARC ou AlphaBIOS</title>

<para>

No menu de seleção de sistema operacional, defina o
<command>linload.exe</command> como gerenciador de inicialização e
<command>milo</command> como o caminho do sistema operacional. Inicialize
usando a nova entrada criada.

</para>
  </sect2>


<sect2 arch="alpha" condition="FIXME" id="booting-from-milo"><title>Inicializando com o MILO</title>
<para>

O MILO contido na mídia de inicialização está configurado para
iniciar automaticamente o Linux. Caso deseje intervir, tudo
que precisa fazer é pressionar espaço durante a contagem regressiva
do MILO.

</para><para>

Caso deseje especificar todos os bits explicitamente (por exemplo,
para fornecer parâmetros adicionais), você poderá usar um comando
como este:

<informalexample><screen>
MILO> boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1 <!-- argumentos -->
</screen></informalexample>

Se estiver inicializando através de alguma outra mídia que não seja
um disquete, substitua <filename>fd0</filename> do exemplo acima com
o nome de dispositivo apropriado na notação do Linux. O comando
<command>help</command> deve lhe dar uma breve referência do MILO.

</para>
 </sect2>
