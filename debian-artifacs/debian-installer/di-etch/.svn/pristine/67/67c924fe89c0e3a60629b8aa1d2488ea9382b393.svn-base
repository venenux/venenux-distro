<!-- $Id: x86.xml 44147 2007-01-13 15:50:09Z mck-guest $ -->
<!-- original version: 43730 -->

   <sect3 arch="x86">
   <title>Rozdělení USB zařízení</title>
<note><para>

Protože má většina USB klíčenek přednastavenou jednu velkou oblast
typu FAT16, pravděpodobně nemusíte klíčenku přeformátovávat. Pokud to
však musíte provést, použijte pro vytvoření této oblasti
<command>cfdisk</command> nebo podobný nástroj pro dělení disku. Poté
vytvořte souborový systém příkazem

<informalexample><screen>
<prompt>#</prompt> <userinput>mkdosfs /dev/<replaceable>sda1</replaceable></userinput>
</screen></informalexample>

Příkaz <command>mkdosfs</command> je obsažen v balíku
<classname>dosfstools</classname>. Pozorně se přesvědčete, že
používáte správný název zařízení!

</para></note><para>

Pro zavedení jádra z klíčenky je zapotřebí zavaděče. Přestože byste
mohli použít téměř libovolný zavaděč (např. <command>LILO</command>),
je zvykem používat <command>SYSLINUX</command>, protože používá oblast
typu FAT16 a jeho nastavení se provádí úpravou jednoduchého textového
souboru. Díky tomu můžete zavaděč konfigurovat z téměř libovolného
operačního systému.

</para><para>

Pro instalaci <command>SYSLINUX</command>u na 1. oblast USB klíčenky
musíte mít nainstalované balíky <classname>syslinux</classname> a
<classname>mtools</classname> a daná oblast nesmí být připojená. Poté
zadejte příkaz

<informalexample><screen>
<prompt>#</prompt> <userinput>syslinux /dev/<replaceable>sda1</replaceable></userinput>
</screen></informalexample>

čímž se na začátek oblasti zapíše zaváděcí sektor a vytvoří se soubor
<filename>ldlinux.sys</filename>, který obsahuje hlavní část zavaděče.

</para><para>

Připojte oblast (<userinput>mount /dev/sda1 /mnt</userinput>)
a nakopírujte na ni následující soubory:

<itemizedlist>
<listitem><para>

<filename>vmlinuz</filename> (jádro)

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename> (obraz ramdisku)

</para></listitem>
<listitem><para>

<filename>syslinux.cfg</filename> (konfigurační soubor SYSLINUXu)

</para></listitem>
<listitem><para>

volitelné jaderné moduly

</para></listitem>
</itemizedlist>

Pokud si chcete soubory přejmenovat, pamatujte, že
<command>SYSLINUX</command> umí pracovat pouze s DOSovými (8.3) názvy
souborů.

</para><para>

Konfigurační soubor <filename>syslinux.cfg</filename> by měl obsahovat
následující dvě řádky:

<informalexample><screen>
default vmlinuz
append initrd=initrd.gz
</screen></informalexample>

</para>
   </sect3>
