<chapter id="translators"><title>For translators: translating &d-i;</title>

<sect1 id="levels">
<title>
The &d-i; five levels of translation
</title>

<para>
The Debian Installer translator process has been divided into several
<emphasis>levels</emphasis> which represent steps towards a complete translation of the
installer for the Debian GNU/Linux distribution.
</para>

<para>Translators should work in the order detailed below, level by
level. Inside each level, complete the steps in the described
order. This order is related to the degree of <emphasis>importance</emphasis> of each
material needing translation.
</para>

<para>The following parts of this chapter will give details and
context about each level components, instructions for
getting the material which needs translations as well as sending back
the translated material.
</para>

<para>
Translators should read them carefully, even if they already worked on
Debian translations.
</para>

<para>
Due to the granularity of Debian work and more specifically the
packages system, translators may find differences between the various packages
a bit annoying (some use <acronym>SVN</acronym>, some others <acronym>CVS</acronym>, some other no
repository at all...). This is a consequence of the current work organization
in Debian. 
</para>

</sect1>

<sect1 id="levels_summary">
<title>
Levels summary
</title>

<para>
This section is intended to give translators a general idea of each
level contents.
</para>

	  <orderedlist>
	    <listitem>
	      <para>
		level 1: all core &d-i; packages;
	      </para>
	    </listitem>
	    <listitem>
	      <para>
		level 2: all non core &d-i; material involved for
		user interaction screens during a <emphasis>default
		priority</emphasis> installation of a Debian base
		system with default choices;
	      </para>
	    </listitem>
	    <listitem>
	      <para>
		level 3: all non-core &d-i; material involved for
		user interaction screens during the install of a standard desktop or laptop system et default priority;
	      </para>
	    </listitem>
	    <listitem>
	      <para>
		level 4: all non-core &d-i; material involved for
		user interaction screens during the install of optional tasks
                of the task selection step, at the default priority;
	      </para>
	    </listitem>
	    <listitem>
	      <para>
		level 5: all packages that have been previously part
		of &d-i; levels and are considered important enough on
		a standard Debian system to be kept here.
		</para>
	    </listitem>
	  </orderedlist>


</sect1>

&translators-level1.xml;
&translators-level2.xml;
&translators-level3.xml;
&translators-level4.xml;
&translators-level5.xml;
&string-freezes.xml;

</chapter>
