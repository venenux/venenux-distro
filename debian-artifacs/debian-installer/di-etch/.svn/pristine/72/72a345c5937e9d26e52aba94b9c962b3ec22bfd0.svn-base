<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 29000 -->

<sect1><title>� propos des copyrights et des licences des logiciels</title>

<para>
Vous avez problablement lu les licences fournies avec les
logiciels commerciaux&nbsp;: elles disent que vous ne pouvez utiliser 
qu'une seule copie du logiciel et sur un seul ordinateur. La licence du 
syst�me Debian GNU/Linux est totalement diff�rente. Nous vous encourageons � 
copier le syst�me Debian GNU/Linux sur tous les ordinateurs de votre �cole ou 
de votre entreprise. Passez-le � vos amis et aidez-les � l'installer sur leur 
ordinateur. Vous pouvez m�me faire des milliers de copies et les 
<emphasis>vendre</emphasis> &mdash; avec quelques restrictions cependant. La distribution
Debian est en effet fond�e sur le <emphasis>logiciel libre</emphasis>.
</para>

<para>
Qu'un logiciel soit <emphasis>libre</emphasis> ne veut pas dire qu'il est 
d�pourvu de copyright et ne signifie pas que le c�d�rom qui contient ce 
logiciel doit �tre distribu� gratuitement. Cela signifie d'une part que les 
licences des programmes individuels ne vous obligent pas � payer pour avoir 
le droit d'utiliser ou de distribuer ces programmes. Et cela signifie d'autre 
part que non seulement on peut �tendre, adapter ou modifier un programme, 
mais qu'on peut aussi distribuer le r�sultat de ce travail. 

<note><para>
Le projet Debian met � disposition beaucoup de paquets qui ne satisfont pas 
� nos crit�res de libert� &mdash; c'est une concession pragmatique � nos 
utilisateurs. 
Ces paquets ne font pas partie de la distribution officielle, et ils sont 
distribu�s dans les parties <userinput>contrib</userinput> et 
<userinput>non-free</userinput> des miroirs Debian, ou bien sur des c�d�roms 
vendus par des tiers&nbsp;; voyez la 
<ulink url="&url-debian-faq;">FAQ Debian</ulink>,
dans les <quote>archives FTP Debian</quote>, pour plus d'informations sur 
l'organisation et le contenu des archives.</para></note>
</para>

<para>
Beaucoup de programmes dans le syst�me Debian sont distribu�s selon les termes
de la licence <emphasis>GNU</emphasis> 
<emphasis>General Public License</emphasis>, souvent simplement appel�e 
la <quote>GPL</quote>. La licence <emphasis>GPL</emphasis> oblige � donner 
le <emphasis>code source</emphasis> du programme lorsque vous distribuez une 
copie binaire de ce programme�; cet article assure que tout utilisateur pourra 
modifier le programme. Et c'est pourquoi nous avons inclus le code
source <footnote>
<para>Pour savoir o� trouver et comment d�compresser et construire les paquets 
source Debian, voyez la 
<ulink url="&url-debian-faq;">FAQ Debian</ulink>, sous le titre 
<quote>Basics of the Debian Package Management System</quote>.
</para></footnote> de tous les programmes pr�sents dans le syst�me Debian.
</para>

<para>
D'autres formes de copyright et de licence sont utilis�es dans le syst�me 
Debian. Vous pourrez trouver les copyrights et les licences de chaque 
programme dans le r�pertoire
<filename>/usr/share/doc/<replaceable>nom-du-paquet</replaceable>/copyright</filename>
une fois le paquet install�.
</para>

<para>
Pour en savoir plus sur les licences et comment Debian d�cide de ce qui est
suffisamment libre pour �tre inclus dans la distribution principale,
consultez les
<ulink url="&url-dfsg;">directives Debian pour le logiciel libre</ulink>.
</para>

<para>
L�galement, l'avertissement le plus important est que ce logiciel 
est fourni <emphasis>sans aucune garantie</emphasis>. Les programmeurs qui 
ont cr�� ce logiciel l'ont fait pour le b�n�fice de la communaut�. Il n'est 
donn� aucune garantie quant � la pertinence du logiciel pour un quelconque 
usage. Cependant, puisque ce logiciel est libre, vous pouvez le modifier
autant que vous le d�sirez &mdash; et vous pouvez ainsi
profiter du travail de ceux qui ont am�lior� le logiciel.
</para>

</sect1>
