partman-auto-raid (5) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A
  * Dutch (nl.po) by Bart Cornelis

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 18:17:04 +0100

partman-auto-raid (4) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Latvian (lv.po) by Aigars Mahinovs
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 12:20:39 +0100

partman-auto-raid (3) unstable; urgency=low

  * Move init.d scripts to auto.d to avoid progress bar conflicts.
    Requires partman-base 99. See: #391676.

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Estonian (et.po) by Siim Põder
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Fri,  8 Dec 2006 18:32:09 +0100

partman-auto-raid (2) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Giuseppe Sacco
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Albanian (sq.po) by Elian Myftiu
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 15:50:25 +0200

partman-auto-raid (1) unstable; urgency=low

  [ Simon Huggins ]
  * Initial release.

  [ Frans Pop ]
  * Kernels >=2.6.18 use have module raid456 instead of raid5.
    Thanks to Sven Luther for the heads-up (#389079).

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Jamil Ahmed
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Persian (fa.po) by Arash Bijanzadeh
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Gujarati (gu.po) by Kartik Mistry
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Kazakh (kk.po) by Talgat Daniyarov
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Sat, 14 Oct 2006 01:12:13 +0200
