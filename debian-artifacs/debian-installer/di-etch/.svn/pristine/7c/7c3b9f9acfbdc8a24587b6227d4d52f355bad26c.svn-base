<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43734 -->

   <sect3 id="pkgsel">
<!--
   <title>Selecting and Installing Software</title>
-->
   <title>ソフトウェアの選択・インストール</title>

<para>

<!--
During the installation process, you are given the opportunity to select
additional software to install. Rather than picking individual software
packages from the &num-of-distrib-pkgs; available packages, this stage of
the installation process focuses on selecting and installing predefined
collections of software to quickly set up your computer to perform various
tasks.
-->
インストール処理中に、追加ソフトウェアをインストールする機会があります。
&num-of-distrib-pkgs; 個もの利用可能パッケージから、
個々のパッケージを取り上げるよりも、インストール処理のこの段階では、
いち早く様々なコンピュータのタスクをセットアップするよう、
定義済みのソフトウェア集合を選択・インストールするのに集中します。


</para><para>

<!-- TODO: Should explain popcon first -->

<!--
So, you have the ability to choose <emphasis>tasks</emphasis> first,
and then add on more individual packages later.  These tasks loosely
represent a number of different jobs or things you want to do with
your computer, such as <quote>Desktop environment</quote>,
<quote>Web server</quote>, or <quote>Print server</quote><footnote>
-->
ですから、まず<emphasis>タスク</emphasis>を選択し、
後で個々のパッケージを追加できます。
タスクは、様々なジョブやあなたがコンピュータにやらせたいことを、
いくつか大まかに表しています。<quote>デスクトップ環境</quote>、
<quote>Web サーバ</quote>、<quote>Print サーバ</quote>といった具合です
<footnote>

<para>

<!--
You should know that to present this list, the installer is merely
invoking the <command>tasksel</command> program. It can be run at any
time after installation to install more packages (or remove them), or
you can use a more fine-grained tool such as <command>aptitude</command>.
If you are looking for a specific single package, after
installation is complete, simply run <userinput>aptitude install
<replaceable>package</replaceable></userinput>, where
<replaceable>package</replaceable> is the name of the package you are
looking for.
-->
表示されるリストは、
インストーラが単に <command>tasksel</command> プログラムを起動しているだけ、
ということを知っておいてください。インストールの後で、
他のパッケージをインストール (または削除) するのにいつでも実行できます。
また <command>aptitude</command> のような、よりきめ細かいツールも利用できます。
インストール完了後、特定の 1 パッケージを探すのなら、単に 
<userinput>aptitude install <replaceable>パッケージ名</replaceable></userinput>
を実行してください。
<replaceable>パッケージ名</replaceable>は、探したいパッケージ名です。

</para>

<!--
</footnote>. <xref linkend="tasksel-size-list"/> lists the space
requirements for the available tasks.
-->
</footnote>。 <xref linkend="tasksel-size-list"/> に、
利用可能タスクの必要容量一覧があります。

</para><para>

<!--
Some tasks may be pre-selected based on the characteristics of the
computer you are installing. If you disagree with these selections you can
un-select the tasks. You can even opt to install no tasks at all at this point.
-->
いくつかのタスクは、インストールするコンピュータの特性により、
あらかじめ選択されている可能性があります。
選択されているものが合わない場合は、そのタスクの選択をはずせます。
全くタスクを選ばないようにもできます。

</para>
<note><para>

<!-- TODO: Explain the "Standard system" task first -->

<!--
The <quote>Desktop environment</quote> task will install the GNOME desktop
environment. The options offered by the installer currently do not allow to
select a different desktop environment such as for example KDE.
-->
<quote>デスクトップ環境</quote> タスクは、
GNOME デスクトップ環境をインストールします。
現在インストーラで提供されているオプションでは、
KDE のような他のデスクトップ環境を選択できません。

</para><para>

<!--
It is possible to get the installer to install KDE by using preseeding
(see <xref linkend="preseed-pkgsel"/>) or by adding
<literal>tasks="standard, kde-desktop"</literal> at the boot prompt
when starting the installer. However, this will only work if the packages
needed for KDE are actually available. If you are installing using a full
CD image, they will need to be downloaded from a mirror as KDE packages are
not included on the first full CD; installing KDE this way should work fine
if you are using a DVD image or any other installation method.
-->
preseed (<xref linkend="preseed-pkgsel"/> 参照) を使用したり、
インストーラの起動時にブートプロンプトで 
<literal>tasks="standard, kde-desktop"</literal> と指定して、
インストーラが KDE をインストールするようにできます。
しかし、KDE に必要なパッケージが有効な場合のみ動作します。
フル CD イメージでインストールしている場合、
KDE のパッケージがフル CD の 1 枚目に入っていないので、
ミラーサイトからダウンロードする必要があります。
DVD イメージやその他のインストール方法では、
KDE でのインストールがうまくいくでしょう。

</para><para>

<!--
The various server tasks will install software roughly as follows.
DNS server: <classname>bind9</classname>;
File server: <classname>samba</classname>, <classname>nfs</classname>;
Mail server: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Print server: <classname>cups</classname>;
SQL database: <classname>postgresql</classname>;
Web server: <classname>apache</classname>.
-->
各サーバタスクでは、おおまかに以下のソフトウェアをインストールします。
DNS サーバ: <classname>bind9</classname>。
ファイルサーバ: <classname>samba</classname>, <classname>nfs</classname>。
メールサーバ: <classname>exim4</classname>, 
<classname>spamassassin</classname>, <classname>uw-imap</classname>。
印刷サーバ: <classname>cups</classname>。
SQL データベース: <classname>postgresql</classname>。
Web サーバ: <classname>apache</classname>。

</para></note>
<para>

<!--
Once you've selected your tasks, select <guibutton>OK</guibutton>. At this
point, <command>aptitude</command> will install the packages that are part
of the tasks you've selected.
-->
タスクを選択したら、<guibutton>OK</guibutton> を選択してください。
<command>aptitude</command> が選択したパッケージの一部をインストールし始めます。

</para>
<note><para>

<!--
In the standard user interface of the installer, you can use the space bar
to toggle selection of a task.
-->
インストーラの標準ユーザインターフェースでは、
タスクの選択をスペースバーでトグルできます。

</para></note>
<para>

<!--
You should be aware that especially the Desktop task is very large.
Especially when installing from a normal CD-ROM in combination with a
mirror for packages not on the CD-ROM, the installer may want to retrieve
a lot of packages over the network. If you have a relatively slow
Internet connection, this can take a long time. There is no option to
cancel the installation of packages once it has started.
-->
デスクトップタスクは非常に大きいことを意識していてください。
特に、通常の CD-ROM と、
ミラーサイトにある CD-ROM 外のパッケージを組み合わせる場合、インストーラが、
ネットワークから大量のパッケージを取得しようとするかもしれません。
インターネット接続が低速な場合、長い時間かかるでしょう。
一度、パッケージのインストールを始めたら、
キャンセルするオプションはありません。

</para><para>

<!--
Even when packages are included on the CD-ROM, the installer may still
retrieve them from the mirror if the version available on the mirror is
more recent than the one included on the CD-ROM. If you are installing
the stable distribution, this can happen after a point release (an update
of the original stable release); if you are installing the testing
distribution this will happen if you are using an older image.
-->
パッケージが CD-ROM に含まれている場合でも、
CD-ROM にあるパッケージよりもミラーサイトにあるパッケージの方が新しければ、
インストーラはミラーサイトから取得しようとします。
安定版をインストールしている場合はポイントリリース 
(オリジナルの安定版リリースの更新) 後に、
テスト版をインストールしている場合は古いイメージを使用していると、
こういったことが起こり得ます。

</para><para>

<!--
Each package you selected with <command>tasksel</command> is downloaded,
unpacked and then installed in turn by the <command>apt-get</command> and
<command>dpkg</command> programs. If a particular program needs more
information from the user, it will prompt you during this process.
-->
<command>tasksel</command> で選択したパッケージを、
今度は <command>apt-get</command> プログラムと 
<command>dpkg</command> プログラムでそれぞれダウンロード・展開し、
インストールします。
口うるさいプログラムが、ユーザからの情報をもっと必要とする場合は、
この処理中に入力をうながしてきます。

</para>
   </sect3>
