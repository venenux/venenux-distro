<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 44026 -->


 <chapter id="d-i-intro"><title>Debian Installer の使用法</title>

 <sect1><title>インストーラの動作</title>
<para>

<!--
The Debian Installer consists of a number of special-purpose
components to perform each installation task. Each component performs
its task, asking the user questions as necessary to do its job.
The questions themselves are given priorities, and the priority
of questions to be asked is set when the installer is started.
-->
Debian Installer は各インストールタスクを実行するために、
たくさんの特定用途コンポーネントから成ります。
各コンポーネントは、必要ならユーザに質問をして、そのタスクを実行します。
この質問には優先度が設定されており、
この優先度はインストーラの起動時に設定することができます。

</para><para>

<!--
When a default installation is performed, only essential (high priority)
questions will be asked. This results in a highly automated installation
process with little user interaction. Components are automatically run
in sequence; which components are run depends mainly on the installation
method you use and on your hardware. The installer will use default values
for questions that are not asked.
-->
デフォルトのインストールでは、不可欠な (優先度が高い) 質問しかしません。
これにより、ユーザの入力をほとんど行わず、
高度な自動インストールを行うことができます。
コンポーネントは自動的に順番に実行されます。
どのコンポーネントを実行するかは、
主に使用するインストール法やハードウェアに左右されます。
インストーラは、質問しない項目についてはデフォルト値を使用します。

</para><para>
<!--
If there is a problem, the user will see an error screen, and the
installer menu may be shown in order to select some alternative
action. If there are no problems, the user will never see the
installer menu, but will simply answer questions for each component
in turn. Serious error notifications are set to priority
<quote>critical</quote> so the user will always be notified.
-->
問題がある場合はエラー画面を表示し、
インストーラメニューに、代替アクションを選択するように表示するかもしれません。
いずれも問題なければ、ユーザはインストールメニューを目にすることはなく、
単に順番に各コンポーネントの質問に答えて行くだけでしょう。
重大なエラー通知は優先度を<quote>重要</quote>に設定されているため、常に表示されます。

</para><para>

<!--
Some of the defaults that the installer uses can be influenced by passing
boot arguments when &d-i; is started. If, for example, you wish to
force static network configuration (DHCP is used by default if available),
you could add the boot parameter <userinput>netcfg/disable_dhcp=true</userinput>.
See <xref linkend="installer-args"/> for available options.
-->
インストーラが使用するデフォルト値は、
&d-i; の起動時にパラメータで渡して指定できます。
たとえば、強制的に静的ネットワーク設定をしたい場合、
(デフォルトでは可能なら DHCP を利用します) 起動パラメータに 
<userinput>netcfg/disable_dhcp=true</userinput> を加えることができます。
利用できるオプションは <xref linkend="installer-args"/> を参照してください。

</para><para>

<!--
Power users may be more comfortable with a menu-driven interface,
where each step is controlled by the user rather than the installer
performing each step automatically in sequence. To use the installer
in a manual, menu-driven way, add the boot argument
<userinput>priority=medium</userinput>.
-->
パワーユーザは、メニュードリブンインターフェース 
(自動で順に各ステップを実行するインストーラではなく、
ユーザが各ステップを制御する) の方が、満足するかもしれません。
手動 (メニュー駆動) でインストーラを使用するには、
起動引数に <userinput>priority=medium</userinput> を加えてください。

</para><para>

<!--
If your hardware requires you to pass options to kernel modules as
they are installed, you will need to start the installer in
<quote>expert</quote> mode. This can be done by either using the
<command>expert</command> command to start the installer or by adding
the boot argument <userinput>priority=low</userinput>.
Expert mode gives you full control over &d-i;.
-->
ハードウェアをインストールする際に、
オプションをカーネルモジュールへ渡す必要がある場合、
<quote>エキスパート</quote>モードでインストーラを起動する必要があります。
これは、インストーラを起動するコマンドに <command>expert</command> を使用する、
あるいは起動引数に <userinput>priority=low</userinput>
を加えることで行います。
エキスパートモードでは &d-i; をフルコントロールできます。

</para><para>

<!--
The normal installer display is character-based (as opposed to the now
more familiar graphical interface). The mouse is not operational in
this environment. Here are the keys you can use to navigate within the
various dialogs.  The <keycap>Tab</keycap> or <keycap>right</keycap>
arrow keys move <quote>forward</quote>, and the <keycombo> <keycap>Shift</keycap>
<keycap>Tab</keycap> </keycombo> or <keycap>left</keycap> arrow keys
move <quote>backward</quote> between displayed buttons and selections.
The <keycap>up</keycap> and <keycap>down</keycap> arrow select
different items within a scrollable list, and also scroll the list
itself. In addition, in long lists, you can type a letter to cause the
list to scroll directly to the section with items starting with the
letter you typed and use <keycap>Pg-Up</keycap> and
<keycap>Pg-Down</keycap> to scroll the list in sections.  The
<keycap>space bar</keycap> selects an item such as a checkbox.  Use
&enterkey; to activate choices.
-->
通常インストーラの表示はキャラクタベースです。
(今、よりよく知られているグラフィカルインターフェースに対立する用語として) 
マウスはこの環境では使用できません。
ここでは、様々なダイアログでナビゲートするキーを紹介します。
ボタンや選択肢が表示されている間は、
<keycap>Tab</keycap> キーや<keycap>右</keycap>矢印は<quote>前方</quote>へ、
<keycombo><keycap>Shift</keycap> <keycap>Tab</keycap></keycombo> や
<keycap>左</keycap>矢印は<quote>後方</quote>へ移動します。
<keycap>上</keycap><keycap>下</keycap>矢印は、
スクロールするリスト内の項目を選択し、またリスト自体もスクロールさせます。
さらに、長いリストでは、タイプした文字で始まる項目に直接スクロールしますし、
リストのスクロールに <keycap>Pg-Up</keycap> や <keycap>Pg-Down</keycap> も
使用できます。
<keycap>スペースバー</keycap> は、チェックボックスのような項目を選択します。
選択肢を有効にするには &enterkey; を使用してください。

</para><para arch="s390">

<!--
S/390 does not support virtual consoles. You may open a second and third
ssh session to view the logs described below.
-->
S/390 は仮想コンソールをサポートしません。以下で説明するように、
ログの参照用に第 2 第 3 の ssh セッションを開いてください。

</para><para>

<!--
Error messages and logs are redirected to the fourth console.
You can access this console by
pressing <keycombo><keycap>Left Alt</keycap><keycap>F4</keycap></keycombo>
(hold the left <keycap>Alt</keycap> key while pressing the
<keycap>F4</keycap> function key); get back to
the main installer process with
<keycombo><keycap>Left Alt</keycap><keycap>F1</keycap></keycombo>.
-->
エラーメッセージとログは第 4 コンソールに
リダイレクトされます。このコンソールへは
<keycombo><keycap>左 Alt</keycap><keycap>F4</keycap></keycombo> 
(左 <keycap>Alt</keycap> キーを押しながら
<keycap>F4</keycap> ファンクションキーを押す) を押してアクセスしてください。
<keycombo><keycap>左 Alt</keycap><keycap>F1</keycap></keycombo> で、
メインのインストーラプロセスに戻ります。

</para><para>

<!--
These messages can also be found in
<filename>/var/log/syslog</filename>.  After installation, this log
is copied to <filename>/var/log/installer/syslog</filename> on your
new system. Other installation messages may be found in
<filename>/var/log/</filename> during the
installation, and <filename>/var/log/installer/</filename>
after the computer has been booted into the installed system.
-->
これらのメッセージは <filename>/var/log/syslog</filename> で
見つけることもできます。
インストールの後、このログはあなたの新システムの 
<filename>/var/log/installer/syslog</filename> にコピーされます。
他のインストールメッセージは、インストール中には
<filename>/var/log/</filename> に、
インストールしたシステムでコンピュータが起動した後には 
<filename>/var/log/installer/</filename> にあります。

</para>
 </sect1>


  <sect1 id="modules-list"><title>コンポーネント入門</title>
<para>

<!--
Here is a list of installer components with a brief description
of each component's purpose. Details you might need to know about
using a particular component are in <xref linkend="module-details"/>.
-->
ここではインストーラコンポーネントを各コンポーネントの簡単な説明を添えて
一覧します。
特定のコンポーネントを使用するにあたり、
知る必要があるかもしれない詳細は <xref linkend="module-details"/> にあります。

</para>

<variablelist>
<varlistentry>

<term>main-menu</term><listitem><para>

<!--
Shows the list of components to the user during installer operation,
and starts a component when it is selected. Main-menu's
questions are set to priority medium, so if your priority is set to
high or critical (high is the default), you will not see the menu. On
the other hand, if there is an error which requires your intervention,
the question priority may be downgraded temporarily to allow you
to resolve the problem, and in that case the menu may appear.
-->
インストーラの操作中にユーザにコンポーネントのリストを見せ、
選択されたコンポーネントを起動します。
main-menu では質問の優先度が「中」に設定されています。
そのため、優先度が「高」や「重要」 (デフォルトは「高」) 
に設定されている場合は、メニューを見ることはないでしょう。
一方、あなたの入力が必要なエラーが起きた場合、その問題を解決するために、
質問の優先度が一時的に格下げされるかもしれません。
その場合、メニューが表示される可能性があります。

</para><para>

<!--
You can get to the main menu by selecting the <quote>Back</quote> button
repeatedly to back all the way out of the currently running component.
-->
現在実行しているコンポーネントから抜けるために、
<quote>Back</quote> ボタンを繰り返し選択してメインメニューに戻れます。


</para></listitem>
</varlistentry>
<varlistentry>

<term>localechooser</term><listitem><para>

<!--
Allows the user to select localization options for the installation and
the installed system: language, country and locales. The installer will
display messages in the selected language, unless the translation for
that language is not complete in which case some messages may be shown
in English.
-->
インストール中・インストールしたシステムの、
地域オプション (言語、国、ロケール) の選択を行います。
インストーラは選択した言語でメッセージを表示しますが、
その言語でのメッセージの翻訳が完了していない場合は、英語で表示します。

</para></listitem>
</varlistentry>
<varlistentry>

<term>kbd-chooser</term><listitem><para>

<!--
Shows a list of keyboards, from which the user chooses the model which
matches his own.
-->
キーボードのリストを表示します。
お持ちのキーボードに一致するモデルを選択してください。

</para></listitem>
</varlistentry>
<varlistentry>

<term>hw-detect</term><listitem><para>

<!--
Automatically detects most of the system's hardware, including network
cards, disk drives, and PCMCIA.
-->
システムのほとんどのハードウェアを自動検出します。
これには、ネットワークカード、ディスクドライブ、PCMCIA が含まれます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-detect</term><listitem><para>

<!--
Looks for and mounts a Debian installation CD.
-->
Debian インストール CD を探しマウントします。

</para></listitem>
</varlistentry>
<varlistentry>

<term>netcfg</term><listitem><para>

<!--
Configures the computer's network connections so it can communicate
over the internet.
-->
インターネットへ通信できるように、コンピュータのネットワーク接続を
設定します。

</para></listitem>
</varlistentry>
<varlistentry>

<term>iso-scan</term><listitem><para>

<!--
Looks for ISO file systems, which may be on a CD-ROM or on the
hard drive.
-->
CD-ROM や ハードディスクにある ISO ファイルシステムを探します。

</para></listitem>
</varlistentry>
<varlistentry>

<term>choose-mirror</term><listitem><para>

<!--
Presents a list of Debian archive mirrors. The user may choose
the source of his installation packages. 
-->
Debian アーカイブミラーのリストを表示します。
インストールするパッケージの取得元を選択できるでしょう。

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-checker</term><listitem><para>

<!--
Checks integrity of a CD-ROM. This way, the user may assure him/herself
that the installation CD-ROM was not corrupted.
-->
CD-ROM の整合性チェック。
これにより、インストール CD-ROM が壊れていないか自分で保証できます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>lowmem</term><listitem><para>

<!--
Lowmem tries to detect systems with low memory and then does various
tricks to remove unnecessary parts of &d-i; from the memory (at the
cost of some features).
-->
lowmem はシステムの搭載するメモリが少ないかを確認し、
少なければ  &d-i; の不必要な部分を、メモリから (いくつかの機能を犠牲にして) 
削除する様々なトリックを行います。

</para></listitem>
</varlistentry>
<varlistentry>

<term>anna</term><listitem><para>

<!--
Anna's Not Nearly APT. Installs packages which have been retrieved
from the chosen mirror or CD.
-->
Anna's Not Nearly APT. (Anna はちっとも APT (適切) じゃない) 
選択したミラーサーバや CD から、パッケージを取得してインストールします。

</para></listitem>
</varlistentry>
<varlistentry>

<term>partman</term><listitem><para>

<!--
Allows the user to partition disks attached to the system, create file
systems on the selected partitions, and attach them to the
mountpoints. Included are also interesting features like a fully
automatic mode or LVM support. This is the preferred partitioning tool
in Debian.
-->
システムの内蔵ディスクを分割し、
選択したパーティションのファイルシステムを作成し、
マウントポイントにそのファイルシステムをマウントすることができます。
完全自動モードや LVM サポートといったさらに面白い機能があります。
これは Debian での好ましいパーティション分割ツールです。

</para></listitem>
</varlistentry>
<varlistentry>

<term>autopartkit</term><listitem><para>

<!--
Automatically partitions an entire disk according to preset
user preferences.
-->
プリセットされたユーザの選択によって、自動的に全ディスクを分割します。

</para></listitem>
</varlistentry>
<varlistentry>

<term>partitioner</term><listitem><para>

<!--
Allows the user to partition disks attached to the system. A
partitioning program appropriate to your computer's architecture
is chosen.
-->
システムのディスクを分割することができます。
あなたのコンピュータのアーキテクチャに最適な、
パーティション分割プログラムが選ばれます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>partconf</term><listitem><para>

<!--
Displays a list of partitions, and creates file systems on
the selected partitions according to user instructions.
-->
パーティションのリストを表示します。
また、選択したパーティションにファイルシステムを作成します。

</para></listitem>
</varlistentry>
<varlistentry>

<term>lvmcfg</term><listitem><para>

<!--
Helps the user with the configuration of the
<firstterm>LVM</firstterm> (Logical Volume Manager).
-->
<firstterm>LVM</firstterm> (Logical Volume Manager) の設定について、
ユーザの補助を行います。

</para></listitem>
</varlistentry>
<varlistentry>

<term>mdcfg</term><listitem><para>

<!--
Allows the user to setup Software <firstterm>RAID</firstterm>
(Redundant Array of Inexpensive Disks). This Software RAID is usually
superior to the cheap IDE (pseudo hardware) RAID controllers found on
newer motherboards.
-->
ソフトウェア <firstterm>RAID</firstterm> (Redundant Array of Inexpensive Disks)
の設定をユーザに許可します。
このソフトウェア RAID は、新しめのマザーボードに見られる、
安い IDE (疑似ハードウェア) RAID コントローラより通常優秀です。

</para></listitem>
</varlistentry>
<varlistentry>
       
<term>tzsetup</term><listitem><para>

<!--
Selects the time zone, based on the location selected earlier.
-->
あらかじめ選択した場所を元に、タイムゾーンを選択します。
               
</para></listitem>
</varlistentry>
<varlistentry>

<term>clock-setup</term><listitem><para>

<!--
Determines whether the clock is set to UTC or not.
-->
クロックを UTC に設定するかどうかを決定します。

</para></listitem>
</varlistentry>
<varlistentry>
       
<term>user-setup</term><listitem><para>

<!--
Sets up the root password, and adds a non-root user.
-->
root パスワードの設定や、root 以外のユーザの追加を行います。

</para></listitem>
</varlistentry>
<varlistentry>

<term>base-installer</term><listitem><para>

<!--
Installs the most basic set of packages which would allow
the computer to operate under Linux when rebooted.
-->
再起動時にコンピュータが Linux で動作することができるように、
もっとも基本的なパッケージのセットをインストールします。

</para></listitem>
</varlistentry>
<varlistentry>
       
<term>apt-setup</term><listitem><para>

<!--
Configures apt, mostly automatically, based on what media the installer is
running from.
-->
インストーラを起動したメディアを元に、ほとんど自動で apt の設定を行います。

</para></listitem>
</varlistentry>
<varlistentry>
       
<term>pkgsel</term><listitem><para>

<!--
Uses <classname>tasksel</classname> to select and install additional software.
-->
追加ソフトウェアをインストールするのに
<classname>tasksel</classname> を使用します。

</para></listitem>
</varlistentry>
<varlistentry>
       
<term>os-prober</term><listitem><para>

<!--
Detects currently installed operating systems on the computer and
passes this information to the bootloader-installer, which may offer
you an ability to add discovered operating systems to the bootloader's
start menu. This way the user could easily choose at the boot time
which operating system to start.
-->
コンピュータに現在インストールされている OS を検出し、
この情報を (bootloader のスタートメニューに発見した OS を加える機能を提供する) 
bootloader-installer へ渡します。
これは、起動時にどの OS で起動するかを、ユーザが簡単に決める方法です。


</para></listitem>
</varlistentry>
<varlistentry>

<term>bootloader-installer</term><listitem><para>

<!--
The various bootloader installers each install a boot loader program on the
hard disk, which is necessary for the computer to start up using Linux
without using a floppy or CD-ROM.  Many boot loaders allow the user to
choose an alternate operating system each time the computer boots.
-->
様々なブートローダインストーラがそれぞれ、
ハードディスクにブートローダプログラムをインストールします。
これは、フロッピーや CD-ROM を使用しないで Linux を起動するのに必要です。
ブートローダの多くは、コンピュータが起動するごとに
代替オペレーティングシステムを選ぶことができます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>shell</term><listitem><para>

<!--
Allows the user to execute a shell from the menu, or in the second
console.
-->
メニューから、もしくは第 2 コンソールで shell を実行できます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>save-logs</term><listitem><para>

<!--
Provides a way for the user to record information on a floppy disk, network,
hard disk, or other media when trouble is encountered, in order to accurately
report installer software problems to Debian developers later.
-->
後で Debian 開発者へ、インストーラソフトウェアの障害を正確に報告するために、
障害に遭遇した際の、フロッピーディスク、ネットワーク、
ハードディスク、その他メディアに情報を記録する方法を提供します。

</para></listitem>
</varlistentry>

</variablelist>

 </sect1>

&using-d-i-components.xml;

</chapter>

