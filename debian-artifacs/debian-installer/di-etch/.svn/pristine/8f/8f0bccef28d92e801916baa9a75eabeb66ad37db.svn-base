<!-- retain these comments for translator revision tracking -->
<!-- original version: 35612 -->
<!-- revised by Herbert Parentes Fortes Neto (hpfn) 2006.05.06 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.06.01 -->

  <sect2 arch="mipsel" id="boot-tftp"><title>Inicializando com o TFTP</title>

   <sect3>
   <title>Inicialização do Cobalt TFTP</title>
<para>

Falando de maneira clara, Cobalt não usa o TFTP e sim o NFS para inicializar.
Você precisa instalar um servidor NFS e colocar os arquivos do processo de
inicialização em <filename>/nfsroot</filename>. Quando você inicializar seu
Cobalt, você tem que pressionar os botões direito e esquerdo do cursor ao mesmo
tempo e a máquina irá inicializar através da rede pelo NFS. Então, serão
exibidas várias opções na tela. Existem os seguintes dois métodos de
instalação:

<itemizedlist>
<listitem><para>

Via SSH (padrão): Neste caso, o processo de instalação irá configurar a rede
via DHCP e iniciar um servidor SSH. Ele irá então indicar uma senha aleatória
e outra informação de login (como um endereço IP) no LCD do Cobalt. Quando
você conectar na máquina com um cliente SSH você pode começar a instalação.

</para></listitem>
<listitem><para>

Via console serial: Usando um cabo nulo de modem, você pode conectar à porta
serial de sua máquina Cobalt (usando 115200 bps) e proceder com a instalação
desta maneira. Essa opção não está disponível nas máquinas  Qube 2700 (Qube1)
já que elas não têm portas seriais.

</para></listitem>
</itemizedlist>

</para>
  </sect3>

   <sect3>
   <title>Inicialização através de TFTP no Broadcom BCM91250A e BCM91480B</title>
<!-- Note to translators: this is the same section as in mips.xml -->
<para>

Nas placas de avaliação Broadcom BCM91250A e BCM91480B, será necessário
carregar o gerenciador de partida SiByl através do TFTP que então carregará e
iniciará o Instalador Debian. Na maioria dos casos, você primeiramente obterá
um endereço IP através do DHCP, mas também é possível configurar um endereço
fixo. Para usar um endereço DHCP, você deverá digitar o seguinte comando na
linha de comando do CFE:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Assim que obter um endereço IP, você poderá carregar o SiByl com o seguinte
comando:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

Substitua o endereço IP listado neste exemplo pelo nome ou endereço IP
do seu servidor TFTP. Assim que executar este comando o programa de
instalação será automaticamente carregado.

</para>
</sect3>
  </sect2>

  <sect2 arch="mipsel"><title>Parâmetros de Inicialização</title>

   <sect3>
   <title>Inicialização do Cobalt TFTP</title>
<para>

Você não pode passar nenhum parâmetro de inicialização diretamente. Ao invés
disso, você tem que editar o arquivo <filename>/nfsroot/default.colo</filename>
no servidor NFS e adicionar seus parâmetros à variável
<replaceable>args</replaceable>.

</para>
  </sect3>

   <sect3>
<!-- Note to translators: this is the same section as in mips.xml -->
   <title>Inicialização através de TFTP na Broadcom BCM91250A e BCM91480B</title>
<para>

Você não poderá passar qualquer parâmetro de inicialização diretamente através
da linha de comando do CFE. Ao invés disso, poderá editar o arquivo
<filename>/boot/sibyl.conf</filename> no servidor TFTP e adicionar
seus parâmetros de inicialização na variável <replaceable>extra_args</replaceable>.

</para>
  </sect3>

  </sect2>
