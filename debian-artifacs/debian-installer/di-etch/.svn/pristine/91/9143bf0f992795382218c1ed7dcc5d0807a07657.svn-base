<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44026 -->
<!-- Traducido por Javier Fernandez-Sanguino, julio 2006 -->
<!-- Revisado por Igor Tamara, enero 2007 -->

   <sect3 id="partman-crypto">
   <title>Configurar vol�menes cifrados</title>
<para>

&d-i; permite la configuraci�n de particiones cifradas. Cualquier fichero que
se guarde en una partici�n de estas caracter�sticas se guardar� al dispositivo
cifrado. El acceso a los datos s�lo se puede conseguir una vez haya introducido
la <firstterm>clave</firstterm> utilizada para crear originalmente la partici�n
cifrada. Esta funci�n es �til para proteger datos sensibles en caso
de que alguien robe su port�til o disco duro. El ladr�n podr� lograr acceder al
disco duro pero los datos en el disco parecer�n ser caracteres aleatorios y no
podr� acceder a los mismos  si no sabe la clave correcta.

</para><para>

Las particiones m�s importantes a cifrar son: la partici�n �home� que
es donde se guardan sus datos privados y la partici�n de intercambio (�swap�)
ya que pueden guardarse datos sensibles en �sta durante la operaci�n del
sistema. Por supuesto, nada impide que vd. cifre otras particiones que podr�an
ser de inter�s. Por ejemplo <filename>/var</filename>, que es donde se guardan
los datos de los sistemas de base de datos, servidores de correo o servidores
de impresora, o <filename>/tmp</filename>, que lo utilizan algunos programas
para almacenar algunos datos temporales que pudieran ser de inter�s. Algunos
usuarios pueden querer incluso cifrar todo su sistema. La �nica excepci�n
es que la partici�n <filename>/boot</filename> debe permanecer sin 
cifrar ya que de momento no hay ninguna forma de cargar un n�cleo de una
partici�n cifrada.

</para><note><para>

Tenga en cuenta que el rendimiento de las particiones cifradas ser�
peor que el de las particiones sin cifrar porque se tienen que 
descifrar o cifrar los datos en cada acceso de lectura o escritura.
El impacto en el rendimiento dependen de la velocidad de su CPU,
y del cifrado y longitud de clave escogidos.

</para></note><para>

Debe crear una nueva partici�n en el men� de particionado seleccionando 
espacio libre si quiere utilizar el cifrado. Otra opci�n es utilizar
una partici�n existente (p.ej. una partici�n normal, o un volumen l�gico
LVM  o RAID). Tiene que seleccionar <guimenuitem>volumen f�sico
para cifrado</guimenuitem> en el men� <guimenu>Configuraci�n de la 
partici�n</guimenu> en la opci�n <menuchoice> <guimenu>Utilizar como:</guimenu> </menuchoice>. El men� cambiar� para mostrar distintas opciones
criptogr�ficas para la partici�n.

</para><para>

Puede utilizar distintos m�todos de cifrado en &d-i;. El
m�todo por omisi�n es <firstterm>dm-crypt</firstterm> (disponible
en las �ltimas versiones del n�cleo de Linux que pueden incluir
vol�menes LVM f�sicos) y la otra es 
<firstterm>loop-AES</firstterm> (m�s antigua y mantenida de forma
separada del �rbol del n�cleo de Linux). Se le recomienda la primera
opci�n a no ser que tenga razones importantes para no utilizarla.

<!-- TODO: link to the "Debian block device encryption guide"
     once Max writes it :-) -->

</para><para>

En primer lugar veamos las opciones disponibles cuando seleccione
como m�todo de cifrado <userinput>Device-mapper (dm-crypt)</userinput>. Como
siempre: cuando tenga dudas utilice los valores por omisi�n, se han escogido
con mucho cuidado pensando en la seguridad de su sistema.

<variablelist>

<varlistentry>
<term>Cifrado: <userinput>aes</userinput></term>

<listitem><para>

Esta opci�n le permite seleccionar el algoritmo de cifrado
(<firstterm>cifra</firstterm>) que se utiliza para cifrar los datos
en la partici�n. Actualmente &d-i; ofrece soporte para los siguientes
cifrados de bloque: <firstterm>aes</firstterm>,
<firstterm>blowfish</firstterm>,
<firstterm>serpent</firstterm>, y <firstterm>twofish</firstterm>.
La discusi�n de la calidad de los distintos algoritmos de cifrado
queda fuera del alcance de este documento. Sin embargo, puede ayudarle
a tomar una decisi�n el hecho de que en el a�o 2000 el 
Instituto Nacional de Est�ndares y Tecnolog�a Norteamericano (�American
National Institute of Standards and Technology�, NIST) escogi� AES
como el algoritmo de cifrado est�ndar para proteger informaci�n
sensible en el siglo XXI.

</para></listitem>
</varlistentry>

<varlistentry>
<term>Longitud de clave: <userinput>256</userinput></term>

<listitem><para>

En este punto puede especificar la longitud de la clave de cifrado.
El cifrado es mejor cuanto mayor sea la longitud de cifrado. Pero, por otro
lado, un incremento en el tama�o de la clave de cifrado tiene un impacto
negativo en el rendimiento. En funci�n del cifrado utilizado dispondr�
de distintos tama�os de longitud de clave.

</para></listitem>
</varlistentry>

<varlistentry>
<term>Algoritmo de VI: <userinput>cbc-essiv:sha256</userinput></term>

<listitem><para>

El algoritmo del <firstterm>Vector de Inicializaci�n</firstterm> o
<firstterm>VI</firstterm> (IV en ingl�s) se utiliza en criptograf�a
para asegurar que la aplicaci�n del cifrado en los mismos datos de
<firstterm>texto en claro</firstterm> con la misma clave generan
siempre un <firstterm>texto cifrado</firstterm> �nico. El objetivo es impedir
que un atacante pueda deducir informaci�n bas�ndose en patrones repetidos en los
datos cifrados.

</para><para>

De las alternativas disponibles el valor por omisi�n
<userinput>cbc-essiv:sha256</userinput> es actualmente el menos vulnerable a
ataques conocidos. Utilice las otras alternativas s�lo si tiene que asegurar
compatibilidad con algunos sistemas instalados previamente que no sean capaces
de utilizar los nuevos algoritmos.

</para></listitem>
</varlistentry>

<varlistentry>
<term>Clave de cifrado: <userinput>Frase de contrase�a</userinput></term>

<listitem><para>

Aqu� puede introducir el tipo de clave de cifrado para esta partici�n.

 <variablelist>
 <varlistentry>
 <term>Frase de contrase�a</term>
 <listitem><para>

La clave de cifrado se computar�<footnote>
<para>

La utilizaci�n de una contrase�a como clave significa que la partici�n
se configurar� utilizando <ulink url="&url-luks;">LUKS</ulink>.

</para></footnote> bas�ndose en la contrase�a que podr� introducir
en el proceso m�s adelante.

 </para></listitem>
 </varlistentry>

 <varlistentry>
 <term>Clave aleatoria</term>
 <listitem><para>

Se generar� una nueva clave de cifrado con valores aleatorios cada vez que se
arranque la partici�n cifrada. En otras palabras: cada vez que se reinicie el
sistema el contenido de la partici�n se perder� al borrarse la clave de la
memoria. Por supuesto, podr�a intentar adivinar la contrase�a a trav�s de un
ataque de fuerza bruta pero, a no ser que haya una debilidad desconocida en el
algoritmo de cifrado, no es algo realizable en un tiempo razonable.

 </para><para>

Las claves aleatorias son �tiles para las particiones de intercambio
porque no es deseable acordarse de una frase de contrase�a ni 
es recomendable borrar la informaci�n sensible de dicha partici�n antes de 
apagar su sistema. Sin embargo esto tambi�n significa que 
<emphasis>no</emphasis> podr� utilizar la funcionalidad
<quote>suspend-to-disk</quote> (suspensi�n a disco, N. del T.)
ofrecida por los nuevos n�cleos ya que ser� imposible (en el rearranque
posterior del sistema) recuperar los datos de la suspensi�n del sistema
guardados en la partici�n de intercambio.

 </para></listitem>
 </varlistentry>
 </variablelist>

</para></listitem>
</varlistentry>

<varlistentry>
<term>Borrar los datos: <userinput>s�</userinput></term>

<listitem><para>

Indica si deber�a borrarse el contenido de la partici�n con datos
aleatorios antes de configurar el cifrado. Se recomienda hacer esto
porque en caso contrario un posible atacante podr�a determinar qu� partes de
la partici�n se est�n utilizando y cu�les no. Adem�s, esto har� m�s 
dif�cil la recuperaci�n de datos que permanecieran en la partici�n 
asociados a instalaciones previas<footnote><para>

Se cree, sin embargo, que las personas que trabajan en agencias gubernamentales
de �tres letras� (FBI, NSA, CIA, N. del T.) pueden recuperar los
datos aunque se hayan realizado varias escrituras en medios 
magneto-�pticos.

</para></footnote>.

</para></listitem>
</varlistentry>

</variablelist>

</para><para>

El men� cambiar� si selecciona <menuchoice> <guimenu>M�todo de
cifrado:</guimenu> <guimenuitem>Loopback (loop-AES)</guimenuitem> </menuchoice>
y se presentar�n las siguientes opciones:


<variablelist>
<varlistentry>
<term>Cifrado: <userinput>AES256</userinput></term>

<listitem><para>

En el caso de loop-AES, y a diferencia de dm-crypt, est�n combinadas
las opciones de cifrado y tama�o de clave de forma que puede
seleccionar ambas al mismo tiempo. Consulte la informaci�n previa
sobre cifrados y longitudes de clave para m�s informaci�n.

</para></listitem>
</varlistentry>

<varlistentry>
<term>Clave de cifrado: <userinput>Fichero de clave (GnuPG)</userinput></term>

<listitem><para>

Aqu� puede seleccionar el tipo de clave de cifrado para esta partici�n.

 <variablelist>
 <varlistentry>
 <term>Fichero de clave (GnuPG)</term>
 <listitem><para>

La clave de cifrado se generar� bas�ndose en datos aleatorios durante
la instalaci�n. Sin embargo, esta clave se cifrar� con 
<application>GnuPG</application> por lo que, para poder utilizarla,
se le preguntar� una contrase�a (que se le solicitar� durante el proceso).

 </para></listitem>
 </varlistentry>

 <varlistentry>
 <term>Clave aleatoria</term>
 <listitem><para>

Consulte la secci�n precedente si desea m�s informaci�n de las claves
aleatorias.

 </para></listitem>
 </varlistentry>
 </variablelist>

</para></listitem>
</varlistentry>

<varlistentry>
<term>Borrar datos: <userinput>s�</userinput></term>

<listitem><para>

Consulte la secci�n precedente si desea m�s informaci�n del borrado
de datos.

</para></listitem>
</varlistentry>

</variablelist>

</para>
<note condition="gtk"><para>

Tenga en cuenta que la versi�n <emphasis>gr�fica</emphasis> del
instalador a�n tiene algunas limitaciones frente a la versi�n en
modo texto. En el caso del cifrado esto hace que s�lo pueda configurar
vol�menes con <emphasis>contrase�a</emphasis> como clave de cifrado.

</para></note>
<para>

Una vez ha seleccionado los par�metros para su partici�n cifrada debe volver al
men� de particionado principal. Aqu� deber�a encontrar un nuevo elemento del
men� llamado <guimenu>Configurar los vol�menes cifrados</guimenu>. Una vez lo
seleccione se le pedir� confirmaci�n para borrar los datos de las particiones
marcadas para ser borradas as� como otras opciones como, por ejemplo, la
escritura de la tabla de particiones en disco.  Estas tareas tardar�n un tiempo
si est� trabajando con particiones grandes.

</para><para>

A continuaci�n se le pedir� que introduzca una frase de contrase�a
para las particiones que haya configurado para que la utilicen.
Una buena frase de contrase�a tendr� m�s de ocho caracteres, ser� una
mezcla de letras, n�meros y otros caracteres que no se pueden encontrar en
palabras comunes del diccionario y que no est�n relacionadas con informaci�n
que pueda asociarse a vd. con facilidad (como son fecha de nacimiento,
aficiones, nombre de mascotas, nombres de miembros de la familia, etc.).

</para><warning><para>

Antes de introducir cualquier frase de contrase�a deber�a asegurarse de que su
teclado est� configurado adecuadamente y genera los caracteres que vd. supone.
Si no est� seguro deber�a cambiar a la segunda consola virtual y escribir alg�n
texto en el indicador. Esto asegura que no vaya a sorprenderse m�s adelante,
por ejemplo, si introduce su frase de contrase�a en un teclado configurado como
QWERTY cuando en realidad ha utilizado una configuraci�n de teclado AZERTY 
durante la instalaci�n. Esta situaci�n puede provocarse por varias situaciones:
quiz�s cambio de configuraci�n de teclado durante la instalaci�n, o la
configuraci�n de teclado que ha elegido no est� disponible cuando vaya a
introducir la frase de contrase�a para el sistema de ficheros ra�z.

</para></warning><para>

Si ha seleccionado para generar las claves de cifrado m�todos distintos de la
frase de contrase�a se generar�n ahora. El proceso puede tomar mucho tiempo
dado que el n�cleo puede no haber sido capaz de obtener suficiente informaci�n
de entrop�a en este punto de la instalaci�n. Puede ayudar a acelerar este
proceso si genera entrop�a, por ejemplo: si pulsa teclas al azar o si cambia a
la segunda consola virtual y genera tr�fico de red o de disco (como pueda ser
una descarga de algunos ficheros o enviar ficheros muy grandes a 
<filename>/dev/null</filename>, etc.).

<!-- TODO: Mention hardware random generators when we will support
     them -->

Este paso se repetir� para cada partici�n a cifrar.

</para><para>

Ver� todos los vol�menes cifrados como particiones adicionales que
puede configurar igual que las particiones normales una vez vuelva
al men� de particionado principal. El siguiente ejemplo muestra
dos vol�menes distintos. El primero est� cifrado con dm-crypt y
el segundo con loop-AES.

<informalexample><screen>
Volumen cifrado (<replaceable>sda2_crypt0</replaceable>) - 115.1 GB Linux device-mapper
     #1 115.1 GB  F ext3

Loopback (<replaceable>loop0</replaceable>) - 515.2 MB AES256 keyfile
     #1 515.2 MB  F ext3
</screen></informalexample>

Ahora es cuando puede asignar los puntos de montaje a los vol�menes
y cambiar, opcionalmente, los tipos de sistema de ficheros si los valores
por omisi�n no se ajustan a sus necesidades.

</para><para>

Cabe destacar aqu� la asociaci�n entre  los identificadores entre par�ntesis
(<replaceable>sda2_crypt0</replaceable> y <replaceable>loop0</replaceable> en este
caso) y los puntos de montaje asignados a cada volumen cifrado. Necesitar�
conocer esta informaci�n m�s adelante cuando vaya a arrancar el sistema. Podr�
encontrar m�s informaci�n sobre las diferencias entre el proceso de arranque
normal y el proceso de arranque con vol�menes cifrados en 
<xref linkend="mount-encrypted-volumes"/>.

</para><para>

Contin�e con la instalaci�n cuando est� satisfecho con el esquema
de particionado.

</para>
   </sect3>
