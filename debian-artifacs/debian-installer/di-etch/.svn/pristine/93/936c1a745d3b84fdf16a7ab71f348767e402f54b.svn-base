<!-- $Id: graphical.xml 44970 2007-02-04 15:06:13Z mck-guest $ -->
<!-- original version: 44580 -->

 <sect1 condition="gtk" id="graphical">
 <title>Grafický instalátor</title>
<para>

Grafická verze instalačního systému je dostupná pouze na několika
architekturách, kam patří i &arch-title;. Funkcionalita grafického
instalátoru je shodná s textovou verzí, protože se jedná o stejné
programy, liší se pouze vzhled.

</para><para>

Přestože je funkcionalita stejná, přesto přináší grafická verze
několik výhod. Tou hlavní je, že podporuje více jazyků, konkrétně ty,
jejichž znaková sada se nedá zobrazit v běžném rozhraní
<quote>newt</quote>. Grafické rozhraní také zlepšuje použitelnost tím,
že můžete volitelně používat myš a že je v některých případech
zobrazeno více otázek na jediné obrazovce (typicky zadání hesla a jeho
ověření).

</para><para arch="x86">

Grafický instalátor je dostupný na všech obrazech CD a také při
instalaci z pevného disku metodou hd-media. Protože grafický
instalátor používá samostatný (mnohem větší) initrd než běžný
instalátor, musíte jej místo tradičního <userinput>install</userinput>
zavést volbou <userinput>installgui</userinput>. Analogicky, expertní
nebo záchranný režim zavedete volbami <userinput>expertgui</userinput>
resp. <userinput>rescuegui</userinput>.

</para><para arch="x86">

Jednou výjimkou je speciální <quote>mini</quote> ISO
obraz<footnote id="gtk-miniiso">

<para>
Mini ISO můžete stáhnout z debianího zrcadla stejně jako ostatní
soubory v <xref linkend="downloading-files"/>, hledejte něco s názvem
<quote>gtk-miniiso</quote>.
</para>

</footnote>, které se používá hlavně pro testování; toto zavedete
volbou <userinput>install</userinput>. Pro zavádění ze sítě neexistuje
grafická verze instalačního obrazu.

</para><para arch="powerpc">

Pro &arch-title; je nyní dostupný pouze experimentální
<quote>mini</quote> ISO obraz<footnote id="gtk-miniiso">

<para>
Mini ISO můžete stáhnout z debianího zrcadla stejně jako ostatní
soubory v <xref linkend="downloading-files"/>, hledejte něco s názvem
<quote>gtk-miniiso</quote>.
</para>

</footnote>. Měl by fungovat na téměř všech systémech PowerPC s
grafickou kartou ATI, což se nedá říci o ostatních systémech.

</para><para>

Grafický instalátor vyžaduje pro běh mnohem více paměti než tradiční
verze: &minimum-memory-gtk;. Pokud instalace zjistí nedostatek paměti,
automaticky se přepne do tradičního frontendu <quote>newt</quote>.

</para><para>

Při startu grafického instalátoru můžete používat zaváděcí parametry
stejně, jako u tradiční verze. Jedním z těchto parametrů je přepnutí
myši do levorukého režimu. Úplný seznam parametrů naleznete v kapitole
<xref linkend="boot-parms"/>.

</para>

  <sect2 id="gtk-using">
  <title>Používání grafického instalátoru</title>
<para>

Jak jsme již zmínili, grafický instalátor pracuje úplně stejně jako
instalátor textový a tedy můžete pro instalaci plně využít informací
sepsaných ve zbytku příručky.

</para><para>

Preferujete-li ovládání pomocí klávesnice, měli byste vědět dvě
věci. Pro rozbalení/sbalení seznamu (např. při výběru kontinentů a
zemí) můžete použít klávesy <keycap>+</keycap> a <keycap>-</keycap>.
U otázek, kde můžete vybrat více než jednu možnost (např. výběr úloh),
musíte pro pokračování dále nejprve přeskákat tabulátorem na tlačítko
<guibutton>Pokračovat</guibutton> a poté stisknout &enterkey;. Pokud
byste stiskli &enterkey; rovnou, změnili byste výběr položky, ale na
tlačítko <guibutton>Pokračovat</guibutton> by to nemělo žádný vliv.

</para><para>

Pro přepnutí do jiné konzoly musíte kromě <keycap>Levého Altu</keycap>
a funkční klávesy stisknout i klávesu <keycap>Ctrl</keycap>, stejně
jako v systému X Window. Například pro přepnutí do prvního virtuálního
terminálu byste použili: <keycombo> <keycap>Ctrl</keycap> <keycap>Levý
Alt</keycap> <keycap>F1</keycap> </keycombo>.

</para>
  </sect2>

  <sect2 id="gtk-issues">
  <title>Známé problémy</title>
<para>

Etch je prvním vydáním Debianu, které obsahuje grafický instalátor a
používá relativně nové technologie. Při instalaci se vyskytuje několik
známých problémů, u kterých očekáváme, že budou vyřešeny v příštím
vydání &debian;u.

</para>

<itemizedlist>
<listitem><para>

Informace na některých obrazovkách nejsou formátovány do pěkných
sloupců, ale jsou mírně rozházené. Nejvíce je to vidět na první
obrazovce s výběrem jazyka nebo na hlavní obrazovce partmanu.

</para></listitem>
<listitem><para>

Nemusí fungovat psaní všech znaků a v některých případech se mohou
místo písmen zobrazovat různé symboly. Konkrétním případem je
používání mrtvých kláves, které používáme pro psaní písmen jako ó nebo
ď.

</para></listitem>
<listitem><para>

Podpora pro touchpady není zcela vyladěná.

</para></listitem>
<listitem><para>

Pokud je instalátor zaneprázdněný, tak byste se neměli přepínat do
jiné konzoly, protože by to mohlo shodit grafický frontend. Frontend
se sice automaticky restartuje, ale stále to může při instalaci
způsobovat problémy. Přepnutí na jinou konzolu v okamžiku, kdy systém
čeká na vstup od uživatele, by mělo fungovat bez problémů.

</para></listitem>
<listitem><para>

Podpora pro vytváření šifrovaných oblastí je omezená a není možné
vytvořit náhodný šifrovací klíč pro šifrování oblasti. Jediný
podporovaný způsob šifrování oblasti je šifrování oblasti za použití
přístupové fráze.

</para></listitem>
<listitem><para>

Spuštění shellu z grafického rozhraní není zatím podporováno. To
znamená, že příslušné volby nebudou v hlavním menu instalace (případně
v záchranném režimu) zobrazeny. Místo toho se budete muset přepnout do
shellů dostupných na druhé a třetí virtuální konzoli, jak je popsáno
výše.

</para><para>

Po zavedení instalace v záchranném režimu bývá užitečné spustit shell
v kořenové oblasti již nainstalovaného systému. Toho dosáhnete tak, že
se po výběru oblasti, která se má použít jako kořenová, přepnete na
druhou nebo třetí virtuální konzoli a zadáte příkaz

<informalexample><screen>
<prompt>#</prompt> <userinput>chroot /target</userinput>
</screen></informalexample>

</para></listitem>
</itemizedlist>

  </sect2>
 </sect1>
