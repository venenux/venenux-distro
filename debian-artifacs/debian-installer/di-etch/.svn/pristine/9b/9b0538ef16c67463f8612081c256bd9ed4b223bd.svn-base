<!-- $Id: mips.xml 37321 2006-05-14 16:17:19Z mck-guest $ -->
<!-- original version: 35613 -->

  <sect2 arch="mips" id="boot-tftp"><title>Zavedení z TFTP</title>
   <sect3><title>TFTP zavádění na SGI</title>
<para>

Na počítačích SGI vstupte do <quote>command monitoru</quote>, kde
příkazem

<informalexample><screen>
bootp():
</screen></informalexample>

zavedete Linux a spustíte instalaci Debianu. Aby to fungovalo, budete
možná muset v <quote>command monitoru</quote> zrušit proměnnou
<envar>netaddr</envar> příkazem

<informalexample><screen>
unsetenv netaddr
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>TFTP zavádění na Broadcom BCM91250A a BCM91480B</title>
<para>

Na Broadcom BCM91250A a BCM91480B musíte zavést zavaděč SiByl přes TFTP a ten pak
nahraje a spustí instalaci Debianu. Ve většině případů nejprve
obdržíte IP adresu přes DHCP, ale nic vám nebrání nastavit si
statickou adresu ručně. Chcete-li použít DHCP, zadejte v CFE promptu
následující příkaz:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Poté, co obdržíte IP adresu, můžete zavést zavaděč SiByl příkazem:

<informalexample><screen>
boot <replaceable>192.168.1.1</replaceable>:/boot/sibyl
</screen></informalexample>

IP adresu v příkladu samozřejmě musíte nahradit jménem nebo IP adresou
vašeho místního TFTP serveru. Instalační program by se měl zavést
automaticky.

</para>
   </sect3>
  </sect2>


  <sect2 arch="mips"><title>Zaváděcí parametry</title>
   <sect3>
   <title>SGI</title>
<para>

Na počítačích SGI můžete přidat zaváděcí parametry příkazu
<command>bootp():</command> v <quote>command monitoru</quote>.

</para><para>

Pokud jste například bootp/dhcp serveru explicitně nezadali název
souboru, který se má zavést, můžete teď zadat jeho jméno a umístění.
Například

<informalexample><screen>
bootp():/boot/tftpboot.img
</screen></informalexample>

Další parametry jádra můžete zadat přes <command>append</command>:

<informalexample><screen>
bootp(): append="root=/dev/sda1"
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Broadcom BCM91250A a BCM91480B</title>
<para>

Přímo z CFE promptu nemůžete zadávat žádné příkazy. Místo toho musíte
na FTP serveru upravit soubor <filename>/boot/sibyl.conf</filename>
a přidat své parametry do proměnné
<replaceable>extra_args</replaceable>.

</para>
   </sect3>
  </sect2>
