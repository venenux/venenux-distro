<!-- retain these comments for translator revision tracking -->
<!-- original version: 45616 untranslated -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Preparing Files for USB Memory Stick Booting</title>

<para>

To prepare the USB stick, you will need a system where GNU/Linux is
already running and where USB is supported. You should ensure that the
usb-storage kernel module is loaded (<userinput>modprobe
usb-storage</userinput>) and try to find out which SCSI device the USB
stick has been mapped to (in this example
<filename>/dev/sda</filename> is used). To write to your stick, you
may have to turn off its write protection switch.

</para><para>

Note that the USB stick should be at least 256 MB in size (smaller
setups are possible if you follow <xref linkend="usb-copy-flexible"/>).

</para>

  <sect2 id="usb-copy-easy">
  <title>Copying the files &mdash; the easy way</title>
<para arch="x86">

There is an all-in-one file <filename>hd-media/boot.img.gz</filename>
which contains all the installer files (including the kernel) as well
as <command>SYSLINUX</command> and its configuration file. You only
have to extract it directly to your USB stick:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para><para arch="powerpc">

There is an all-in-one file <filename>hd-media/boot.img.gz</filename>
which contains all the installer files (including the kernel) as well
as <command>yaboot</command> and its configuration file. Create a
partition of type "Apple_Bootstrap" on your USB stick using
<command>mac-fdisk</command>'s <userinput>C</userinput> command and
extract the image directly to that:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda2</replaceable>
</screen></informalexample>

</para>
<warning><para>

Using this method will destroy anything already on the device. Make sure
that you use the correct device name for your USB stick.

</para></warning>
<para>

After that, mount the USB memory stick (<userinput>mount
<replaceable arch="x86">/dev/sda</replaceable>
<replaceable arch="powerpc">/dev/sda2</replaceable>
/mnt</userinput>), which will now have
<phrase arch="x86">a FAT filesystem</phrase>
<phrase arch="powerpc">an HFS filesystem</phrase>
on it, and copy a Debian netinst or businesscard ISO image to it
(see <xref linkend="usb-add-iso"/>).
Unmount the stick (<userinput>umount /mnt</userinput>) and you are done.

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>Copying the files &mdash; the flexible way</title>
<para>

If you like more flexibility or just want to know what's going on, you
should use the following method to put the files on your stick.

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

   </sect2>

   <sect2 id="usb-add-iso">
   <title>Adding an ISO image</title>
<para>

The installer will look for a Debian ISO image on the stick as its source
for additional data needed for the installation. So your next step is to
copy a Debian ISO image (businesscard, netinst or even a full CD image)
onto your stick (be sure to select one that fits). The file name of the
image must end in <filename>.iso</filename>.

</para><para>

If you want to install over the network, without using an ISO image,
you will of course skip the previous step. Moreover you will have to
use the initial ramdisk from the <filename>netboot</filename>
directory instead of the one from <filename>hd-media</filename>,
because <filename>hd-media/initrd.gz</filename> does not have network
support.

</para><para>

When you are done, unmount the USB memory stick (<userinput>umount
/mnt</userinput>) and activate its write protection switch.

</para>
  </sect2>

  <!-- TODO: doesn't this section belong later? -->
  <sect2 arch="x86">
  <title>Booting the USB stick</title>
<warning><para>

If your system refuses to boot from the memory stick, the stick may
contain an invalid master boot record (MBR). To fix this, use the
<command>install-mbr</command> command from the package
<classname>mbr</classname>:

<informalexample><screen>
# install-mbr /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
