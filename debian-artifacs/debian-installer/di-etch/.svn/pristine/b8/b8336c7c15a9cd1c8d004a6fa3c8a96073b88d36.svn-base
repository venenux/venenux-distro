<!-- retain these comments for translator revision tracking -->
<!-- original version: 43693 -->


  <sect2 arch="arm">
  <!-- <title>CPU, Main Boards, and Video Support</title> -->
  <title>CPU, schede madri e video supportate</title>
<para>

<!--
Each distinct ARM architecture requires its own kernel. Because of
this the standard Debian distribution only supports installation on
a number of the most common platforms. The Debian userland however may be
used by <emphasis>any</emphasis> ARM CPU.
-->

Ogni singola architettura ARM richiede un kernel specifico. Per questo
motivo la distribuzione Debian si installa solo sulle piattaforme più
comuni. Invece tutti i pacchetti Debian possono essere usati su
<emphasis>qualsiasi</emphasis> CPU ARM.

</para><para>

<!--
Most ARM CPUs may be run in either endian mode (big or little). However,
the majority of current system implementation uses little-endian mode.
Debian currently only supports little-endian ARM systems.
-->

La maggior parte delle CPU ARM può funzionare sia in big endian che in
little endian anche se la maggioranza dei sistemi usano la sola modalità
little endian. Attualmente Debian supporta solo i sistemi ARM little
endian.

</para><para>

<!--
The supported platforms are:
-->

Le piattaforme supportate sono:

<variablelist>

<varlistentry>
<term>Netwinder</term>
<listitem><para>

<!--
This is actually the name for the group of machines
based upon the StrongARM 110 CPU and Intel 21285 Northbridge (also known
as Footbridge). It
comprises of machines like: Netwinder (possibly one of the most common ARM
boxes), CATS (also known as the EB110ATX), EBSA 285 and Compaq
personal server (cps, aka skiff).
-->

Ormai questo è diventato il nome di un gruppo di macchine basate sulla
CPU StrongARM 110 e sul Nothbridge Intel 21285 (conosciuto anche come
Footbridge). Rientrano in questo gruppo le Netwinder (forse uno dei
sistemi ARM più diffusi), CATS (conosciuto anche come EB110ATX), EBSA
285 e i personal server Compaq (cps, noti anche come skiff).

</para></listitem>
</varlistentry>

<varlistentry>
<term>IOP32x</term>
<listitem><para>

<!--
Intel's I/O Processor (IOP) line is found in a number of products related
to data storage and processing.  Debian currently supports the IOP32x
platform, featuring the IOP 80219 and 32x chips commonly found in Network
Attached Storage (NAS) devices.  Debian explicitly supports two such
devices: the <ulink url="&url-arm-cyrius-glantank;">GLAN Tank</ulink> from
IO-Data and the <ulink url="&url-arm-cyrius-n2100;">Thecus N2100</ulink>.
-->

I processori di I/O (IOP) prodotti da Intel sono presenti in molti prodotti
legati alla memorizzazione e all'elaborazione dei dati. Attualmente Debian
supporta la piattaforma IOP32xx, compresi i chip IOP 80219 e 32x molto
comuni nei dispositivi NAS (Network Attached Storage). In particolare Debian
supporta due dispositivi di questo tipo:
<ulink url="&url-arm-cyrius-glantank;">GLAN Tank</ulink> di IO-Data e
<ulink url="&url-arm-cyrius-n2100;">Thecus N2100</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>IXP4xx</term>
<listitem><para>

<!--
The IXP4xx platform is based on Intel's XScale ARM core.  Currently, only
one IXP4xx based system is supported, the Linksys NSLU2.
The Linksys NSLU2 (Network Storage Link for USB 2.0 Disk Drives) is a small
device which allows you to easily provide storage via the network.  It
comes with an Ethernet connection and two USB ports to which hard drives
can be connected.  There is an external site with <ulink
url="&url-arm-cyrius-nslu2;">installation instructions</ulink>.
-->

La piattaforma IXP4xx è basata sul core XScale ARM di Intel. Attualmente è
supportato un solo sistema basato su IXP4xx, il Linksys NSLU2.
NSLU2 (Network Storage Link for USB 2.0 Disk Drives) prodotto da Linksys
è un piccolo dispositivo che permette di fornire semplicemente dello spazio
di memorizzazione tramite la rete. Dispone di un connettore Ethernet e di
due porte USB a cui si possono collegare dei dischi fissi. Si rimanda al
sito con le <ulink url="&url-arm-cyrius-nslu2;">istruzioni per
l'installazione</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>RiscPC</term>
<listitem><para>

<!--
This machine is the oldest supported hardware but support for it in
our new installer is incomplete.
It has RISC OS in ROM, Linux can be booted from that OS using
linloader. The RiscPC has a modular CPU card and typically has a 30MHz
610, 40MHz 710 or 233MHz Strongarm 110 CPU fitted. The mainboard has
integrated IDE, SVGA video, parallel port, single serial port, PS/2
keyboard and proprietary mouse port. The proprietary module expansion
bus allows for up to eight expansion cards to be fitted depending on
configuration, several of these modules have Linux drivers.
-->

Questa macchina è l'hardware più vecchio supportato, purtroppo il suo
supporto nella nuova versione dell'installatore è incompleto.
Ha RISC OS nella ROM e Linux può essere caricato da questo OS
usando linloader. Il RiscPC ha una scheda con la CPU modulare e tipicamente
monta una 610 a 30&nbsp;MHz, una 710 a 40&nbsp;MHz oppure una Strongarm
110 a 233&nbsp;MHz. La scheda madre ha porte IDE, video SVGA, parallela,
seriale, una PS/2 per la tastiera e una porta proprietaria per il mouse.
Il modulo proprietario di espansione del bus permette di connettere fino
a otto schede di espansione a seconda della configurazione; parecchi di
questi moduli hanno i driver per Linux.

</para></listitem>
</varlistentry>

</variablelist>

</para>
  </sect2>
