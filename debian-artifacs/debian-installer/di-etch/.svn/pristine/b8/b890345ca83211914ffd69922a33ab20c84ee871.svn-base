<!-- retain these comments for translator revision tracking -->
<!-- original version: 33725 -->

   <sect3 id="localechooser">
   <!-- <title>Selecting Localization Options</title> -->
   <title>Selezione delle opzioni di localizzazione</title>
<para>

<!--
In most cases the first questions you will be asked concern the selection
of localization options to be used both for the installation and for the
installed system. The localization options consist of language, country
and locales.
-->

Nella maggior parte dei casi le prime domande a cui si risponde riguardano
le opzioni relative alla localizzazione da usare per l'installazione e sul
sistema installato. Le opzioni di localizzazione sono la lingua, la nazione
e i locale.

</para><para>

<!--
The language you choose will be used for the rest of the installation
process, provided a translation of the different dialogs is available.
If no valid translation is available for the selected language, the
installer will default to English.
-->

La lingua scelta viene usata per resto del processo d'installazione che
prosegue fornendo, se disponibili, i dialoghi tradotti. Se per la lingua
scelta non esiste una traduzione, il programma d'installazione prosegue
usando la lingua predefinita, cioè l'inglese.

</para><para>

<!--
The selected country will be used later in the installation process to
pick the default timezone and a Debian mirror appropriate for your
geographic location. Language and country together will be used to set
the default locale for your system and to help select your keyboard.
-->

La nazione scelta verrà usata in seguito durante il processo d'installazione
per selezionare il fuso orario predefinito e il mirror Debian più vicino
alla propria locazione geografica. Lingua e nazione sono usate per impostare
il valore predefinito per il locale e per guidare la scelta della tastiera.

</para><para>

<!--
You will first be asked to select your preferred language. The language
names are listed in both English (left side) and in the language itself
(right side); the names on the right side are also shown in the proper
script for the language. The list is sorted on the English names.
At the top of the list is an extra option that allows you to select the
<quote>C</quote> locale instead of a language. Choosing the <quote>C</quote>
locale will result in the installation proceding in English; the installed
system will have no localization support as the <classname>locales</classname>
package will not be installed.
-->

Per prima cosa viene chiesto di scegliere la lingua che si preferisce. I
nomi delle lingue sono elencati in inglese (sulla sinistra) e nella lingua
stessa (sulla destra); i nomi sulla destra sono mostrati usando i
caratteri corretti. L'elenco è ordinato in base ai nomi in inglese. La
prima voce dell'elenco consente di usare il locale <quote>C</quote>
anziché una lingua. La scelta del locale <quote>C</quote> comporta che
l'installazione proseguirà in inglese e che il sistema installato non avrà
supporto per la localizzazione dato che non verrà installato il pacchetto
<classname>locales</classname>.

</para><para>

<!--
If you selected a language that is recognized as an official language for
more than one country<footnote>
-->

Se si sceglie una lingua che è riconosciuta come lingua ufficiale per più
di un paese<footnote>

<para>

<!--
In technical terms: where multiple locales exist for that language with
differing country codes.
-->

In termini tecnici: per la lingua esistono più locale che si differenziano
in base al codice del paese.

</para>

<!--
</footnote>, you will next be asked to select a country.
If you choose <guimenuitem>Other</guimenuitem> at the bottom of the list,
you will be presented with a list of all countries, grouped by continent.
If the language has only one country associated with it, that country
will be selected automatically.
-->

</footnote> verrà chiesto di scegliere un paese. Se si seleziona la voce
<guimenuitem>Altro</guimenuitem> alla fine dell'elenco viene mostrato un
altro l'elenco con tutti i paesi raggruppati per continente. Invece se la
lingua è associata a un solo paese, viene automaticamente selezionato quel
paese.

</para><para>

<!--
A default locale will be selected based on the selected language and country.
If you are installing at medium or low priority, you will have the option
of selecting a different default locale and of selecting additional locales to
be generated for the installed system.
-->

In base alla lingua e alla nazione selezionate viene scelto un locale
predefinito. Se l'installazione avviene con priorità media o bassa, è
possibile scegliere un locale diverso da quello predefinito e di
aggiungere altri locale da generare sul sistema installato.

</para>
   </sect3>
