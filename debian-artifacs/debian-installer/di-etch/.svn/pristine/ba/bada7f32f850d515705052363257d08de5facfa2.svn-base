<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45438 -->
<!-- revisado por jfs, 28 octubre 2004 -->
<!-- revisado Rudy Godoy, 24 feb. 2005 -->
<!-- revisado por Igor Tamara, enero 2007 -->

<chapter id="boot-new">
 <title>Arrancar desde su nuevo sistema Debian</title>

 <sect1 id="base-boot"><title>El momento de la verdad</title>
<para>

El primer arranque aut�nomo de su sistema es lo que los ingenieros
el�ctricos llaman <quote>la prueba de humo</quote>.

</para><para arch="x86">

Lo primero que deber�a ver cuando arranque el sistema es el men� del cargador
de arranque <classname>grub</classname> o <classname>lilo</classname> si ha
hecho la instalaci�n por omisi�n.  Las primeras opciones en el men� ser�n las
de su nuevo sistema Debian. Se listar�n m�s abajo otros sistemas operativos en
su sistema (como Windows) si los ten�a y los detect� el sistema de instalaci�n.

</para><para>

No se preocupe si el sistema no llega a arrancar. Si la instalaci�n se
complet� con �xito es posible que s�lo haya un problema menor que
impida que su sistema arranque Debian. En muchos casos estos problemas
pueden arreglarse sin tener que repetir la instalaci�n. Una opci�n
disponible para arreglar problemas de arranque es utilizar el modo
de rescate que est� disponible en el propio instalador (consulte <xref
linkend="rescue"/>).
 
</para><para>
Es posible que necesite ayuda de usuarios m�s experimentados si es
nuevo a Debian y a Linux.
<phrase arch="x86">Puede intentar obtener ayuda directamente en l�nea en
los canales de IRC #debian o #debian-boot en la red OFTC.
Tambi�n puede contactar la <ulink url="&url-list-subscribe;">lista de
usuarios debian-user</ulink> (en ingl�s) o <ulink url="&url-list-subscribe;">lista de
usuarios debian-user-spanish</ulink> (en espa�ol).</phrase>
<phrase arch="not-x86">Para arquitecturas menos habituales como es el
caso de &arch-title;, su mejor opci�n es preguntar en la 
<ulink url="&url-list-subscribe;">lista de correo debian-&arch-listname; 
</ulink>.</phrase>
Tambi�n puede enviar un informe de instalaci�n tal y como se describe
en <xref linkend="submit-bug"/>. Por favor, aseg�rese de que describe
claramente su problema y de que incluye cualquier mensaje que se
muestra de forma que otros puedan diagnosticar el problema.

</para><para arch="x86">

Si ten�a alg�n sistema operativo en su ordenador que no se detect� o
se detect� incorrectamente, por favor, env�e un informe de
instalaci�n.

</para>

  <sect2 arch="m68k"><title>Arranque de BVME 6000</title>
<para>

Deber� introducir alguna de las �rdenes mostradas a continuaci�n,
una vez que el sistema ha cargado el programa
<command>tftplilo</command> desde el servidor TFTP, y en el cursor
<prompt>LILO Boot:</prompt> si ha efectuado una instalaci�n sin disco
en una m�quina BMV o Motorola VMEbus.

<itemizedlist>
<listitem><para>

<userinput>b6000</userinput> seguido de &enterkey;
para arrancar un BVME 4000/6000

</para></listitem><listitem><para>

<userinput>b162</userinput> seguido de &enterkey;
para arrancar un MVME162

</para></listitem><listitem><para>

<userinput>b167</userinput> seguido de &enterkey;
para arrancar un MVME166/167

</para></listitem>
</itemizedlist>

</para>

   </sect2>

  <sect2 arch="m68k"><title>Arranque en Macintosh</title>

<para>

Vaya al directorio donde se localizan los ficheros de instalaci�n e
inicie el gestor de arranque <command>Penguin</command>, para ello mantenga presionada
la tecla <keycap>command</keycap>. Vaya a la opci�n
<userinput>Settings</userinput> (<keycombo>
<keycap>command</keycap> <keycap>T</keycap> </keycombo>), y localice
la l�nea de opciones de n�cleo. Esta l�nea debe ser parecida a
<userinput>root=/dev/ram ramdisk_size=15000</userinput>.

</para><para>

Deber� cambiar dicha l�nea a
<userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>.
Sustituya <replaceable>yyyy</replaceable> por el nombre Linux de
la partici�n en la que ha instalado el sistema.
(p. ej. <filename>/dev/sda1</filename>). Usted ha anotado este nombre antes.
Si su pantalla es peque�a, se le recomienda que utilice
<userinput>fbcon=font:VGA8x8</userinput> (o
<userinput>video=font:VGA8x8</userinput> en versiones del n�cleo anteriores a
la 2.6), ya que pueden ayudar a hacer el texto m�s legible. Podr� cambiar este
tipo de letra en cualquier momento.

</para><para>

Si no desea arrancar GNU/Linux inmediatamente cada vez que encienda el sistema,
desmarque la opci�n <userinput>Auto Boot</userinput>. Guarde su
configuraci�n en el fichero <filename>Prefs</filename> con la
opci�n <userinput>Save Settings As Default</userinput>.

</para><para>

Ahora elija <userinput>Boot Now</userinput> (<keycombo>
<keycap>command</keycap> <keycap>B</keycap> </keycombo>) para arrancar
su nueva instalaci�n de GNU/Linux en lugar del sistema del sistema
de instalaci�n en RAMdisk.

</para><para>

Deber�a arrancar Debian y deber�a ver los mismos mensajes que
cuando arranc� anteriormente el sistema de instalaci�n, seguidos de
algunos mensajes nuevos.

</para>
   </sect2>


  <sect2 arch="powerpc"><title>PowerMacs OldWorld</title>
<para>

Si la m�quina falla al arrancar despu�s de finalizar la instalaci�n
y se detiene mostrando el cursor <prompt>boot:</prompt>, intente escribir
<userinput>Linux</userinput> seguido de &enterkey;. (La configuraci�n
de arranque predeterminada en <filename>quik.conf</filename> est�
etiquetada como Linux). Podr� ver las etiquetas definidas en
<filename>quik.conf</filename> si presiona la tecla
<keycap>Tab</keycap> en el cursor <prompt>boot:</prompt>.
Tambi�n puede intentar volver a arrancar v�a  el instalador y editar el fichero
<filename>/target/etc/quik.conf</filename> que se ha generado en
el paso <guimenuitem>Instalar quik en un disco duro</guimenuitem>.
Puede encontrar algunas pistas para adaptar
<command>quik</command> a su caso espec�fico en <ulink
url="&url-powerpc-quik-faq;"></ulink>.

</para><para>

Para volver al MacOS sin reiniciar la nvram, escriba
<userinput>bye</userinput> en el cursor de �OpenFirmware� (siempre y cuando
no haya eliminado MacOS de la m�quina). Para obtener el cursor,
mantenga presionadas las teclas <keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>o</keycap> <keycap>f</keycap>
</keycombo> mientras la m�quina reinicia en fr�o. Si necesita restaurar
los cambios hechos a la nvram de OpenFirmware al valor por omisi�n
MacOS para poder volver a arrancar MacOS, mantenga presionadas la
teclas <keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap>
</keycombo> mientras se arranca la m�quina en fr�o.

</para><para>

En el caso de que utilice <command>BootX</command> para arrancar el
sistema instalado,
s�lo tendr� que elegir el n�cleo que desea del directorio
<filename>Linux Kernels</filename>, desmarcar la opci�n ramdisk y
a�adir el dispositivo ra�z correspondiente a su instalaci�n;
p. ej. <userinput>/dev/hda8</userinput>.

</para>
   </sect2>


  <sect2 arch="powerpc"><title>PowerMacs NewWorld</title>
<para>

En el caso de m�quinas G4 e iBooks, puede mantener presionada la tecla
<keycap>option</keycap> y obtener una pantalla gr�fica con un
bot�n para cada sistema operativo arrancable, &debian; ser� un
bot�n representando con el icono de un ping�ino peque�o.

</para><para>

Si mantiene MacOS y en alg�n momento cambia la variable
<envar>boot-device</envar> de �OpenFirmware� deber� reiniciar �ste a su
configuraci�n predeterminada. Para hacer esto mantenga presionadas
las teclas <keycombo>
<keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap>
<keycap>r</keycap> </keycombo> mientras la m�quina arranca en fr�o.

</para><para>

Se mostrar�n las etiquetas definidas en <filename>yaboot.conf</filename>
si presiona la tecla <keycap>Tab</keycap> en el cursor
<prompt>boot:</prompt>.

</para><para>

El reinicio de �OpenFirmware� en hardware G3 � G4 har� que se
arranque &debian; en forma predeterminada (si primero ha particionado
correctamente y localizado la partici�n �Apple_Bootstrap�).
Puede que esto no funcione si su instalaci�n tiene &debian; en un disco SCSI
y MacOS en un disco IDE, tendr� que acceder al �OpenFirmware� para
configurar la variable <envar>boot-device</envar>.
Generalmente <command>ybin</command> hace esto autom�ticamente.

</para><para>

Despu�s de arrancar &debian; por primera vez puede a�adir cuantas opciones
adicionales desee (como por ejemplo, opciones de arranque dual) al fichero
<filename>/etc/yaboot.conf</filename> y ejecutar <command>ybin</command>
para actualizar la partici�n de arranque con la nueva configuraci�n.
Para m�s informaci�n, por favor lea el
<ulink url="&url-powerpc-yaboot-faq;">C�MO de yaboot</ulink>.

</para>
   </sect2>
 </sect1>

&mount-encrypted.xml;

 <sect1 id="login">
 <title>Acceso</title>

<para>

Se le presentar� el cursor de �login� (acceso, N. del t.) despu�s de
que haya arrancado su sistema.  Puede acceder usando
la cuenta personal y clave que ha seleccionado durante la instalaci�n. Su
sistema est� ahora listo para ser usado.

</para><para>

Si usted es un usuario novel, tal vez quiera explorar la documentaci�n
que ya est� instalada en su sistema mientras empieza a utilizarlo.
Actualmente existen varios sistemas de documentaci�n, aunque se est�
trabajando en integrar los diferentes tipos disponibles. Aqu�
encontrar� algunas gu�as que le indicar�n d�nde empezar a buscar.

</para><para>

La documentaci�n que acompa�a a los programas que ha instalado se
encuentra en el directorio <filename>/usr/share/doc/</filename>, bajo
un subdirectorio cuyo nombre coincide con el del programa (o, m�s
exactamente, el nombre del paquete Debian que contiene el
programa). Sin embargo podr� encontrar documentaci�n m�s extensa en
paquetes independientes de documentaci�n que generalmente no se
instalan por omisi�n. Por ejemplo, puede encontrar documentaci�n de la
herramienta de gesti�n de paquetes <command>apt</command> en los
paquetes <classname>apt-doc</classname> o
<classname>apt-howto</classname>.

</para><para>


Adem�s, existen algunos directorios especiales dentro de la jerarqu�a
de <filename>/usr/share/doc/</filename>. Puede encontrar los C�MOs de
Linux en formato <emphasis>.gz</emphasis> (comprimido), en
<filename>/usr/share/doc/HOWTO/en-txt/</filename>. Encontrar� un
�ndice navegable de la documentaci�n instalada en
<filename>/usr/share/doc/HTML/index.html</filename> una vez
instale <classname>dhelp</classname>.

</para><para>

Una forma f�cil de consultar estos documentos utilizando un navegador
con interfaz de texto es ejecutar las siguientes �rdenes:

<informalexample><screen>
$ cd /usr/share/doc/
$ w3c .
</screen></informalexample>

El punto despu�s de la orden <command>w3c</command> le indica que debe
mostrar los contenidos del directorio actual.

 </para><para>
 
Puede utilizar el navegador web del entorno gr�fico de escritorio si
tiene instalado uno. Arranque el navegador web del men� de aplicaci�n
y escriba <userinput>/usr/share/doc/</userinput> en la barra de
direcciones.

</para><para>

Tambi�n puede escribir <userinput>info
<replaceable>programa</replaceable></userinput> o <userinput>man
<replaceable>programa</replaceable></userinput> para consultar la
documentaci�n de la mayor�a de los programas disponibles en la l�nea
de �rdenes. Si escribe <userinput>�help�</userinput> se le mostrar� una
ayuda sobre las �rdenes del gui�n de l�nea de �rdenes. Habitualmente,
si escribe el nombre de un programa seguido de
<userinput>--help</userinput> se le mostrar� un breve resumen del uso
de este programa. Si la salida es mayor que el tama�o de su pantalla,
escriba <userinput>|&nbsp;more</userinput> despu�s de la llamada anterior
para hacer que los resultados se pausen antes de que sobrepasen el
tama�o de la pantalla. Puede tambi�n ver la lista de todos los
programas disponibles que empiezan con una cierta letra. Simplemente,
escriba la letra en cuesti�n y luego presione dos veces el tabulador.

</para>

 </sect1>
</chapter>
