<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 -->


 <sect1 id="partition-sizing">
 <title>Debian のパーティションとそのサイズを決める</title>
<para>

<!--
At a bare minimum, GNU/Linux needs one partition for itself.  You can
have a single partition containing the entire operating system,
applications, and your personal files. Most people feel that a
separate swap partition is also a necessity, although it's not
strictly true.  <quote>Swap</quote> is scratch space for an operating system,
which allows the system to use disk storage as <quote>virtual
memory</quote>. By putting swap on a separate partition, Linux can make much
more efficient use of it.  It is possible to force Linux to use a
regular file as swap, but it is not recommended.
-->
必要最小限の構成でも、GNU/Linux は自身のために
少なくとも 1 つのパーティションを必要とします。
オペレーティングシステム全体、アプリケーション、
個人ファイルは 1 つのパーティションに収めることが可能です。
多くの人はこれと別にスワップパーティションも必要だと思っているようですが、
これは厳密には正しくありません。
<quote>スワップ</quote>とはオペレーティングシステムが用いる
メモリの一時退避用空間で、これを用いると
システムはディスク装置を<quote>仮想メモリ</quote>として使えるようになります。
スワップを独立したパーティションに割り当てると、
Linux からの利用がずっと効率的になります。
Linux は普通のファイルを無理やりスワップとして
利用することもできますが、これはお勧めできません。

</para><para>

<!--
Most people choose to give GNU/Linux more than the minimum number of
partitions, however.  There are two reasons you might want to break up
the file system into a number of smaller partitions. The first is for
safety.  If something happens to corrupt the file system, generally
only one partition is affected. Thus, you only have to replace (from
the backups you've been carefully keeping) a portion of your
system. At a bare minimum, you should consider creating what is
commonly called a <quote>root partition</quote>. This contains the most essential
components of the system. If any other partitions get corrupted, you
can still boot into GNU/Linux to fix the system. This can save you the
trouble of having to reinstall the system from scratch.
-->
とはいえ大抵の人は、この最低限必要な数よりは多くのパーティションを
GNU/Linux に割り当てます。
ファイルシステムをいくつかのより小さなパーティションに
分割する理由は 2 つあります。
1 つめは安全性です。もし偶然に何かがファイルシステムを破壊したとしても、
一般的にその影響を被るのは 1 つのパーティションだけです。
そのため、システムの一部を (注意深く保持しておいたバックアップと)
置き換えるだけですみます。少なくとも、
いわゆる<quote>ルートパーティション</quote>は別にすることを考慮しましょう。
ここにはシステムの最も基本的な構成部分が収められており、
もし他のパーティションに破損が生じたとしても、
Linux を起動してシステムを補修できます。
システムをゼロから再インストールしなければ
ならないようなトラブルが防げるのです。

</para><para>

<!--
The second reason is generally more important in a business setting,
but it really depends on your use of the machine. For example, a mail
server getting spammed with e-mail can easily fill a partition. If you
made <filename>/var/mail</filename> a separate partition on the mail
server, most of the system will remain working even if you get spammed.
-->
2 つめの理由は、一般的にビジネスで使う際により重要になってくるものですが、
これはコンピュータの利用方法にかなり依存します。例えばスパムメールを
たくさん受け取ったメールサーバは、パーティションを簡単に溢れさせてしまう
かもしれません。もしメールサーバ上の独立したパーティションを
<filename>/var/mail</filename>に割り当てれば、スパムメールを取り込んでも
システムの大半は問題なく動作するでしょう。

</para><para>

<!--
The only real drawback to using more partitions is that it is often
difficult to know in advance what your needs will be. If you make a
partition too small then you will either have to reinstall the system
or you will be constantly moving things around to make room in the
undersized partition. On the other hand, if you make the partition too
big, you will be wasting space that could be used elsewhere. Disk
space is cheap nowadays, but why throw your money away?
-->
たくさんのパーティションを利用する際に唯一の不利になる点は、
どのようなパーティションが必要となるかをあらかじめ予測するのが、
ほとんどの場合は難しいということです。
用意したパーティションが小さすぎると、
システムを再インストールしたり、
容量の足りないパーティションからしょっちゅうファイルを移動して、
スペースを空けたりしなければならないでしょう。
一方、あまりに大きなパーティションを用意すれば、
他で利用できるスペースを浪費しかねません。
近頃はディスクも安価になったとはいえ、お金を無駄に使う必要はないでしょう?

</para>
 </sect1>
