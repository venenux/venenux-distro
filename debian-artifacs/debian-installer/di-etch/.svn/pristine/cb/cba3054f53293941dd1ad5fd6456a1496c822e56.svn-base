<!-- retain these comments for translator revision tracking -->
<!-- original version: 45616 -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Dateien vorbereiten für das Booten von einem USB-Memory-Stick</title>

<para>

Um den USB-Stick vorzubereiten, benötigen Sie ein System, auf dem
GNU/Linux bereits läuft und das USB unterstützt. Stellen Sie sicher,
dass das usb-storage-Kernelmodul geladen ist
(<userinput>modprobe usb-storage</userinput>) und versuchen Sie herauszufinden,
welches SCSI-Gerät dem USB-Stick zugewiesen wurde (in diesem Beispiel
benutzen wir <filename>/dev/sda</filename>). Um den Stick zu beschreiben,
müssen Sie eventuell noch den Schreibschutz-Schalter ausschalten.

</para><para>

Beachten Sie, dass der USB-Stick mindestens 256 MB groß sein sollte
(kleinere Setups sind möglich, wenn Sie sich nach <xref linkend="usb-copy-flexible"/>
richten).

</para>

  <sect2 id="usb-copy-easy">
  <title>Die Dateien kopieren &ndash; der einfache Weg</title>
<para arch="x86">

Es gibt ein <quote>Alles-in-einem</quote>-Image <filename>hd-media/boot.img.gz</filename>,
das alle Dateien des Installers enthält (inklusive des Kernels) wie auch
das <command>SYSLINUX</command>-Programm mit der zugehörigen
Konfigurationsdatei. Sie müssen das Image lediglich direkt auf den USB-Stick
entpacken:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para><para arch="powerpc">

Es gibt ein <quote>Alles-in-einem</quote>-Image <filename>hd-media/boot.img.gz</filename>,
das alle Dateien des Installers enthält (inklusive des Kernels) wie auch
<command>yaboot</command> mit der zugehörigen Konfigurationsdatei.
Erstellen Sie eine Partition des Typs <quote>Apple_Bootstrap</quote> auf Ihrem USB-Stick,
indem Sie das <userinput>C</userinput>-Kommando von <command>mac-fdisk</command>
verwenden, und entpacken Sie das Image direkt dorthin:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda2</replaceable>
</screen></informalexample>

</para>
<warning><para>

Durch diese Methode wird alles, was auf dem Gerät gespeichert ist,
zerstört. Versichern Sie sich, dass Sie die richtige Gerätebezeichnung
für Ihren USB-Stick verwenden.

</para></warning>
<para>

Danach hängen Sie den USB-Memory-Stick ins Dateisystem ein
(<userinput>mount <replaceable arch="x86">/dev/sda</replaceable>
<replaceable arch="powerpc">/dev/sda2</replaceable>
/mnt</userinput>), der jetzt ein <phrase arch="x86">FAT-Dateisystem</phrase>
<phrase arch="powerpc">HFS-Dateisystem</phrase> enthält, und kopieren ein
Debian-<quote>netinst</quote>- oder -<quote>businesscard</quote>-ISO-Image dorthin
(siehe <xref linkend="usb-add-iso"/>). Achten Sie darauf,
dass die Datei auf <filename>.iso</filename> endet. Hängen Sie den
Stick aus dem Dateisystem aus (<userinput>umount /mnt</userinput>) &ndash; Sie
haben es geschafft.

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>Die Dateien kopieren &ndash; der flexible Weg</title>
<para>

Wenn Sie flexibler sein wollen oder einfach nur wissen möchten, was passiert,
sollten Sie die folgende Methode nutzen, um die Dateien auf den Stick zu
befördern:

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

   </sect2>

   <sect2 id="usb-add-iso">
   <title>Ein ISO-Image hinzufügen</title>
<para>

Da der Stick die Quelle für zusätzliche Daten ist, die für die
Installation benötigt werden, wird der Installer dort nach einem
Debian-ISO-Image suchen. Sie müssen also als nächstes ein
solches Image (<quote>businesscard</quote>,
<quote>netinst</quote> oder
sogar ein Komplett-Image) auf den Stick laden (wählen Sie eines, das
auch auf den Stick passt). Der Dateiname des Images muss auf
<filename>.iso</filename> enden.

</para><para>

Wenn Sie per Netzwerk booten möchten, ohne ein ISO-Image zu nutzen,
können Sie natürlich den vorigen Schritt überspringen. Sie müssen
dabei außerdem die Initial RAM-Disk aus dem Verzeichnis
<filename>netboot</filename> verwenden statt aus
<filename>hd-media</filename>, da <filename>hd-media/initrd.gz</filename>
keine Netzwerkunterstützung hat.

</para><para>

Wenn Sie dies erledigt haben, hängen Sie den USB-Memory-Stick aus dem
Dateisystem aus (<userinput>umount /mnt</userinput>) und aktivieren
den Schreibschutz.

</para>
  </sect2>

  <!-- TODO: doesn't this section belong later? -->
  <sect2 arch="x86">
  <title>Vom USB-Stick booten</title>
<warning><para>

Wenn Ihr System es ablehnt, von dem Memory-Stick zu booten, könnte
es sein, dass der Stick einen defekten Master-Boot-Record (MBR)
enthält. Um dies zu beheben, nutzen Sie den
<command>install-mbr</command>-Befehl aus dem Paket
<classname>mbr</classname>:

<informalexample><screen>
# install-mbr /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
