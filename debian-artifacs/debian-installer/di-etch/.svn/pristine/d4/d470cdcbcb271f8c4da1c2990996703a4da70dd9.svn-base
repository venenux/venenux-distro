<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 untranslated -->


  <sect2 arch="ia64"><title>Partitioning for &arch-title;</title>
<para>

ia64 EFI firmware supports two partition table (or disk label)
formats, GPT and MS-DOS.  MS-DOS is the format typically used on i386
PCs, and is no longer recommended for ia64 systems.  The installer
provides two partitioning programs, 
<ulink url="cfdisk.txt"><command>cfdisk</command></ulink> and 
<ulink url="parted.txt"><command>parted</command></ulink>.
<command>parted</command> can manage both GPT and MS-DOS tables, while
<command>cfdisk</command> can only manage MS-DOS tables.  It is very
important to note that if your disk has previously been partitioned
with a GPT table, and you now want to use MS-DOS tables, you must use
<command>parted</command> to create the new partition table.  This is
because the two tables use different areas of a disk, and
<command>cfdisk</command> does not know how to remove a GPT table.

</para><para>

An important difference between <command>cfdisk</command> and
<command>parted</command> is the way they identify a partition
``type''.  <command>cfdisk</command> uses a byte in the partition
table (for example, 83 for a linux ext2 partition), while
<command>parted</command> identifies a partition ``type'' by examining
the data on that partition.  This means that <command>parted</command>
will not consider a partition to be a swap partition until you format
it as such.  Similarly, it won't consider a partition a linux ext2
partition until you create a file system on it.
<command>parted</command> does allow you to create file systems and
format swap space, and you should do that from within
<command>parted</command>.

</para><para>

Unfortunately, <command>parted</command> is a command line driven
program and so not as easy to use as <command>cfdisk</command>.
Assuming that you want to erase your whole disk and create a GPT table
and some partitions, then something similar to the following command
sequence could be used:

</para><para>
<informalexample><screen>

      mklabel gpt
      mkpartfs primary fat 0 50
      mkpartfs primary linux-swap 51 1000
      mkpartfs primary ext2 1001 3000
      set 1 boot on
      print
      quit

</screen></informalexample>
</para><para>

That creates a new partition table, and three partitions to be used as
an EFI boot partition, swap space, and a root file system.  Finally it
sets the boot flag on the EFI partition.  Partitions are specified in
Megabytes, with start and end offsets from the beginning of the disk.
So, for example, above we created a 1999MB ext2 file system starting
at offset 1001MB from the start of the disk.  Note that formatting swap
space with <command>parted</command> can take a few minutes to
complete, as it scans the partition for bad blocks.

</para>
  </sect2>

  <sect2 arch="ia64"><title>Boot Loader Partition Requirements</title>

<para>

ELILO, the ia64 boot loader, requires a partition containing a FAT
file system.  If you used GPT partition tables, then that partition
should have the <userinput>boot</userinput> flag set; if you used
MS-DOS partition tables, then that partition should be of type "EF".
The partition must be big enough to hold the boot loader and any
kernels or RAMdisks you may wish to boot.  A minimum size would be
about 16MB, but if you are likely to be doing development, or
experimenting with different kernels, then 128MB might be a better
size.

</para>
  </sect2>