installation-guide (20070319) unstable; urgency=low

  [ Frans Pop ]
  * Cleaned up definitions of some entities and removed unused ones.
  * Implement infrastructure that allows entities to be translated.
    Thanks to Miroslav Kure for providing the patch. Closes: #344048, #406515.
  * Disable Korean PDF/PS formats for builds for the website.
  * appendix/files: update information about mouse configuration, based on a
    patch from Peter Samuelson (for which thanks). Closes: #406290.
  * boot-installer, appendix/preseed: document preseed/interactive parameter.

  [ Joey Hess ]
  * Fix package name for installation-reports, and manual unfuzzy of all
    translations. Closes: #408408

  [ Frans Pop ]
  * welcome:
    - make links to FSF and GNU more consistent (closes: #410317)
    - we do not necessarily share the ideals of the FSF (closes: #410129)
    Thanks to Francesco Poli for suggesting these changes.
  * using-d-i: guided partitioning now creates swap inside LVM.
    Thanks to Francesco Poli. Closes: #411399.
  * Document installs over PPPoE. Based on a text proposed by Eddy Petrisor.
    Closes: #408340.
  * installation-howto: update for the integration of base-config into D-I.
  * appendix/preseed:
    - as exim no longer asks any questions during installations, there is no
      real need to document it anymore
    - add link to Philip Hand's website as it contains many creative examples
    - document how to select the ftp protocol during mirror selection
    - document how to install from CD/DVD only (and not use a network mirror)
  * Update base/standard system and task sizes.

  [ Joey Hess ]
  * Document debian-installer/allow_unauthenticated.

  [ Wouter Verhelst ]
  * Update mac68k installation after notes from Finn Thain, upstream mac68k
    kernel hacker.

  [ Frans Pop ]
  * Replace entity in example-preseed.txt file. Thanks to Geert Stappers for
    spotting the error. Closes: #413257.

 -- Frans Pop <fjp@debian.org>  Mon, 19 Mar 2007 15:00:12 +0100

installation-guide (20070122) unstable; urgency=low

  * Enable Catalan, Brazillian, Korean and Spanish again.
  * Enable new Hungarian translation.

 -- Frans Pop <fjp@debian.org>  Mon, 22 Jan 2007 12:41:57 +0100

installation-guide (20070115) unstable; urgency=low

  [ Colin Watson ]
  * Document new pkgsel/include setting.
  * Fix incorrect documentation on preseeding additional apt sources.
  * Fix typo in hppa hardware support documentation
    (https://launchpad.net/bugs/55164).
  * Adjust chroot-install guide to take account of debootstrap being
    Architecture: all now (https://launchpad.net/bugs/64765).

  [ Frans Pop ]
  * Various corrections suggested by Clytie Siddall and Holger Wansing.
    Closes: #378404, #370484, #370425.
  * Update instructions on how to set module parameters for PLIP installation.
  * Update chroot install instructions to use aptitude instead of apt-get.
  * Add /srv in overview of directory tree in partitioning appendix.
  * Update section on partitioning using LVM to reflect recent changes.
  * Apply various patches by or based on suggestions from Nathanael Nerode.
    Some of the bigger changes include:
    - Update of processor support for the i386 architecture.
    - Update information on setting up a RARP server.
  * Add variant for the AMD64 architecture. Closes: #305977.
  * Suppress warnings about missing IDs for sections and such.
  * Reorganize supported hardware table; i386 no longer has flavors.
  * Document SMP-alternatives as one of the variants of SMP support.
  * Document how to shut down the system. Based on patch from Holger Levsen.
    Closes: #248067.

  [ Miroslav Kure ]
  * Update information about booting with encrypted partitions.
    Closes: #378651.

  [ Joey Hess ]
  * Update preseeding docs to document listing more than one disk for
    partman-auto/disk and grub-installer/bootdev

  [ Changwoo Ryu ]
  * Add support for Korean PDF output. Requires latex-hangul-ucs-hlatex.
    Closes: #381657.

  [ David Härdeman ]
  * Update preseeding documentation for partman-auto.

  [ Frans Pop ]
  * Correct error for alpha netboot installations. Thanks to Aurélien Géröme.
    Closes: #386591.
  * Correct number of release arches. Thanks to Holger Wansing. Closes: #384321.
  * Minor corrections suggested by Malcolm Parsons. Closes: #388138.
  * Document new boot parameters theme and directfb/hw-accel.
  * Remove now obsolete distinction between classic and common kpkg.
  * Update all arches to kernel version 2.6.17.
  * Set default value for manual_release in build wrapper scripts.
  * Add new section in preseeding appendix on partman-auto-raid. Not included
    in preseed text file as it is still somewhat experimental.
  * Document changes in guided partitioning and added encrypted LVM support.
  * Disable m68k for official builds as it is not a release architecture.
  * buildweb.sh: use 'mv -f' to avoid errors during builds on the webserver.
  * Minor corrections suggested by Clytie Siddall. Closes: #394413, #394471.
  * Update task sizes as per D-I RC1.
  * New troubleshooting section on how to deal with CD-ROM related problems.
  * Document how to pass parameters to kernel modules on boot.
  * Document how to install KDE instead of Gnome in the section on pkgsel.

  [ Joey Hess ]
  * Update for recent floppy changes.
  * Remove hardcoded paths to executables in buildone.sh.
  * Set HOME to a temporary directory before running w3m, to avoid it
    trying to write to a home directory that may not exist on the buildds.
    (See #393641)
  * Update the installation report template to match current one in
    installation-report.

  [ Philip Hands ]
  * Clarify how -- affects which parameters end up in the target system's
    bootloader's command line.

  [ Joey Hess ]
  * Applied Phil's auto mode documentation patch, edited it, and moved
    some things around for clarity. Closes: #395910
  * Document auto=true boot parameter.

  [ Frans Pop ]
  * Add condition "g-i" for arches that support the graphical installer.
  * Document mouse/left boot parameter.
  * Explain usage of "owner" prefix when setting values for debconf variables
    intended for the target system at the boot prompt.
  * Give some examples of preseeding questions from the boot prompt.
  * Document how to install the KDE desktop environment instead of GNOME.
  * Don't list unsupported frontends in boot parameters section.
  * Minor corrections suggested by Tapio Lehtonen. Closes: #397790.
  * Minor corrections suggested by Holger Wansing. Closes: #397974.

  [ Martin Michlmayr ]
  * Mention that RiscPC support is incomplete.
  * Mention Intel IOP32x support.
  * Talk about ixp4xx rather than the more specific nslu2.

  [ Frans Pop ]
  * Update all arches to kernel version 2.6.18.
  * Minor corrections and some rewrites based on suggestions from
    Clytie Siddall. Closes: #367861, #380588.
  * Update Sparc specific information and document common installation issues
    for Sparc. Many thanks to Jurij Smakov for reviewing the Sparc manual and
    for his overview of issues.
  * Etch ships with X.Org 7.1.
  * Update number of developers, packages and mailing lists.
  * General revision of minimum hardware requirements throughout the manual.
  * Update of installed/download size of tasks.
  * Document the graphical installer, including the main known issues.
  * Avoid generating XML files multiple times for translations using PO files
    when using build.sh or buildweb.sh sripts.
  * bookinfo:
    - Add note for m68k that it is not a release architecture for Etch.
  * hardware:
    - Remove ancient section on parity RAM. Closes: #403035.
    - Update documenation for Sparc, based on patch by Jurij Smakov.
      Closes: #389516.
    - PowerPC/APUS is currently not supported; removed from hardware table.
    - Remove sun4cd (sparc32) from and add sun4v (sparc64) in hardware table.
    - Rewrite of NIC support, document wireless support (for selected arches)
      and explain issues surrounding drivers that require firmware.
  * preparing:
    - Clean out ancient hardware issues.
  * boot-installer/parameters:
    - Document anna/choose_modules and mirror/protocol boot parameters.
    - Use a clearer way to list the short form of parameters.
    - Document new option to blacklist kernel modules.
  * boot-installer/trouble:
    - Fix reference to Save debug logs menu option.
      Thanks, Philippe Batailler. Closes: #402439.
    - Add a note that installation reports will be published.
  * install-methods:
    - All architectures that support TFTP booting also support DCHP, so remove
      the "supports-dhcp" condition.
    - According to the text, Alpha supports BOOTP.
    - Use dhcp3-server for examples rather than dhcp (version 2).
    - tftpd-hpa does not use /tftpboot by default; allow for this in examples.
      Closes: #342076.
  * using-d-i:
    - Extend description of lowmem installs based on a patch suggested by
      Holger Wansing. Closes: #400263.
  * boot-new:
    - Remove reference to apt guide (not installed by default) and obsolete
      debian-guide. Thanks, Philippe Batailler. Closes: #402492.
    - As lynx is no longer installed, suggest w3m as text based browser.
    - Explain how to browse /usr/share/doc/ using a graphical web browser.
    - Mention documentation packages.
    - Clarify what happens on first boot and how to get help to in case of
      problems.
  * post-install:
    - Remove section on reactivating Windows; the installer should detect it
      automatically after all. Closes: #402437.
    - Add extended section on email configuration (moved from using-d-i as
      exim4 now configures for "local mail only" by default). Closes: #402857.
  * appendix/preseed:
    - Clarifications based on comments from Tapio Lehtonen and Holger Wansing.
      Closes: #397923.
  * appendix/chroot-install:
    - Changes suggested in review by Wiktor Wandachowicz. Closes: #394929.

  [ Frans Pop ]
  * Prepare for release. This release is mostly for testing purposes, the real
    release targeted at Etch will be next week.
  * Add and enable amd64 architecture.
  * Update control file for dropped m68k and added amd64 architectures.
  * Disable Catalan, Korean, Spanish and Brazillian translations as they are
    not yet complete.
  * Enable Finnish translation.

 -- Frans Pop <fjp@debian.org>  Mon, 15 Jan 2007 22:10:16 +0100

installation-guide (20060726) unstable; urgency=low

  [ Joey Hess ]
  * Update kernel building section to conditionalise some more 2.4 kernel
    stuff.
  * Suggest passing --initrd to make-kpkg since the docs say nothing about
    compiling disk drivers into the kernel.
  * Document new passwd/root-login setting.
  * Booting a USB stick via a boot floppy is not supported by current
    installer images.
  * Update preseeding docs to use task names, not short descriptions, since
    that will work now and is more convenient for preseeding.
  * Document the kde-desktop and gnome-desktop subtasks.

  [ Frans Pop ]
  * Various corrections suggested by Clytie Siddall.
  * chroot-install:
    - add /etc/hosts to be created for network configuration (closes: #364517)
    - make kernel/linux-image variable (closes: #345482)
  * Document -- command line option for passing parameters to boot loader
    configuration (closes: #309889).
  * Switch Finnish from XML-based to PO-based translation.
  * Bring numbering and layout of GPL more in line with original. With thanks
    to Holger Wansing.
  * Add functionality to include optional paragraphs in translations.
    Initially two have been defined:
    - a paragraph in bookinfo for information about the translation;
    - a note in appendix/gpl.xml required with unofficial GPL translations.

  [ Joey Hess ]
  * Document new preseed/run setting.
  * Prebaseconfig renamed to finish-install, which changes some documentation,
    and. notably, renames one of the xml files.
  * hd-media size changed to 256 mb.
  * Document new aliases added in preseed 1.17 for common debconf boot
    parameters: fb, url, file, locale, interface.

  [ Frans Pop ]
  * Document guided partitioning using LVM and update partitioning section.

  [ Martin Michlmayr ]
  * The RiscPC flavour is now known as "rpc" rather than "riscpc".
  * As of 2.6, r4k-ip22 also rupports r5k machines do the r5k-ip22 flavour
    has been dropped.

  [ Holger Levsen ]
  * Added the remaining suggestions about nubus and non-powerpc macs
    (closes: #364546).
  * Restructured the chapter about powerpc supported hardware, moved
    unsupported machines to the end.
  * Updated the information about existing kernel flavours.

  [ Miroslav Kure ]
  * Document installation to encrypted partitions (partman-crypto).
  * Update XOrg to 7.0.

  [ Frans Pop ]
  * Etch has been assigned version 4.0.
  * Update kernel versions to 2.6.16.
  * Base languages to be built for website on list of release languages.
  * arm, mips and mipsel switched to 2.6 kernels.
  * Enable Brazillian Portuguese and Vietnamese translation for release.
  * Disable Greek translation as it is too incomplete.

 -- Frans Pop <fjp@debian.org>  Wed, 26 Jul 2006 07:27:52 +0200

installation-guide (20060427) unstable; urgency=low

  [ Joey Hess ]
  * Update preseeding docs for base-config removal.
  * Add documentation of how to change apt-setup security source with
    preseeding.
  * Remove the sarge preseeding docs.
  * Remove mentions of base-config and move base-config stuff to elsewhere.
  * Add documentation of apt-setup, user-setup, clock-setup, tzsetup, and
    pkgsel.
  * Change some references to tty3 (messages) to instead refer to tty4
    (syslog).
  * Change references to /var/log/debian-installer to just /var/log/installer.
  * Remove docs for baseconfig-udeb; going away with base-config.
  * Removed various bits of sarge cruft when it made things easier.

  [ Frans Pop ]
  * Change default build to Etch.
  * Remove all Sarge specific content and make Etch specific content default.
  * Delete some obsolete entities.
  * Change references from package d-i-manual to package installation-guide.
  * Explicitly specify XML declaration as required by openjade 1.4devel1-15
    (closes: #360241).
  * Update appendix on chroot installs:
    - mention that debootstrap also requires basic Unix/Linux commands;
    - comment out link to outdated rpm packages of debootstrap.

  [ Colin Watson ]
  * Add a few more supports-floppy-boot and bootable-usb conditionals to
    make it easier to turn all this off in rebuilds of the manual.
  * Etch uses X.Org 6.9, and xserver-xorg instead of xserver-xfree86.
  * Document how to pick a particular interface with
    netcfg/choose_interface, including how to do this before reading a
    preseed file.
  * Remove obsolete substvar.
  * Build-depend on libhtml-parser-perl for build/preseed.pl.

  [ Joey Hess ]
  * Add docs about preseeding a fallback to a static IP if dhcp fails.
  * Update copyright date.
  * It's possible to use preseeding to set up LVM now, so remove stuff about
    it not working.
  * Document DHCP preseeding. Note that this documents it as of netcfg 1.24
    and preseed 1.13.

  [ Martin Michlmayr ]
  * Bring ARM up to date with reality: remove unsupported flavours, add
    NSLU2.
  * Fix Netwinder/CATS download location.
  * Improve the Netwinder TFTP boot instructions.
  * The Netwinder firmware is no longer available due to license problems.
  * Mention NSLU2 download location.
  * Describe how to flash the installer image onto the NSLU2.
  * Describe how to boot a Cobalt machine.

  [ Frans Pop ]
  * Document new option to preseed additional apt sources for target system.
  * Document how to create a preconfiguration file.
  * Enable Catalan, Italian and Swedish translations for release.
  * Disable Brazillian Portuguese translation as it is too incomplete.

 -- Frans Pop <fjp@debian.org>  Thu, 27 Apr 2006 03:00:32 +0200

installation-guide (20060102) unstable; urgency=low

  [ Frans Pop ]
  * Fix build error caused by description for s390. Closes: #337382.
  * Update kernel package names and kernel versions per architecture.
  * Move documentation on preseeding to separate appendix for etch.
  * Use new, more flexible perl script to extract example preseed file from
    new appendix.
  * Set proper POT-Creation-Date when generating new POT files.
  * New translations started: Polish, Swedish, Vietnamese.
  * First release of Greek and Korean translations.
  * Exclude Traditional Chinese translation as it was not updated.
  * Switch release name to Etch (3.1+δ).

  [ Joey Hess ]
  * Update installation report template and instructions for etch.

 -- Frans Pop <fjp@debian.org>  Mon,  2 Jan 2006 18:44:59 +0100

installation-guide (20051025) unstable; urgency=low

  * Mention in copyright that full GPL is included in the manual.
    Closes: #334925
  * Register installed documents with doc-base.
  * Minor updates in English text and translations.

 -- Frans Pop <fjp@debian.org>  Tue, 25 Oct 2005 17:37:25 +0200

installation-guide (20051019) unstable; urgency=low

  [ Joey Hess ]
  * Split off from the debian-installer source package.
    See the changelog for debian-installer for older changelog entries.

  [ Frans Pop ]
  * Change over to building arch all packages in section doc. Build separate
    packages for each arch containing all translations.
  * Change package name to installation-guide.
  * Include translations already available on official website.

 -- Frans Pop <fjp@debian.org>  Tue, 18 Oct 2005 17:12:35 +0200
