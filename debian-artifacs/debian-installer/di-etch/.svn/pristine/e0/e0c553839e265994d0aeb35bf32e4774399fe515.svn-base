<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45186 -->

 <sect1 id="what-is-linux">
 <title>GNU/Linux とは?</title>
<para>

<!--
Linux is an operating system: a series of programs that let you
interact with your computer and run other programs.
-->
Linux はオペレーティングシステム (あなたとコンピュータの間に立ち、
他のプログラムを実行させる一連のプログラム) です。
</para><para>

<!--
An operating system consists of various fundamental programs which are
needed by your computer so that it can communicate and receive
instructions from users; read and write data to hard disks, tapes, and
printers; control the use of memory; and run other software. The most
important part of an operating system is the kernel.  In a GNU/Linux
system, Linux is the kernel component.  The rest of the system
consists of other programs, many of which were written by or for the
GNU Project.  Because the Linux kernel alone does not form a working
operating system, we prefer to use the term <quote>GNU/Linux</quote>
to refer to systems that many people casually refer to as
<quote>Linux</quote>.
-->
オペレーティングシステムは、様々な基礎的なプログラムを含んでいます。
それらによって、コンピュータは、ユーザと交信したり指示を受け取ったり、
ハードディスクやテープ、プリンタにデータを読み書きしたり、
メモリの使い方を制御したり、他のソフトウェアを実行したりすることが
できます。オペレーティングシステムの最も重要な部分は、カーネルです。
GNU/Linux システムにおいては、Linux がカーネルです。システムの
残りの部分は、他のプログラムでできており、その大部分は GNU
プロジェクトによって書かれたものです。Linux カーネルだけでは
動作するオペレーティングシステムを構成できませんので、
多くの人が日常的に<quote>Linux</quote>と呼ぶシステムのことを、
私たちは<quote>GNU/Linux</quote>と呼ぶようにしています。

</para><para>

<!--
Linux is modelled on the Unix operating system. From the start, Linux
was designed to be a multi-tasking, multi-user system. These facts are
enough to make Linux different from other well-known operating
systems.  However, Linux is even more different than you might
imagine. In contrast to other operating systems, nobody owns
Linux. Much of its development is done by unpaid volunteers.
-->
Linux は Unix オペレーティングシステムを手本にしています。
当初から、Linux は マルチタスク、マルチユーザシステムとして設計されました。
この事実により、Linux は他の有名なオペレーティングシステムに対し、
充分差別化できています。
しかし、Linux はあなたが想像するよりもさらに異なっています。
他のオペレーティングシステムとは対照的に、誰も Linux を所有しません。
その開発の多くは無償のボランティアによって行われます。

</para><para>

<!--
Development of what later became GNU/Linux began in 1984, when the
<ulink url="&url-fsf;">Free Software Foundation</ulink>
began development of a free unix -like operating system called GNU.
-->
後に GNU/Linux になるものの開発は 1984 年、
<ulink url="&url-fsf;">フリーソフトウェア財団</ulink>
が GNU という Unix ライクなオペレーティングシステムの開発を始めたときに
始まりました。

</para><para>

<!--
The <ulink url="&url-gnu;">GNU Project</ulink> has developed a
comprehensive set of free software
tools for use with Unix&trade; and Unix-like operating systems such as
Linux.  These tools enable users to perform tasks ranging from the
mundane (such as copying or removing files from the system) to the
arcane (such as writing and compiling programs or doing sophisticated
editing in a variety of document formats).
-->
<ulink url="&url-gnu;">GNU プロジェクト</ulink>は、
Unix&trade; や、Linux などの Unix ライクな
オペレーティングシステムと共に使うための一連のフリーソフトウェア
ツールを開発してきました。これらのツールは、
ファイルのコピー・削除といった日常的な作業から、
プログラムの作成・コンパイルや様々なドキュメントフォーマットの
高度な編集といった作業までを可能にします。

</para><para>

<!--
While many groups and individuals have contributed to Linux, the
largest single contributor is still the Free Software Foundation,
which created not only most of the tools used in Linux, but also the
philosophy and the community that made Linux possible.
-->
多くのグループや個人が Linux に寄与する中で、
最大の単独貢献者はいまだに (Linux の中で使用されるほとんどのツールだけでなく
哲学も作成した) フリーソフトウェア財団と、
Linux を可能にしたコミュニティーです。

</para><para>

<!--
The <ulink url="&url-kernel-org;">Linux kernel</ulink> first
appeared in 1991, when a Finnish computing science student named Linus
Torvalds announced an early version of a replacement kernel for Minix
to the Usenet newsgroup <userinput>comp.os.minix</userinput>.  See
Linux International's
<ulink url="&url-linux-history;">Linux History Page</ulink>.
-->
<ulink url="&url-kernel-org;">Linux カーネル</ulink>は、
Linus Torvalds というフィンランド人の計算機科学の学生が
1991 年に、Usenet の <userinput>comp.os.minix</userinput> ニュースグループに
Minix の代替カーネルの初期バージョンを公表したのが始まりです。
Linux International の
<ulink url="&url-linux-history;">Linux 史のページ</ulink>
参照して下さい。

</para><para>

<!--
Linus Torvalds continues to coordinate the work of several hundred
developers with the help of a few trusty deputies.  An excellent
weekly summary of discussions on the
<userinput>linux-kernel</userinput> mailing list is
<ulink url="&url-kernel-traffic;">Kernel Traffic</ulink>.
More information about the <userinput>linux-kernel</userinput> mailing
list can be found on the
<ulink url="&url-linux-kernel-list-faq;">linux-kernel mailing list FAQ</ulink>.
-->
Linus Torvalds は、数人の信用できる協力者の助けを得て、
数百人の開発者の仕事を調整し続けています。
<userinput>linux-kernel</userinput> メーリングリストにおける議論の
すばらしい要約が、毎週
<ulink url="&url-kernel-traffic;">Kernel Traffic</ulink> で
読むことができます。<userinput>linux-kernel</userinput> メーリングリストの
より詳しい情報は、
<ulink url="&url-linux-kernel-list-faq;">linux-kernel メーリングリスト FAQ</ulink>
 で読むことができます。

</para><para>

<!--
Linux users have immense freedom of choice in their software. For
example, Linux users can choose from a dozen different command line
shells and several graphical desktops. This selection is often
bewildering to users of other operating systems, who are not used to
thinking of the command line or desktop as something that they can
change.
-->
Linux ユーザは、それらのソフトウェアの大きな選択の自由を持っています。
例えば、Linuxユーザは、1 ダースの異なるコマンドラインシェルや
数種のグラフィカルデスクトップの中から選ぶことができます。
この選択できるということが、しばしばコマンドラインやデスクトップを
変更できるという考えに慣れていない、
他のオペレーティングシステムのユーザを当惑させています。

</para><para>

<!--
Linux is also less likely to crash, better able to run more than one
program at the same time, and more secure than many operating
systems. With these advantages, Linux is the fastest growing operating
system in the server market. More recently, Linux has begun to be
popular among home and business users as well.
-->
Linux はまた、ほとんどクラッシュせず、
複数のプログラムを同時に実行するのに優秀で、
多くのオペレーティングシステムより安全です。
これらの利点により、Linux はサーバ市場で最も急成長している
オペレーティングシステムです。
さらに最近、Linuxは、ホーム・ビジネスユーザにも人気が出始めました。

</para>

 </sect1>

