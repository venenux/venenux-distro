<!-- retain these comments for translator revision tracking -->
<!-- original version: 43923 -->
<!-- revised by Marco Carvalho (macs) 2006.01.01 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.06.06 -->
<!-- updated 33886:43923 by Felipe Augusto van de Wiel (faw) 2007.01.20 -->

  <sect2 arch="powerpc" id="boot-cd"><title>Inicializando através de um CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

Atualmente as únicas sub-arquiteturas &arch-title; que suportam a
inicialização através de CD-ROM são a PReP (embora não sejam todos
os sistemas) e New World PowerMacs New World. Nos PowerMacs, pressione
a tecla <keycap>c</keycap> ou então a combinação de
<keycap>Command</keycap>, <keycap>Option</keycap>,
<keycap>Shift</keycap> e <keycap>Delete</keycap>
juntas enquanto inicializa através de um CD-ROM.

</para><para>

PowerMacs OldWorld não inicializarão usando o CD da Debian, porque
os computadores OldWorld confiam que o driver Mac OSR OM esteja presente no
CD e uma versão em software livre deste driver não está disponível.
Todos os sistemas OldWorld possuem unidades de disquetes, assim
use o disquete para iniciar o programa de instalação e
aponte o programa de instalação para o CD quando ele procurar por
arquivos necessários.

</para><para>

Caso seu sistema não inicialize diretamente a partir de um CD-ROM, você
ainda poderá usar o CD-ROM para instalar o sistema. Em NewWorlds, você
também poderá usar um comando da OpenFirmware para inicializar manualmente
através de uma unidade de CD-ROM. Siga as instruções em
<xref linkend="boot-newworld"/> para inicializar através de um disco
rígido, exceto use o caminho para o <command>yaboot</command> no CD
no aviso OF, tal como

<informalexample><screen>
0 &gt; boot cd:,\install\yaboot
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="powerpc" id="install-drive">
  <title>Inicializando a partir do Disco Rígido</title>

&boot-installer-intro-hd.xml;

  <sect3><title>Inicializando o CHRP através do OpenFirmware</title>

<para>

  <emphasis>Ainda não escrito.</emphasis>

</para>
   </sect3>

   <sect3><title>Inicializando PowerMacs OldWorld através do MacOS</title>
<para>

Caso tenha configurado o BootX em <xref linkend="files-oldworld"/>,
você poderá usa-lo para iniciar o sistema de instalação. De um clique
duplo no ícone da aplicação <guiicon>BootX</guiicon>. Clique no botão
<guibutton>Options</guibutton> e selecione <guilabel>Use
Specified RAM Disk</guilabel>. Isto lhe dará a chance de selecionar
o arquivo <filename>ramdisk.image.gz</filename>. Talvez você precise
selecionar a caixa de checagem <guilabel>No Video Driver</guilabel>,
dependendo do seu hardware. Então clique no botão
<guibutton>Linux</guibutton> para fechar o MacOS e carregar o programa
de instalação.

</para>
   </sect3>


  <sect3 id="boot-newworld">
  <title>Inicializando Macs NewWorld através do OpenFirmware</title>
<para>

É necessário que já tenha substituído os arquivos <filename>vmlinux</filename>,
<filename>initrd.gz</filename>, <filename>yaboot</filename> e
<filename>yaboot.conf</filename> no diretório raiz da partição HFS como
descrito em <xref linkend="files-newworld"/>.
Reinicie o computador e imediatamente (durante o beep) pressione juntas
as teclas <keycap>Option</keycap>, <keycap>Command (cloverleaf/Apple)</keycap>,
<keycap>o</keycap> e <keycap>f</keycap>. Após alguns segundos
você verá o aviso de comando do Open Firmware. Então digite:

<informalexample><screen>
0 &gt; boot hd:<replaceable>x</replaceable>,yaboot
</screen></informalexample>

trocando <replaceable>x</replaceable> pelo número da partição da
partição HFS onde os arquivos kernel e o yaboot estão localizados, seguidos
de  &enterkey;. Em algumas máquinas, você poderá usar
<userinput>ide0:</userinput> ao invés de <userinput>hd:</userinput>.
Após mais alguns segundos você verá o aviso de comando do yaboot

<informalexample><screen>
boot:
</screen></informalexample>

No aviso de comando <prompt>boot:</prompt> do yaboot, você poderá digitar
ou <userinput>install</userinput> ou <userinput>install video=ofonly</userinput>
seguido de &enterkey;. O argumento <userinput>video=ofonly</userinput>
mantém o máximo de compatibilidade; você poderá tenta-lo caso
<userinput>install</userinput> não funcione. O programa de instalação
da Debian também deverá iniciar.

</para>
   </sect3>
  </sect2>

  <sect2 arch="powerpc" condition="bootable-usb" id="usb-boot">
  <title>Inicialização através da memória stick USB</title>
<para>

Atualmente, sistemas PowerMac NewWorld possuem suporte
a inicialização através de USB.

</para>

<para>

Tenha certeza de ter preparado tudo de acordo com o passo <xref
linkend="boot-usb-files"/>. Para inicializar em um sistema
Macintosh através de uma memória stick USB, você precisará usar o
aviso do Open Firmware, pois o Open Firmware não pesquisa em dispositivos
USB por padrão.
<!-- TODO: embora isto possa ser feito; observe este espaço -->
Para obter o aviso de comandos, pressione a tecla
<keycombo><keycap>Command</keycap> <keycap>Option</keycap>
<keycap>o</keycap> <keycap>f</keycap></keycombo> juntas
durante a inicialização (veja <xref linkend="invoking-openfirmware"/>).

</para><para>

Você precisará alterar o local onde o dispositivo USB aparece na árvore
de dispositivos, pois no momento o <command>ofpath</command> não pode
trabalhar automaticamente. Digite <userinput>dev / ls</userinput> e
<userinput>devalias</userinput> no aviso de comandos do Open Firmware
para obter uma lista de todos os dispositivos conhecidos
e apelidos de cada um. No sistema do autor com vários tipos de
memórias stick USB, caminhos tais como
<filename>usb0/disk</filename>, <filename>usb0/hub/disk</filename>,
<filename>/pci@f2000000/usb@1b,1/disk@1</filename> e
<filename>/pci@f2000000/usb@1b,1/hub@1/disk@1</filename> funcionam.

</para><para>

Tendo funcionado o caminho de dispositivo, use um comando como este
para iniciar o programa de instalação:

<informalexample><screen>
boot <replaceable>usb0/disk</replaceable>:<replaceable>2</replaceable>,\\:tbxi
</screen></informalexample>

O <replaceable>2</replaceable> confere com a partição Apple_HFS ou
Apple_Bootstrap partition em que copiou a imagem de inicialização
anteriormente e a parte <userinput>,\\:tbxi</userinput> instrui o
Open Firmware para inicializar através do arquivo com um tipo de
sistemas de arquivos HFS"tbxi" (i.e.
<command>yaboot</command>) no diretório anteriormente abençoado com o
<command>hattrib -b</command>.

</para><para>

O sistema deverá inicializar agora, e lhe presenteará com o
aviso <prompt>boot:</prompt> prompt. Aqui você poderá entrar com
parâmetros iniciais de partida ou simplesmente pressionar &enterkey;.

</para><warning><para>

Este método de inicialização é novo e pode ser difícil para faze-lo
funcionar em sistemas NewWorld. Se tiver problemas, por favor envie um
relatório de instalação como explicado em <xref linkend="submit-bug"/>.

</para></warning>
  </sect2>

  <sect2 arch="powerpc" id="boot-tftp"><title>Inicializando com o TFTP</title>

&boot-installer-intro-net.xml;

<para>

Atualmente, sistemas PReP e PowerMac New World suportam a inicialização
através da rede.

</para><para>

Em máquinas com a Open Firmware, tal como Power Macs NewWorld, entre
no monitor de inicialização (veja <xref linkend="invoking-openfirmware"/>) e
use o comando <command>boot enet:0</command>.  Máquinas PReP e CHRP também
podem ter métodos diferentes de endereçar a rede. Em uma máquina PReP,
você deverá tentar
<userinput>boot net:<replaceable>server_ipaddr</replaceable>,<replaceable>file</replaceable>,<replaceable>client_ipaddr</replaceable></userinput>.
Em alguns sistemas PReP (e.g. máquinas Motorola PowerStack) o comando
<userinput>help boot</userinput> pode dar uma descrição da sintaxe e
das opções disponíveis.

</para>
  </sect2>


  <sect2 arch="powerpc" condition="supports-floppy-boot">
  <title>Inicializando através de Disquetes</title>
<para>

A inicialização através de disquetes é suportado para  &arch-title;,
no entanto ela somente é aplicável para sistemas OldWorld. Sistema
NewWorld não são equipados com unidades de disquetes e a
inicialização através de controladores de disquetes USB conectados na máquina
não é suportada.

</para><para>

Você terá que ter baixado as imagens de disquetes que precisa e
criar os disquetes a partir destas imagens como descrito em
<xref linkend="create-floppy"/>.

</para><para>

Para inicializar através do disquete <filename>boot-floppy-hfs.img</filename>,
coloque-o na unidade de disquete após desligar o computador e antes
de pressionar o botão power-on.

</para><note><para>
Estes que não estão familiarizados com as operações de disquetes no Macintosh:
um disquete colocado na máquina antes da inicialização será o primeiro
dispositivo de prioridade em que o sistema iniciará. Um disquete
sem um sistema válido de inicialização será ejetado e a máquina
então procurará por partições de disco inicializáveis.

</para></note><para>

Após a inicialização, o disquete <filename>root.bin</filename> será
pedido. Insira o disquete e pressione &enterkey;. O programa de
instalação será automaticamente carregado após o sistema de arquivos
raiz ser carregado para a memória.

</para>
  </sect2>


  <sect2 arch="powerpc"><title>Parâmetros de Inicialização no PowerPC</title>
<para>

Muitos monitores antigos Apple usavam o modo 640x480 67Hz. Caso seu
vídeo apareça distorcido em um monitor antigo da Apple, tente adicionar
a linha de inicialização o argumento
<userinput>video=atyfb:vmode:6</userinput>, que irá selecionar aquele modo
para a maioria dos hardwares de vídeo Mach64 e Rage. Para o hardware
Rage 128, isto muda para <userinput>video=aty128fb:vmode:6</userinput>.

</para>
  </sect2>
