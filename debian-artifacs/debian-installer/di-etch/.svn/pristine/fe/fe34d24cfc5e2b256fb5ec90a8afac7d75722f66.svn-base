Template: aboot-installer/apt-install-failed
Type: boolean
Default: true
_Description: Aboot installation failed.  Continue anyway?
 The aboot package failed to install into /target/.  Installing aboot
 as a boot loader is a required step.  The install problem might however be
 unrelated to aboot, so continuing the installation may be possible.

Template: aboot-installer/bootdev
Type: select
Choices: ${PARTITIONS}
_Description: Device for boot loader installation:
 Aboot needs to install the boot loader on a bootable device containing
 an ext2 partition.  Please select the ext2 partition that you wish
 aboot to use.  If this is not the root file system, your kernel image
 and the configuration file /etc/aboot.conf will be copied to that
 partition.

Template: aboot-installer/non-bsd-partitions
Type: boolean
Default: false
_Description: Install /boot on a disk with an unsupported partition table?
 To be bootable from the SRM console, aboot and the kernel it loads must
 be installed on a disk which uses BSD disklabels for its partition
 table.  Your /boot directory is not located on such a disk.  If you
 proceed, you will not be able to boot your system using aboot, and will
 need to boot it some other way.

Template: aboot-installer/non-ext2-boot
Type: boolean
Default: false
_Description: Use unsupported file system type for /boot?
 Aboot requires /boot to be located on an ext2 partition on a bootable
 device.  This means that either the root partition must be an ext2
 file system, or you must have a separate ext2 partition mounted at
 /boot.
 .
 Currently, /boot is located on a partition of type ${PARTTYPE}.  If you
 keep this setting, you will not be able to boot your Debian system
 using aboot.

Template: aboot-installer/no-space-for-aboot
Type: boolean
Default: false
_Description: Install without aboot?
 Your /boot directory is located on a disk that has no space for the
 aboot boot loader.  To install aboot, you must have a special aboot
 partition at the beginning of your disk.
 .
 If you continue without correcting this problem, aboot will not be
 installed and you will not be able to boot your new system from the SRM
 console.

Template: debian-installer/aboot-installer/title
Type: text
#  Main menu item
_Description: Install aboot on a hard disk

Template: aboot-installer/no-partitions
Type: error
_Description: No ext2 partitions found
 No ext2 file systems were found on your computer.  To load the Linux
 kernel from the SRM console, aboot needs an ext2 partition on a disk
 that has been partitioned with BSD disklabels.  You will need to
 configure at least one ext2 partition on your system before continuing.

Template: aboot-installer/progress/title
Type: text
_Description: Installing the aboot boot loader

Template: aboot-installer/progress/step_install
Type: text
_Description: Installing the 'aboot' package...

Template: aboot-installer/progress/step_bootdev
Type: text
_Description: Determining aboot boot device...

Template: aboot-installer/progress/step_install_loader
Type: text
_Description: Installing aboot on ${BOOTDISK}...

Template: aboot-installer/progress/step_config_loader
Type: text
_Description: Configuring aboot to use partition ${PARTNUM}...

Template: aboot-installer/progress/step_copy_kernel
Type: text
_Description: Copying kernel images to ${BOOTDEV}...

Template: partman-aboot/text/method
Type: text
_Description: Aboot boot partition

Template: partman/method_long/aboot
Type: text
_Description: Aboot boot partition

Template: partman/method_short/aboot
Type: text
_Description: aboot
