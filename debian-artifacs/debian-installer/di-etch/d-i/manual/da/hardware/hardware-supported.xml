<!-- retain these comments for translator revision tracking -->
<!-- original version: 22237 untranslated -->

 <sect1 id="hardware-supported">
 <title>Supported Hardware</title>

<para>

Debian does not impose hardware requirements beyond the requirements
of the Linux kernel and the GNU tool-sets.  Therefore, any
architecture or platform to which the Linux kernel, libc,
<command>gcc</command>, etc. have been ported, and for which a Debian
port exists, can run Debian. Please refer to the Ports pages at
<ulink url="&url-ports;"></ulink> for
more details on &arch-title; architecture systems which have been
tested with Debian.

</para><para>

Rather than attempting to describe all the different hardware
configurations which are supported for &arch-title;, this section
contains general information and pointers to where additional
information can be found.

</para>

  <sect2><title>Supported Architectures</title>

<para>

Debian &release; supports eleven major architectures and several
variations of each architecture known as 'flavors'. 

</para><para>

<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>Architecture</entry><entry>Debian Designation</entry>
  <entry>Subarchitecture</entry><entry>Flavor</entry>
</row>
</thead>

<tbody>
<row>
  <entry morerows="2">Intel x86-based</entry>
  <entry morerows="2">i386</entry>
  <entry morerows="2"></entry>
  <entry>vanilla</entry>
</row><row>
  <entry>speakup</entry>
</row><row>
  <entry>linux26</entry>
</row>

<row>
  <entry morerows="5">Motorola 680x0</entry>
  <entry morerows="5">m68k</entry>
  <entry>Atari</entry>
  <entry>atari</entry>
</row><row>
  <entry>Amiga</entry>
  <entry>amiga</entry>
</row><row>
  <entry>68k Macintosh</entry>
  <entry>mac</entry>
</row><row>
  <entry morerows="2">VME</entry>
  <entry>bvme6000</entry>
</row><row>
  <entry>mvme147</entry>
</row><row>
  <entry>mvme16x</entry>
</row>

<row>
  <entry morerows="2">DEC Alpha</entry>
  <entry morerows="2">alpha</entry>
  <entry morerows="2"></entry>
  <entry>generic</entry>
</row><row>
  <entry>jensen</entry>
</row><row>
  <entry>nautilus</entry>
</row>

<row>
  <entry morerows="1">Sun SPARC</entry>
  <entry morerows="1">sparc</entry>
  <entry morerows="1"></entry>
  <entry>sun4cdm</entry>
</row><row>
  <entry>sun4u</entry>
</row>

<row>
  <entry morerows="3">ARM and StrongARM</entry>
  <entry morerows="3">arm</entry>
  <entry morerows="3"></entry>
  <entry>netwinder</entry>
</row><row>
  <entry>riscpc</entry>
</row><row>
  <entry>shark</entry>
</row><row>
  <entry>lart</entry>
</row>

<row>
  <entry morerows="3">IBM/Motorola PowerPC</entry>
  <entry morerows="3">powerpc</entry>
  <entry>CHRP</entry>
  <entry>chrp</entry>
</row><row>
  <entry>PowerMac</entry>
  <entry>powermac, new-powermac</entry>
</row><row>
  <entry>PReP</entry>
  <entry>prep</entry>
</row><row>
  <entry>APUS</entry>
  <entry>apus</entry>
</row>

<row>
  <entry morerows="1">HP PA-RISC</entry>
  <entry morerows="1">hppa</entry>
  <entry>PA-RISC 1.1</entry>
  <entry>32</entry>
</row><row>
  <entry>PA-RISC 2.0</entry>
  <entry>64</entry>
</row>

<row>
  <entry>Intel ia64-based</entry>
  <entry>ia64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry morerows="2">MIPS (big endian)</entry>
  <entry morerows="2">mips</entry>
  <entry morerows="1">SGI Indy/Indigo 2</entry>
  <entry>r4k-ip22</entry>
</row><row>
  <entry>r5k-ip22</entry>
</row><row>
  <entry>Broadcom BCM1250 (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row>

<row>
  <entry morerows="3">MIPS (little endian)</entry>
  <entry morerows="3">mipsel</entry>
  <entry>Cobalt</entry>
  <entry>cobalt</entry>
</row><row>
  <entry morerows="1">DECstation</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Broadcom BCM1250 (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row>

<row>
  <entry morerows="1">IBM S/390</entry>
  <entry morerows="1">s390</entry>
  <entry morerows="1"></entry>
  <entry>tape</entry>
</row><row>
  <entry>vmrdr</entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

This document covers installation for the
<emphasis>&arch-title;</emphasis> architecture.  If you are looking
for information on any of the other Debian-supported architectures
take a look at the
<ulink url="http://www.debian.org/ports/">Debian-Ports</ulink> pages.

</para><para condition="new-arch">

This is the first official release of &debian; for the &arch-title;
architecture.  We feel that it has proven itself sufficiently to be
released. However, because it has not had the exposure (and hence
testing by users) that some other architectures have had, you may
encounter a few bugs. Use our
<ulink url="&url-bts;">Bug Tracking System</ulink> to report any 
problems; make sure to mention the fact that the bug is on the
&arch-title; platform. It can be necessary to use the 
<ulink url="&url-list-subscribe;">debian-&architecture; mailing list</ulink>
as well.
  
</para>

  </sect2>

<!-- supported cpu docs -->
&supported-alpha.xml;
&supported-arm.xml;
&supported-hppa.xml;
&supported-i386.xml;
&supported-ia64.xml;  <!-- FIXME: currently missing -->
&supported-m68k.xml;
&supported-mips.xml;
&supported-mipsel.xml;
&supported-powerpc.xml;
&supported-s390.xml;
&supported-sparc.xml;

  <sect2 id="gfx" arch="not-s390"><title>Graphics Card</title>

<para arch="x86">

You should be using a VGA-compatible display interface for the console
terminal. Nearly every modern display card is compatible with
VGA. Ancient standards such CGA, MDA, or HGA should also work,
assuming you do not require X11 support.  Note that X11 is not used
during the installation process described in this document.

</para><para>

Debian's support for graphical interfaces is determined by the
underlying support found in XFree86's X11 system.  The newer AGP video
slots are actually a modification on the PCI specification, and most
AGP video cards work under XFree86.  Details on supported graphics
buses, cards, monitors, and pointing devices can be found at 
<ulink url="&url-xfree86;"></ulink>.  Debian &release; ships 
with XFree86 version &x11ver;.

</para><para arch="mips">

The XFree86 X11 window system is only supported on the SGI Indy.  The
Broadcom BCM1250 evaluation board has standard 3.3v PCI slots and supports
VGA emulation or Linux framebuffer on a selected range of graphics cards.
A <ulink url="&url-bcm91250a-hardware;">compatibility listing</ulink> for
the BCM1250 is available.

</para><para arch="mipsel">

The XFree86 X11 window system is supported on some DECstation models.  The
Broadcom BCM1250 evaluation board has standard 3.3v PCI slots and supports
VGA emulation or Linux framebuffer on a selected range of graphics cards.
A <ulink url="&url-bcm91250a-hardware;">compatibility listing</ulink> for
the BCM1250 is available.

</para>

  </sect2>

  <sect2 arch="x86" id="laptops"><title>Laptops</title>
<para>

Laptops are also supported.  Laptops are often specialized or contain
proprietary hardware.  To see if your particular laptop works well
with GNU/Linux, see the 
<ulink url="&url-x86-laptop;">Linux Laptop pages</ulink>

</para>
   </sect2>


  <sect2 condition="defaults-smp">
<title>Multiple Processors</title>

<para>

Multi-processor support &mdash; also called ``symmetric multi-processing''
or SMP &mdash; is supported for this architecture.  The standard Debian
&release; kernel image was compiled with SMP support.  This should not
prevent installation, since the SMP kernel should boot on non-SMP systems;
the kernel will simply cause a bit more overhead.

</para><para>

In order to optimize the kernel for single CPU systems, you'll have to
replace the standard Debian kernel.  You can find a discussion of how
to do this in <xref linkend="kernel-baking"/>.  At this time
(kernel version &kernelversion;) the way you disable SMP is to deselect
``symmetric multi-processing'' in the ``General'' section of the
kernel config.

</para>

  </sect2>


  <sect2 condition="supports-smp">
 <title>Multiple Processors</title>
<para>

Multi-processor support &mdash; also called ``symmetric
multi-processing'' or SMP &mdash; is supported for this architecture.
However, the standard Debian &release; kernel image does not support
SMP.  This should not prevent installation, since the standard,
non-SMP kernel should boot on SMP systems; the kernel will simply use
the first CPU.

</para><para>

In order to take advantage of multiple processors, you'll have to
replace the standard Debian kernel.  You can find a discussion of how
to do this in <xref linkend="kernel-baking"/>.  At this time
(kernel version &kernelversion;) the way you enable SMP is to select
``symmetric multi-processing'' in the ``General'' section of the
kernel config.

</para>
  </sect2>
  
  <sect2 condition="supports-smp-sometimes">
 <title>Multiple Processors</title>
<para>

Multi-processor support &mdash; also called ``symmetric
multi-processing'' or SMP &mdash; is supported for this architecture,
and is supported by a precompiled Debian kernel image. Depending on your
install media, this SMP-capable kernel may or may not be installed by
default. This should not prevent installation, since the standard,
non-SMP kernel should boot on SMP systems; the kernel will simply use
the first CPU.

</para><para>

In order to take advantage of multiple processors, you should check to see
if a kernel package that supports SMP is installed, and if not, choose an
appropriate kernel package.

You can also build your own customised kernel to support SMP. You can find
a discussion of how to do this in <xref linkend="kernel-baking"/>.  At this
time (kernel version &kernelversion;) the way you enable SMP is to select
``symmetric multi-processing'' in the ``General'' section of the
kernel config.

</para>
  </sect2>
 </sect1>
