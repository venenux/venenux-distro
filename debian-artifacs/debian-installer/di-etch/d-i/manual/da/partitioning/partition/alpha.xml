<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 untranslated -->


  <sect2 arch="alpha"><title>Partitioning for &arch-title;</title>
<para>

If you have chosen to boot from the SRM console, you must use
<command>fdisk</command> to partition your disk, as it is the only
partitioning program that can manipulate the BSD disk labels required
by <command>aboot</command> (remember, the SRM boot block is
incompatible with MS-DOS partition tables - see 
<xref linkend="alpha-firmware"/>).
<command>debian-installer</command> will run <command>fdisk</command>
by default if you have not booted from <command>MILO</command>.

</para><para>

If the disk that you have selected for partitioning already contains a
BSD disk label, <command>fdisk</command> will default to BSD disk
label mode.  Otherwise, you must use the `b' command to enter disk
label mode.

</para><para>

Unless you wish to use the disk you are partitioning from Tru64 Unix
or one of the free 4.4BSD-Lite derived operating systems (FreeBSD,
OpenBSD, or NetBSD), it is suggested that you do
<emphasis>not</emphasis> make the third partition contain the whole
disk.  This is not required by <command>aboot</command>, and in fact,
it may lead to confusion since the <command>swriteboot</command>
utility used to install <command>aboot</command> in the boot sector
will complain about a partition overlapping with the boot block.

</para><para>

Also, because <command>aboot</command> is written to the first few
sectors of the disk (currently it occupies about 70 kilobytes, or 150
sectors), you <emphasis>must</emphasis> leave enough empty space at
the beginning of the disk for it.  In the past, it was suggested that
you make a small partition at the beginning of the disk, to be left
unformatted.  For the same reason mentioned above, we now suggest that
you do not do this on disks that will only be used by GNU/Linux.

</para><para>

For ARC installations, you should make a small FAT partition at the
beginning of the disk to contain <command>MILO</command> and
<command>linload.exe</command> - 5 megabytes should be sufficient, see
<xref linkend="non-debian-partitioning"/>. Unfortunately, making FAT
file systems from the menu is not yet supported, so you'll have to do
it manually from the shell using <command>mkdosfs</command> before
attempting to install the boot loader.

</para>
  </sect2>