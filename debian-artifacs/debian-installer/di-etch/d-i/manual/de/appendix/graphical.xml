<!-- retain these comments for translator revision tracking -->
<!-- original version: 44580 -->

 <sect1 condition="gtk" id="graphical">
 <title>Der Grafische Installer</title>
<para>

Die grafische Version des Installers ist nur für eine begrenzte Anzahl von
Architekturen verfügbar, unter anderem für &arch-title;. Die Funktionalität
des grafischen Installers ist grundsätzlich die gleiche wie die des normalen
Installers, da er die gleichen Programme verwendet, nur mit einem anderen
Frontend (Bedienoberfläche).

</para><para>

Obwohl die Funktionalität identisch ist, hat der grafische Installer trotzdem
einige bedeutende Vorteile. Der hauptsächliche Vorteil ist, dass mehr Sprachen
unterstützt werden, nämlich solche, die einen Zeichensatz verwenden, der mit
dem normalen <quote>newt</quote>-Frontend nicht dargestellt werden kann.
Außerdem gibt es einige weitere Vorteile bezüglich der Bedienung, zum Beispiel
die Nutzung einer Maus sowie die Möglichkeit, in einigen Fällen mehrere Fragen
in einem Bild darstellen zu können.

</para><para arch="x86">

Der grafische Installer ist auf allen CD-Images und sowie bei der
hd-media-Installationsmethode verfügbar. Da er eine separate initrd nutzt
(die auch erheblich größer ist als beim normalen Installer), muss er mit
<userinput>installgui</userinput> gebootet werden statt mit
<userinput>install</userinput>. Analog dazu werden der Expert- und der
Rettungs-Modus mit <userinput>expertgui</userinput> beziehungsweise
<userinput>rescuegui</userinput> gebootet.

</para><para arch="x86">

Außerdem ist er auf einem speziellen <quote>mini</quote> ISO-Image
erhältlich<footnote id="gtk-miniiso">

<para>
Das mini-ISO-Image kann von einem Debian-Spiegelserver heruntergeladen werden
(wie in <xref linkend="downloading-files"/> beschrieben). Suchen Sie nach
<quote>gtk-miniiso</quote>.
</para>

</footnote>, das hauptsächlich für Testzwecke verwendet wird; bei diesem Image
wird einfach mit <userinput>install</userinput> gebootet. Es gibt kein Image
mit grafischem Installer, dass per netboot gestartet werden kann.

</para><para arch="powerpc">

Für &arch-title; existiert zur Zeit lediglich ein experimentelles
<quote>mini</quote> ISO-Image<footnote id="gtk-miniiso">

<para>
Das mini-ISO-Image kann von einem Debian-Spiegelserver heruntergeladen werden
(wie in <xref linkend="downloading-files"/> beschrieben). Suchen Sie nach
<quote>gtk-miniiso</quote>.
</para>

</footnote>. Es sollte auf beinahe allen PowerPC-Systemen mit ATI-Grafikkarte
funktionieren, wird aber wahrscheinlich auf anderen Systemen nicht laufen.

</para><para>

Der grafische Installer benötigt erheblich mehr Arbeitsspeicher als der
normale Installer, und zwar &minimum-memory-gtk;. Falls nicht genügend
Speicher verfügbar ist, wird automatisch das normale
<quote>newt</quote>-Frontend gebootet.

</para><para>

Sie können zusätzliche Boot-Parameter angeben, wenn Sie den grafischen
Installer starten, genauso wie beim normalen Installer. Ein solcher Parameter
erlaubt es zum Beispiel, die Maus für Linkshänder zu konfigurieren.
<xref linkend="boot-parms"/> gibt Infos über gültige Parameter.

</para>

  <sect2 id="gtk-using">
  <title>Den grafischen Installer verwenden</title>
<para>

Wie bereits erwähnt, funktioniert der grafische Installer genauso wie der
normale Installer und deshalb kann Sie der Rest dieses Handbuchs auch durch
den weiteren Installationsprozess leiten.

</para><para>

Wenn Sie es vorziehen, statt der Maus die Tastatur zu verwenden, gibt es zwei
Dinge, die Sie wissen sollten. Um eine ausklappbare Liste (wie z.B. die zur
Auswahl der Länder sortiert nach Kontinenten) ein- oder auszuklappen, können
Sie die Tasten <keycap>+</keycap> und <keycap>-</keycap> benutzen. Bei Fragen,
bei denen mehr als eine Option ausgewählt werden kann (z.B. bei der Auswahl der
Programmgruppen), müssen Sie (wenn Sie Ihre Auswahl getroffen haben) zunächst
mit der <keycap>Tab</keycap>-Taste den Fokus zum
<guibutton>Weiter</guibutton>-Knopf wechseln, bevor Sie Enter drücken; einfach
nur Enter zu drücken würde die gerade aktive Markierung umschalten, aber
nicht den <guibutton>Weiter</guibutton>-Knopf aktivieren.

</para><para>

Um auf eine andere Konsole umzuschalten, müssen Sie zusätzlich die
<keycap>Strg</keycap>-Taste benutzen, wie beim X-Window-System. Um zum Beispiel
auf VT1 umzuschalten, benutzen Sie: <keycombo> <keycap>Strg</keycap>
<keycap>Alt</keycap> <keycap>F1</keycap> </keycombo>.

</para>
  </sect2>

  <sect2 id="gtk-issues">
  <title>Bekannte Probleme</title>
<para>

ETCH ist die erste &debian;-Veröffentlichung, die den grafischen Installer
und damit eine relativ neue Technologie nutzt. Es gibt einige bekannte
Probleme, auf die Sie während der Installation stoßen könnten. Wir gehen davon
aus, diese bis zur nächsten &debian;-Version lösen zu können.

</para>

<itemizedlist>
<listitem><para>

Auf einigen Seiten sind die Texte nicht schön in Spalten formatiert, wie es
eigentlich sein sollte. Das augenscheinlichste Beispiel hierfür ist der erste
Bildschirm, auf dem Sie Ihre Sprache auswählen. Ein anderes ist der
Hauptbildschirm von partman (dem Partitionierungsprogramm).

</para></listitem>
<listitem><para>

In einigen Fällen könnte die Eingabe von bestimmten Zeichen nicht funktionieren
oder die falschen Zeichen werden angezeigt. Zum Beispiel das 
<quote>Zusammensetzen</quote> eines Zeichens aus einem Akzent und dem
Buchstaben, unter bzw. über dem der Akzent erscheinen soll, funktioniert nicht.

</para></listitem>
<listitem><para>

Die Unterstützung für Touchpads auf Laptops ist noch nicht optimal.

</para></listitem>
<listitem><para>

Sie sollten nicht auf eine andere Konsole umschalten, während der Installer
beschäftigt ist; dies könnte dazu führen, dass das Frontend abstürzt. Es wird
zwar automatisch neu gestartet, aber dies könnte trotzdem zu Problemen bei
der Installation führen. Das Umschalten auf eine andere Konsole, während der
Installer auf eine Eingabe wartet, sollte aber ohne Probleme funktionieren.

</para></listitem>
<listitem><para>

Die Unterstützung für verschlüsselte Partitionen ist eingeschränkt, da es
unmöglich ist, zufällige Schlüssel zu erzeugen. Es ist jedoch möglich, eine
verschlüsselte Partition unter Verwendung einer Passphrase als Schlüssel
einzurichten.

</para></listitem>

<listitem><para>

Im grafischen Frontend ist es derzeit nicht möglich, eine Shell zu starten.
Das bedeutet, dass der entsprechende Eintrag des Hauptmenüs im
Installationssystem und im Rettungsmodus nicht angezeigt wird (während er
verfügbar ist, wenn Sie das normale textbasierte Frontend benutzen).
Stattdessen müssen Sie (wie bereits vorher beschrieben) auf die
virtuellen Konsolen VT2 und VT3 wechseln und dort die Shell benutzen.

</para><para>

Wenn Sie den Installer im Rettungsmodus gestartet haben, könnte es sinnvoll
sein, eine Shell in der root-Partition des installierten Systems zu starten.
Dies ist möglich, indem Sie (nachdem Sie die Partition ausgewählt haben, so
dass sie als root-Partition eingehängt wird) auf VT2 oder VT3 wechseln und
den folgenden Befehl eingeben:

<informalexample><screen>
# chroot /target
</screen></informalexample>

</para></listitem>
</itemizedlist>

  </sect2>
 </sect1>
