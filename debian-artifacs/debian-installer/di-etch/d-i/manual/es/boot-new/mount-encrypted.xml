<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 41817 -->
<!-- movido por jfs, enero 2007 -->
<!-- revisado por Igor Tamara, enero 2007 -->

 <sect1 id="mount-encrypted-volumes">
 <title>Montaje de vol�menes cifrados</title>

<para>

Se le solicitar� la contrase�a para cada uno de los vol�menes cifrados
durante el arranque si ha creado vol�menes cifrados durante la
instalaci�n y los ha asociado a puntos de montaje. El procedimiento
difiere ligeramente en funci�n de si se utiliza �dm-crypt� o
�loop-AES�.

</para>

  <sect2 id="mount-dm-crypt">
  <title>dm-crypt</title>

<para>

Se mostrar� la siguiente indicaci�n durante el arranque para las
particiones que est�n cifradas con �dm-crypt�:

<informalexample><screen>
Starting early crypto disks... <replaceable>part</replaceable>_crypt(starting)
Enter LUKS passphrase:
</screen></informalexample>

En la primera l�nea del indicador, <replaceable>part</replaceable> es
el nombre de la partici�n subyacente, p.ej. sda2 o md0.  La pregunta
que puede hacerse es �<emphasis>para qu� volumen</emphasis> est�
introduciendo la contrase�a? �Se trata de <filename>/home</filename> o
de <filename>/var</filename>?  Por supuesto, si tiene solamente un
volumen cifrado es muy sencillo y s�lo tendr� que introducir la clave
que utiliz� cuando defin�a esta volumen. Las notas que escribi� tras
el �ltimo paso en <xref linkend="partman-crypto"/> le ser�n ahora de
utilidad si configur� m�s de un volumen cifrado durante la
instalaci�n. Si no tomo nota de la relaci�n entre
<filename><replaceable>part</replaceable>_crypt</filename> y los
puntos de montaje anteriormente a�n podr� encontrarla en los ficheros
<filename>/etc/crypttab</filename> y <filename>/etc/fstab</filename>
de su nuevo sistema

</para><para>

El indicador puede ser un poco distinto cuando lo que se monta es el
sistema de ficheros ra�z. El mensaje exacto depender� del generador de
initramfs que se utiliz� para generar el initrd utilizado para el
arranque del sistema. El ejemplo que se muestra a continuaci�n
corresponde al mensaje del initrd generado con
<classname>initramfs-tools</classname>:

<informalexample><screen>
Begin: Mounting <emphasis>root file system</emphasis>... ...
Begin: Running /scripts/local-top ...
Enter LUKS passphrase:
</screen></informalexample>

</para><para>

No se mostrar� ning�n car�cter (ni siquiera asteriscos) mientras vd.
introduce la clave. Si introduce mal la clave tendr� dos intentos m�s
para corregirla. Despu�s del tercer intento err�neo el proceso de
arranque saltar� ese volumen y continuar� intentando montar el
siguiente sistemas de ficheros.  Para m�s informaci�n consulte <xref
linkend="crypto-troubleshooting"/>.

</para><para>


El proceso de arranque deber�a continuar normalmente una vez haya introducido
todas las claves.

</para>
  </sect2>

  <sect2 id="mount-loop-aes">
  <title>loop-AES</title>
<para>

Se le mostrar� el siguiente indicador durante el arranque en el caso
de que tenga particiones cifradas con �loop-AES�:

<informalexample><screen>
Checking loop-encrypted file systems.
Setting up /dev/loop<replaceable>X</replaceable> (/<replaceable>mountpoint</replaceable>)
Password:
</screen></informalexample>

No se mostrar� ning�n car�cter (ni siquiera asteriscos) mientras vd.
introduce la clave. Si introduce mal la clave tendr� dos intentos m�s
para corregirla. Despu�s del tercer intento err�neo el proceso de
arranque saltar� ese volumen y continuar� intentando montar el
siguiente sistemas de ficheros.  Para m�s informaci�n consulte <xref
linkend="crypto-troubleshooting"/>.

</para><para>

El proceso de arranque deber�a continuar normalmente una vez haya introducido
todas las claves.

</para>
  </sect2>

  <sect2 id="crypto-troubleshooting">
  <title>Solucionar problemas</title>

<para>

Tendr� que montar manualmente los vol�menes cifrados si no se pudieron montar
porque no introdujo bien la clave. Aqu� se dan ciertos casos distintos:

</para>

<itemizedlist>
<listitem><para>

El primer caso est� asociado a la partici�n ra�z. El proceso de arranque no
podr� continuar y se parar� si no se monta �sta correctamente, con lo que
tendr� que reiniciar el equipo e intentarlo de nuevo.

</para></listitem>
<listitem><para>

El caso m�s sencillo se da en los vol�menes cifrados que guardan datos
como pueda ser el caso de 
<filename>/home</filename> o <filename>/srv</filename>. Simplemente
puede intentar montarlo de nuevo tras el arranque. En el caso de
�loop-AES� se hace con una operaci�n de un solo paso:

<informalexample><screen>
<prompt>#</prompt> <userinput>mount <replaceable>/punto_de_montaje</replaceable></userinput>
<prompt>Password:</prompt>
</screen></informalexample>

donde deber�a reemplazar <replaceable>/punto_de_montaje</replaceable> por el
directorio correspondiente (p.ej. <filename>/home</filename>).  La �nica
diferencia con el montaje normal de sistemas de ficheros es que se le
preguntar� la contrase�a para este volumen.

</para><para>

Es un poco m�s complicado para el caso de �dm-crypt�. Primero tendr� 
que registrar los vol�menes con el <application>device mapper</application>
ejecutando:

<informalexample><screen>
<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>
</screen></informalexample>

Esto har� que se sondeen todos los vol�menes descritos
en <filename>/etc/crypttab</filename> y se crear�n todos los dispositivos
necesarios en el directorio <filename>/dev</filename> tras introducir
la contrase�a correctamente. Se omitir�n los vol�menes que ya est�n
registrados por lo que puede repetir esta orden tantas veces como
necesite. Una vez que haya registrado con �xito el dispositivo s�lo
tiene que montarlos de la forma habitual:

<informalexample><screen>
<prompt>#</prompt> <userinput>mount <replaceable>/punto_de_montaje</replaceable></userinput>
</screen></informalexample>

</para></listitem>
<listitem><para>

El sistema deber�a arrancar a�n cuando no se puedan montar los
sistemas de ficheros que no contengan ficheros del sistema cr�ticos
(<filename>/usr</filename> o <filename>/var</filename>). Por lo que deber�a
poder montar los vol�menes manualmente como se ha descrito anteriormente. Sin
embargo, tendr� que arrancar o reiniciar los servicios que se ejecutan en su
nivel de ejecuci�n normal porque es muy probable que no se hayan podido
arrancar. La forma m�s f�cil de conseguir esto es cambiando al primer nivel de
ejecuci�n y volver al nivel actual introduciendo lo siguiente:

<informalexample><screen>
<prompt>#</prompt> <userinput>init 1</userinput>
</screen></informalexample>

y en el indicador del int�rprete de �rdenes pulse
<keycombo> <keycap>Control</keycap> <keycap>D</keycap> </keycombo>
cuando se le pregunte la contrase�a de root.

</para></listitem>
</itemizedlist>

  </sect2>
 </sect1>
