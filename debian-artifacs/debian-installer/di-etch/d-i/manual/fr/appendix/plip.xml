<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 39644 -->

 <sect1 id="plip" arch="x86">
 <title>Installer &debian; gr�ce au protocole PLIP (IP sur port parall�le)</title>

<para>

Cette section explique comment installer &debian; sur un ordinateur sans carte
Ethernet mais qui est reli� � une passerelle distante par un c�ble appel� Null-Modem
ou Null-Printer. La passerelle doit �tre connect�e � un r�seau qui poss�de un
miroir Debian (p. ex. internet).

</para><para>

Dans cet exemple, nous allons configurer une connexion PLIP � une passerelle
qui est connect�e � internet avec une connexion par t�l�phone (ppp0).
Nous utiliserons les adresses 192.168.0.1 et 192.168.0.2 pour les interfaces PLIP
du syst�me cible et du syst�me source. Ces adresses ne doivent pas �tre utilis�es par
ailleurs.

</para><para>

La connexion PLIP configur�e pendant l'installation sera disponible apr�s le
r�amor�age du syst�me install�, voyez <xref linkend="boot-new"/>.

</para><para>

Avant de commencer, vous devez v�rifier la configuration du BIOS pour les ports parall�le 
(adresses IO et IRQ) des deux syst�mes (cible et source). Les valeurs habituelles sont
<literal>io=0x378</literal>, <literal>irq=7</literal>.

</para>

  <sect2>
  <title>Pr�-requis</title>

<itemizedlist>
<listitem><para>

Une machine cible, appel�e <emphasis>target</emphasis>, o� Debian sera install�&nbsp;;

</para></listitem>
<listitem><para>

un support d'installation, voyez <xref linkend="installation-media"/>&nbsp;;

</para></listitem>
<listitem><para>

une autre machine connect�e � internet, appel�e <emphasis>source</emphasis>,
qui fonctionnera comme passerelle&nbsp;;

</para></listitem>
<listitem><para>

un c�ble Null-Modem DB-25, voyez le
<ulink url="&url-plip-install-howto;">PLIP-Install-HOWTO</ulink> pour des
informations sur ce c�ble et sur la mani�re d'en fabriquer un.

</para></listitem>
</itemizedlist>

  </sect2>

  <sect2>
  <title>Configuration du syst�me source</title>
<para>

Le script suivant montre comment configurer simplement la machine source en tant que
passerelle vers internet avec ppp0.

<informalexample><screen>
#!/bin/sh

# Suppression des modules du noyau pour �viter des conflits
# et reconfiguration.
modprobe -r lp parport_pc
modprobe parport_pc io=<replaceable>0x378</replaceable> irq=<replaceable>7</replaceable>
modprobe plip

# Configuration de l'interface plip (plip0 ici, voyez dmesg | grep plip)
ifconfig <replaceable>plip0 192.168.0.2</replaceable> pointopoint <replaceable>192.168.0.1</replaceable> netmask 255.255.255.255 up

# Configuration de la passerelle
modprobe iptable_nat
iptables -t nat -A POSTROUTING -o <replaceable>ppp0</replaceable> -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Installation du syst�me cible</title>
<para>

Amorcez le support d'installation. L'installation doit se faire en mode expert. Pour cela,
saisissez <userinput>expert</userinput> � l'invite d'amor�age.
Si vous avez besoin de donner des param�tres pour le noyau, vous devez le faire aussi
� ce moment. Par exemple, si vous voulez d�marrer l'installateur avec des valeurs
pour les options <quote>io</quote> et <quote>irq</quote> du module parport_pc,
saisissez&nbsp;:

<informalexample><screen>
expert parport_pc.io=<replaceable>0x378</replaceable> parport_pc.irq=<replaceable>7</replaceable>
</screen></informalexample>

Voici maintenant les r�ponses que vous devrez donner � diff�rents moments de l'installation.

</para>

<orderedlist>
<listitem><para>

<guimenuitem>Chargement des composants de l'installateur � partir d'un c�d�rom</guimenuitem>

</para><para>

S�lectionnez l'option <userinput>plip-modules</userinput> dans la liste. Le pilote PLIP sera
ainsi disponible pour le syst�me d'installation.

</para></listitem>
<listitem><para>

<guimenuitem>D�tection du mat�riel r�seau</guimenuitem>

</para>

 <itemizedlist>
 <listitem><para>

Si la machine cible poss�de une carte r�seau, une liste des modules pour la carte d�tect�e sera
affich�e. Si vous voulez que l'installateur utilise plip, vous devez d�s�lectionner tous les
modules affich�s. Il est �vident que si la machine ne poss�de pas de carte r�seau, cette liste 
ne sera pas affich�e.

 </para></listitem>
 <listitem><para>

Puisqu'aucune carte r�seau n'a �t� d�tect�e ou s�lectionn�e, l'installateur vous demande de
choisir un pilote r�seau dans une liste. Choisissez le module <userinput>plip</userinput>.

 </para></listitem>
 </itemizedlist>

</listitem>
<listitem><para>

<guimenuitem>Configuration du r�seau</guimenuitem>
 
 <itemizedlist>
 <listitem><para>

Configuration automatique du r�seau avec DHCP : No

 </para></listitem>
 <listitem><para>

Adresse IP&nbsp;: <userinput><replaceable>192.168.0.1</replaceable></userinput>

 </para></listitem>
 <listitem><para>

Adresse Point-to-Point&nbsp;:
<userinput><replaceable>192.168.0.2</replaceable></userinput>

 </para></listitem>
 <listitem><para>

Adresses des serveurs de noms&nbsp;: vous pouvez mettre les adresses utilis�es par le syst�me
source, voyez <filename>/etc/resolv.conf</filename>.

 </para></listitem>
 </itemizedlist>

</para></listitem>
</orderedlist>

  </sect2>
 </sect1>
