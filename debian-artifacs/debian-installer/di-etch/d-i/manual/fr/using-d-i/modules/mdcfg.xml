<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44026 -->

<sect3 id="mdcfg">
 <title>Configuration d'un p�riph�rique multidisque (RAID logiciel)</title>
<para>
Si vous avez plusieurs disques durs <footnote><para>

Pour �tre honn�te, on peut construire un p�riph�rique multidisque
m�me avec les partitions d'un seul disque, mais �a n'apporte pas grand chose.
</para></footnote>

sur votre machine, vous pouvez utiliser <command>mdcfg</command> pour
obtenir de vos disques de meilleures performances et une meilleure fiabilit� de
vos donn�es. Le r�sultat est un p�riph�rique multidisque, 
<firstterm>Multidisk Device</firstterm>, ou <firstterm>RAID logiciel</firstterm>.
</para>
<para>
Un p�riph�rique multidisque n'est qu'un ensemble de partitions
situ�es sur diff�rents disques mais r�unies pour former un p�riph�rique
<emphasis>logique</emphasis>. Ce p�riph�rique peut alors �tre utilis� comme
une simple partition, c'est-�-dire une partition qu'on peut monter et 
formater avec <command>partman</command>.
</para>
<para>
Ce que vous gagnez d�pend du type de p�riph�rique cr��. Voici ceux qui
sont reconnus&nbsp;:
<variablelist>
<varlistentry>

<term>RAID0</term><listitem><para>
Ce type vise principalement la performance. RAID0 divise toutes les donn�es
entrantes en <firstterm>bandes</firstterm> et les distribue �galement sur
tous les disques. Cela peut augmenter la vitesse des op�rations d'�criture et
de lecture, mais si l'un des disques a un probl�me, vous perdez 
<emphasis>tout</emphasis>&nbsp;: une partie des informations est encore sur
les disques sains, mais l'autre <emphasis>�tait</emphasis> sur le disque
d�fectueux.
</para>
<para>
L'utilisation standard de RAID0 est une partition pour du travail vid�o.
</para></listitem>
</varlistentry>

<varlistentry>
<term>RAID1</term><listitem><para>
Ce type est adapt� l� o� la fiabilit� est le premier souci. Il consiste en 
plusieurs partitions de m�me taille (deux, habituellement) qui contiennent
exactement les m�mes donn�es. Cela signifie essentiellement trois choses.
Premi�rement, si l'un des disques a un probl�me, les donn�es sont encore sur 
les autres disques. Deuxi�mement, vous pouvez utiliser une partie seulement 
de la capacit� disponible, pr�cis�ment, la taille de la partition la plus 
petite du RAID. Troisi�mement, la charge de lecture des fichiers est 
r�partie entre tous les disques, ce qui peut am�liorer les performances d'un
serveur, notamment les serveurs de fichiers o� les lectures sont
plus nombreuses que les �critures de fichiers. 
</para>
<para>
Vous pouvez aussi inclure dans un tel ensemble un disque de rechange qui prendra
la place du disque d�fectueux en cas de probl�me.  
  </para></listitem>
</varlistentry>
<varlistentry>

<term>RAID5</term><listitem><para>
Ce type est un bon compromis entre vitesse, fiabilit� et redondance des 
donn�es. RAID5, comme RAID0, divise toutes les donn�es entrantes en bandes et 
les distribue �galement sur tous les disques. Mais contrairement � RAID0, RAID5
calcule aussi l'information de <firstterm>parit�</firstterm>, qui est �crite
sur le disque restant. Le disque de parit� n'est pas fixe (ce serait RAID4), il
change p�riodiquement et ainsi l'information de parit� est distribu�e �galement
sur tous les disques. Quand l'un des disques s'arr�te, la partie manquante des
donn�es peut �tre calcul�e avec les donn�es restantes et la parit�. RAID5 doit
comporter au moins trois partitions actives. Vous pouvez aussi inclure un disque
de rechange qui prendra la place du disque d�fectueux en cas de probl�me.
</para>
<para>
Comme on le voit, RAID5 est aussi fiable que RAID1 tout en �vitant de la
redondance. D'un autre c�t�, il sera un tout petit peu moins rapide dans les
op�rations d'�criture que RAID0, � cause du calcul de l'information de
parit�.
</para></listitem>
</varlistentry>
</variablelist>

Pour r�sumer&nbsp;:

<informaltable>
<tgroup cols="5">
<thead>
<row>
  <entry>Type</entry>
  <entry>Nombre minimum de disques</entry>
  <entry>Disque de rechange</entry>
  <entry>Supporte l'�chec d'un disque ?</entry>
  <entry>Espace disponible</entry>
</row>
</thead>

<tbody>
<row>
  <entry>RAID0</entry>
  <entry> 2</entry>
  <entry>non</entry>
  <entry>non</entry>
  <entry>Taille de la plus petite partition multipli�e par le nombre des p�riph�riques dans l'ensemble</entry>
</row>

<row>
  <entry>RAID1</entry>
  <entry> 2</entry>
  <entry>facultatif</entry>
  <entry>oui</entry>
  <entry>Taille de la plus petite partition dans l'ensemble</entry>
</row>

<row>
  <entry>RAID5</entry>
  <entry> 3</entry>
  <entry>facultatif</entry>
  <entry>oui</entry>
  <entry>
    Taille de la plus petite partition multipli�e par le nombre des 
p�riph�riques dans l'ensemble moins un.
  </entry>
</row>

</tbody></tgroup></informaltable>
</para>

<para>
Si vous voulez tout savoir sur le RAID logiciel, lisez le
<ulink url="&url-software-raid-howto;">HOWTO sur le RAID logiciel</ulink>.

</para>

<para>
Pour cr�er un p�riph�rique multidisque, vous devez pr�parer les 
partitions n�cessaires. Vous pouvez le faire avec <command>partman</command>,
dans le menu <guimenu>Caract�ristiques de la partition</guimenu> o� vous
choisirez <menuchoice><guimenu>Utiliser comme :</guimenu> 
<guimenuitem>volume physique pour RAID</guimenuitem>
</menuchoice>
</para>
<warning><para>
L'installateur g�re le RAID logiciel depuis peu. Vous pourrez rencontrer
des probl�mes, pour certains niveaux RAID et en combinaison avec les
programmes d'amor�age, si vous essayez d'utiliser un p�riph�rique de ce type 
pour la partition racine, <filename>/</filename>. Les experts pourront
sans doute contourner ces probl�mes en ex�cutant certaines �tapes de 
configuration ou d'installation dans un shell.
</para></warning>
<para>
Ensuite, vous choisissez <guimenuitem>Configurer le RAID logiciel</guimenuitem>
dans le menu principal de <command>partman</command>. 
Le menu n'appara�t que si vous avez s�lectionn� au moins une partition �
utiliser comme <guimenuitem>volume physique pour RAID</guimenuitem>.
Dans le premier �cran de <command>mdcfg</command>, s�lectionnez 
<guimenuitem>Cr�er un p�riph�rique multidisque</guimenuitem>. Une liste
des types accept�s pour ces p�riph�riques est affich�e et vous pouvez en
choisir un, par exemple RAID1. La suite d�pend du type que vous avez choisi.
</para>

<itemizedlist>
<listitem><para>
Le type RAID0 est simple. Une liste des partitions RAID disponibles est
pr�sent�e et tout ce que vous avez � faire est de choisir les partitions qui
composeront le p�riph�rique.
</para></listitem>
<listitem><para>
Le type RAID1 est un peu plus compliqu�. On vous demandera d'abord le nombre
de p�riph�riques actifs et le nombre de p�riph�riques de rechange qui
composeront le p�riph�rique. Ensuite vous devrez choisir dans la liste des
partitions RAID celles qui seront actives et celles qui resteront en r�serve.
Le nombre de ces partitions devra �tre �gal � celui donn� l'instant d'avant.
Si vous n'indiquez pas la m�me valeur, l'installateur ne vous laissera 
pas continuer et vous demandera une correction.
</para></listitem>
<listitem><para>
RAID5 se configure comme RAID1, mais vous devez utiliser au moins trois
partitions actives.
</para></listitem>
</itemizedlist>

<para>
Il est parfaitement possible d'avoir plusieurs types de p�riph�riques en m�me
temps. Par exemple, avec trois disques durs de 200&nbsp;Go, chacun contenant
deux partitions de 100&nbsp;Go, vous pouvez cr�er un p�riph�rique de type
RAID0 avec les premi�res partitions des trois disques, soit une partition
rapide de 300&nbsp;Go pour le travail vid�o&nbsp;; et vous pouvez combiner
les trois partitions restantes (deux actives, une de rechange) dans un
p�riph�rique RAID1, soit une partition tr�s fiable de 100&nbsp;Go pour
<filename>/home</filename>.
</para>
<para>
Quand vous avez d�fini vos p�riph�riques, vous pouvez choisir
<guimenuitem>Terminer</guimenuitem> <command>mdcfg</command> pour retourner
au menu de <command>partman</command> et cr�er les syst�mes de fichiers et les
options habituelles comme les points de montage pour ces nouveaux 
p�riph�riques.
</para>
</sect3>