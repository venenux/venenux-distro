<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 28997 -->


  <sect2 arch="powerpc"><title>MacOS からのディスクイメージの書き込み</title>
<para>

<!--
An AppleScript, <application>Make Debian Floppy</application>, is
available for burning floppies from the provided disk image files. It
can be downloaded from
<ulink url="ftp://ftp2.sourceforge.net/pub/sourceforge/d/de/debian-imac/MakeDebianFloppy.sit"></ulink>. To
use it, just unstuff it on your desktop, and then drag any floppy
image file to it. You must have Applescript installed and enabled in
your extensions manager. Disk Copy will ask you to confirm that you
wish to erase the floppy and proceed to write the file image to it.
-->
<application>Make Debian Floppy</application> という AppleScript を使えば、
提供されているディスクイメージファイルをフロッピーに書き込むことが出来ます。
この AppleScript は
<ulink url="ftp://ftp2.sourceforge.net/pub/sourceforge/d/de/debian-imac/MakeDebianFloppy.sit"></ulink>
からダウンロードできます。デスクトップに unstuff して、
フロッピーイメージファイルをその上にドラッグするだけです。
AppleScript をインストールし、
extensions manager で有効にしておく必要があります。
フロッピーの内容を消去してファイルイメージの書き込みを進めて良いかを
Disk Copy から尋ねられます。

</para><para>

<!--
You can also use the MacOS utility <command>Disk Copy</command>
directly, or the freeware utility <command>suntar</command>. The
<filename>root.bin</filename> file is an example of a floppy
image. Use one of the following methods to create a floppy from the
floppy image with these utilities.
-->
MacOS のユーティリティ <command>Disk Copy</command> を直接使ったり、
フリーウェアのユーティリティ <command>suntar</command> を使うこともできます。
<filename>root.bin</filename> ファイルをフロッピーイメージの例として、
これらのユーティリティでフロッピーイメージから
フロッピーディスクを作製する方法を以下にいくつか紹介します。

</para>

   <sect3>
   <title><command>Disk Copy</command> を用いたディスクイメージの書き込み</title>
<para>

<!--
If you are creating the floppy image from files which were originally
on the official &debian; CD, then the Type and Creator are already set
correctly. The following <command>Creator-Changer</command> steps are
only necessary if you downloaded the image files from a Debian mirror.
-->
公式 &debian; CD にあったファイルからフロッピーイメージを作る場合は、
Type と Creator は既に適切な値になっています。
ここに示す <command>Creator-Changer</command> の作業は、イメージファイルを
Debian ミラーからダウンロードした場合でのみ必要となります。

</para>
<orderedlist>
<listitem><para>

<!--
Obtain
<ulink url="&url-powerpc-creator-changer;">Creator-Changer</ulink>
and use it to open the <filename>root.bin</filename> file.
-->
<ulink url="&url-powerpc-creator-changer;">Creator-Changer</ulink> 
を取得し、これを用いて <filename>root.bin</filename> ファイルをオープンします。

</para></listitem>
<listitem><para>

<!--
Change the Creator to <userinput>ddsk</userinput> (Disk Copy), and the
Type to <userinput>DDim</userinput> (binary floppy image). The case is
sensitive for these fields.
-->
Creator を <userinput>ddsk</userinput> (Disk Copy) に変更し、
Type を <userinput>DDim</userinput> (バイナリフロッピーイメージ) に変更します。
これらのフィールドでは大文字小文字が区別されますので注意してください。

</para></listitem>
<listitem><para>

<!--
<emphasis>Important:</emphasis> In the Finder, use <userinput>Get
Info</userinput> to display the Finder information about the floppy
image, and <quote>X</quote> the <userinput>File Locked</userinput> check box so
that MacOS will be unable to remove the boot blocks if the image is
accidentally mounted.
-->
<emphasis>重要:</emphasis>
Finder から <userinput>Get Info</userinput> を用いて、
フロッピーイメージの Finder 情報を表示させてください。
ここで <userinput>File Locked</userinput> チェックボックスに <quote>X</quote> を入れ、
イメージを間違ってマウントしてしまった場合でも
MacOS がブートブロックを消してしまわないようにしてください。


</para></listitem>
    <listitem><para>

<!--
Obtain <command>Disk Copy</command>; if you have a MacOS system or CD it
will very likely be there already, otherwise try
<ulink url="&url-powerpc-diskcopy;"></ulink>.
-->
<command>Disk Copy</command> を入手します。
MacOS システムや CD をお持ちなら、既にその中にあるでしょう。
持っていなければ
<ulink url="&url-powerpc-diskcopy;"></ulink> を試してみてください。

</para></listitem>
<listitem><para>

<!--
Run <command>Disk Copy</command>, and select <menuchoice>
<guimenu>Utilities</guimenu> <guimenuitem>Make a Floppy</guimenuitem>
</menuchoice>, then select the
<emphasis>locked</emphasis> image file from the resulting dialog. It
will ask you to insert a floppy, then ask if you really want to erase
it. When done it should eject the floppy.
-->
<command>Disk Copy</command> を実行します。<menuchoice>
<guimenu>Utilities</guimenu> <guimenuitem>Make a Floppy</guimenuitem>
</menuchoice> を選び、
現われたダイアログで <emphasis>locked</emphasis> イメージファイルを選んでください。
フロッピーを挿入するよう求められ、
続いてその内容を本当に消していいかどうかを訊かれます。
書き込みが終わったらフロッピーはイジェクトされます。

</para></listitem>
</orderedlist>

   </sect3>

   <sect3>
   <title><command>suntar</command> を用いたディスクイメージの書き込み</title>
<para>

<orderedlist>
<listitem><para>

<!--
Obtain <command>suntar</command> from <ulink url="&url-powerpc-suntar;">
</ulink>. Start the <command>suntar</command> program and select
<quote>Overwrite Sectors...</quote> from the <userinput>Special</userinput>
menu.
-->
<command>suntar</command> を <ulink url="&url-powerpc-suntar;"></ulink>
から入手します。<command>suntar</command> プログラムを起動し、
<userinput>Special</userinput> メニューから <quote>Overwrite Sectors...</quote> を選びます。

</para></listitem>
<listitem><para>

<!--
Insert the floppy disk as requested, then hit &enterkey; (start at
sector 0).
-->
求められたところでフロッピーディスクを挿入し、
&enterkey; を押します (セクタ 0 から始めます)。

</para></listitem>
<listitem><para>

<!--
Select the <filename>root.bin</filename> file in the file-opening dialog.
-->
file-opening ダイアログから <filename>root.bin</filename> ファイルを選びます。

</para></listitem>
<listitem><para>

<!--
After the floppy has been created successfully, select <menuchoice>
<guimenu>File</guimenu> <guimenuitem>Eject</guimenuitem> </menuchoice>.
If there are any errors writing the floppy, simply toss that floppy and
try another.
-->
フロッピーがうまく作成されたら、<menuchoice>
<guimenu>File</guimenu> <guimenuitem>Eject</guimenuitem> </menuchoice>
を選びます。フロッピーの書き込みでエラーが起こったら、
そのフロッピーは捨てて別のフロッピーを試してください。

</para></listitem>
</orderedlist>

<!--
Before using the floppy you created, <emphasis>set the write protect
tab</emphasis>!  Otherwise if you accidentally mount it in MacOS,
MacOS will helpfully ruin it.
-->
作成したフロッピーを使う前に、
<emphasis>ツメを動かして書き込み防止にしてください</emphasis>
こうしておかないと、間違って MacOS にマウントされてしまった場合、
MacOS がフロッピーを壊してしまいます。

</para>
   </sect3>
  </sect2>
