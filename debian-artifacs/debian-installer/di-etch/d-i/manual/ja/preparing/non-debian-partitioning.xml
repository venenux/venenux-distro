<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 39622 -->

 <sect1 id="non-debian-partitioning">
 <title>マルチブートシステムでの事前パーティション分割</title>
<para>

<!--
Partitioning your disk simply refers to the act of breaking up your
disk into sections. Each section is then independent of the others.
It's roughly equivalent to putting up walls inside a house; if you add
furniture to one room it doesn't affect any other room.
-->
「ディスクのパーティション分割」とは、ディスクをセクションに分けることです。
各セクションは他のセクションから独立しています。
この作業は要するに、家の中に壁を作るようなものです。
ある部屋に家具を入れても、それは他の部屋には影響しないというわけです。

</para><para arch="s390">

<!--
Whenever this section talks about <quote>disks</quote> you should translate
this into a DASD or VM minidisk in the &arch-title; world. Also a machine
means an LPAR or VM guest in this case.
-->
このセクションで <quote>ディスク</quote> という言葉が出てきた場合、
&arch-title; の世界ではこれを DASD や VM ミニディスクと置き換えてください。
またこの場合 <quote>マシン</quote> は LPAR や VM ゲストと置き換えてください。

</para><para>

<!--
If you already have an operating system on your system
-->
システム上に既にオペレーティングシステム 

<phrase arch="x86">
(Windows 9x, Windows NT/2000/XP, OS/2, MacOS, Solaris, FreeBSD, &hellip;)
</phrase>

<phrase arch="alpha">
(Tru64 (Digital UNIX), OpenVMS, Windows NT, FreeBSD, &hellip;)
</phrase>

<phrase arch="s390">
(VM, z/OS, OS/390, &hellip;)
</phrase>

<phrase arch="m68k">
(Amiga OS, Atari TOS, Mac OS, &hellip;)
</phrase>

<!--
and want to stick Linux on the same disk, you will need to repartition
the disk. Debian requires its own hard disk partitions. It cannot be
installed on Windows or MacOS partitions. It may be able to share some
partitions with other Linux systems, but that's not covered here. At
the very least you will need a dedicated partition for the Debian
root.
-->
が入っていて、同じディスクに Linux も入れたい場合には、
ディスクのパーティション分割をやり直す必要があります。
Linux は Windows や MacOS のパーティションにはインストールできません。
他の Linux システムとはパーティションを共有することも可能かもしれませんが、
ここではそれは取り扱いません。
少なくとも、Debian の root には専用のパーティションが必要となります。


</para><para>

<!--
You can find information about your current partition setup by using
a partitioning tool for your current operating system<phrase
arch="x86">, such as fdisk or PartitionMagic</phrase><phrase
arch="powerpc">, such as Drive Setup, HD Toolkit, or MacTools</phrase><phrase
arch="m68k">, such as HD SC Setup, HDToolBox, or SCSITool</phrase><phrase
arch="s390">, such as the VM diskmap</phrase>. Partitioning tools always
provide a way to show existing partitions without making changes.
-->
現在のパーティションの設定は、<phrase 
arch="x86">fdisk や PartitionMagic</phrase><phrase 
arch="powerpc">Drive Setup, HD Toolkit, MacTools</phrase><phrase 
arch="m68k">HD SC Setup, HDToolBox, SCSITool</phrase><phrase 
arch="s390">VM diskmap</phrase>のような、
現在の OS に対応したパーティション分割ツールを使えばわかります。
パーティション分割ツールには、
必ず既存のパーティションを (変更せずに) 表示する機能が付いています。

</para><para>


<!--
In general, changing a partition with a file system already on
it will destroy any information there. Thus you should always make
backups before doing any repartitioning.  Using the analogy of the
house, you would probably want to move all the furniture out of the
way before moving a wall or you risk destroying it.
-->
一般には、既にファイルシステムの入っているパーティションを変更すると、
そこの情報はすべて破壊されてしまいます。
従って、パーティション分割をやり直す前には、
必ずバックアップを取っておくべきです。
また家の比喩を用いてみましょう。
壁を動かす前には、家具が壊れないよう、それらは前もってどけておくでしょう?

</para><para arch="hppa" condition="FIXME">

<emphasis>FIXME: HP-UX のディスクについては?</emphasis>

</para><para>

<!--
If your computer has more than one hard disk, you may want to dedicate
one of the hard disks completely to Debian. If so, you don't need to
partition that disk before booting the installation system; the
installer's included partitioning program can handle the job nicely.
-->
コンピュータに 2 台以上のハードディスクがある場合は、
その内の 1 台を Debian 専用にするといいかもしれません。
そうすれば、インストールシステムの起動前に
パーティション分割を行う必要はありません。
インストーラに含まれているパーティション分割プログラムが、
この仕事を的確にこなしてくれます。

</para><para>

<!--
If your machine has only one hard disk, and you would like to
completely replace the current operating system with &debian;,
you also can wait to partition as part of the installation process
(<xref linkend="partman"/>), after you have booted the
installation system.  However this only works if you plan to boot the
installer system from tapes, CD-ROM or files on a connected machine.
Consider: if you boot from files placed on the hard disk, and then
partition that same hard disk within the installation system, thus
erasing the boot files, you'd better hope the installation is
successful the first time around.  At the least in this case, you
should have some alternate means of reviving your machine like the
original system's installation tapes or CDs.
-->
マシンに 1 台しかディスクがなくても、現在の OS を &debian;
で完全に置き換えてしまうつもりなら、
パーティション分割はインストーラを起動した後で、
インストール作業の一部として行って構いません (<xref linkend="partman"/>)。
しかしこれが可能なのは、インストーラシステムをテープ、CD-ROM、
接続されたマシンのファイルのいずれかから起動する場合だけです。
ちょっと考えてみてください。ハードディスクにあるファイルから起動して、
起動したインストールシステムからそのファイルのあるディスクを
パーティション分割し、つまり起動ファイルを消してしまったとしたら。
そのインストールが一発でうまいこと行くように祈るしかないですね。
まあこの場合に最悪の状況となったとしても、
もともと入っていたシステムのインストールテープや CD などで、
コンピュータを元の状態に戻す方法はきっとあるでしょうが。

</para><para>

<!--
If your machine already has multiple partitions, and enough space can
be provided by deleting and replacing one or more of them, then you
too can wait and use the Debian installer's partitioning program. You
should still read through the material below, because there may be
special circumstances like the order of the existing partitions within
the partition map, that force you to partition before installing
anyway.
-->
既にコンピュータに複数のパーティションがあり、
それらの一部を消したり置き換えたりすることによって
充分な空き領域が確保できる場合にも、
Debian インストーラのパーティション分割プログラムで作業を行って構いません。
しかしこの場合でも、以降の内容は目を通しておきましょう。
パーティションマップ中の現在のパーティションの並び順などによって、
いずれにしてもインストール前にパーティション分割作業を
しなければならないような場合もあり得るからです。

</para><para arch="x86">
<!--
If your machine has a FAT or NTFS filesystem, as used by DOS and Windows,
you can wait and use Debian installer's partitioning program to
resize the filesystem.
-->
DOS や Windows で使用していて、マシンに FAT・NTFS ファイルシステムがある場合、
Debian インストーラのパーティション分割プログラムで
ファイルシステムをリサイズできます (時間がかかりますが)。
</para><para>

<!--
If none of the above apply, you'll need to partition your hard disk before
starting the installation to create partition-able space for
Debian. If some of the partitions will be owned by other operating
systems, you should create those partitions using native operating
system partitioning programs. We recommend that you do
<emphasis>not</emphasis> attempt to create partitions for &debian;
using another operating system's tools. Instead, you should just
create the native operating system's partitions you will want to
retain.
-->
上記のどれにも当てはまらない場合、インストールをはじめる前に
パーティション分割を行い、Debian に割り当て可能な領域を
作ってやらなければなりません。
一部のパーティションを他の OS に使う場合は、
そのパーティションはその OS のパーティション分割ツールで
作成するほうが良いでしょう。
しかし &debian; 用のパーティションは、
他の OS のツールでは<emphasis>作らない</emphasis>ようお勧めします。
そのツールで作るのは、
残しておきたい OS のパーティションだけにしてください。

</para><para>

<!--
If you are going to install more than one operating system on the same
machine, you should install all other system(s) before proceeding with
Linux installation. Windows and other OS installations may destroy
your ability to start Linux, or encourage you to reformat non-native
partitions.
-->
同じマシンに複数の OS をインストールするつもりでしたら、
Linux をインストールする前に、
他の OS を全部先にインストールしておきましょう。
Windows などの他の OS をインストールすると、
Linux を起動する機能が破壊されてしまったり、
あるいはその OS のものでないパーティションを
フォーマットし直すよう促されたりするからです。

</para><para>

<!--
You can recover from these actions or avoid them, but installing
the native system first saves you trouble.
-->
このような動作から復旧したり、そのような提案を断ったりすることはできますが、
先にそちらのシステムをインストールしておけば、
最初からトラブルを避けることができます。

</para><para arch="powerpc">

<!--
In order for OpenFirmware to automatically boot &debian; the Linux
partitions should appear before all other partitions on the disk,
especially MacOS boot partitions. This should be kept in mind when
pre-partitioning; you should create a Linux placeholder partition to
come <emphasis>before</emphasis> the other bootable partitions on the
disk. (The small partitions dedicated to Apple disk drivers are not
bootable.) You can delete the placeholder with the Linux partition
tools later during the actual install, and replace it with Linux
partitions.
-->
OpenFirmware に &debian; を自動的に起動させるには、
Linux パーティションを他の OS のパーティション 
(特に MacOS のブートパーティション) よりディスクの先頭近くに
置かなければなりません。
事前パーティション分割を行うときにはこのことを心に留めておきましょう。
Linux 用に使う場所を埋めておくパーティションを、
他の起動可能なパーティションよりも
ディスクの <emphasis>前の方</emphasis> に作らなければなりません
(Apple ディスクドライバ用の小さなパーティションは起動可能ではありません)。
この場所埋めのパーティションは、後で Linux をインストールするときに削除し、
実際の Linux パーティションと置き換えることができます。

</para><para>

<!--
If you currently have one hard disk with one partition (a common setup
for desktop computers), and you want to multi-boot the native
operating system and Debian, you will need to:
-->
現在ディスクがひとつ、パーティションもひとつ 
(デスクトップコンピュータだと普通の設定)
になっていて、元の OS と Debian との
デュアルブートにしたい場合は、以下の手順を踏む必要があります。

  <orderedlist>
<listitem><para>

<!--
Back up everything on the computer.
-->
コンピュータのすべてをバックアップする。

</para></listitem>
<listitem><para>

<!--
Boot from the native operating system installer media such as CD-ROM
or tapes.
-->
元の OS のインストールメディア (CD-ROM やテープ) から起動する。

<!--
<phrase arch="powerpc">When booting from a MacOS CD, hold the
<keycap>c</keycap> key while
booting to force the CD to become the active MacOS system.</phrase>
-->
<phrase arch="powerpc">MacOS CD から起動する場合は、
起動中に <keycap>c</keycap> キーを押しっぱなしにして、
強制的に CD をアクティブな MacOS にする。</phrase>

</para></listitem>
<listitem><para>

<!--
Use the native partitioning tools to create native system
partition(s). Leave either a place holder partition or free space for
&debian;.
-->
既存の OS のパーティション分割ツールを使って、
そのシステムのパーティションを作る。
&debian; 用にも場所埋めのパーティションか、空き領域を作る。

</para></listitem>
<listitem><para>

<!--
Install the native operating system on its new partition.
-->
その OS を、新しくつくったパーティションにインストールする。

</para></listitem>
<listitem><para>

<!--
Boot back into the native system to verify everything's OK,
    and to download the Debian installer boot files.
-->
新しく入れたその OS で起動しなおして、すべて問題ないか確かめる。
問題なければ Debian インストーラの起動ファイルをダウンロードする。

</para></listitem>
<listitem><para>

<!--
Boot the Debian installer to continue installing Debian.
-->
Debian インストーラを起動して、Debian のインストールを続ける。

</para></listitem>
</orderedlist>

</para>

&nondeb-part-alpha.xml;
&nondeb-part-x86.xml;
&nondeb-part-m68k.xml;
&nondeb-part-sparc.xml;
&nondeb-part-powerpc.xml;

 </sect1>
