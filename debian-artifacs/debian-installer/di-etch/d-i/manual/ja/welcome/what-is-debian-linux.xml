<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 25496 -->

 <sect1 id="what-is-debian-linux">
 <title>&debian; とは?</title>
<para>

<!--
The combination of Debian's philosophy and methodology and the GNU
tools, the Linux kernel, and other important free software, form a
unique software distribution called Debian GNU/Linux. This
distribution is made up of a large number of software
<emphasis>packages</emphasis>.  Each package in the distribution
contains executables, scripts, documentation, and configuration
information, and has a <emphasis>maintainer</emphasis> who is
primarily responsible for keeping the package up-to-date, tracking bug
reports, and communicating with the upstream author(s) of the packaged
software. Our extremely large user base, combined with our bug
tracking system ensures that problems are found and fixed quickly.
-->
Debian の哲学や方法論と、GNU ツール・Linux カーネル・その他の
重要なフリーソフトウェアとを組み合わせることにより、&debian;
と呼ばれるユニークなディストリビューションが形成されています。
このディストリビューションは、多数のソフトウェア
<emphasis>パッケージ</emphasis>から構成されています。ディストリビューションに
含まれる個々のパッケージは、実行ファイル・スクリプト・ドキュメント・
設定情報などから構成されています。また各パッケージには、
そのパッケージに責任を持つ<emphasis>メンテナ</emphasis>がいて、
そのパッケージを最新に保ち、バグ報告を追跡し、パッケージにされている
ソフトウェアの上流開発者と連絡をとることについて、第一に責任を負います。
大きなユーザベースが、バグ追跡システムとあいまって、
問題がすぐに発見・解決されることを保証しています。

</para><para>

<!--
Debian's attention to detail allows us to produce a high-quality,
stable, and scalable distribution.  Installations can be easily
configured to serve many roles, from stripped-down firewalls to
desktop scientific workstations to high-end network servers.
-->
Debian は、細部に注意を払うことで、高品質で安定したスケーラブルな
ディストリビューションとなっています。小さなファイアウォールから
科学用途のデスクトップワークステーションやハイエンドネットワーク
サーバまで、様々な用途に合わせたインストールが可能です。

</para><para>

<!--
Debian is especially popular among advanced users because of its
technical excellence and its deep commitment to the needs and
expectations of the Linux community. Debian also introduced many
features to Linux that are now commonplace.
-->
Debian は、技術的な優越性や Linux コミュニティのニーズや期待への深い
コミットメントによって、上級ユーザに特に人気があります。
Debian はさらに、現在 Linux が普通に持っている多くの特徴を導入しました。

</para><para>

<!--
For example, Debian was the first Linux distribution to include a
package management system for easy installation and removal of
software. It was also the first Linux distribution that could be
upgraded without requiring reinstallation.
-->
例えば、Debian はソフトウェアの簡単なインストール・削除用に
パッケージ管理システムを持った初めての Linux ディストリビューションでした。
さらに、再インストールせずにシステムの更新ができる、
初めての Linux ディストリビューションでした。

</para><para>

<!--
Debian continues to be a leader in Linux development. Its development
process is an example of just how well the Open Source development
model can work &mdash; even for very complex tasks such as building and
maintaining a complete operating system.
-->
Debian は Linux 開発のリーダーであり続けています。
その開発プロセスは (完全なオペレーティングシステムを構築し維持するような
非常に複雑なタスクであったとしても) オープンソース開発モデルが、
どれほどうまくいくことができるかの好例となっています。

</para><para>

<!--
The feature that most distinguishes Debian from other Linux
distributions is its package management system.  These tools give the
administrator of a Debian system complete control over the packages
installed on that system, including the ability to install a single
package or automatically update the entire operating system.
Individual packages can also be protected from being updated.  You can
even tell the package management system about software you have
compiled yourself and what dependencies it fulfills.
-->
Debian を他の GNU/Linux ディストリビューションと区別する最大の
特徴は、パッケージ管理システムです。Debian システムの管理者は、
システムにインストールされるパッケージに関して、ひとつのパッケージの
インストールからオペレーティングシステム全体の自動アップデートまで、
完全に制御することができます。個々のパッケージをアップデートしない
ように設定することもできます。あなた自身がコンパイルしたソフトウェアに
ついて、その依存関係を設定することもできます。

</para><para>

<!--
To protect your system against <quote>Trojan horses</quote> and other malevolent
software, Debian's servers verify that uploaded packages come from
their registered Debian maintainers.  Debian packagers also take great
care to configure their packages in a secure manner.  When security
problems in shipped packages do appear, fixes are usually available
very quickly.  With Debian's simple update options, security fixes can
be downloaded and installed automatically across the Internet.
-->
<quote>トロイの木馬</quote>や他の悪意あるソフトウェアからあなたのシステムを
守るために、Debian のサーバは、アップロードされてきたパッケージが
登録された Debian 開発者からのものかどうかを確かめます。
また、Debian の各パッケージは
より安全な設定となるように細心の注意が払われています。
もしリリースされたパッケージにセキュリティ上の問題が発生すれば、
その修正版は通常すぐに利用可能になります。
Debian の簡単なアップデートオプションによって、
セキュリティ修正はインターネットを通じて自動的にダウンロード・
インストールすることができます。

</para><para>

<!--
The primary, and best, method of getting support for your &debian;
system and communicating with Debian Developers is through
the many mailing lists maintained by the Debian Project (there are
more than &num-of-debian-maillists; at this writing).  The easiest
way to subscribe to one or more of these lists is visit
<ulink url="&url-debian-lists-subscribe;">
Debian's mailing list subscription page</ulink> and fill out the form
you'll find there.
-->
あなたの &debian; システムについてサポートを受けたり、Debian の
開発者たちと連絡したりする第一の、そして最良の方法は、Debian
プロジェクトが運営する多数のメーリングリストを用いることです
(この文章の執筆時点で &num-of-debian-maillists; 以上のメーリングリストがあります)。
メーリングリストを簡単に講読するためには、
<ulink url="&url-debian-lists-subscribe;">
Debian メーリングリスト講読ページ</ulink>
を訪れて、フォームに必要事項を記入するとよいです。

</para>

 </sect1>
