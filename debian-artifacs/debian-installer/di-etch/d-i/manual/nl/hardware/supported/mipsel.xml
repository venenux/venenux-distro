<!-- retain these comments for translator revision tracking -->
<!-- original version: 36732 untranslated -->


  <sect2 arch="mipsel"><title>CPU, Main Boards, and Video Support</title>
<para>

Debian on &arch-title; supports the following platforms:

<itemizedlist>
<listitem><para>

DECstation: various models of the DECstation are supported.

</para></listitem>
<listitem><para>

Cobalt Microserver: only MIPS based Cobalt machines are covered here.  This
includes the Cobalt Qube 2700 (Qube1), RaQ, Qube2 and RaQ2, and the Gateway
Microserver.

</para></listitem>
<listitem><para>

Broadcom BCM91250A (SWARM): this is an ATX form factor evaluation board
from Broadcom based on the dual-core SB1 1250 CPU.

</para></listitem>
<listitem><para>

Broadcom BCM91480B (BigSur): this is an ATX form factor evaluation board
from Broadcom based on the quad-core SB1A 1480 CPU.

</para></listitem>
</itemizedlist>

Complete information regarding supported mips/mipsel machines can be found
at the <ulink url="&url-linux-mips;">Linux-MIPS homepage</ulink>.  In the
following, only the systems supported by the Debian installer will be
covered.  If you are looking for support for other subarchitectures, please
contact the <ulink url="&url-list-subscribe;">
debian-&arch-listname; mailing list</ulink>.

</para>

   <sect3><title>CPU/Machine types</title>

<para>

Currently only DECstations with R3000 and R4000/R4400 CPUs are
supported by the Debian installation system on little endian MIPS.
The Debian installation system works on the following machines:

</para><para>

<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>System Type</entry><entry>CPU</entry><entry>Code-name</entry>
  <entry>Debian subarchitecture</entry>
</row>
</thead>

<tbody>
<row>
  <entry>DECstation 5000/1xx</entry>
  <entry>R3000</entry>
  <entry>3MIN</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/150</entry>
  <entry>R4000</entry>
  <entry>3MIN</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>DECstation 5000/200</entry>
  <entry>R3000</entry>
  <entry>3MAX</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/240</entry>
  <entry>R3000</entry>
  <entry>3MAX+</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/260</entry>
  <entry>R4400</entry>
  <entry>3MAX+</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>Personal DECstation 5000/xx</entry>
  <entry>R3000</entry>
  <entry>Maxine</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Personal DECstation 5000/50</entry>
  <entry>R4000</entry>
  <entry>Maxine</entry>
  <entry>r4k-kn04</entry>
</row>
</tbody></tgroup></informaltable>

</para><para>

All Cobalt machines are supported.  In the past, only machines with a
serial console were supported (that is, all machines except for the Qube
2700, aka Qube1).  However, installations are now also possible through
SSH.

</para><para>

The Broadcom BCM91250A evaluation board comes with an SB1 1250 chip with
two cores which are supported in SMP mode by this installer.  Similarly,
the BCM91480B evaluation board contains an SB1A 1480 chip with four cores
which are supported in SMP mode.

</para>
   </sect3>

   <sect3><title>Supported console options</title>
<para>

A serial console is available on all supported DECstations (9600 bps,
8N1). To use the serial console, you have to boot the installer image
with the <literal>console=ttyS</literal><replaceable>x</replaceable> kernel
parameter (with <replaceable>x</replaceable> being the number
of the serial port you have your terminal connected to &mdash; usually
<literal>2</literal>, but <literal>0</literal> for the Personal DECstations).
On 3MIN and 3MAX+ (DECstation 5000/1xx, 5000/240 and 5000/260) a local console
is available with the PMAG-BA and the PMAGB-B graphics options.

</para><para>

If you have a Linux system to use as serial terminal, an easy way
is to run <command>cu</command><footnote>

<para>
In Woody this command was part of the <classname>uucp</classname> package,
but in later releases it is available as a separate package.
</para>

</footnote> on it. Example:

<informalexample><screen>
$ cu -l /dev/ttyS1 -s 9600
</screen></informalexample>

where the option <literal>-l</literal> (line) sets the serial port to use
and <literal>-s</literal> (speed) sets the speed for the connection (9600
bits per second).

</para><para>

Both Cobalt and Broadcom BCM91250A/BCM91480B use 115200 bps.

</para>
   </sect3>
  </sect2>
