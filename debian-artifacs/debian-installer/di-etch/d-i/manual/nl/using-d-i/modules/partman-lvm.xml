<!-- original version: 43577 -->

   <sect3 id="partman-lvm">
   <title>Logisch volumebeheer (LVM) instellen</title>
<para>

Als u met computers werkt op het niveau van systeembeheerder of
<quote>gevorderde</quote> gebruiker, heeft u vast de situatie meegemaakt
waar op een partitie (meestal de meest belangrijke) onvoldoende ruimte
beschikbaar was, terwijl een andere partitie grotendeels ongebruikt was.
Als oplossing heeft u mogelijk bestanden moeten verplaatsen met symbolische
verwijzingen vanaf de oude lokatie.

</para><para>

Om deze situatie te voorkomen, kunt u gebruik maken van Logisch volumebeheer
(Logical Volume Management &mdash; LVM). Met LVM kunt u uw partities (in de
terminologie van LVM <firstterm>fysieke volumes</firstterm>) combineren tot
een virtuele harde schijf (ofwel <firstterm>volumegroep</firstterm>); deze
kan op zijn beurt worden opgedeeld in virtuele partities (<firstterm>logische
volumes</firstterm>). Het nut hiervan is dat logische volumes (en natuurlijk
ook de onderliggende volumegroepen) verschillende fysieke harde schijven
kunnen omvatten.

</para><para>

Als u bijvoorbeeld vervolgens ontdekt dat u meer ruimte nodig heeft op uw
oude 160GB <filename>/home</filename> partitie, kunt u eenvoudig een extra
300GB harde schijf in de computer plaatsen, deze toevoegen in uw bestaande
volumegroep en vervolgens het logische volume vergroten waarop uw
<filename>/home</filename> bestandssysteem zich bevindt. En klaar is Kees: uw
gebruikers kunnen beschikken over de extra ruimte op een vernieuwde partitie
van 460GB. Dit voorbeeld is uiteraard enigszins gesimplificeerd. Wij raden u
aan om, voor zover u dat nog niet heeft gedaan, de
<ulink url="&url-lvm-howto;">LVM HOWTO</ulink> te raadplegen.

</para><para>

Het instellen van LVM in &d-i; is relatief eenvoudig en volledig ondersteund
in <command>partman</command>. Allereerst dient u de partitie(s) die u wilt
gebruiken als fysieke volumes voor LVM, te markeren.
Hiervoor selecteert u in <command>partman</command>
vanuit het menu <guimenu>Partitie-instellingen</guimenu>
de optie <menuchoice> <guimenu>Gebruiken als:</guimenu>
<guimenuitem>Fysiek volume voor LVM</guimenuitem> </menuchoice>.

</para><para>

Als u terugkeert op het hoofdscherm van <command>partman</command>, zult u
een nieuwe optie <guimenuitem>Logisch volumebeheer (LVM) instellen</guimenuitem>.
Als u deze selecteert, zal u eerst worden gevraagd om nog niet vastgelegde
wijzigingen in de partitietabel te bevestigen (als die er zijn) en vervolgens
zal het configuratiemenu voor LVM worden getoond. Het menu is contextgevoelig
en toont alleen toegestane acties. De mogelijke acties zijn:

<itemizedlist>
  <listitem><para>
    <guimenuitem>Toon configuratiedetails</guimenuitem>:
    toont onder andere de structuur van LVM-elementen en de namen en groottes
    van logische volumes
  </para></listitem>
  <listitem><para>
    <guimenuitem>Volumegroep aanmaken</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Logisch volume aanmaken</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Volumegroep verwijderen</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Logisch volume verwijderen</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Volumegroep uitbreiden</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Volumegroep verkleinen</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Einde</guimenuitem>:
    keer terug naar het hoofdscherm van <command>partman</command>
  </para></listitem>
</itemizedlist>

</para><para>

Gebruik de opties in dit menu om eerst een volume groep en vervolgens
daaronder de gewenste logische volumes.

</para><para>

Als u wederom terugkeert op het hoofdscherm van <command>partman</command>,
zult u de aangemaakte logische volumes als ware het gewone partities
terugvinden in het menu (en u kunt ze verder ook als zodanig behandelen).

</para>
   </sect3>
