# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../cdrom-checker.templates:4
msgid "Check CD-ROM integrity?"
msgstr ""

#. Type: boolean
#. Description
#: ../cdrom-checker.templates:4
msgid "Warning: this check depends on your hardware and may take some time."
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:9
msgid "Insert a Debian CD-ROM"
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:9
msgid ""
"Please insert one of the official Debian CD-ROMs into the drive before "
"continuing."
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:15
msgid "Failed to mount CD-ROM"
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:15
msgid ""
"The CD-ROM ${CDROM} could not be mounted correctly. Please check the media "
"and cables, and try it again."
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:21
msgid "No valid Debian CD-ROM"
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:21
msgid ""
"The CD-ROM you have inserted is not a valid Debian CD-ROM. Please change the "
"disk."
msgstr ""

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#: ../cdrom-checker.templates:28
msgid "Failed to open checksum file"
msgstr ""

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#: ../cdrom-checker.templates:28
msgid ""
"Opening the MD5 file on the CD-ROM failed. This file contains the checksums "
"of the files located on the CD-ROM."
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:34
msgid "Integrity test successful"
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:34
msgid "The CD-ROM integrity test was successful. The CD-ROM is valid."
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:39
msgid "Integrity test failed"
msgstr ""

#. Type: error
#. Description
#: ../cdrom-checker.templates:39
msgid ""
"The ${FILE} file failed the MD5 checksum verification. Your CD-ROM or this "
"file may have been corrupted."
msgstr ""

#. Type: boolean
#. Description
#: ../cdrom-checker.templates:46
msgid "Check the integrity of another CD-ROM?"
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:50
msgid "Insert Debian boot CD-ROM"
msgstr ""

#. Type: note
#. Description
#: ../cdrom-checker.templates:50
msgid ""
"Please make sure you have inserted the Debian boot CD-ROM  to continue with "
"the installation."
msgstr ""

#. Type: text
#. Description
#: ../cdrom-checker.templates:56
msgid "Checking CD-ROM integrity"
msgstr ""

#. Type: text
#. Description
#: ../cdrom-checker.templates:60
msgid "Checking file: ${FILE}"
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. Translators: keep it under 65 columns
#: ../cdrom-checker.templates:66
msgid "Check the CD-ROM(s) integrity"
msgstr ""
