#!/bin/sh -e
#
# Report the detected HW. Note that this needs to run both inside d-i
# and in a regular debian system, as well as behaving robustly if commands
# are missing or broken.

addinfo () {
	sed "s%^%$1: %"
}

addfile () {
	if [ -r "$1" ]; then
		cat "$1" | addinfo "$1"
	fi
}

list_loaded_modules () {
	for address in /sys/bus/pci/devices/*; do
		if [ -d "$address/driver/module" ]; then
			module=$(basename "$(readlink $address/driver/module)")
			if grep -q "^$module " /proc/modules; then
				address=$(basename "$address" | sed s/0000://)
				echo "$(lspci -n -s "$address" | tail -n 1 | cut -d ' ' -f 3) $module"
			fi
		fi
	done
}

uname -a 2>&1 | addinfo "umame -a"

if type lspci >/dev/null 2>&1; then
	lspci -nn  2>&1 | addinfo "lspci -nn"
	lspci -vnn 2>&1 | addinfo "lspci -vnn"

	# List which modules are referring to which PCI devices
	if [ -d /sys/bus/pci/devices ] ; then
		list_loaded_modules | addinfo modulemap
	fi
else
	addfile /proc/pci
	addfile /proc/bus/pci/devices
fi

lsmod 2>&1 | addinfo lsmod
df 2>&1 | addinfo df
free 2>&1 | addinfo free

pccardctl status 2>&1 | addinfo "cardctl status"
pccardctl ident 2>&1 | addinfo "cardctl ident"
cardctl status 2>&1 | addinfo "cardctl status"
cardctl ident 2>&1 | addinfo "cardctl ident"

for file in cpuinfo ioports iomem interrupts meminfo bus/usb/devices \
	bus/input/devices; do
	addfile /proc/$file
done
dmidecode 2>&1 | addinfo dmidecode

if [ "$DEBIAN_FRONTEND" = gtk ]; then
	addfile /proc/fb
	addfile /etc/directfbrc
	if [ -x /usr/lib/directfb-0.9.25/bin/dfbinfo ]; then
		/usr/lib/directfb-0.9.25/bin/dfbinfo 2>&1 | addinfo dfbinfo
	fi
fi
