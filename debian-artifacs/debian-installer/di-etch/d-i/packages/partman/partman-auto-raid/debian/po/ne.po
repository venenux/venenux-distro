# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/ne.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# translation of ne.po to Nepali
# Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2006.
# Shiva Pokharel <pokharelshiva@hotmail.com>, 2006.
# Shyam Krishna Bal <shyam@mpp.org.np>, 2006.
# Shiva Prasad Pokharel <pokharelshiva@hotmail.com>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: ne\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: 2006-10-18 11:05+0545\n"
"Last-Translator: Shiva Prasad Pokharel <pokharelshiva@hotmail.com>\n"
"Language-Team: Nepali <info@mpp.org.np>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2;plural=(n!=1)\n"
"X-Generator: KBabel 1.11.2\n"

#. Type: error
#. Description
#: ../partman-auto-raid.templates:3
msgid "Error while setting up RAID"
msgstr "RAID सेटअप गर्दा त्रुटि"

#. Type: error
#. Description
#: ../partman-auto-raid.templates:3
msgid ""
"An unexpected error occurred while setting up a preseeded RAID configuration."
msgstr "चयन गरिएको RAID कन्फिगरेसन सेटअप गर्दा एउटा अनपेक्षित त्रुटि देखापर्यो ।"

#. Type: error
#. Description
#: ../partman-auto-raid.templates:3
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "विस्तृतका लागि /var/log/syslog जाच्नुहोस् वा अवास्तविक कन्सोल ४ लाई हेर्नुहोस् । "

#. Type: error
#. Description
#: ../partman-auto-raid.templates:20
msgid "Not enough RAID partitions specified"
msgstr "पर्याप्त RAID विभाजनहरू निर्दिष्ट गरिएको छैन"

#. Type: error
#. Description
#: ../partman-auto-raid.templates:20
msgid ""
"There are not enough RAID partitions specified for your preseeded "
"configuration. You need at least 3 devices for a RAID5 array."
msgstr ""
"तपाईँले चयन गर्नु भएको कन्फिगरेशनका लागि पर्याप्त RAID विभाजनहरू निर्दिष्ट गरिएको छैन । "
"तपाईँलाई RAID5 एरेको लागि कम्तिमा ३ यन्त्रहरूको आवश्यक छ ।"
