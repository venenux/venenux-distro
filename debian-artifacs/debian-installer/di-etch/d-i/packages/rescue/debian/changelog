rescue (1.9) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 17:00:36 +0100

rescue (1.8) unstable; urgency=low

  * Do not offer to start a shell if the gtk frontend is used as that is
    currently not supported. Closes: #408437.
  * Add myself to uploaders.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovenian (sl.po) by Matej Kovačič
  * Tamil (ta.po) by drtvasudevan

 -- Frans Pop <fjp@debian.org>  Mon, 29 Jan 2007 04:29:31 +0100

rescue (1.7) unstable; urgency=low

  * Kernels >=2.6.18 use have module raid456 instead of raid5.
    Thanks to Sven Luther for the heads-up (#389079).

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Romanian (ro.po) by Eddy Petrișor
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 16:27:41 +0200

rescue (1.6) unstable; urgency=low

  [ Colin Watson ]
  * Use list-devices to find partitions. Requires di-utils 1.34.

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Indonesian (id.po) by Arief S Fitrianto
  * Latvian (lv.po) by Aigars Mahinovs
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Sat, 16 Sep 2006 11:55:17 +0200

rescue (1.5) unstable; urgency=low

  [ Frans Pop ]
  * mdrun is being deprecated; use mdadm instead; based on patch by
    Martin Krafft.
  * Add Lintian override for standards-version.

  [ Colin Watson ]
  * Fix substitution of device name into rescue/mount-failed.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Catalan (ca.po) by Jordi Mallach
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Finnish (fi.po) by Tapio Lehtonen
  * Gujarati (gu.po) by Kartik Mistry
  * Japanese (ja.po) by Kenshi Muto
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 15 Aug 2006 02:51:05 +0200

rescue (1.4) unstable; urgency=low

  [ Christian Perrier ]
  * Replace "filesystem" by "file system"

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Jun 2006 22:12:06 -0400

rescue (1.3) unstable; urgency=low

  * Revert previous change. Excluding .svn dirs should be done by correctly
    setting up the build environment (which I've done now).

 -- Frans Pop <fjp@debian.org>  Sat, 28 Jan 2006 17:02:52 +0100

rescue (1.2) unstable; urgency=low

  [ Martin Michlmayr ]
  * Make sure we don't end up with .svn dirs in the udeb.

  [ Updated translations ]
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Slovenian (sl.po) by Jure Cuhalev
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 28 Jan 2006 14:17:24 +0000

rescue (1.1) unstable; urgency=low

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Finnish (fi.po) by Tapio Lehtonen
  * Hebrew (he.po) by Lior Kaplan
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Malagasy (pa_IN.po) by Amanpreet Singh Alam
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Slovak (sk.po) by Peter Mann
  * Albanian (sq.po) by Elian Myftiu
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Mon, 23 Jan 2006 20:16:12 +0100

rescue (1.0) unstable; urgency=low

  [ Fabio M. Di Nitto ]
  * Fix rescue/$item/intro to get to the right template.

  [ Colin Watson ]
  * Make README more consistent with reality (recommend type text for intro
    template, not note).
  * Go back to the rescue operation menu if the user backs up from an intro
    question.
  * Bump to 1.0 as rescue is fairly stable now.

  [ Frans Pop ]
  * rescue.d/reboot: Safe to use 'umount -a' again.

  [ Updated translations ]
  * Catalan (ca.po) by Guillem Jover
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Finnish (fi.po) by Tapio Lehtonen
  * Galician (gl.po) by Jacobo Tarrio
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Frans Pop
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Colin Watson <cjwatson@debian.org>  Fri,  6 Jan 2006 13:30:21 +0000

rescue (0.9) unstable; urgency=low

  * Fix out-of-date text in rescue/root.
  * Use umount -a replacement from prebaseconfig to avoid problems in
    initramfs (#317062).
  * Avoid annoying screen-clear in rescue-check.

  [ Updated translations ]
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Colin Watson <cjwatson@debian.org>  Wed, 30 Nov 2005 10:18:32 +0000

rescue (0.8) unstable; urgency=low

  * Use debconf-disconnect from di-utils 1.18.
  * After selecting the root filesystem, display a menu with the things you
    can do to the root filesystem rather than just dropping you into a
    shell. This also provides a mechanism for other udebs to provide custom
    rescue operations.
  * Stop suppressing udeb retrieval with anna/standard_modules, since other
    udebs may now be useful.
  * Remove Standards-Version:, not applicable to udebs.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Colin Watson <cjwatson@debian.org>  Wed, 23 Nov 2005 09:30:14 +0000

rescue (0.7) unstable; urgency=low

  [ Colin Watson ]
  * Update GPL notices with the FSF's new address.

  [ Joey Hess ]
  * Use log-output.

  [ Updated translations ]
  * Catalan (ca.po) by Guillem Jover
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Holger Wansing
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Giuseppe Sacco
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrisor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 17:22:06 +0200

rescue (0.6) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Catalan (ca.po) by Guillem Jover
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Arief S Fitrianto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Yuri Kozlov
    - Albanian (sq.po) by Elian Myftiu
    - Tagalog (tl.po) by Eric Pareja
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 17:11:38 +0300

rescue (0.5) unstable; urgency=low

  * Colin Watson
    - Set anna/standard_modules to false at startup to suppress unnecessary
      udeb retrieval. Requires anna 1.08 (but will degrade gracefully).
  * Updated translations:
    - Czech (cs.po) by Miroslav Kure
    - Greek, Modern (1453-) (el.po) by Kostas Papadimas
    - Japanese (ja.po) by Kenshi Muto
    - Portuguese (Brazil) (pt_BR.po) by Carlos Eduardo Pedroza Santiviago

 -- Colin Watson <cjwatson@debian.org>  Mon,  9 May 2005 16:23:30 +0100

rescue (0.4) unstable; urgency=low

  * Colin Watson
    - Set HOME=/ when chrooting; vim gets upset if HOME is unset.
    - Set a "Rescue mode" info message. Requires cdebconf-udeb 0.75 and
      main-menu 1.03.
    - Add RAID (and LVM-on-RAID) support.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Italian (it.po) by Stefano Canepa
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Colin Watson <cjwatson@debian.org>  Tue,  3 May 2005 12:57:21 +0100

rescue (0.3) experimental; urgency=low

  * Matt Kraai
    - Fix the spelling of "file system".
  * Colin Watson
    - Add LVM support: list all the logical volumes we can find as well as
      just block devices.
  * Updated translations:
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Colin Watson <cjwatson@debian.org>  Tue, 15 Feb 2005 19:51:46 +0000

rescue (0.2) experimental; urgency=low

  * Colin Watson
    - Instead of requiring the user to type in devfs device names, find all
      the block devices on the system and present them as a select list.
      (Ideally this could present more information in partman style.)
  * Note that this contains substitution fixes in translated templates.
  * Updated translations:
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Indonesian (id.po) by Arief S Fitrianto
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Bøkmal, Norwegian (nb.po) by Hans Fredrik Nordhaug
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Colin Watson <cjwatson@debian.org>  Mon,  7 Feb 2005 17:43:12 +0000

rescue (0.1) experimental; urgency=low

  * Initial release.

 -- Colin Watson <cjwatson@debian.org>  Wed,  5 Jan 2005 17:16:22 +0000
