#!/bin/sh
set -e

if [ "$1" = "-v" ]; then
	VERBOSE=1
	shift 1
fi

DATESTAMP=$(date +%Y%m%d%H%M)
MACHINE="$1"
shift 1

if [ -z "$MACHINE" ]; then
	echo "Usage: $0 [-v] machine scheme [scheme ...]" >&2
	exit 1
fi

config () {
	var="$1"
	shift 1
	eval "$var='$@'"
}

use () {
	. $DI_TESTDIR/schemes/$MACHINE/$1
}

overview () {
	grep -v "$MACHINE-$SCHEME " $LOGDIR/overview.log > $LOGDIR/overview.log.new || true
	LANG=C echo "$ARCH ($(date)) $(whoami)@$(hostname | cut -d . -f 1) $MACHINE-$SCHEME $1" >> $LOGDIR/overview.log.new
	mv $LOGDIR/overview.log.new $LOGDIR/overview.log
}

log () {
	local machine=$1
	local scheme=$2
	local logfile=$LOGDIR/$DATESTAMP/$machine-$scheme.log

	if [ "$VERBOSE" = 1 ]; then
		tee $logfile
	else
		cat > $logfile
	fi
}

DIGRESSDIR=$(dirname $0)
cd $DIGRESSDIR

PATH=$PATH:$DIGRESSDIR/boot:$DIGRESSDIR/preboot:$DIGRESSDIR/console:$DIGRESSDIR/test_1:$DIGRESSDIR/test_2:$DIGRESSDIR/shutdown:$DIGRESSDIR/utils
export PATH

if [ -z "$DI_TESTDIR" ]; then
	DI_TESTDIR=$DIGRESSDIR
fi

if [ -z "$DI_TESTDIR" ] || [ ! -e "$DI_TESTDIR/config" ]; then
	echo "Cannot determine DI_TESTDIR" >&2
	exit 1
fi

. $DI_TESTDIR/config

for SCHEME in $@; do
	RETRY=""
	LOGDIR=$DI_TESTDIR/logs
	use common
	use $SCHEME
	
	mkdir -p $LOGDIR/$DATESTAMP
	touch $LOGDIR/overview.log

	overview incomplete

	FAILURE_STATUS_FILE=$(tempfile)
	export FAILURE_STATUS_FILE
	
	ln -sf $LOGDIR/$DATESTAMP/$MACHINE-$SCHEME.log $LOGDIR/$MACHINE-$SCHEME.log
	(if $DIGRESSDIR/test-harness $MACHINE $SCHEME; then
		overview success
	else
		if [ -n "$RETRY" ]; then
			if $DIGRESSDIR/test-harness $MACHINE $SCHEME 2>&1; then
				overview "success second try"
			else
				overview "failed $(cat $FAILURE_STATUS_FILE)"
			fi
		else
			overview "failed $(cat $FAILURE_STATUS_FILE)"
		fi
	fi) 2>&1 | log $MACHINE $SCHEME

	rm -f $FAILURE_STATUS_FILE
done
