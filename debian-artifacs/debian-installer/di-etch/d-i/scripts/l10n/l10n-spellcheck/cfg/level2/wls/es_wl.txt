## Keyboard maps names with hyphens
# Should go in the individual languages lists because of a limitation of the
# spellchecker
amiga-de
amiga-es
amiga-fr
amiga-it
amiga-se
amiga-sg
amiga-us
atari-de
atari-fr
atari-se
atari-uk
atari-us
br-abnt
br-latin
CH-latin
cz-lat
cz-us
de-latin
dk-latin
fi-latin
fr-latin
is-latin
la-latin
lv-latin
mac-de
mac-fr
mac-us-ext
mac-us-std
mac-usb-be
mac-usb-de
mac-usb-de-latin
mac-usb-de-nodeadkeys
mac-usb-dk-latin
mac-usb-dvorak
mac-usb-es
mac-usb-fi-latin
mac-usb-fr
mac-usb-it
mac-usb-pt-latin
mac-usb-se
mac-usb-uk
mac-usb-us
no-latin
pt-latin
se-latin
sg-latin
sk-qwerty
sr-cy
sun-pl

# Technical words with hyphens and dots
# They have to go in the individual languages lists
# because of a limitation of the spellchecker
a-z
aliases.O
base-config
Big
conf.d
dpkg-reconfigure
exim-tls
list-tasks
new-install
popularity-contest
README.SMTP-AUTH
sources.list
t-t
task-desc
task-packages
Reply-To
Return-Path
