Backspace
Checksum
DOS
abort
about
admin
backend
basedir
binary
bitwise
cache
cdrom
character
chmod
chroot
clear
client
close
command
compress
compressor
configuration
configure
contrib
copy
debian
device
disinstallazione
done
dos
emacs
eof
error
ethernet
exec
filesystem
frontend
ftp
group
header
home
host
kernel
lock
main
mount
purge
query
repository
seek
socket
stdout
subject
swap
sync
workgroup
# Words with hyphens and dots which should be here
# but can't because of a limitation in the spellchecker
# They should be copied to languages lists
# and uncommented there
samba-doc
dhcp.conf
log.nmbd
log.smbd
passdb.tdb
smb.conf
