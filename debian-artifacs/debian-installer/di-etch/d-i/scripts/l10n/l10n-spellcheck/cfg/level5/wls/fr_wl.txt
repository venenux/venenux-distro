# Mots joints entre eux
ActionLongue
nomfic
NomFichier
NomPgm
TailleDeb
TailleInst
TailleT�l�chargement
UtilDisque
VerPgm
VersCand
VersInst
�tatLong

# Des mots avec des majuscules en plein milieu car ce sont
# des raccourcis
d�paqUet�
fichier-Config
inconnU

# Des mots coupes parce que ce sont des entrees de menu
# - "G^eler"
eler
# I^nformation
nformation
nstallation
# Annu^ler
Annu
les
ler

# Dans aptitude : on a "%n%nDes paquets..." et c'est normal
nDes
# On a aussi \tPr�cise
tPr�cise
tPr�ciser

# De l'anglais qu'on garde volontairement
Authentication
column
Readline
stat
thread

# Unites
bps
# de "%ss"
ss

# Abr�viations
Eff
N�c
pqt
t�l�ch

# Acronymes francais
AZERTY
Auth
Del
fac
FAI
M-�-J
Pqts
Suppr

# Noms de fichiers specifiques du francais
help-fr.txt
mine-help-fr.txt
README.fr

# Inconnus de aspell
Badaboum
#Franchement, je sais pas ce que c'est que ce cockatrice, mais bon
c�d�rom
cockatrice
courriel
courriels
Crypto
cryptographiques
d�bogage
descripteur
formatage
identificateur
incr�mentale
pr�visualisation
recommendations
recompilation
Reconfiguration
reconfigure
reconfigurer
reconfigurez
rediriger
r�solveur
solveur
superutilisateur
tabulation
T�l�charge
t�l�chargeable
T�l�chargement
t�l�chargement
t�l�chargements
T�l�charger
t�l�charger
T�l�charg�
t�l�charg�s
trilobite
visualisateurs
�mulateurs

# mots precedes de "B" pour cause de mise en gras
BATTENTION
Bcompromettre
BConserver
Bnon
BABSOLUMENT
BGarder
BInstaller
BMettre
BR�installer
BSupprimer

# Idem F
Fc
Fd
Fe
Fn
Fo
Fq
Fr
Fs
Fv

# Idem F
nAptitude
nCertains
nCopyright
nSi
n�tes-vous

# Idem s
sB

# Elisions
d'acc�der
d'Aide
d'apt
d'Aptitude
d'incoming
l'Intel
l'Interface
l'Internet
qu'actuellement
qu'administrateur
qu'alternative

# Mots avec points, noms de fichiers
default.kmap


# Mot composes avec tirets
challenge-r�ponse
Choisissez-la
d�tournement-vers
exp-rationnelle
Faut-il
faut-il
indiquez-la
irez-vous
micro-contr�leurs
mini-tampon
Non-interactive
nom-fichier
non-s�rie
Palm-Pilots
poss�dez-vous
Post-connexion
post-connexion
post-installation
Souhaitez-vous
souhaitez-vous
sous-processus
Voulez-vous
Pr�-connexion
pr�-connexion
Pr�-d�pend
pr�-d�pend
pr�-s�lectionn�
Pr�-visualisation
pr�-�crits

# Words with hyphens and dots, common to all langs
# Because of a limitation in the spellchecker, they have to be copied
# in each language list
console-data
console-tools
debian-tasks.desc
default-wordlist
dict-pre-FHS
dictionaries-common
dir-file
dist-upgrade
diversions-new
diversions-old
divert-to
dpkg-reconfigure
forbid-version
forget-new
Ignore-Trust-Violations
info-dir
INFO-DIR-ENTRY
info-file


suppRim�
tD�tection
uitter
�lectionner
tactive
taffiche
tD�finit
tforce
tFusionne
tpas
tque
treconfigure
tsp�cifie
tSupprime
tutilise

# Mots abreges
Abr�v
Bog
Ctb
Ctrl
directem
dispo
dscs
Entr
EXPIR
horiz
i.e
infos
Inser
maxi
nomr�p
N�c
num
outrepass
perso
posit
pr�c�d
qq
REP
r�p
SQEL
Suppr
v�rif

# Pr�c�d� de "\t"
tN

# De l'anglais qu'on garde volontairement
core
ctrlarea
forking
General
License
mod�les.ll

# Unites
ko

# Mots pr�fix�s
Auto-assembler
pr�-configuration
pr�-configurer
Pr�-D�pend
pr�-d�pend
pr�-d�pendance
pr�-d�pendances

# Acronymes francais
AAAA-MM-JJ
RNIS

# Noms de fichiers specifiques du francais

# Inconnus de aspell
courriel
c�d�rom
c�d�roms
descripteur
d�bogage
d�compactage
d�compacte
d�compacteur
d�compact�
d�compact�s
D�configuration
d�configurer
d�configur�
d�fecteux
d�sactivation
d�sinstallation
d�sinstaller
d�s�lectionn�
formatage
Identificateur
installable
octal
Pr�configuration
pr�configuration
pr�configurer
reblocage
reconfiguration
reconfigurer
Redessiner
redessiner
renommage
r�allocation
r�assembl�s
r�ouvrir
superutilisateur
Surlignage
surlignage
surlign�
surlign�e
T�l�charge
T�l�chargement
t�l�chargement
t�l�charger
t�l�charg�
t�l�charg�s
troncation
versionn�

# mots precedes de qq chose (variable, etc.)
sinfo
tNo
tNom
tT�l�phone

# Elisions
d'acc�der
d'APT
d'apt-extracttemplates
d'apt-get
d'Emacs
d'exactement
d'includes
d'Incoming
d'info
d'Unix
d'acc�der
l'avant-plan
l'ID
l'uid
l'URI

# Mot composes avec tirets
Auto-assemble
avant-plan
contentez-vous
devrez-vous
devriez-vous
d�placez-vous
enregistrez-le
entrez-le
Faut-il
lecture-�criture
manque-t-il
modem-fax
mot-de-passe
multi-partie
multi-octets
nom-de-paquet
non-alphanum�rique
non-captur�
non-disponibles
non-d�tourn�e
non-install�
non-r�pertoire
non-valide
pr�-configuration
pr�-configurer
Pr�-D�pend
pr�-d�pend
pr�-d�pendance
pr�-d�pendances
quasi-totalit�
re-�crire
Reportez-vous
reportez-vous
r�-ouvrir
semi-install�
Souhaitez-vous
sous-liste
sous-processus
tubez-les
voulez-vous

�chec-conFig
fonctionne-t

# Words with hyphens and dots which should be here
# but can't because of a limitation in the spellchecker
# They should be copied to languages lists
# and uncommented there
abort-after
apt-cache
apt-cdrom
apt.conf
apt-config
apt-extracttemplates
apt-ftparchive
apt-get
apt-sortpkgs
apt-utils
assert-long-filenames
assert-multi-conrep
assert-support-predepends
assert-working-epoch
auto-deconfigure
auto-select
bad-path
bad-verify
base-dir
binary-i
build-dep
build-dependencies
clear-avail
clear-selections
close-on-exec
command-fd
compare-versions
configure-any
Configured-Version
config-version
Config-Version
Content-Length
Content-Range
create-home
debconf-communicate
debconf-mergetemplate
debsig-verify
default-priority
depends-version
dep.s
dist-upgrade
dpkg-deb
dpkg-dev
dpkg-reconfigure
dpkg-scanpackages
dpkg-scansources
dpkg-split
drop-old-templates
dry-run
dselect-upgrade
fix-missing
force-help
Force-LoopBreak
force-yes
forget-old-unavail
fsys-tarfile
ge-nl
get-selections
GNU-Linux
gt-nl
home-dir
ignore-depends
install-keymap
k-blocs
keep-all
keep-old
keep-tokens
Kim-Minh
le-nl
lock-time
login.defs
lt-nl
merge-avail
MS-DOS
no-act
no-check
no-debsig
no-delink
no-force
non-free
non-us
not-root
overwrite-dir
overwrite-diverted
Package-Revision
po-debconf
predep-package
print-architecture
print-avail
print-gnu-build-architecture
print-installation-architecture
print-libgcc-file-name
record-avail
refuse-downgrade
remove-all
remove-essential
remove-exactly
remove-reinstreq
selected-only
select-default-ispell
select-default-wordlist
sensible-pager
set-selections
set-UID
shift-V
skip-same-version
s.old
source-override
sources.list
START-INFO-DIR-ENTRY
status-fd
statoverride-old
t-D
trivial-only
unseen-only
update-avail
update-alternatives
yet-to-unpack
YYYY-MM-DD
YYYY-MM-DD
zone-ctrl
