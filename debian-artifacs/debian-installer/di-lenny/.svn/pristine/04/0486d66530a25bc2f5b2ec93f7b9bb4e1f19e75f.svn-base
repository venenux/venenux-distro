# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# German messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Holger Wansing <linux@wansing-online.de>, 2008.
# Jens Seidel <jensseidel@users.sf.net>, 2005, 2006, 2007, 2008.
# Dennis Stampfer <seppy@debian.org>, 2003, 2004, 2005.
# Alwin Meschede <ameschede@gmx.de>, 2003, 2004.
# Bastian Blank <waldi@debian.org>, 2003.
# Jan Luebbe <jluebbe@lasnet.de>, 2003.
# Thorsten Sauter <tsauter@gmx.net>, 2003.
#
# This file is maintained by Jens Seidel <jensseidel@users.sf.net>
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-04-22 22:49+0000\n"
"PO-Revision-Date: 2008-05-05 12:08+0200\n"
"Last-Translator: Jens Seidel <jensseidel@users.sf.net>\n"
"Language-Team: Debian German <debian-l10n-german@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:1001
msgid "Starting up the partitioner"
msgstr "Lade Programm für die Partitionierung"

#. Type: text
#. Description
#. :sl1:
#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:2001 ../partman-base.templates:24001
msgid "Please wait..."
msgstr "Bitte warten ..."

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:3001
msgid "Scanning disks..."
msgstr "Durchsuche Festplatten ..."

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:4001
msgid "Detecting file systems..."
msgstr "Ermittle Dateisysteme ..."

#. Type: error
#. Description
#. :sl2:
#: ../partman-base.templates:5001
msgid "Device in use"
msgstr "Gerät wird verwendet"

#. Type: error
#. Description
#. :sl2:
#: ../partman-base.templates:5001
msgid ""
"No modifications can be made to the device ${DEVICE} for the following "
"reasons:"
msgstr "Das Gerät ${DEVICE} kann wegen Folgendem nicht modifiziert werden:"

#. Type: error
#. Description
#. :sl2:
#: ../partman-base.templates:6001
msgid "Partition in use"
msgstr "Partition wird verwendet"

#. Type: error
#. Description
#. :sl2:
#. This should be translated as "partition *number* ${PARTITION}"
#. In short, ${PARTITION} will indeed contain the partition
#. NUMBER and not the partition NAME
#: ../partman-base.templates:6001
msgid ""
"No modifications can be made to the partition #${PARTITION} of device "
"${DEVICE} for the following reasons:"
msgstr ""
"Die Partition ${PARTITION} des Geräts ${DEVICE} kann wegen Folgendem nicht "
"modifiziert werden:"

#. Type: select
#. Description
#. :sl1:
#: ../partman-base.templates:9001
msgid ""
"This is an overview of your currently configured partitions and mount "
"points. Select a partition to modify its settings (file system, mount point, "
"etc.), a free space to create partitions, or a device to initialize its "
"partition table."
msgstr ""
"Dies ist eine Übersicht über Ihre konfigurierten Partitionen und "
"Einhängepunkte. Wählen Sie eine Partition, um Änderungen vorzunehmen "
"(Dateisystem, Einhängepunkt, usw.), freien Speicher, um Partitionen "
"anzulegen oder ein Gerät, um eine Partitionstabelle zu erstellen."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-base.templates:10001
msgid "Continue with the installation?"
msgstr "Mit der Installation fortfahren?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-base.templates:10001
msgid ""
"No partition table changes and no creation of file systems have been planned."
msgstr ""
"Es wurden keine Änderungen der Partitionstabelle oder Dateisysteme "
"festgelegt."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-base.templates:10001
msgid ""
"If you plan on using already created file systems, be aware that existing "
"files may prevent the successful installation of the base system."
msgstr ""
"Wenn Sie ein bereits vorhandenes Dateisystem zur Installation benutzen "
"möchten, beachten Sie bitte, dass die darauf vorhandenen Daten eventuell die "
"Installation behindern."

#. Type: boolean
#. Description
#. :sl1:
#: ../partman-base.templates:11001
msgid "Write the changes to disks?"
msgstr "Änderungen auf die Festplatten schreiben?"

#. Type: boolean
#. Description
#. :sl1:
#: ../partman-base.templates:11001
msgid ""
"If you continue, the changes listed below will be written to the disks. "
"Otherwise, you will be able to make further changes manually."
msgstr ""
"Wenn Sie fortfahren, werden alle unten aufgeführten Änderungen auf die "
"Festplatte(n) geschrieben. Andernfalls können Sie weitere Änderungen manuell "
"durchführen."

#. Type: boolean
#. Description
#. :sl1:
#: ../partman-base.templates:11001
msgid ""
"WARNING: This will destroy all data on any partitions you have removed as "
"well as on the partitions that are going to be formatted."
msgstr ""
"WARNUNG: Dies zerstört alle Daten auf Partitionen, die Sie entfernt haben, "
"sowie alle Daten auf Partitionen, die formatiert werden sollen."

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:12001
msgid "The following partitions are going to be formatted:"
msgstr "Die folgenden Partitionen werden formatiert:"

#. Type: text
#. Description
#. :sl2:
#. for example: "partition #6 of IDE0 master as ext3 journaling file system"
#: ../partman-base.templates:13001
msgid "partition #${PARTITION} of ${DEVICE} as ${TYPE}"
msgstr "Partition ${PARTITION} auf ${DEVICE} mit ${TYPE}"

#. Type: text
#. Description
#. :sl2:
#. for devices which have no partitions
#. for example: "LVM VG Debian, LV Root as ext3 journaling file system"
#: ../partman-base.templates:14001
msgid "${DEVICE} as ${TYPE}"
msgstr "${DEVICE} als ${TYPE}"

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:15001
msgid "The partition tables of the following devices are changed:"
msgstr "Die Partitionstabellen folgender Geräte wurden geändert:"

#. Type: select
#. Description
#. :sl2:
#: ../partman-base.templates:16001
msgid "What to do with this device:"
msgstr "Wie mit diesem Gerät verfahren:"

#. Type: select
#. Description
#. :sl2:
#: ../partman-base.templates:17001
msgid "How to use this free space:"
msgstr "Wie mit freiem Speicher fortfahren:"

#. Type: select
#. Description
#. :sl2:
#: ../partman-base.templates:18001
msgid "Partition settings:"
msgstr "Partitionseinstellungen:"

#. Type: select
#. Description
#. :sl2:
#: ../partman-base.templates:18001
msgid ""
"You are editing partition #${PARTITION} of ${DEVICE}. ${OTHERINFO} "
"${DESTROYED}"
msgstr ""
"Sie bearbeiten Partition ${PARTITION} auf ${DEVICE}. ${OTHERINFO} "
"${DESTROYED}"

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:19001
msgid "This partition is formatted with the ${FILESYSTEM}."
msgstr "Die Partition ist mit ${FILESYSTEM} formatiert."

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:20001
msgid "No existing file system was detected in this partition."
msgstr "Auf dieser Partition wurde kein vorhandenes Dateisystem gefunden."

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:21001
msgid "All data in it WILL BE DESTROYED!"
msgstr "Alle Daten darauf WERDEN ZERSTÖRT!"

#. Type: note
#. Description
#. :sl2:
#: ../partman-base.templates:22001
msgid "The partition starts from ${FROMCHS} and ends at ${TOCHS}."
msgstr "Die Partition beginnt bei ${FROMCHS} und endet bei ${TOCHS}."

#. Type: note
#. Description
#. :sl2:
#: ../partman-base.templates:23001
msgid "The free space starts from ${FROMCHS} and ends at ${TOCHS}."
msgstr "Der freie Speicher beginnt bei ${FROMCHS} und endet bei ${TOCHS}."

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:25001
msgid "Partitions formatting"
msgstr "Partitionen formatieren"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:26001
msgid "Processing..."
msgstr "Bearbeite ..."

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:28001 ../partman-base.templates:32001
msgid "Show Cylinder/Head/Sector information"
msgstr "Anzeigen der Zylinder/Kopf/Sektor-Informationen"

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:29001
msgid "Done setting up the partition"
msgstr "Anlegen der Partition beenden"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:30001
msgid "Finish partitioning and write changes to disk"
msgstr "Partitionierung beenden und Änderungen übernehmen"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:31001
msgid "Undo changes to partitions"
msgstr "Änderungen an den Partitionen rückgängig machen"

#. Type: text
#. Description
#. :sl2:
#: ../partman-base.templates:33001
#, no-c-format
msgid "Dump partition info in %s"
msgstr "Anzeige der Partitionsinformationen in %s"

#. Type: text
#. Description
#. Keep short
#. :sl1:
#: ../partman-base.templates:34001
msgid "FREE SPACE"
msgstr "FREIER SPEICHER"

#. Type: text
#. Description
#. "unusable free space".  No more than 8 symbols.
#. :sl1:
#: ../partman-base.templates:35001
msgid "unusable"
msgstr "unben."

#. Type: text
#. Description
#. "primary partition".  No more than 8 symbols.
#. :sl1:
#: ../partman-base.templates:36001
msgid "primary"
msgstr "primär"

#. Type: text
#. Description
#. "logical partition".  No more than 8 symbols.
#. :sl1:
#: ../partman-base.templates:37001
msgid "logical"
msgstr "logisch"

#. Type: text
#. Description
#. "primary or logical".  No more than 8 symbols.
#. :sl1:
#: ../partman-base.templates:38001
msgid "pri/log"
msgstr "pri/log"

#. Type: text
#. Description
#. How to print the partition numbers in your language
#. Examples:
#. %s.
#. No %s
#. N. %s
#. :sl1:
#: ../partman-base.templates:39001
#, no-c-format
msgid "#%s"
msgstr "Nr. %s"

#. Type: text
#. Description
#. For example IDE0 master (hda)
#. :sl1:
#: ../partman-base.templates:40001
#, no-c-format
msgid "IDE%s master (%s)"
msgstr "IDE%s Master (%s)"

#. Type: text
#. Description
#. For example IDE1 slave (hdd)
#. :sl1:
#: ../partman-base.templates:41001
#, no-c-format
msgid "IDE%s slave (%s)"
msgstr "IDE%s Slave (%s)"

#. Type: text
#. Description
#. For example IDE1 master, partition #5 (hdc5)
#. :sl1:
#: ../partman-base.templates:42001
#, no-c-format
msgid "IDE%s master, partition #%s (%s)"
msgstr "IDE%s Master, Partition #%s (%s)"

#. Type: text
#. Description
#. For example IDE2 slave, partition #5 (hdf5)
#. :sl1:
#: ../partman-base.templates:43001
#, no-c-format
msgid "IDE%s slave, partition #%s (%s)"
msgstr "IDE%s Slave, Partition #%s (%s)"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:44001
#, no-c-format
msgid "SCSI%s (%s,%s,%s) (%s)"
msgstr "SCSI%s (%s,%s,%s) (%s)"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:45001
#, no-c-format
msgid "SCSI%s (%s,%s,%s), partition #%s (%s)"
msgstr "SCSI%s (%s,%s,%s), Partition #%s (%s)"

#. Type: text
#. Description
#. :sl3:
#: ../partman-base.templates:46001
#, no-c-format
msgid "RAID%s device #%s"
msgstr "RAID%s Gerät #%s"

#. Type: text
#. Description
#. :sl3:
#: ../partman-base.templates:47001
#, no-c-format
msgid "Encrypted volume (%s)"
msgstr "Verschlüsseltes Volume (%s)"

#. Type: text
#. Description
#. For example: Serial ATA RAID isw_dhiiedgihc_Volume0 (mirror)
#. :sl3:
#: ../partman-base.templates:48001
#, no-c-format
msgid "Serial ATA RAID %s (%s)"
msgstr "Serial ATA RAID %s (%s)"

#. Type: text
#. Description
#. For example: Serial ATA RAID isw_dhiiedgihc_Volume01 (partition #1)
#. :sl3:
#: ../partman-base.templates:49001
#, no-c-format
msgid "Serial ATA RAID %s (partition #%s)"
msgstr "Serial ATA RAID %s (Partition %s)"

#. Type: text
#. Description
#. Translators: "multipath" is a pretty tricky term to translate
#. You'll find some documentation about it at
#. http://www.redhat.com/docs/manuals/csgfs/browse/4.6/DM_Multipath/index.html
#. "Short" definition:
#. Device Mapper Multipathing (DM-Multipath) allows you to configure
#. multiple I/O paths between server nodes and storage arrays into a
#. single device. These I/O paths are physical SAN connections that can
#. include separate cables, switches, and controllers. Multipathing
#. aggregates the I/O paths, creating a new device that consists of the
#. aggregated paths.
#. WWID stands for World-Wide IDentification
#. :sl3:
#: ../partman-base.templates:50001
#, no-c-format
msgid "Multipath %s (WWID %s)"
msgstr "Multipath %s (WWID %s)"

#. Type: text
#. Description
#. :sl3:
#: ../partman-base.templates:51001
#, no-c-format
msgid "Multipath %s (partition #%s)"
msgstr "Multipath %s (Partition %s)"

#. Type: text
#. Description
#. :sl3:
#: ../partman-base.templates:52001
#, no-c-format
msgid "LVM VG %s, LV %s"
msgstr "LVM VG %s, LV %s"

#. Type: text
#. Description
#. :sl3:
#: ../partman-base.templates:53001
#, no-c-format
msgid "Loopback (loop%s)"
msgstr "Loopback (loop%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:54001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:55001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s), Partition #%s"

#. Type: text
#. Description
#. eg. Virtual disk 1 (xvda)
#. :sl4:
#: ../partman-base.templates:56001
#, no-c-format
msgid "Virtual disk %s (%s)"
msgstr "Virtuelle Festplatte %s (%s)"

#. Type: text
#. Description
#. eg. Virtual disk 1, partition #1 (xvda1)
#. :sl4:
#: ../partman-base.templates:57001
#, no-c-format
msgid "Virtual disk %s, partition #%s (%s)"
msgstr "Virtuelle Festplatte %s, Partition %s (%s)"

#. Type: text
#. Description
#. :sl1:
#: ../partman-base.templates:58001
msgid "Cancel this menu"
msgstr "Dieses Menü verlassen"

# FIXME: Text wird nach "partitionie" abgeschnitten, da Dialoginhalt zu klein ist
# (Typ der neuen Partition)
#. Type: text
#. Description
#. Main menu entry
#. :sl1:
#: ../partman-base.templates:59001
msgid "Partition disks"
msgstr "Festplatten partitionieren"
