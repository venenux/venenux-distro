<!-- retain these comments for translator revision tracking -->
<!-- original version: 56425 -->
<!-- updated 39614:54017 by Felipe Augusto van de Wiel (faw) 2008.07.17 -->

 <sect1 condition="supports-tftp" id="install-tftp">
 <title>Preparando os arquivos para inicialização via rede usando TFTP</title>
<para>

Caso sua máquina esteja conectada a uma rede de área local, é possível
inicia-la através da rede a partir de outra máquina
usando o servidor TFTP. Se tem a intenção de iniciar o sistema
de instalação para outra arquitetura, os arquivos de inicialização
precisarão ser colocados em localizações específicas da máquina e
a máquina configurada para suportar inicialização em sua máquina
específica.

</para><para>

Você precisará configurar um servidor TFTP e, para muitas máquinas, um
servidor DHCP<phrase condition="supports-rarp">, ou um servidor
RARP</phrase><phrase condition="supports-bootp">, ou um servidor
BOOTP</phrase>.

</para><para>

<phrase condition="supports-rarp">O Reverse Address Resolution Protocol (RARP) é
o único método para dizer aos clientes qual endereço IP usar para si mesmo.
Outro método é usar o protocolo BOOTP.</phrase>

<phrase condition="supports-bootp">O BOOTP é um protocolo IP que informa
um computador de seu endereço IP e onde na rede será obtida a imagem de
inicialização.</phrase>

<phrase arch="m68k">Outra alternativa ainda existe nos sistemas
VMEbus: o endereço IP pode ser manualmente configurado na ROM de
inicialização.</phrase>

O DHCP (Dynamic Host Configuration Protocol) é uma extensão mais flexível,
compatível com versões mais antigas do BOOTP. Alguns sistemas somente podem
ser configurados via DHCP.

</para><para arch="powerpc">

Para o PowerPC, se tiver uma máquina Power Macintosh NewWorld,
será uma boa idéia de usar o DHCP ao invés do BOOTP. Algumas das
últimas máquinas sã incapazes de inicializar usando o BOOTP.

</para><para arch="alpha">

Ao contrário do Open Firmware encontrado em máquinas Sparc e PowerPC,
o console SRM <emphasis>não</emphasis> usará RARP para obter seu
endereço IP, e então você deverá usar o BOOTP para inicializar via
rede seu Alpha<footnote>

<para>
Os sistemas Alpha também podem ser inicializados via rede usando o
DECNet MOP (Maintenance Operations Protocol), mas isto nao será
discutido aqui. Presumivelmente, seu
operador local do OpenVMS estará satisfeito em ajuda-lo se você
tiver que esquentar a cabeça para usar o MOP para inicializar o
Linux em seu Alpha.
</para>

</footnote>. Você também poderá entrar com a configuração IP de suas
interfaces de rede diretamente no console SRM.

</para><para arch="hppa">

Algumas máquinas HPPA antigas (e.g. 715/75) usarão o RBOOTD ao invés do BOOTP.
Há um pacote <classname>rbootd</classname> disponível no Debian.

</para><para>

O protocolo Trivial File Transfer Protocol (TFTP) é usado para servidor
uma imagem de inicialização ao cliente. Teoricamente, qualquer
servidor, em qualquer plataforma que implementa estes protocolos poderá
ser usados. Nos exemplos desta seção, nós mostraremos comando para
o SunOS 4.x, SunOS 5.x (a.k.a. Solaris), e para o GNU/Linux.

<note arch="x86"><para>

Para utilizar o método Pre-boot Execution Environment (PXE) de inicialização
através de TFTP, você precisará configurar um servidor TFTP coom suporte a
<userinput>tsize</userinput>. Em um servidor &debian; os pacotes
<classname>atftpd</classname> e <classname>tftpd-hpa</classname> se
qualificam; nós recomendamos o pacote <classname>tftpd-hpa</classname>.

</para></note>

</para>

&tftp-rarp.xml;
&tftp-bootp.xml;
&tftp-dhcp.xml;

  <sect2 id="tftpd">
  <title>Ativando o servidor TFTP</title>
<para>

Para ter um servidor TFTP funcionando, primeiro deverá ter certeza
que o <command>tftpd</command> está ativado. Ele normalmente é ativado
através da seguinte linha no seu arquivo <filename>/etc/inetd.conf</filename>:

<informalexample><screen>
tftp dgram udp wait nobody /usr/sbin/tcpd in.tftpd /tftpboot
</screen></informalexample>

Os pacotes da Debian geralmente configurarão isto corretamente por padrão
quando forem instalados.

</para>
<note><para>

Historicamente, servidores TFTP usavam <filename>/tftpboot</filename> como
diretório para servir imagens. No entanto, pacotes &debian; podem usar outros
diretórios para serem compatíveis com a <ulink url="&url-fhs-home;">Filesystem
Hierarchy Standard</ulink> (Padrão de Hierarquia de Diretórios). Por exemplo,
<classname>tftpd-hpa</classname> por padrão usa
<filename>/var/lib/tftpboot</filename>. Você pode ter que ajustar os exemplos
de configuração nesta seção.

</para></note>
<para>

Olhe em <filename>/etc/inetd.conf</filename> e lembre-se do diretório que foi
usado como argumento do <command>in.tftpd</command><footnote>

<para>
Todas as alternativas <command>in.tftpd</command> disponíveis no Debian
deveriam, por padrão, registrar nos logs do sistemas todas as requisições
TFTP. Algumas delas dão suporte ao argumento <userinput>-v</userinput>
para aumentar o nível de detalhes (<quote>verbose</quote>).
É recomendado verificar estas mensagens de log em caso de problemas
de inicialização pois elas são um bom ponto de partida para diagnostica
a causa dos erros.
</para>

</footnote>; você precisará disto abaixo.
Se você tiver que mudar o <filename>/etc/inetd.conf</filename>, você terá
que notificar o processo em execução <command>inetd</command> de que o
arquivo foi modificado. Em máquinas Debian, execute
<userinput>/etc/init.d/inetd reload</userinput>; em outras máquinas,
encontre o ID do processo do <command>inetd</command> e execute o
comando <userinput>kill -HUP <replaceable>inetd-pid</replaceable></userinput>.

</para><para arch="mips">

Se tiver a intenção de instalar a Debian em uma máquina SGI e seu servidor
TFTP é uma máquina GNU/Linux executando o kernel do Linux 2.4, você precisará
configurar o seguinte em seu servidor:

<informalexample><screen>
# echo 1 &gt; /proc/sys/net/ipv4/ip_no_pmtu_disc
</screen></informalexample>

para desligar a descoberta de caminho MTU, caso contrário
o PROM do SGI não poderá baixar o kernel. Além disso, tenha
certeza que os pacotes TFTP são enviados de uma porta de origem
que não seja maior que 32767 ou o download irá parar após o
primeiro pacote. Novamente o Linux 2.4.x causando bugs na PROM
você poderá evitar isto fazendo

<informalexample><screen>
echo "2048 32767" &gt; /proc/sys/net/ipv4/ip_local_port_range
</screen></informalexample>

para ajustar a faixa de portas de origem que o servidor TFTP
do Linux utilizará.

</para>
  </sect2>

  <sect2 id="tftp-images">
  <title>Movendo as imagens TFTP para o Local</title>
<para>

Como próximo passo, coloque a imagem de inicialização TFTP
que precisa, como encontrada no
<xref linkend="where-files"/> no diretório de imagens de inicialização
do <command>tftpd</command>. Você pode ter que fazer um link
deste arquivo para o arquivo que o <command>tftpd</command> usará
para inicializar em cliente em particular. Infelizmente, o nome do
arquivo é determinado pelo cliente TFTP e não existem padrões
rígidos.

</para><para arch="powerpc">

Em máquinas Power Macintosh NewWorld, você precisará configurar
o gerenciador de partida <command>yaboot</command>
como a imagem de inicialização do TFTP. O <command>Yaboot</command>
então copiará as imagens de kernel e disco em RAM via TFTP. Você terá
que baixar os arquivos a seguir do diretório <filename>netboot/</filename>:

<itemizedlist>
<listitem><para>

<filename>vmlinux</filename>

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename>

</para></listitem>
<listitem><para>

<filename>yaboot</filename>

</para></listitem>
<listitem><para>

<filename>yaboot.conf</filename>

</para></listitem>
<listitem><para>

<filename>boot.msg</filename>

</para></listitem>
</itemizedlist>

</para><para arch="x86">
Para a inicialização usando o PXE, tudo que precisa fazer já está feito no
arquivo tar <filename>netboot/netboot.tar.gz</filename>. Simplesmente
descompacte este arquivo no diretório de imagem de inicialização do
<command>tftpd</command>. Tenha certeza que seu servidor DHCP está configurado
para passar o arquivo <filename>pxelinux.0</filename> através do
<command>tftpd</command> para inicializar através dele.

</para><para arch="ia64">
Para inicialização através do PXE, tudo que precisa fazer já está feito
no arquivo tar <filename>netboot/netboot.tar.gz</filename>. Simplesmente
descompacte este arquivo para o diretório de imagem de <command>tftpd</command>.
Tenha certeza que seu servidor dhcp está configurado para passar o arquivo
<filename>/debian-installer/ia64/elilo.efi</filename> para o
<command>tftpd</command> para inicializar através dele.

</para>

   <sect3 arch="alpha">
   <title>Inicialização através de TFTP no Alpha</title>
<para>
No Alpha, você deverá especificar um nome de arquivo (como um
caminho relativo ao diretório da imagem de inicialização)
usando o argumento <userinput>-file</userinput> ou o comando
<userinput>boot</userinput> do SRM ou definindo a variável de
ambiente <userinput>BOOT_FILE</userinput>. Alternativamente, o nome
do arquivo deverá ser fornecido via BOOTP (no <command>dhcpd</command> da ISC,
use a diretiva <userinput>filename</userinput>). Ao contrário da
Open Firmware,  <emphasis>não existe nome padrão</emphasis> no SRM, assim
você <emphasis>deverá</emphasis> especificar um nome de arquivo
usando um destes métodos.

</para>
   </sect3>

   <sect3 arch="sparc">
   <title>Inicialização através TFTP no SPARC</title>
<para>

Algumas arquiteturas SPARC adicionaram nomes de sub-arquiteturas, tais como
<quote>SUN4M</quote> ou <quote>SUN4C</quote>, ao nome de arquivo. Assim, se a
sub-arquitetura do seu sistema é SUN4C e, seu endereço IP é 192.168.1.3, o
nome do arquivo deverá ser <filename>C0A80103.SUN4C</filename>. Contudo,
existem ainda sub-arquiteturas onde o arquivo que o cliente procura é apenas
<filename>ip-do-cliente-em-hexa</filename>. Um método fácil de determinar o
código hexadecimal para o endereço IP é executar o seguinte comando no
interpretador de comandos (assumindo que o IP da máquina é 10.0.0.4):

<informalexample><screen>
$ printf '%.2x%.2x%.2x%.2x\n' 10 0 0 4
</screen></informalexample>

Para obter o nome do arquivo correto, vocé precisará mudar todas as letras
para maiúsculo e, se necessário, adicionar o nome da sub-arquitetura.

</para><para>

Se você fez tudo isso corretamente, executando o comando <userinput>boot
net</userinput> a partir do OpenPROM deveria carregar a imagem. Se a
imagem não for encontrada, tente verificar os logs do seu servidor tftp
para ver qual nome de imagem está sendo requisitado.

</para><para>

Você também poderá forçar alguns sistemas sparc a procurar por
nomes de arquivos específicos adicionando-o no final da linha de
comando, por exemplo
<userinput>boot net my-sparc.image</userinput>. Este arquivo também
deverá residir no diretório que o servidor TFTP procura.

</para>
   </sect3>

   <sect3 arch="m68k">
   <title>Inicialização TFTP no BVM/Motorola</title>
<para>

Para os sistemas BVM e Motorola VMEbus copie os arquivos
&bvme6000-tftp-files; para <filename>/tftpboot/</filename>.

</para><para>

A seguir, configure o servidor BOOTP ou boot ROMs para
carregar inicialmente o arquivo <filename>tftplilo.bvme</filename> ou
<filename>tftplilo.mvme</filename> a partir do servidor TFTP. Veja o
arquivo <filename>tftplilo.txt</filename> de sua sub-arquitetura para
informações adicionais específicas de sistema.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Inicialização TFTP nos SGI</title>
<para>

Nas m�quinas SGI você poderá usar o <command>bootpd</command> para
fornecer o nome do arquivo TFTP. Ele poderá ser fornecido na
forma <userinput>bf=</userinput> no <filename>/etc/bootptab</filename> ou
como opção <userinput>filename=</userinput> no arquivo
<filename>/etc/dhcpd.conf</filename>.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Inicialização através de TFTP nas Broadcom BCM91250A e BCM91480B</title>
<para>

Você não precisa configurar o DHCP de uma forma especial porque você
passará o caminho completo do arquivo que será carregado para o CFE.

</para>
   </sect3>
  </sect2>

<!-- FIXME: commented out since it seems too old to be usable and a current
            way is not known

  <sect2 id="tftp-low-memory">
  <title>Instalação via TFTP em sistemas com pouca memória</title>
<para>

Em alguns sistemas, o disco RAM de instalação padrão, combinado
com os requerimentos de memória da imagem de inicialização do TFTP,
não cabem na memória. Neste caso, você ainda poderá instalar usando
o TFTP, você terá que ir através do passo adicional de montagem
do sistema de arquivos raiz via NFS pela rede também. Este tipo de
configuração também é apropriada para clientes sem discos (diskless) ou
sem dados (dataless).

</para><para>

Primeiro, siga todos os passos acima descritos em <xref linkend="install-tftp"/>.

<orderedlist>
<listitem><para>

Copie a imagem do kernel do Linux para seu servidor TFTP
usando a imagem <userinput>a.out</userinput> para a arquitetura
que estiver inicializando.

</para></listitem>
<listitem><para>

Descompacte o arquivo raiz tar em seu servidor NFS (pode ser o mesmo
sistema que seu servidor TFTP):

<informalexample><screen>
# cd /tftpboot
# tar xvzf root.tar.gz
</screen></informalexample>

Tenha certeza de utilizar o comando <command>tar</command> da GNU (ao invés
de outros programas, como o do SunOS, que manipula os dispositivos de
forma incorreta como texto plano).

</para></listitem>
<listitem><para>

Exporte seu diretório <filename>/tftpboot/debian-sparc-root</filename>
com acesso root para seus clientes. E.g., adicione a seguinte linha
ao seu arquivo <filename>/etc/exports</filename> (sintaxe do GNU/Linux,
deverá ser similar no SunOS):

<informalexample><screen>
/tftpboot/debian-sparc-root <replaceable>client</replaceable>(rw,no_root_squash)
</screen></informalexample>

NOTA: <replaceable>cliente</replaceable> é o nome de estação ou endereço IP
reconhecido pelo servidor para o sistema que deseja inicializar.

</para></listitem>
<listitem><para>

Crie um link simbólico do endereço IP do seu cliente em notação
pontuada para o arquivo <filename>debian-sparc-root</filename> no
diretório <filename>/tftpboot</filename> directory. Por exemplo,
caso o endereço IP do cliente seja 192.168.1.3, faça:

<informalexample><screen>
# ln -s debian-sparc-root 192.168.1.3
</screen></informalexample>

</para></listitem>
</orderedlist>

</para>

  </sect2>

  <sect2 condition="supports-nfsroot">
  <title>Instalando com o TFTP e NFS root</title>
<para>

A instalação com TFTP e NFS raiz é similar a
<xref linkend="tftp-low-memory"/> porque você não precisará
carregar o disco RAM mas inicializará através do seu novo
sistema de arquivos NFS criado. Você precisará então substituir o link
simbólico para a imagem tftpboot por um link simbólico apontando para
uma imagem de kernel (por exemplo,
<filename>linux-a.out</filename>).

</para><para>

O RARP/TFTP requer que todos os daemons estejam sendo executados no
mesmo servidor (a estação de trabalho que estiver enviando requisições
TFTP de volta para o servidor que respondeu a requisição RARP anterior).

</para>

  </sect2>
END FIXME -->
 </sect1>
