<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 untranslated -->

  <sect2 arch="arm" id="boot-tftp"><title>Booting from TFTP</title>

&boot-installer-intro-net.xml;

  <sect3 arch="arm"><title>Booting from TFTP on NetWinder</title>

<para>

NetWinders have two network interfaces: The 10Mbps NE2000-compatible
card is <filename>eth0</filename> and the 100Mbps Tulip card is
<filename>eth1</filename>.

</para><note><para>

You need NeTTrom 2.2.1 or later to boot the
installation system. NeTTrom 2.3.3 is recommended: get these files
from 
<ulink url="ftp://ftp.netwinder.org/pub/netwinder/firmware/"></ulink>:
<itemizedlist>
<listitem><para>

<filename>nettrom-2.3-3.armv4l.rpm</filename>

</para></listitem>
<listitem><para>

<filename>nettrom-2.3.3.bin</filename>

</para></listitem>
<listitem><para>

<filename>nettrom-2.3.3.bin.md5sum</filename>

</para></listitem>
</itemizedlist>

</para></note><para>

After rebooting and interrupting the boot process during the countdown, you 
must first configure the network either with a static address:
<informalexample><screen>

    NeTTrom command-> setenv eth0_ip 192.168.0.10/24

</screen></informalexample>
where 24 is the number of set bits in the netmask, or a dynamic address:
<informalexample><screen>

    NeTTrom command-> boot diskless

</screen></informalexample>

</para><para>

You may also need to configure the <userinput>route1</userinput>
settings if the TFTP
server is not on the local subnet.  The rest of the config is pretty 
standard (the save-all step is optional):
<informalexample><screen>

    NeTTrom command-> setenv kerntftpserver 192.168.0.1
    NeTTrom command-> setenv kerntftpfile tftpboot.img
    NeTTrom command-> save-all
    NeTTrom command-> setenv netconfig_eth0 flash
    NeTTrom command-> setenv kernconfig tftp

</screen></informalexample>

</para><para>

Only the last two of these interfere with normal disk booting, so it is 
safe to <command>save-all</command> right before it, which will
store the network settings in case you need to boot from the network
again. Use the <command>printenv</command> command to review your
environment settings. Finally, if your <envar>cmdappend</envar>
NeTTrom variable has the <option>noinitrd</option> option (which is  
necessary to boot 2.4 kernels), you must remove it so the downloaded 
kernel can boot with its attached ramdisk.

</para>
  </sect3>

  <sect3 arch="arm"><title>Booting from TFTP on CATS</title>

<para>

On CATS machines, use <command>boot de0:</command> or similar at the
Cyclone prompt.

</para>
   </sect3>
  </sect2>



  <sect2 arch="arm"><title>Booting from CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

To boot a CD-ROM from the Cyclone console prompt, use the command
<command>boot cd0:cats.bin</command>

</para>
  </sect2>
