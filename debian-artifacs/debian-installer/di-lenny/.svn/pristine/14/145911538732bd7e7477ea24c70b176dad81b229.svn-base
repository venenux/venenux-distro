# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# German messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Holger Wansing <linux@wansing-online.de>, 2008.
# Jens Seidel <jensseidel@users.sf.net>, 2005, 2006, 2007, 2008.
# Dennis Stampfer <seppy@debian.org>, 2003, 2004, 2005.
# Alwin Meschede <ameschede@gmx.de>, 2003, 2004.
# Bastian Blank <waldi@debian.org>, 2003.
# Jan Luebbe <jluebbe@lasnet.de>, 2003.
# Thorsten Sauter <tsauter@gmx.net>, 2003.
#
# This file is maintained by Jens Seidel <jensseidel@users.sf.net>
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-08-30 00:33+0200\n"
"Last-Translator: Jens Seidel <jensseidel@users.sf.net>\n"
"Language-Team: Debian German <debian-l10n-german@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Rettungsmodus"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Rettungsmodus starten"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Keine Partitionen gefunden"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Der Installer konnte keine Partitionen finden. Sie werden deswegen nicht in "
"der Lage sein, ein Root-Dateisystem einzubinden. Gründe dafür können sein, "
"dass der Kernel Ihre Festplatte nicht erkannt hat, die Partitionstabelle "
"nicht lesen konnte oder die Platte nicht partitioniert ist. Falls Sie es "
"wünschen, können Sie dies in einer Shell im Installer untersuchen."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Gerät, das als Root-Dateisystem verwendet wird:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Geben Sie ein Gerät ein, das Sie als Ihr Root-Dateisystem verwenden wollen. "
"Sie werden zwischen verschiedenen Rettungsoperationen auswählen können, um "
"auf diesem Dateisystem arbeiten zu können."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Gerät nicht gefunden"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Das als Root-Dateisystem angegebene Gerät (${DEVICE}) wurde nicht gefunden. "
"Bitte versuchen Sie es noch einmal."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Einbinden fehlgeschlagen."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Während dem Einbinden des angegebenen Gerätes (${DEVICE}) als Root-"
"Dateisystem nach /target ist ein Fehler aufgetreten."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr ""
"Bitte überprüfen Sie die Systemmeldungen (syslog) für weitere Informationen."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Rettungsaktionen"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Rettungsaktion fehlgeschlagen"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "Die Rettungsaktion »${OPERATION}« schlug mit Fehlercode ${CODE} fehl."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Eine Shell in ${DEVICE} ausführen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Eine Shell in der Installer-Umgebung ausführen"

# FIXME: Überschrift oder Aufforderung?
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Ein anderes Wurzeldateisystem wählen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "System neustarten"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Eine Shell ausführen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Nach dieser Nachricht erhalten Sie eine Shell, in der ${DEVICE} auf »/« "
"eingebunden wurde. Benötigen Sie weitere Dateisysteme (z.B. ein separates »/"
"usr«), müssen Sie diese selbst einbinden."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Fehler beim Ausführen der Shell in /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Auf Ihrem Dateisystem (${DEVICE}) wurde eine Shell (${SHELL}) gefunden. Beim "
"Ausführen der Shell trat jedoch ein Fehler auf."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Keine Shell in /target gefunden"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Auf dem Root-Dateisystem (${DEVICE}) wurde keine nutzbare Shell gefunden."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Interaktive Shell auf ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Nach dieser Nachricht erhalten Sie eine Shell, in der ${DEVICE} auf »/"
"target« eingebunden wurde. Sie können mit den Werkzeugen der Installer-"
"Umgebung darin arbeiten. Wenn Sie möchten, dass es temporär Ihr Root-"
"Dateisystem wird, dann starten Sie »chroot /target«. Benötigen Sie weitere "
"Dateisysteme (z.B. ein separates »/usr«), müssen Sie diese selbst einbinden."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Nach dieser Nachricht öffnet sich eine Shell im Installer. Da der Installer "
"keine Partitionen finden konnte, wurden keine Dateisysteme für Sie "
"eingebunden."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Interaktive Shell in der Installer-Umgebung"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Passphrase für ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr ""
"Bitte geben Sie die Passphrase für das verschlüsselte Volume ${DEVICE} ein."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Falls Sie nicht irgendwas eingeben, wird das Volume während Rettungsaktionen "
"nicht verfügbar sein."
