<!-- retain these comments for translator revision tracking -->
<!-- original version: 39920 -->


  <sect2 arch="alpha"><title>Fer particions per a &arch-title;</title>
<para>

Per arrencar Debian des de la consola SRM (l'únic mètode d'arrencada de
disc suportat per &releasename;) necessiteu tenir una etiqueta de disc BSD,
i no una taula de particions DOS, al vostre disc d'arrencada. (Recordeu, el
bloc d'arrencada de SRM és incompatible amb les tabules de particions
MS-DOS &mdash; vegeu <xref linkend="alpha-firmware"/>). Quan s'usa
<command>partman</command> a un maquinari &architecture;, crea etiquetes
de disc BSD, però si el vostre disc té una taula de particions DOS al
disc, <command>partman</command> necessitarà esborrar primer la taula de
particions existents per que la pugui convertir en un disc amb etiqueta.

</para><para>

Si heu escollit el <command>fdisk</command> per partir el vostre
disc, i el disc que esteu fent particions no té una etiqueta de disc BSD,
hauríeu d'utilitzar l'ordre <quote>b</quote> per accedir al mode d'etiqueta
de disc.

</para><para>

A no ser que desitgeu utilitzar el disc que esteu fent particions des del
Unix Tru64 o un dels sistemes operatius lliures derivats de 4.4BSD-Lite
(FreeBSD, OpenBSD o NetBSD), <emphasis>no</emphasis> hauríeu de crear
la tercera partició com a partició <quote>disc sencer</quote> (es a dir
amb el sector inicial i el final que abastin tot el disc), ja que el fa
incompatible amb les eines utilitzades per fer-lo arrencable amb aboot.
Açò vol dir que el disc que ha configurat l'instal·lador per utilitzar-lo
amb el disc d'arranc Debian no serà accessible als sistemes operatius
anomenats abans.

</para><para>

També, com que el <command>aboot</command> s'escriu als primers sectors
del disc (en aquest moment ocupa uns 70 KiB o 150 sectors),
<emphasis>necessiteu</emphasis> deixar prou espai buit al principi del
disc per ell. Abans es suggeria que es fera una xicoteta partició al inici
del disc, i que es deixara per formatar. Per la mateixa raó d'abans, ara
suggerim que no feu això al discs que tan sols utilitzen GNU/Linux. Quan
utilitzeu <command>partman</command>, es crearà una partició xicoteta per
l'<command>aboot</command> per que es considera convenient.

</para><para condition="FIXME">

Per instal·lacions ARC, hauríeu de fer una partició xicoteta FAT al
principi del disc per que contingui el <command>MILO</command> i el
<command>linload.exe</command> &mdash; 5 MiB seran suficients, vegeu
<xref linkend="non-debian-partitioning"/>. Malauradament, fer sistemes de
fitxers FAT no està encara suportat des del menú, així que haureu de fer-ho
manualment des de l'intèrpret d'ordres utilitzant <command>mkdosfs</command>
abans d'intentar instal·lar el carregador de l'arrencada.

</para>
  </sect2>
