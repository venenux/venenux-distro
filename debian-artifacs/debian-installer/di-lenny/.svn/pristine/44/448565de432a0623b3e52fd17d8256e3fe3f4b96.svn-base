msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-05-30 00:09+0000\n"
"PO-Revision-Date: 2008-05-12 00:23+0900\n"
"Last-Translator: KURASAWA Nozomu <nabetaro@caldron.jp>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Japanese\n"

#. Tag: title
#: bookinfo.xml:5
#, no-c-format
msgid "&debian; Installation Guide"
msgstr "&debian; インストールガイド"

#. Tag: para
#: bookinfo.xml:8
#, no-c-format
msgid ""
"This document contains installation instructions for the &debian; &release; "
"system (codename <quote>&releasename;</quote>), for the &arch-title; "
"(<quote>&architecture;</quote>) architecture. It also contains pointers to "
"more information and information on how to make the most of your new Debian "
"system."
msgstr ""
"この文書は &arch-title; (<quote>&architecture;</quote>) アーキテクチャ用 "
"&debian; &release; システム (コードネーム <quote>&releasename;</quote>) のイ"
"ンストール説明書です。また、さらに詳しい情報へのポインタや、新しく Debian シ"
"ステムを構築する方法にも言及しています。"

#. Tag: para
#: bookinfo.xml:17
#, no-c-format
msgid ""
"Because the &arch-title; port is not a release architecture for "
"&releasename;, there is no official version of this manual for &arch-title; "
"for &releasename;. However, because the port is still active and there is "
"hope that &arch-title; may be included again in future official releases, "
"this development version of the Installation Guide is still available."
msgstr ""
"&arch-title; の移植版は &releasename; のリリースアーキテクチャではないため、"
"公式な &arch-title; 用 &releasename; のマニュアルはありません。しかし、移植版"
"がまだ利用できますし、&arch-title; の公式リリースが将来復活する望みもあります"
"ので、開発版のインストールガイドはまだ利用できます。"

#. Tag: para
#: bookinfo.xml:25
#, no-c-format
msgid ""
"Because &arch-title; is not an official architecture, some of the "
"information, and especially some links, in this manual may be incorrect. For "
"additional information, please check the <ulink url=\"&url-ports;"
"\">webpages</ulink> of the port or contact the <ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; mailing list</ulink>."
msgstr ""
"&arch-title; は公式アーキテクチャではありませんから、本マニュアルの情報や、特"
"にリンクは誤っている可能性があります。さらなる情報は、移植版の <ulink url="
"\"&url-ports;\">web ページ</ulink> をチェックしたり、<ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; メーリングリスト</ulink> でお問い合わせく"
"ださい。"

#. Tag: para
#: bookinfo.xml:36
#, no-c-format
msgid ""
"This installation guide is based on an earlier manual written for the old "
"Debian installation system (the <quote>boot-floppies</quote>), and has been "
"updated to document the new Debian installer. However, for &architecture;, "
"the manual has not been fully updated and fact checked for the new "
"installer. There may remain parts of the manual that are incomplete or "
"outdated or that still document the boot-floppies installer. A newer version "
"of this manual, possibly better documenting this architecture, may be found "
"on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</ulink>. You "
"may also be able to find additional translations there."
msgstr ""
"このインストールガイドは、旧 Debian インストールシステム (<quote>boot-"
"floppies</quote>) 用に書かれた初期のマニュアルをベースにしており、新しい "
"Debian インストーラ用に更新しています。しかし &architecture; について、完全に"
"は更新・事実関係チェックが行われていません。不完全・時代遅れだったり、まだ "
"boot-floppies インストーラのマニュアルのままの部分が残っているかもしれませ"
"ん。このマニュアルの新版 (このアーキテクチャに対して、できる限りよく書かれて"
"います) は、インターネット (<ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>) で見つけられます。またそこで、追加翻訳も見つかるでしょう。"

#. Tag: para
#: bookinfo.xml:49
#, no-c-format
msgid ""
"Although this installation guide for &architecture; is mostly up-to-date, we "
"plan to make some changes and reorganize parts of the manual after the "
"official release of &releasename;. A newer version of this manual may be "
"found on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>. You may also be able to find additional translations there."
msgstr ""
"この &architecture; 用インストールガイドはほぼ最新版ですが、&releasename; の"
"公式リリース後に、いくつか変更を加えたり細部の再構成を計画しています。このマ"
"ニュアルの新版は、インターネット (<ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>) で見つけられます。またそこで、追加翻訳も見つかるでしょう。"

#. Tag: para
#: bookinfo.xml:58
#, no-c-format
msgid ""
"Translators can use this paragraph to provide some information about the "
"status of the translation, for example if the translation is still being "
"worked on or if review is wanted (don't forget to mention where comments "
"should be sent!). See build/lang-options/README on how to enable this "
"paragraph. Its condition is \"translation-status\"."
msgstr ""
"日本語訳については、<email>debian-doc@debian.or.jp</email> (要 subscribe) で"
"議論を行っています。また、<ulink url=\"http://www.debian.or.jp/MailingList."
"html#docML\">Debian JP Project: メーリングリスト</ulink> に購読に関する簡単な"
"説明があり、<ulink url=\"http://lists.debian.or.jp/debian-doc/\">debian-doc "
"Mailing List Archive</ulink> では過去のメールを読むことができます。"

#. Tag: holder
#: bookinfo.xml:75
#, no-c-format
msgid "the Debian Installer team"
msgstr "the Debian Installer team"

#. Tag: para
#: bookinfo.xml:79
#, no-c-format
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License. Please refer to the license in "
"<xref linkend=\"appendix-gpl\"/>."
msgstr ""
"本マニュアルはフリーソフトウェアです。GNU 一般公有使用許諾にそって、配布・改"
"変する事ができます。<xref linkend=\"appendix-gpl\"/> のライセンスを参照してく"
"ださい。"
