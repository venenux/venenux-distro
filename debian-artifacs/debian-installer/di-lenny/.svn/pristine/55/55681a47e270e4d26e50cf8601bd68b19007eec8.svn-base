#!/bin/sh
set -e

. /usr/share/debconf/confmodule

log() {
	logger -t colo-installer "$@"
}

error() {
	log "error: $@"
}

info() {
	log "info: $@"
}

findfs () {
	mount | grep "on /target${1%/} " | cut -d' ' -f1
}

db_progress START 0 3 colo-installer/progress

# detect the partitions /target and /target/boot
rootfs_devfs=$(findfs /)
bootfs_devfs=$(findfs /boot)
[ "$bootfs_devfs" ] || bootfs_devfs="$rootfs_devfs"

rootfs=$(mapdevfs $rootfs_devfs)
bootfs=$(mapdevfs $bootfs_devfs)
bootfs_no_dev=${bootfs#/dev/}

db_progress INFO colo-installer/apt-install

if ! apt-install colo ; then
	info "Calling 'apt-install colo' failed"
	db_input critical colo-installer/apt-install-failed || [ $? -eq 30 ]
	if ! db_go; then
		exit 10 # back up to menu
	fi
	db_get colo-installer/apt-install-failed
	if [ true != "$RET" ] ; then
		db_progress stop
		exit 1
	fi
fi

db_progress STEP 2
db_progress INFO colo-installer/conf

if [ -e /target/boot/vmlinux ]; then
	kernel=/boot/vmlinux
else
	if [ "$rootfs" = "$bootfs" ] ; then
		kernel=/vmlinux
	else
		kernel=`find /target/boot -name 'vmlinu*cobalt*' | tail -n 1`
		kernel=${kernel#/target}
	fi
fi

# Is /boot on a separate partition?
if [ "$rootfs" != "$bootfs" ] ; then
	kernel="${kernel#/boot}"
fi
info "Using kernel '$kernel'"

cat > /target/boot/default.colo <<EOF
#:CoLo:#
#
# load the Debian kernel from $bootfs_no_dev
#
mount $bootfs_no_dev
load $kernel
lcd "Booting Debian"
var cons_opts ''
-var cons_opts console=ttyS0,{console-speed}
execute root=$rootfs {cons_opts}
EOF

db_progress STEP 1
db_progress STOP

