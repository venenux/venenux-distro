<!-- $Id: alpha.xml 44147 2007-01-13 15:50:09Z mck-guest $ -->
<!-- original version: 43789 -->

 <sect2 arch="alpha" id="alpha-firmware">
 <title>Firmware Alpha konzoly</title>
<para>

Firmware konzoly je uložen ve flash ROM a je spuštěn vždy při zapnutí
nebo při resetu systému. Na Alpha systémech se používají dvě
odlišné specifikace konzolí a tudíž se používají i dva typy firmwaru:

</para>

<itemizedlist>
<listitem><para>

<emphasis>SRM konzola</emphasis> je založená na specifikaci
<quote>Alpha Console Subsystem</quote> a poskytuje operační
prostředí pro systémy OpenVMS, Tru64 UNIX a Linux.

</para></listitem>
<listitem><para>

<emphasis>ARC, AlphaBIOS nebo ARCSBIOS konzola</emphasis>, založená na
specifikaci <quote>Advanced RISC Computing</quote> (ARC), poskytuje
operační prostředí pro Windows NT.

</para></listitem>
</itemizedlist>

<para>

Z pohledu uživatele je mezi SRM a ARC konzolami největší rozdíl
v tom, že volbou konzole se odsuzujete k použití určitého typu
rozdělení toho disku, ze kterého budete zavádět systém.

</para><para>

ARC vyžaduje pro zaváděcí disk použití DOSové tabulky oblastí
(tak, jak ji vytvoří <command>cfdisk</command>).
Proto jsou také při startu z ARC konzoly DOSové tabulky oblastí
<quote>nativním</quote> formátem oblastí.
Od té doby co AlphaBIOS obsahuje utilitu na dělení disků,
je lepší rozdělit disky z firmwaru ještě před instalací Linuxu.

</para><para>

Naopak SRM je <emphasis>nekompatibilní</emphasis><footnote><para>

Konkrétně formát zaváděcího sektoru, jak je požadován <quote>Console
Subsystem Specification</quote> koliduje s umístěním DOSové tabulky
oblastí.

</para></footnote> s DOSovou tabulkou oblastí.
Jelikož Tru64 Unix používá formát BSD disklabel, je to pro SRM
instalace <quote>nativní</quote> formát oblastí.

</para><para>

GNU/Linux je na Alpha stanicích jediný operační systém zaveditelný
z obou typů konzol, ovšem &debian; &release; podporuje pouze zavádění
ze SRM konzoly. Pokud pro váš systém neexistuje žádná verze SRM, nebo
pokud chcete mít duální zavádění s Windows NT, nebo pokud vaše
zaváděcí zařízení vyžaduje podporu ARC konzoly pro inicializaci BIOSu,
nemůžete použít instalační program dodávaný s Debianem &release;. Na
těchto systémech stále můžete &debian; &release; nainstalovat tak, že
například nainstalujete Woodyho a aktualizujete systém na verzi
&release;.

</para><para>

Protože program <command>MILO</command> není dostupný pro žádné
aktuálně používané Alpha systémy (únor 2000) a protože již není nutné
si pro získání SRM firmwaru na starší Alphu kupovat licenci OpenVMS
nebo Tru64 Unix, je doporučeno používat SRM všude, kde to je možné.

</para><para>

Následující tabulka shrnuje dostupné a podporované kombinace
systémů a konzol (jména systémů najdete v
<xref linkend="alpha-cpus"/>). Slovo <quote>ARC</quote> v tabulce
označuje jakoukoliv ARC-kompatibilní konzoli.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry>Typ systému</entry>
  <entry>Podporovaný typ konzoly</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry>ARC (viz manuál k základní desce) nebo SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC nebo SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

Žádná z těchto konzolí obvykle neumí zavést Linux přímo, takže je
potřeba zavaděče, který funguje jako prostředník. Pro SRM konzolu se
používá <command>aboot</command>, což je malý, platformově nezávislý
zavaděč. Více informací o tomto programu naleznete v (bohužel starším)
<ulink url="&url-srm-howto;">SRM HOWTO</ulink>.

</para><para condition="FIXME">

Následující odstavce jsou převzaty z instalačního manálu pro Woodyho
a jsou schovány do doby, kdy &debian; opět začne podporovat instalace
přes MILO.

</para><para condition="FIXME">

Žádná z těchto konzolí obvykle neumí zavést Linux přímo, takže je
potřeba zavaděče, který funguje jako prostředník. Existují dva hlavní
linuxové zavaděče: <command>MILO</command> a <command>aboot</command>.

</para><para condition="FIXME">

<command>MILO</command> je samo konzolí, která v paměti nahrazuje ARC
či SRM. <command>MILO</command> může být zavedeno jak z ARC, tak ze SRM
konzoly, přičemž při natažení z ARC konzoly to je jediná cesta, jak z
ní zavést Linux. <command>MILO</command> je platformově závislé
(pro každý typ systému je potřeba jiné <command>MILO</command>) a
existuje pouze pro ty systémy, které podle předchozí tabulky podporují
ARC. Podívejte se také do (bohužel staršího)
<ulink url="&url-milo-howto;">MILO HOWTO</ulink>.

</para><para condition="FIXME">

<command>aboot</command> je malý, platformově nezávislý zavaděč, který
běží pouze z konzoly SRM. Více informací o tomto programu
je v (také starším) <ulink url="&url-srm-howto;">SRM HOWTO</ulink>.

</para><para condition="FIXME">

V závislosti na firmwaru konzoly a na (ne)přítomnosti programu
<command>MILO</command> jsou možné tři scénáře:

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

</para><para condition="FIXME">

Většina AlphaServerů a všechny současné servery a pracovní stanice
obsahují ve firmwaru jak SRM, tak i AlphaBIOS.
Pro <quote>half-flash</quote> stroje, jako třeba nejrůznější
evaluation základní desky, je možné přepálením firmwaru přepnout z
jedné verze na druhou. Po instalaci SRM je možné z diskety spouštět
ARC/AlphaBIOS (povelem <command>arc</command>). Z těchto důvodů
doporučujeme před instalací Debianu přepnout na SRM.

</para><para>

Stejně jako na jiných architekturách byste měli před instalací Debianu
instalovat nejnovější dostupný firmware<footnote><para>

Kromě Jensenu, kde Linux není podporován firmwarem vyšším než verze
1.7 &mdash; Před instalací si přečtěte <ulink url="&url-jensen-howto;"></ulink>

</para></footnote>. Firmware pro systémy Alpha můžete získat z
<ulink url="&url-alpha-firmware;">Alpha Firmware Updates</ulink>.

</para>
 </sect2>


  <sect2 arch="alpha" id="boot-tftp"><title>Zavedení z TFTP</title>
<para>

V SRM konzoli jsou názvy Ethernet rozhraní pojmenovány s předponou
<userinput>ewa</userinput> a budou vypsány ve výstupu příkazu
<userinput>show dev</userinput> (mírně modifikováno):

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

Nejprve musíte nastavit zaváděcí protokol:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

Pak zkontrolujte typ média:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>

Seznam platných módů získáte příkazem
<userinput>&gt;&gt;&gt; set ewa0_mode</userinput>.

</para><para>

Pro zavedení z prvního ethernetového rozhraní byste napsali:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags ""
</screen></informalexample>

což zavede systém s implicitními parametry tak, jak jsou uloženy
přímo v obrazu.

</para><para>

Při použití sériové konzoly <emphasis>musíte</emphasis> jádru předat
parametr <userinput>console=</userinput>, což můžete udělat ze SRM
konzoly přes parametr <userinput>-flags</userinput> příkazu
<userinput>boot</userinput>. Sériové porty mají stejná jména, jako
jejich odpovídající soubory v adresáři <filename>/dev</filename>.
Při zadávání dodatečných parametrů nesmíte zapomenout na nezbytné
parametry, bez kterých by &d-i; nenastartoval.
Například pro zavedení z <userinput>ewa0</userinput> a použití konzoly
na prvním sériovém portu byste napsali:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags "root=/dev/ram ramdisk_size=16384 console=ttyS0"
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha">
  <title>Zavedení z CD-ROM s konzolou SRM</title>
<para>

Instalační CD &debian;u obsahují několik předpřipravených možností pro
VGA i sériové konzole. Pro zavedení pomocí VGA konzole napište

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

kde <replaceable>xxxx</replaceable> je vaše CD mechanika (v notaci
SRM). Pro použití sériové konzole na prvním sériovém zařízení zadejte

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

Analogicky pro konzoli na druhém sériovém portu zadejte

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <title>Zavedení z CD-ROM s konzolou ARC nebo AlphaBIOS</title>
<para>

Nejprve si v <xref linkend="alpha-cpus"/> najděte kódové jméno
své podarchitektury. Potom v ARC konzoli nastavte v menu
<quote>OS Selection Setup</quote> zavaděč na hodnotu
<filename>\milo\linload.exe</filename> a <quote>OS Path</quote> na
hodnotu <filename>\milo\<replaceable>podarch</replaceable></filename>,
kde <replaceable>podarch</replaceable> je název vaší podarchitektury.
Výjimkou je podarchitektura <quote>ruffian</quote>, kde se jako
zavaděč používá <filename>\milo\ldmilo.exe</filename>.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Zavedení z disket s konzolou SRM</title>
<para>

V SRM promptu (<prompt>&gt;&gt;&gt;</prompt>) zadejte následující
příkaz:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

kde <filename>dva0</filename> nahradíte jménem příslušného zařízení.
<filename>dva0</filename> je obvykle disketová mechanika.
Pro seznam zařízení (třeba když chcete zavést z CD) napište:

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

Všimněte si, že pokud zavádíte systém přes MILO, parametr
<command>-flags</command> je ignorován, takže stačí napsat
<command>boot dva0</command>.
Jestliže vše pracuje jak má, uvidíte zavádění linuxového jádra.

</para><para>

Pokud chcete při zavádění přes <command>aboot</command> specifikovat
parametry jádra, použijte následující příkaz:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 param"
</screen></informalexample>

(vše napsáno na jednom řádku). Pokud je to nutné, nahraďte jméno
aktuálního zaváděcího SRM zařízení pro <filename>dva0</filename> za
jméno linuxového zaváděcího zařízení pro <filename>fd0</filename> a
místo <filename>param</filename> zadejte další parametry jádra.

</para><para>

Jestliže chcete zadat parametry jádra při zavedení přes
<command>MILO</command>, budete muset v jisté fázi přerušit zavádění.
Podrobný návod je v <xref linkend="booting-from-milo"/>.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Zavedení z disket s ARC nebo AlphaBIOS konzolou</title>

<para>

V menu <quote>OS Selection</quote> nastavte jako zavaděč
<command>linload.exe</command> a <quote>OS Path</quote> na
hodnotu <command>milo</command>. Zaveďte systém nově vytvořenou
položkou.

</para>
  </sect2>

  <sect2 arch="alpha" id="booting-from-milo" condition="FIXME">
  <title>Zavedení se zavaděčem <command>MILO</command></title>
<para>

Na zaváděcích médiích je <command>MILO</command> nastaveno tak, aby
Linux zavedlo automaticky. Jestliže budete chtít vstoupit do procesu,
stiskněte během odpočítávání mezerník.

</para><para>

Pokud chcete kontrolovat všechny detaily sami (například zadat
speciální parametry), můžete použít následující příkaz:

<informalexample><screen>
MILO&gt; boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1
</screen></informalexample>

</para><para>

Pokud nezavádíte instalační systém z diskety, nahraďte v předchozím
příkladu <filename>fd0</filename> za příslušné zařízení (v linuxové
notaci). Příkaz <command>help</command> vám poskytne stručný popis
MILO příkazů.

</para>
 </sect2>
