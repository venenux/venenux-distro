#!/bin/sh
# backported d-i builder
# Copyright (C) 2006-2009 Kenshi Muto <kmuto@debian.org>
#
# You need to install devscripts before running.

. conf.sh

function log() {
  echo "[$* ...]"
}

function askversion() {
  echo $(LANG=C COLUMNS=130 dpkg -l $1 2>/dev/null | grep ^ii | awk '{print $3}')
}

function didebs() {
  # $1: package name
  # $2: sub directory
  # $3: insert _ in package name which be removed

  log "Building d-i packages ($1)"
  if [ $# -ge 2 ]; then
    cd $DI/packages/$2/$1
  else
    cd $DI/packages/$1
  fi
  if [ $# -ge 3 ]; then
    rm -f ../$1_*.udeb ../$1_*.changes ../$1_*.build
  else
    rm -f ../$1*.udeb ../$1*.changes ../$1*.build
  fi
  debuild -us -uc -b || exit 1
  case "$1" in
	partman*)
		fakeroot debian/rules clean
	;;
  esac

  #svn revert -R .
  if [ $# -ge 3 ]; then
    $CP ../$1_*.udeb $LOCALPACKAGES
  else
    $CP ../$1*.udeb $LOCALPACKAGES
  fi

  touch $STAMPDIR/d-i-deb-$1
}


case "$1" in
distclean)
  # clean all files
  log Cleaning

  rm -f $LOCALPACKAGES/*
  rm -f $STAMPDIR/*
;;

bpo-config)
  log Building bpo-config package

  cd $BPOCONFIG/bpo-config-*
  rm ../bpo-config*.deb ../bpo-config*.build ../bpo-config*.changes
  debuild -us -uc -b || exit 1
  cd ..
  $CP bpo-config*.deb $LOCALPACKAGES

  touch $STAMPDIR/bpo-config
  ;;

bpo-download)
  log Downloading backports packages

  if [ "$USEBPOKERNEL" = "yes" ]; then
    case "$ARCH" in
    i386)
      KERNELS="\
	linux-image-$KERNELVER-486/lenny-backports \
	linux-image-$KERNELVER-686/lenny-backports \
	linux-image-$KERNELVER-686-bigmem/lenny-backports \
	linux-image-$KERNELVER-amd64/lenny-backports \
	linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//')/lenny-backports \
	linux-headers-$KERNELVER-486/lenny-backports \
	linux-headers-$KERNELVER-686/lenny-backports \
	linux-headers-$KERNELVER-686-bigmem/lenny-backports \
	linux-headers-$KERNELVER-amd64/lenny-backports"
      ;;
    amd64)
      KERNELS="\
	linux-image-$KERNELVER-amd64/lenny-backports \
	linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//')/lenny-backports \
	linux-headers-$KERNELVER-amd64/lenny-backports"
      ;;
    armel)
      KERNELS="\
	linux-image-$KERNELVER-iop32x/lenny-backports \
	linux-image-$KERNELVER-ixp4xx/lenny-backports \
	linux-image-$KERNELVER-orion5x/lenny-backports \
	linux-image-$KERNELVER-kirkwood/lenny-backports \
	linux-image-$KERNELVER-versatile/lenny-backports \
	linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//')/lenny-backports \
	linux-headers-$KERNELVER-iop32x/lenny-backports \
	linux-headers-$KERNELVER-ixp4xx/lenny-backports \
	linux-headers-$KERNELVER-orion5x/lenny-backports \
	linux-headers-$KERNELVER-kirkwood/lenny-backports \
	linux-headers-$KERNELVER-versatile/lenny-backports"
      ;;
    esac
  else
    case "$ARCH" in
    i386)
        if [ "$USEOWNFULLKERNEL" = "yes" ]; then
          KERNELS="\
	    linux-image-$KERNELVER-486 \
	    linux-image-$KERNELVER-686 \
	    linux-image-$KERNELVER-686-bigmem \
	    linux-image-$KERNELVER-amd64 \
	    linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//') \
	    linux-headers-$KERNELVER-common \
	    linux-headers-$KERNELVER-486 \
	    linux-headers-$KERNELVER-686 \
	    linux-headers-$KERNELVER-686-bigmem \
  	    linux-headers-$KERNELVER-amd64"
        else
          KERNELS="\
	    linux-image-$KERNELVER-686 \
	    linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//') \
	    linux-headers-$KERNELVER-686"
        fi
      ;;
    amd64)
      KERNELS="\
	linux-image-$KERNELVER-amd64 \
	linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//') \
	linux-headers-$KERNELVER-common \
	linux-headers-$KERNELVER-amd64"
      ;;
    armel)
      KERNELS="\
	linux-image-$KERNELVER-iop32x \
	linux-image-$KERNELVER-ixp4xx \
	linux-image-$KERNELVER-orion5x \
	linux-image-$KERNELVER-kirkwood \
	linux-image-$KERNELVER-versatile \
	linux-kbuild-$(echo -n $KERNELVER|sed -e 's/-.*//')/lenny-backports \
	linux-headers-$KERNELVER-iop32x \
	linux-headers-$KERNELVER-ixp4xx \
	linux-headers-$KERNELVER-orion5x \
	linux-headers-$KERNELVER-kirkwood \
	linux-headers-$KERNELVER-versatile"
      ;;
    esac
  fi
  if [ "$USEBPOMETAKERNEL" = "yes" ]; then
    case "$ARCH" in
    i386)
      METAKERNELS="\
	linux-image-2.6-486/lenny-backports \
	linux-image-2.6-686/lenny-backports \
	linux-image-2.6-686-bigmem/lenny-backports \
	linux-image-2.6-amd64/lenny-backports"
EOF
      ;;
    amd64)
      METAKERNELS=liunx-image-2.6-amd64/lenny-backports
      ;;
    armel)
      METAKERNELS="\
	linux-image-2.6-iop32x/lenny-backports \
	linux-image-2.6-ixp4xx/lenny-backports \
	linux-image-2.6-orion5x/lenny-backports \
	linux-image-2.6-kirkwood/lenny-backports \
	linux-image-2.6-versatile/lenny-backports"
      ;;
    esac
  else
    case "$ARCH" in
    i386)
      if [ "$USEOWNFULLKERNEL" = "yes" ]; then
        METAKERNELS="\
 	  linux-image-2.6-486 \
	  linux-image-2.6-686 \
	  linux-image-2.6-686-bigmem \
	  linux-image-2.6-amd64 \
 	  linux-headers-2.6-486 \
	  linux-headers-2.6-686 \
	  linux-headers-2.6-686-bigmem \
	  linux-headers-2.6-amd64"
      fi
      ;;
    amd64)
      if [ "$USEOWNFULLKERNEL" = "yes" ]; then
        METAKERNELS="linux-image-2.6-amd64"
      fi
      ;;
    armel)
      if [ "$USEOWNFULLKERNEL" = "yes" ]; then
      METAKERNELS="\
	linux-image-2.6-iop32x \
	linux-image-2.6-ixp4xx \
	linux-image-2.6-orion5x \
	linux-image-2.6-kirkwood \
	linux-image-2.6-versatile"
      fi
      ;;
    esac
  fi

  INITRAMFS=""

  if [ "$USEUBUNTUFIRM" = "yes" ]; then
    FIRM="linux-firmware"
  fi

  EXT4="e2fsprogs e2fslibs e2fslibs-dev"


  apt-get $LOCALAPTOPT clean || exit 1
  apt-get $LOCALAPTOPT update || exit 1
  apt-get $LOCALAPTOPT --reinstall --force-yes -y -d install \
	$INITRAMFS \
        $KERNELS $METAKERNELS $FIRM $EXT4 || exit 1

  $CP $LOCALAPT/cache/*_{all,$ARCH}.deb $LOCALPACKAGES
  # FIXME: more configuratable
  $CP -f mtu/*-firmware_*_all.udeb mtu/wpasupplicant*_$ARCH.udeb \
  mtu/*parted*_$ARCH.udeb \
  mtu/*e2fsprogs*_$ARCH.udeb \
  $LOCALPACKAGES

  touch $STAMPDIR/bpo-download
  ;;

didebs-netcfg)
  log "Building d-i packages (netcfg)"

  cd $DI/packages/netcfg
  rm -f ../netcfg*.udeb ../netcfg*.changes ../netcfg*.build
  debuild -us -uc -b || exit 1
  $CP ../netcfg*_$ARCH.udeb $LOCALPACKAGES
  touch $STAMPDIR/d-i-deb-netcfg
  ;;

didebs-partman-ext3)
  didebs partman-ext3 partman
  ;;

didebs-partman-base)
  didebs partman-base partman
  $CP ../partman-utils_*.udeb $LOCALPACKAGES
  ;;

didebs-partman-basicfilesystems)
  didebs partman-basicfilesystems partman
  ;;

didebs-partconf)
  didebs partconf
  ;;

didebs-kernel-wedge)
  log "Building d-i packages (kernel-wedge)"

  cd $DI/packages/kernel/kernel-wedge
  rm -f ../kernel-wedge*.deb ../kernel-wedge*.changes ../kernel-wedge*.build
  debuild -us -uc -b || exit 1

  cd ..
  INSTVER=$(askversion kernel-wedge)
  TARGETVER=$(ls kernel-wedge*.deb|sed -e "s/[^_]*_//" -e "s/_.*//")
  if [ "$INSTVER" != "$TARGETVER" ]; then
    sudo dpkg -i kernel-wedge*.deb || exit 1
  fi

  touch $STAMPDIR/d-i-deb-kernel-wedge
  ;;

separate-kernel)
  log Building kernel udeb packages

  rm -rf $LOCALAPT/kernel/*

  case "$ARCH" in
  i386)
    if [ "$USEBPOKERNEL" = "no" -a "$USEOWNFULLKERNEL" = "no" ]; then
      dpkg -x $LOCALPACKAGES/linux-image-*-$KABI-686_*.deb $LOCALAPT/kernel || exit 1
      depmod -a -F $LOCALAPT/kernel/boot/System.map-$KERNELVER-686 -b $LOCALAPT/kernel $KERNELVER-686
    else
      dpkg -x $LOCALPACKAGES/linux-image-*-$KABI-486_*.deb $LOCALAPT/kernel || exit 1
      depmod -a -F $LOCALAPT/kernel/boot/System.map-$KERNELVER-486 -b $LOCALAPT/kernel $KERNELVER-486
      dpkg -x $LOCALPACKAGES/linux-image-*-$KABI-686-bigmem_*.deb $LOCALAPT/kernel || exit 1
      depmod -a -F $LOCALAPT/kernel/boot/System.map-$KERNELVER-686-bigmem -b $LOCALAPT/kernel $KERNELVER-686-bigmem
    fi
    ;;
  amd64)
    dpkg -x $LOCALPACKAGES/linux-image-*-$KABI-amd64_*.deb $LOCALAPT/kernel || exit 1
    depmod -a -F $LOCALAPT/kernel/boot/System.map-$KERNELVER-amd64 -b $LOCALAPT/kernel $KERNELVER-amd64
    ;;
  armel)
      for a in iop32x ixp4xx kirkwood orion5x versatile; do
        dpkg -x $LOCALPACKAGES/linux-image-*-$KABI-$a_*.deb $LOCALAPT/kernel || exit 1
        depmod -a -F $LOCALAPT/kernel/boot/System.map-$KERNELVER-$a -b $LOCALAPT/kernel $KERNELVER-$a
      done
    ;;
  esac
  ln -sf $LOCALAPT/kernel $DI/packages/kernel/$ARCH
  cd $DI/packages/kernel/linux-kernel-di-$ARCH-2.6
  rm -f ../*.udeb
  kernel-wedge build-all || exit 1
  case "$ARCH" in
  i386)
    if [ "$USEBPOKERNEL" = "no" -a "$USEOWNFULLKERNEL" = "no" ]; then
      $CP ../*-$KABI-686*_i386.udeb $LOCALPACKAGES
    else
      $CP ../*-$KABI-486*_i386.udeb $LOCALPACKAGES
      $CP ../*-$KABI-686*_i386.udeb $LOCALPACKAGES
    fi
    ;;
  amd64)
    $CP ../*-amd64*_amd64.udeb $LOCALPACKAGES
    ;;
  armel)
      $CP ../*-$KABI-iop32x*_armel.udeb $LOCALPACKAGES
      $CP ../*-$KABI-ixp4xx*_armel.udeb $LOCALPACKAGES
      $CP ../*-$KABI-kirkwood*_armel.udeb $LOCALPACKAGES
      $CP ../*-$KABI-orion5x*_armel.udeb $LOCALPACKAGES
      $CP ../*-$KABI-versatile*_armel.udeb $LOCALPACKAGES
    ;;
  esac
  rm -f $DI/packages/kernel/$ARCH

  touch $STAMPDIR/separate-kernel
  ;;

copy-udebs)
  log Copying udeb packages to d-i directory

  rm -f $DI/installer/build/localudebs/*
  $CP $LOCALPACKAGES/*.udeb $DI/installer/build/localudebs

  touch $STAMPDIR/copy-udebs
  ;;

build-di)
  log Building d-i image
  if [ "$SKIPDIBUILD" ]; then
    touch $STAMPDIR/build-di
    exit 0
  fi

  cd $DI/installer/build

  fakeroot make reallyclean || exit 1

  case "$ARCH" in
  i386)
    fakeroot make build_cdrom_isolinux || exit 1
    fakeroot make build_cdrom_gtk || exit 1
    fakeroot make build_hd-media || exit 1
    #fakeroot make build_hd-media TARGET=arch_boot || exit 1
    fakeroot make build_netboot
    ;;
  amd64)
    fakeroot make build_cdrom_isolinux || exit 1
    fakeroot make build_cdrom_gtk || exit 1
    fakeroot make build_hd-media || exit 1
    #fakeroot make build_hd-media TARGET=arch_boot || exit 1
    fakeroot make build_netboot
    ;;
  armel)
    fakeroot make build_cdrom_isolinux || exit 1
    fakeroot make build_cdrom_gtk || exit 1
    fakeroot make build_hd-media || exit 1
    fakeroot make build_netboot
    ;;
  esac

  touch $STAMPDIR/build-di
  ;;

get-cdimage)
  log Retrieving lenny-netinst image

  if [ ! -f $CDIMAGE/$ISOIMAGE ]; then
    cd $CDIMAGE
    if [ "$(echo $ORIGISO|grep http)" ]; then
      wget $ORIGISO || exit 1
    elif [ "$(echo $ORIGISO|grep rsync)" ]; then
      rsync $ORIGISO . || exit 1
    else
      echo "Unknown scheme for downloading $ORIGISO"
      exit 1
    fi
  fi

  touch $STAMPDIR/get-cdimage
  ;;

extract-cdimage)
  log Extracting lenny-netinst image

  sudo mount -o loop $CDIMAGE/$ISOIMAGE $TMPMOUNT
  rm -r $CDIMAGE/archive
  mkdir $CDIMAGE/archive
  cd $TMPMOUNT
  tar --exclude TRANS.TBL -cf - . | tar Cxf $CDIMAGE/archive -
  cd $CDIMAGE
  sudo umount $TMPMOUNT
  chmod -R u+w $CDIMAGE/archive
  sed -i -e "s/Official/Unofficial bpo/i" $CDIMAGE/archive/.disk/info

  touch $STAMPDIR/extract-cdimage
  ;;

remove-packages)
  log Removing duplicate files

  rm -f $CDPOOLDIR/*.*deb
  find $LOCALPACKAGES -follow -name "*.*deb" -a -printf "%f\n" | sed 's/\_.*/_/' > $CDIMAGE/archive/localfiles.$$

  # remove obsolete files
  echo "*-2.6.26-*486" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*686" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*common" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*amd64" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*iop32x" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*ixp4xx" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*kirkwood" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*orion5x" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26-*versatile" >> $CDIMAGE/archive/localfiles.$$
  echo "*-2.6.26_*" >> $CDIMAGE/archive/localfiles.$$
  echo "linux-*-2.6-amd64" >> $CDIMAGE/archive/localfiles.$$
  echo "linux-headers-2.6-*" >> $CDIMAGE/archive/localfiles.$$
  echo "linux-image-2.6-*" >> $CDIMAGE/archive/localfiles.$$
  perl -e '$MASTERDIR=shift; while (<>) { chomp; system("find $MASTERDIR -name \"$_*\" -type f -exec rm \{\} \\;") }' $CDIMAGE/archive < $CDIMAGE/archive/localfiles.$$
  rm $CDIMAGE/archive/localfiles.$$

  touch $STAMPDIR/remove-packages
  ;;

copy-localpackages)
  log Copying local udeb/deb files

  $CP $LOCALPACKAGES/*.*deb $CDPOOLDIR

  touch $STAMPDIR/copy-localpackages
  ;;

create-configfile)
  log Creating config file

  cat <<EOT > $CDIMAGE/config-udeb
Dir { ArchiveDir "$CDIMAGE/archive"; };
TreeDefault { Directory "pool/"; };
BinDirectory "pool/main" {
  Packages "dists/lenny/main/debian-installer/binary-$ARCH/Packages";
};
Default { Packages { Extensions ".udeb"; }; };
EOT

  cat <<EOT > $CDIMAGE/config
Dir { ArchiveDir "$CDIMAGE/archive";
      OverrideDir "$CDIMAGE";
 };
TreeDefault { Directory "pool/"; };
BinDirectory "pool/main" {
  Packages "dists/lenny/main/binary-$ARCH/Packages";
  BinOverride "override.lenny.main";
  ExtraOverride "override.lenny.extra.main";
};
Default { Packages { Extensions ".deb"; }; };
EOT

  [ ! -f $CDIMAGE/override.lenny.main ] && wget -O $CDIMAGE/override.lenny.main.gz $INDICES/override.lenny.main.gz && gzip -d $CDIMAGE/override.lenny.main.gz
  [ ! -f $CDIMAGE/override.lenny.extra.main ] && wget -O $CDIMAGE/override.lenny.extra.main.gz $INDICES/override.lenny.extra.main.gz && gzip -d $CDIMAGE/override.lenny.extra.main.gz

  touch $STAMPDIR/create-configfile
  ;;

update-indices)
  log Updating Packages file

  cd $CDIMAGE
  apt-ftparchive generate config
  apt-ftparchive generate config-udeb
  cd $CDIMAGE/archive/dists/lenny
  # FIXME apt-ftparchive release makes this file?
  LANG=C cat > Release <<EOT
Origin: Debian
Label: Debian
Suite: stable
Codename: lenny
Date: `date`
Architectures: `dpkg --print-architecture`
Components: main contrib non-free
Description: Debian 5.0 + Kenshi Muto's custom edition
MD5Sum:
EOT

  for f in main/binary-$ARCH/Release main/binary-$ARCH/Packages main/binary-$ARCH/Packages.gz main/debian-installer/binary-$ARCH/Packages main/debian-installer/binary-$ARCH/Packages.gz ; do
    printf " %s %8d %s\n" `md5sum $f | awk '{print $1}'` `ls -l $f | awk '{print $5}'` $f >> Release
  done

  touch $STAMPDIR/update-indices
  ;;

copy-di)
  log Copying installer images
 
  case "$ARCH" in
    i386)
        a=386
	;;
    amd64)
	a=amd
	;;
    armel)
	a=armel
	;;
  esac

  cd $DI/installer/build/dest
  [ -f cdrom/vmlinuz ] && cp cdrom/vmlinuz $CDIMAGE/archive/install.$a
  [ -f cdrom/initrd.gz ] && cp cdrom/initrd.gz $CDIMAGE/archive/install.$a
  [ -f cdrom/gtk/initrd.gz ] && cp cdrom/gtk/initrd.gz $CDIMAGE/archive/install.$a/gtk
  [ -f netboot/initrd.gz ] && cp netboot/initrd.gz $CDIMAGE/archive/install.$a/net/net.gz
  [ -f netboot/netboot.tar.gz ] && cp netboot/netboot.tar.gz $CDIMAGE/archive/install.$a/net/netboot.tar.gz
  [ -f netboot/mini.iso ] && cp netboot/mini.iso $CDIMAGE/archive/install.$a/net/mini.iso
#  [ -f cdrom/debian-cd_info.tar.gz ] && tar Czxf $CDIMAGE/archive/isolinux cdrom/debian-cd_info.tar.gz
  [ -f $CDIMAGE/isolinux.cfg-$ARCH ] && cp $CDIMAGE/isolinux.cfg-$ARCH $CDIMAGE/archive/isolinux/isolinux.cfg

  sed -e "s/vga=normal/vga=788 debian-instaler\/allow_unauthenticated=true/" $CDIMAGE/archive/isolinux/txt.cfg > $CDIMAGE/archive/isolinux/txt.cfg.n
  mv $CDIMAGE/archive/isolinux/txt.cfg.n $CDIMAGE/archive/isolinux/txt.cfg

  touch $STAMPDIR/copy-di
  ;;

base-include)
  log Adding base_include file
  cat <<EOT > $CDIMAGE/archive/.disk/base_include
bpo-config
EOT

  touch $STAMPDIR/base-include
  ;;

make-cd)
  log Building CD image
  cd $CDIMAGE/archive
  genisoimage -r -J -no-emul-boot -boot-load-size 4 -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat -o $LOCALCDIMAGE .

  echo $LOCALCDIMAGE > $STAMPDIR/make-cd
  ;;

*)
  cat <<EOT
$0 [distclean | bpo-config | bpo-download | didebs-partman-ext3 |
didebs-partman-basicfilesystems | didebs-partman-base | didebs-kernel-wedge |
separate-kernel | copy-udebs | build-di | get-cdimage | extract-cdimage |
remove-packages | copy-localpackages | create-configfiles |
update-indices | copy-di | base-include | make-cd ]
EOT
  ;;

esac
