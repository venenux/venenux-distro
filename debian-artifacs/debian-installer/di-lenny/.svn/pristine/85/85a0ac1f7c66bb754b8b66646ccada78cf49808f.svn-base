<!-- retain these comments for translator revision tracking -->
<!-- original version: 53521 -->
<!-- updated by Felipe Augusto van de Wiel (faw) 2008.06.10 -->

<bookinfo id="debian_installation_guide">
<title>Guia de Instalação do &debian;</title>

<abstract>
<para>
Este documento contém instruções de instalação do sistema &debian;
&release; (codinome <quote>&releasename;</quote>),
para a arquitetura &arch-title; (<quote>&architecture;</quote>).
Ele também contém referências para mais informações e informações
sobre como obter maior proveito de seu novo sistema Debian.
</para>

<para>
<note arch="m68k"><para>
Devido ao porte &arch-title; não ser uma arquitetura de lançamento para
o &releasename;, não há versão oficial deste manual para &arch-title; para
&releasename;. No entanto, como o porte ainda está ativo e há esperança
que &arch-title; seja incluído novamente em futuros lançamentos oficiais,
esta versão de desenvolvimento do Guia de Instalação ainda está disponível.

</para><para>

Devido a &arch-title; não ser uma arquitetura oficial, algumas das
informações, e especialmente alguns links, neste manual podem estar
incorretos. Para informações adicionais, por favor verifique
<ulink url="&url-ports;">as páginas web</ulink> do porte ou contate a
<ulink url="&url-list-subscribe;">listas de discussão
debian-&arch-listname;</ulink>.

</para></note>

<warning condition="not-checked"><para>
Este guia de instalação é baseado em um manual anterior escrito para
o antigo sistema de instalação do Debian (os <quote>boot-floppies</quote>)
e foi atualizado para documentar o novo instalador do Debian. No entanto,
para a &architecture;, o manual ainda não foi completamente atualizado e
verificado de acordo com o novo instalador. Podem haver partes do manual
que estão incompletas ou desatualizadas ou que ainda documentam o instalador
<quote>boot-floppies</quote>. Uma nova versão deste manual, possivelmente
documentando melhor esta arquitetura pode ser encontrada na Internet na
<ulink url="&url-d-i;">página web do &d-i;</ulink>. Você também poderá
encontrar traduções adicionais lá.
</para></warning>

<note condition="checked"><para>
Embora este guia de instalação para &architecture; está bastante atualizado,
nós planejamos fazer algumas mudanças e reorganizar partes do manual após o
lançamento oficial da &releasename;. Uma nova versão deste manual pode ser
encontrada na Internet na <ulink url="&url-d-i;">página web do &d-i;</ulink>.
Você também poderá encontrar traduções adicionais lá.
</para></note>
</para>

<para condition="translation-status">
<!--
Translators can use this paragraph to provide some information about
the status of the translation, for example if the translation is still
being worked on or if review is wanted (don't forget to mention where
comments should be sent!).

See build/lang-options/README on how to enable this paragraph.
Its condition is "translation-status".
-->
A Equipe de Tradução que trabalhou neste manual pode ser contatada através
da lista <email>debian-l10n-portuguese@lists.debian.org</email>.
A tradução da <quote>GNU GPL</quote> presente neste manual foi retirada do
site da <quote>FSF</quote>, http://www.gnu.org/licenses/translations.html e
pode ser encontrada em http://www.magnux.org/doc/GPL-pt_BR.txt.
</para>
</abstract>

<copyright>
 <year>2004</year>
 <year>2005</year>
 <year>2006</year>
 <year>2007</year>
 <year>2008</year>
 <holder>o time do Instalador Debian (the Debian Installer team)</holder>
</copyright>

<legalnotice>
<para>

Este manual é software livre; você poderá redistribuí-lo e/ou modificá-lo
sob os termos da Licença Pública Geral GNU (<quote>GNU General Public
License</quote>). Por favor, veja a licença em <xref linkend="appendix-gpl"/>.

</para>
</legalnotice>
</bookinfo>
