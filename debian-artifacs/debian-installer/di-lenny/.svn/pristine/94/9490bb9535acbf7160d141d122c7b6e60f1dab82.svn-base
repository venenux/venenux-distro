<!-- retain these comments for translator revision tracking -->
<!-- original version: 22665 untranslated -->

 <sect1 id="module-details">
 <title>Using Individual Components</title>
<para>

In this section we will describe each installer component in
detail. The components have been grouped into stages that should
be recognisable for users. They are presented in the order they
appear during the install. Note that not all modules will be used
for every installation; which modules are actually used depends on
the installation method you use and on your hardware.

</para>

  <sect2 id="di-setup">
  <title>Setting up Debian Installer and Hardware Configuration</title>
<para>

Let's assume the Debian Installer has booted and you are facing its
first screen.  At this time, the capabilities of &d-i; are still quite
limited. It doesn't know much about your hardware, preferred language,
or even the task it should perform. Don't worry. Because &d-i; is quite
clever, it can automatically probe your hardware, locate the rest
of its components and upgrade itself to a capable installation system.

However, you still need to help &d-i; with some information it can't
determine automatically (like selecting your preferred language, keyboard
layout or desired network mirror).

</para><para>

You will notice that &d-i; performs <firstterm>hardware detection</firstterm>
several times during this stage. The first time is targeted specifically
at the hardware needed to load installer components (e.g. your CD-ROM or
network card). As not all drivers may be available during this first run,
hardware detection needs to be repeated later in the process.

</para>

&module-lowmem.xml;
&module-languagechooser.xml;
&module-countrychooser.xml;
&module-kbd-chooser.xml;
&module-s390-netdevice.xml;
&module-s390-dasd.xml;
&module-ddetect.xml;
&module-cdrom-detect.xml;
&module-iso-scan.xml;
&module-anna.xml;
&module-netcfg.xml;
&module-choose-mirror.xml;

  </sect2>

  <sect2 id="di-partition">
  <title>Partitioning and Mount Point Selection</title>
<para>

At this time, after hardware detection has been executed a final time,
&d-i; should be at its full strength, customized for the user's needs
and ready to do some real work.

As the title of this section indicates, the main task of the next few
components lies in partitioning your disks, creating filesystems,
assigning mountpoints and optionally configuring closely related issues
like LVM or RAID devices.

</para>

&module-partman.xml;
&module-autopartkit.xml;
&module-partitioner.xml;
&module-partconf.xml;
&module-mdcfg.xml;
&module-partman-lvm.xml;
  </sect2>

  <sect2 id="di-install-base">
  <title>Installing the Base System</title>
<para>

Although this stage is the least problematic, it consumes most time of
the install because it downloads, verifies and unpacks the whole base
system. If you have a slow computer or network connection, this could
take some time.

</para>

&module-base-installer.xml;
  </sect2>

  <sect2 id="di-make-bootable">
  <title>Making Your System Bootable</title>

<para condition="supports-nfsroot">

If you are installing a diskless workstation, obviously, booting off
the local disk isn't a meaningful option, and this step will be
skipped. <phrase arch="sparc">You may wish to set the OpenBoot to boot
from the network by default; see <xref
linkend="boot-dev-select-sun"/>.</phrase>

</para><para>

Note that multiple operating systems booting on a single machine is
still something of a black art.  This document does not even attempt
to document the various boot managers, which vary by architecture and
even by subarchitecture.  You should see your boot manager's
documentation for more information.

</para>

&module-os-prober.xml;
&module-alpha-aboot-installer.xml;
&module-hppa-palo-installer.xml;
&module-x86-grub-installer.xml;
&module-x86-lilo-installer.xml;
&module-ia64-elilo-installer.xml;
&module-mips-arcboot-installer.xml;
&module-mipsel-colo-installer.xml;
&module-mipsel-delo-installer.xml;
&module-powerpc-yaboot-installer.xml;
&module-powerpc-quik-installer.xml;
&module-s390-zipl-installer.xml;
&module-sparc-silo-installer.xml;
&module-nobootloader.xml;
  </sect2>

  <sect2 id="di-finish">
  <title>Finishing the First Stage</title>
<para>

These are the last bits to do before rebooting to your new Debian. It
mostly consists of tidying up after the &d-i;.

</para>

&module-finish-install.xml;
  </sect2>

  <sect2 id="di-miscellaneous">
  <title>Miscellaneous</title>
<para>

The components listed in this section are usually not involved in the
installation process, but are waiting in the background to help the
user in case something goes wrong.

</para>

&module-save-logs.xml;
&module-cdrom-checker.xml;
&module-shell.xml;
&module-baseconfig.xml;
  </sect2>
 </sect1>
