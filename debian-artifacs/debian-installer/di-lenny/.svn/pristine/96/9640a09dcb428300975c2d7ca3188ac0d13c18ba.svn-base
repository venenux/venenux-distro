<!-- retain these comments for translator revision tracking -->
<!-- original version: 53521 -->

<bookinfo id="debian_installation_guide">
<title>&debian; &ndash; Installationsanleitung</title>

<abstract>
<para>
Dieses Dokument enthält Anweisungen zur Installation des
&debian; &release;-Systems (Codename &releasename-uc;) für die
&arch-title;-Architektur (<quote>&architecture;</quote>).
Es enthält auch Verweise auf andere Informationsquellen
sowie Tipps, wie Sie das Beste aus Ihrem neuen Debian-System machen.
</para>

<para>
<note arch="m68k"><para>
Da &releasename-cap; nicht für die &arch-title;-Architektur veröffentlicht
wird, gibt es auch keine offizielle Version des Installationshandbuchs für
&architecture; in &releasename-cap;. Weil die Portierung aber noch aktiv ist
und weil Hoffnung besteht, dass &architecture; in zukünftigen Veröffentlichungen
wieder enthalten sein wird, ist diese Entwickler-Version des Handbuchs weiter
verfügbar.

</para><para>

Einige Informationen in diesem Handbuch, speziell einige Links, könnten
inkorrekt sein, da &architecture; keine offizielle Architektur für
&releasename-cap; ist. Zusätzliche Informationen finden Sie auf den
<ulink url="&url-ports;">Webseiten</ulink> der Portierung oder kontaktieren
Sie die
<ulink url="&url-list-subscribe;">debian-&arch-listname;-Mailingliste</ulink>.

</para></note>
<warning condition="not-checked"><para>
Diese Installationsanleitung basiert auf einer früheren Version dieses
Handbuchs für das alte Debian-Installationssystem (den
<quote>Boot-Floppies</quote>) und wurde aktualisiert, um den neuen
Debian-Installer zu dokumentieren. Allerdings wurde die Anleitung für
&architecture; noch nicht vollständig für den neuen Installer aufbereitet
und überprüft. Es gibt viele verbliebene Teile in diesem Handbuch, die
unvollständig oder überholt sind oder noch den
<quote>Boot-Floppy-Installer</quote> beschreiben. Sie können möglicherweise
eine neuere Version auf der <ulink url="&url-d-i;">&d-i;-Website</ulink>
finden, in der diese Architektur vielleicht besser dokumentiert ist.
Möglicherweise gibt es dort auch zusätzliche Übersetzungen.
</para></warning>

<note condition="checked"><para>
Obwohl diese Installationsanleitung für &architecture; überwiegend aktuell ist,
planen wir einige Änderungen und Umorganisationen nach der offiziellen
Herausgabe von &releasename-cap;. Sie finden möglicherweise eine neuere Version
dieses Handbuchs auf der <ulink url="&url-d-i;">&d-i;-Website</ulink>.
Möglicherweise gibt es dort auch zusätzliche Übersetzungen.
</para></note>
</para>

<para condition="translation-status">
Translators can use this paragraph to provide some information about
the status of the translation, for example if the translation is still
being worked on or if review is wanted (don't forget to mention where
comments should be sent!).

See build/lang-options/README on how to enable this paragraph.
Its condition is "translation-status".
</para>
</abstract>

<copyright>
 <year>2004</year>
 <year>2005</year>
 <year>2006</year>
 <year>2007</year>
 <year>2008</year>
 <holder>Das Debian-Installer-Team</holder>
</copyright>

<legalnotice>
<para>

Dieses Handbuch ist freie Software; Sie können es unter den Bedingungen
der GNU General Public License weiter veröffentlichen und/oder
verändern. Sie finden die Lizenz im <xref linkend="appendix-gpl"/>.

</para>
</legalnotice>
</bookinfo>
