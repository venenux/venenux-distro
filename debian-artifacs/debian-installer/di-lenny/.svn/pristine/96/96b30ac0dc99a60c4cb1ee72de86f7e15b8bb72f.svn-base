# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to French
# Copyright (C) 2004-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Christian Perrier <bubulle@debian.org>, 2002-2004.
# Pierre Machard <pmachard@debian.org>, 2002-2004.
# Denis Barbier <barbier@debian.org>, 2002-2004.
# Philippe Batailler <philippe.batailler@free.fr>, 2002-2004.
# Michel Grentzinger <mic.grentz@online.fr>, 2003-2004.
# Christian Perrier <bubulle@debian.org>, 2005, 2006, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-08-05 09:12+0200\n"
"Last-Translator: Christian Perrier <bubulle@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Mode de récupération"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Entrer dans le mode de récupération"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Aucune partition trouvée"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Aucune partition n'a été trouvée et il n'est donc pas possible de monter un "
"système de fichiers racine. Cela peut être dû à la non détection du disque "
"dur, à un échec de lecture sur la table de partitions ou à l'utilisation "
"d'un disque non partitionné. Vous avez la possibilité d'examiner ce problème "
"en utilisant un interpréteur de commandes dans l'environnement de "
"l'installateur."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Périphérique à monter comme système de fichiers racine (/) :"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Veuillez choisir un périphérique qui sera utilisé comme système de fichiers "
"racine. Vous pourrez ensuite choisir entre plusieurs opérations de "
"récupération à effectuer sur ce système de fichiers."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Périphérique inexistant"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Le périphérique que vous avez indiqué comme racine (${DEVICE}) n'existe pas. "
"Veuillez recommencer."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Échec du montage"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Une erreur s'est produite au montage sur /target de ${DEVICE} que vous avez "
"indiqué comme système de fichiers racine."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Veuillez consulter le journal du système pour plus d'informations."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Opérations du mode de récupération"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Échec du mode de secours"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"L'exécution de l'opération de secours « ${OPERATION} » a échoué avec le code "
"d'erreur ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Exécuter un shell dans ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Exécuter un shell dans le contexte de l'installateur"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Changer de système de fichiers racine"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Redémarrer le système"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Exécuter un shell (ligne de commande)"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Après ce message, vous aurez accès à la ligne de commande avec ${DEVICE} à "
"la racine (« / »). Si vous avez besoin d'autres systèmes de fichiers, comme "
"un système de fichiers /usr distinct, vous devrez les monter vous-même."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Erreur d'exécution de l'interpréteur de commandes dans /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Un interpréteur de commandes (« shell » : ${SHELL}) a été trouvé sur le "
"système de fichiers racine (${DEVICE}), mais une erreur s'est produite lors "
"de son lancement."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Aucun interpréteur de commandes (« shell ») dans /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Aucun interpréteur de commandes n'a été trouvé à la racine du système de "
"fichiers ${DEVICE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Shell interactif sur ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Après ce message, vous aurez accès à la ligne de commande avec ${DEVICE} "
"monté sur /target. Vous pourrez utiliser les outils disponibles dans "
"l'environnement de l'installateur. Si vous voulez utiliser temporairement /"
"target comme racine, vous pourrez utiliser la commande « chroot /target ». "
"Si vous avez besoin d'autres systèmes de fichiers, comme un système de "
"fichiers /usr distinct, vous devrez les monter vous-même."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Après ce message, vous aurez accès à la ligne de commande dans "
"l'environnement de l'installateur. Comme aucune partition n'a été trouvée, "
"aucun système de fichier ne sera monté automatiquement."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Shell interactif dans le contexte de l'installateur"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Phrase secrète pour ${DEVICE} :"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr ""
"Veuillez indiquer la phrase secrète utilisée pour le chiffrement du volume "
"${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Si vous n'indiquez pas de phrase secrète, ce volume ne sera pas disponible "
"pendant les opérations de récupération."
