# Brazilian Portuguese translation for win32-loader
# Copyright (C) 2008 Felipe Augusto van de Wiel (faw)
# This file is distributed under the same license as the win32-loader package.
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader (20080210)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-04-14 11:29-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: l10n portuguese <debian-l10n-portuguese@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_PORTUGUESEBR"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp437"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Brazilian Portuguese"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Instalador Debian"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "PortugueseBR.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Carregador do Instalador Debian"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Não foi possível encontrar win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini está incompleto. Contate o fornecedor desta mídia."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Este programa detectou que o tipo do seu teclado é \"$0\". Isso está correto?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Por favor, envie um relatório de bug (em inglês) com a seguinte informação:\n"
"\n"
" - Versão do Windows.\n"
" - Configurações Regionais.\n"
" - Tipo real do teclado.\n"
" - Tipo do teclado detectado.\n"
"\n"
"Obrigado."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Parece que não há espaço livre suficiente no disco $c. Para uma instalação "
"completa de área de trabalho (\"desktop\"), é recomendado ter, pelo menos, 3 "
"GB. Se já existe um disco ou partição separada para instalar o Debian, ou se "
"você planeja substituir o Windows completamente, você pode, seguramente, "
"ignorar este aviso."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Erro: não há espaço livre suficiente em disco. Abortando a instalação."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Este programa ainda não oferece suporte ao Windows $windows_version."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"A versão do Debian que você está tentando instalar foi desenhada para ser "
"executada em computadores modernos, 64-bit. No entanto, seu computador é "
"incapaz de executar programas 64-bit.\n"
"\n"
"Use a versão 32-bit (\"i386\") do Debian, ou a versão \"Multi-arch\" que é "
"capaz de instalar as duas versões.\n"
"\n"
"Este instalador abortará agora."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Seu computador é capaz de executar sistemas operacionais modernos, 64-bit. "
"No entanto, a versão do Debian que você está tentando instalar é desenhada "
"para ser executada em hardware antigo, 32-bit.\n"
"\n"
"Você pode continuar com esta instalação, mas para obter o máximo do seu "
"computador, nós recomendamos que você use a versão 64-bit (\"amd64\") do "
"Debian, ou a versão \"Multi-arch\" que é capaz de instalar as duas versões.\n"
"Você gostaria de abortar agora?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Selecione o modo de instalação:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Modo normal. Recomendado para a maioria dos usuários."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Modo \"expert\". Recomendado para usuários avançados que querem ter controle "
"total do processo de instalação."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Selecionar ação:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Instalar o Debian GNU/Linux neste computador."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Reparar um sistema Debian existente (modo \"rescue\")."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Instalação gráfica"

#: win32-loader.c:89
msgid "Text install"
msgstr "Instalação texto"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Baixando %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Conectando ..."

#: win32-loader.c:92
msgid "second"
msgstr "segundo"

#: win32-loader.c:93
msgid "minute"
msgstr "minuto"

#: win32-loader.c:94
msgid "hour"
msgstr "hora"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "s"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) de %dkB a %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s faltando)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Selecione qual versão do Instalador Debian usar:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Versão estável. Será instalado o Debian \"stable\"."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Imagem construída diariamente. Está é uma versão de desenvolvimento do "
"Instalador Debian. Ela instalará o Debian \"testing\" por padrão, e pode ser "
"capaz de também instalar a versão estável (\"stable\") ou a versão instável "
"(\"unstable\")."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"É recomendado que você verifique problemas conhecidos antes de usar uma "
"imagem construída diariamente. Você gostaria de fazer isso agora?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Ambiente de área de trabalho:"

#: win32-loader.c:114
msgid "None"
msgstr "Nenhum"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"O Carregador do Instalador Debian será configurado com os parâmetros a "
"seguir. NÃO mude nenhum deles a menos que você saiba o que está fazendo."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Configurações de proxy (máquina:porta):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Localização do boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "URL base para imagens \"netboot\" (linux e initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Erro"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Erro: falhou ao copiar $0 para $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Gerando $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Anexando informação de pré-configuração a $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Erro: incapaz de executar $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Desabilitando compressão NTFS nos arquivos de \"bootstrap\""

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registrando Instalador Debian no NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registrando Instalador Debian no BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Erro: falhou ao analisar a saída do bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr ""
"Erro: boot.ini não encontrado. Este é realmente o Windows $windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "NOTÍCIA MUITA IMPORTANTE:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"O segundo estágio deste processo de instalação iniciará agora. Após a sua "
"confirmação, este programa reiniciará o Windows no modo DOS, e "
"automaticamente carregará o Instalador do Debian.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Você precisa reinicializar para prosseguir com a sua instalação do Debian. "
"Durante a próxima inicialização, você será questionado se quer iniciar o "
"Windows ou o Instalador do Debian. Escolha o Instalador do Debian para "
"continuar com o processo de instalação.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Durante o processo de instalação, será oferecida a possibilidade de reduzir "
"a sua partição Windows para instalar o Debian ou completamente substituí-lo. "
"Em ambos os casos, é FORTEMENTE RECOMENDADO que você faça, previamente, um "
"backup dos seus dados. Nem os autores deste carregador nem o projeto Debian "
"terão QUALQUER RESPONSABILIDADE em caso de perda de dados.\\n\\nUma vez que "
"sua instalação Debian esteja completa (e você tenha escolhido manter o "
"Windows em seu disco), você pode desinstalar o Carregador do Instalador "
"Debian através da opção Adicionar/Remover Programas do Windows no Painel de "
"Controle."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Você gostaria de reinicializar agora?"

#~ msgid "Debconf preseed line:"
#~ msgstr "Linha de pré-configuração (\"preseed\") debconf:"
