colo-installer (1.13) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Pavel Piatruk
  * Bosnian (bs.po) by Armin Besirovic
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * French (fr.po) by Christian Perrier
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Milo Casagrande
  * Kurdish (ku.po) by Erdal Ronahi
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Russian (ru.po) by Yuri Kozlov
  * Serbian (sr.po) by Veselin Mijušković
  * Turkish (tr.po) by Mert Dirik
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 26 Sep 2008 14:15:18 +0300

colo-installer (1.12) unstable; urgency=low

  [ Updated translations ]
  * Finnish (fi.po) by Esko Arajärvi
  * Indonesian (id.po) by Arief S Fitrianto
  * Polish (pl.po) by Bartosz Fenski
  * Turkish (tr.po) by Recai Oktaş
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 15 Feb 2008 19:05:44 +0100

colo-installer (1.11) unstable; urgency=low

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Korean (ko.po) by Changwoo Ryu
  * Latvian (lv.po) by Viesturs Zarins
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Punjabi (Gurmukhi) (pa.po) by A S Alam

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 04 Feb 2008 23:43:57 +0100

colo-installer (1.10) unstable; urgency=low

  [ Otavio Salvador ]
  * Replace 'base-installer' dependency by 'installed-base' virtual
    package. Needs base-installer 1.81.

 -- Thiemo Seufer <ths@debian.org>  Tue, 11 Sep 2007 15:18:09 +0100

colo-installer (1.9) unstable; urgency=low

  [ Joey Hess ]
  * Multiply menu-item-numbers by 100

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Apr 2007 14:32:00 -0700

colo-installer (1.8) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Feb 2007 20:00:01 +0000

colo-installer (1.7) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Hebrew (he.po) by Lior Kaplan
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 01 Feb 2007 16:34:36 +0100

colo-installer (1.6) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Catalan; Valencian (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish; Castilian (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Gujarati (gu.po) by Kartik Mistry
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Italian (it.po) by Stefano Canepa
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Norwegian Bokmål; Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Panjabi; Punjabi (pa.po) by A S Alam
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Oct 2006 20:41:23 +0100

colo-installer (1.5) unstable; urgency=low

  * Old-style options for head/tail are no longer supported by new busybox.

 -- Thiemo Seufer <ths@debian.org>  Sat, 13 May 2006 23:05:10 +0100

colo-installer (1.4) unstable; urgency=low

  [ Joey Hess ]
  * Take down progress bar before exiting on error.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Baishampayan Ghose
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Norwegian Nynorsk (nn.po)
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovenian (sl.po) by Jure Cuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Jan 2006 22:46:58 +0000

colo-installer (1.3) unstable; urgency=low

  [ Christian Perrier ]
  * Rename the templates file to help out translators working
    on a single file

  [ Joey Hess ]
  * link_in_boot overriding moved to here from cobalt-scripts,
    as the template moved into base-installer.

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - German (de.po) by Holger Wansing
    - Greek (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagazy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Aug 2005 10:11:08 -0400

colo-installer (1.2) unstable; urgency=low

  * Updated translations: 
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Gallegan (gl.po) by Hctor Fenndez Lpez

 -- Thiemo Seufer <ths@debian.org>  Fri, 04 Feb 2005 16:59:18 +0100

colo-installer (1.1) unstable; urgency=low

  * Martin Michlmayr
    - Update postinst to create a configuration file in the target which
      deals with the console being turned on or off.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Italian (it.po) by Davide Meloni
    - Dutch (nl.po) by Bart Cornelis
    - Russian (ru.po) by Russian L10N Team

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 18 Dec 2004 13:28:00 +0000

colo-installer (1.0) unstable; urgency=low

  * Martin Michlmayr
    - Gratuitous version number bump.
  * Updated translations:
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjorn Steensrud
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 07 Oct 2004 14:01:32 +0100

colo-installer (0.12) unstable; urgency=low

  * Updated translations:
    - Swedish (sv.po) by Per Olofsson

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 26 Jul 2004 17:50:55 +0100

colo-installer (0.11) unstable; urgency=low

  * Martin Michlmayr
    - The vmlinux symlink is now in /boot so support that.
    - Make the script more robust and support the case when the
      vmlinux symlink is still in /.
  * Updated translations:
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - hr (hr.po) by Krunoslav Gernhard
    - Norwegian (nn.po) by Håvard Korsvoll

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 24 Jul 2004 18:45:13 +0100

colo-installer (0.10) unstable; urgency=low

  * Colin Watson
    - 'findfs /boot' now returns the empty string if /boot is on the root
      partition. Cope with this.

 -- Colin Watson <cjwatson@debian.org>  Tue,  6 Jul 2004 00:23:36 +0100

colo-installer (0.09) unstable; urgency=low

  * Colin Watson
    - Fix 'findfs /' (mount outputs "on /target", not "on /target/").

 -- Colin Watson <cjwatson@debian.org>  Sun,  4 Jul 2004 20:17:51 +0100

colo-installer (0.08) unstable; urgency=low

  * Martin Michlmayr
    - Stop using df to find the /target partition (failed on 100 gb drives
      due to field width issues), and make the file system type be found in
      a way that is less prey to false positives.
  * Updated translations:
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Hebrew (he.po) by Lior Kaplan
    - Slovenian (sl.po) by Jure Čuhalev

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 19 Jun 2004 17:30:39 +0100

colo-installer (0.07) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Italian (it.po) by Davide Meloni
    - Norwegian (nb.po) by Knut Yrvin
    - Slovenian (sl.po) by Jure Čuhalev
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joshua Kwan <joshk@triplehelix.org>  Tue, 25 May 2004 10:50:50 -0700

colo-installer (0.06) unstable; urgency=low

  * Martin Michlmayr
    - In the configuration file, print "Booting Debian" on the LCD after
      loading the kernel and before executing it.
  * Updated translations:
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - Gallegan (gl.po) by Héctor Fernández López
    - Hungarian (hu.po) by VERÓK István
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 20 May 2004 20:22:28 +0100

colo-installer (0.05) unstable; urgency=low

  * Martin Michlmayr
    - Add a progress bar.
    - Fix colo-installer/apt-install-failed default ("true", not "yes").
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernández-Sanguino
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by Christian Perrier
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Davide Meloni
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by André Dahlqvist
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 10 May 2004 14:46:03 +0100

colo-installer (0.04) unstable; urgency=low

  * Martin Michlmayr
    - Add a title for the main menu.  Closes: 245223
    - Write a default.colo script which CoLo 1.9 understands.
  * Updated translations:
    - Spanish (es.po) by Javier Fernández-Sanguino
    - Norwegian (nn.po) by Håvard Korsvoll
    - Swedish (sv.po) by André Dahlqvist
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 02 May 2004 17:44:19 +0100

colo-installer (0.03) unstable; urgency=low

  * Joey Hess
    - Add output field to force utf-8 encoding.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 24 Apr 2004 14:46:50 +0100

colo-installer (0.02) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Davide Meloni
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Norwegian (nb.po) by Bjørn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Nikolai Prokoschenko
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 23 Apr 2004 17:26:03 +0100

colo-installer (0.01) unstable; urgency=low

  * Initial release.
  * Updated translations:
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Dutch (nl.po) by Bart Cornelis
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Eugene Konev
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by André Dahlqvist
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 15 Apr 2004 21:44:52 +0100

