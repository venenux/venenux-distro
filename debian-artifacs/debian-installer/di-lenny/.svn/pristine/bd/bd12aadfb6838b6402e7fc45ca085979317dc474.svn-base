<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 48595 -->
<!-- traducido por jfs, 19 enero 2007 -->
<!-- revisado por Igor Tamara, enero 2007 -->

 <sect1 id="mail-setup">
 <title>Configurar su sistema para utilizar el correo electr�nico</title>
<para>

Hoy en d�a el correo electr�nico es una parte muy importante de la
vida de muchas personas. Dado que hay muchas opciones para
configurarlo y que para algunas herramientas de Debian es importante 
tener su sistema de correo configurado, se intentar� cubrir lo
b�sico de la configuraci�n del sistema de correo en esta secci�n.

</para><para>

Existen tres elementos principales que forman parte de los sistemas de
correo electr�nico. En primer lugar est� el <firstterm>Agente de
usuario de correo</firstterm> (o<firstterm>Mail User Agent</firstterm>
o MUA) que es el programa que el usuario utiliza para escribir y leer
el correo. Despu�s est� el <firstterm>Agente de transferencia de
correo</firstterm> (<firstterm>Mail Transfer Agent</firstterm> o MTA),
que se encarga de transferir los mensajes de un sistema a otro. Y por
�ltimo est� el <firstterm>Agente de entrega de correo</firstterm>
(<firstterm>Mail Delivery Agent</firstterm> o MDA) que se encarga de
transferir el correo entrante al buz�n del usuario.

</para><para>

Cada una de estas tres funciones la puede realizar un programa
distinto, pero tambi�n pueden combinarse en uno o dos
programas. Tambi�n es posible tener distintos programas que gestionen
estas funciones en funci�n del tipo de correo.

</para><para>

En el caso de Linux y sistemas Unix <command>mutt</command> ha sido
siempre un MUA muy popular. Como la mayor�a de los programas
tradicionales en Linux, se trata de un programa basado en texto. Muchas
veces se utiliza conjuntamente con <command>exim</command> o
<command>sendmail</command> como MTA y con <command>procmail</command>
como MDA.

</para><para>

Al hacerse cada vez m�s populares los entornos de escritorio gr�ficos
tambi�n se han hecho populares los programas gr�ficos para el correo
electr�nico como <command>evolution</command> de GNOME,
<command>kmail</command> de KDE o <command>thunderbird</command> de
Mozilla (que en Debian est� disponible con el nombre
<command>icedove</command><footnote>

<para>
Debian ha tenido que renombrar <command>thunderbird</command> a
<command>icedove</command> por razones de licenciamiento. Los detalles 
del por qu� est�n fuera del �mbito de este manual.
</para>

</footnote>). Estos programas combinan la funci�n de un MUA, MTA y
MDA, pero pueden (y se hace muchas veces) utilizarse en combinaci�n
con las herramientas de Linux tradicionales.


</para>

  <sect2 id="mail-default">
  <title>Configuraci�n de correo electr�nico por omisi�n</title>
<para>

Es importante que se instale y configure correctamente un MTA/MDA
tradicional en su sistema Linux aunque vaya a utilizar un programa
gr�fico de correo el�ctronico. La raz�n principal es que algunas herramientas que se ejecutan
en el sistema<footnote>

<para>
Entre �stas se encuentran: <command>cron</command>, <command>quota</command>,
<command>logcheck</command>, <command>aide</command>, &hellip;
</para>

</footnote> pueden tener que enviar informaci�n mediante el correo
electr�nico para informar al administrador del sistema de problemas
(potenciales) o de cambios que se han producido.


</para><para>

Es por esta misma raz�n por la que se instalar�n por omisi�n los
paquetes <classname>exim4</classname> y <classname>mutt</classname>
(siempre y cuando no haya deseleccionado la tarea
<quote>est�ndar</quote> durante la instalaci�n).
<classname>exim4</classname> es una combinaci�n de MTA/MDA que es
relativamente peque�o y muy flexible. La configuraci�n por omisi�n
har� que s�lo trate el correo local al sistema y env�e los correos
dirigidos al administrador del sistema (usuario root) a la cuenta de
usuario creada durante la instalaci�n<footnote>

<para>
El reenv�o del correo dirigido a root a una cuenta normal de usuario
se configura en <filename>/etc/aliases</filename>. Si no hay ninguna
cuenta de usuario el correo se enviar� a la propia cuenta de root.
</para>

</footnote>.

</para><para>

Cuando se env�an correos del sistema se guardan en el fichero
<filename>/var/mail/<replaceable>nombre_de_la_cuenta</replaceable></filename>.
Estos correos electr�nicos se pueden leer con <command>mutt</command>.

</para>
  </sect2>

  <sect2 id="mail-outgoing">
  <title>Enviar correo electr�nico fuera del sistema</title>
<para>

Como ya se ha mencionado, el instalador de Debian s�lo se configura
para gestionar el correo electr�nico de forma local en el sistema, no
para enviar correo a otros ni para recibir correos de otros.

</para><para>

Si desea que <classname>exim4</classname> gestione correo externo
deber� seguir las indicaciones que se describen en la siguiente
secci�n que describen las opciones de configuraci�n b�sica. Aseg�rese
de probar que el correo puede enviarse y recibirse correctamente.

</para><para>

Si va a utilizar un programa de correo gr�fico y va a utilizar el
servidor de correo de su proveedor de servicios a Internet (�Internet
Service Provider� o ISP), o el servidor de correo de su compa��a, no
necesita configurar <classname>exim4</classname> para gestionar el
correo externo. S�lo debe configurar su programa de correo electr�nico
favorito para que utilice los servidores apropiados para enviar y
recibir correo (describir c�mo hacerlo se sale del �mbito de este
manual).

</para><para>

En este caso, sin embargo, posiblemente tenga que configurar
herramientas individuales para enviar correctamente correos
electr�nicos. Una de estas utilidades es <command>reportbug</command>,
un programa que facilita el env�o de informes de error de paquetes
Debian.  Este programa, por omisi�n, espera poder utilizar
<classname>exim4</classname> para realizar el env�o de los informes de
error.

</para><para>

Para configurar correctamente <command>reportbug</command> para
utilizar un servidor de correo externo debe utilizar la orden
<command>reportbug --configure</command> y responder <quote>no</quote>
cuando se le pregunte si hay un MTA disponible.  A continuaci�n se le
preguntar� qu� servidor SMTP se debe utilizar para enviar informes de
error.

</para>
  </sect2>

 <sect2 id="config-mta">
 <title>Configurar su agente de transporte de correo</title>

<para>

Si quiere que su sistema gestione correo electr�nico saliente a
Internet deber� reconfigurar el paquete <classname>exim4</classname>
package<footnote>

<para>
Puede, por supuesto, eliminar <classname>exim4</classname> y reemplazarlo
por un MTA/MDA alternativo.
</para>

</footnote>:

<informalexample><screen>
# dpkg-reconfigure exim4-config
</screen></informalexample>

</para><para>

Una vez introduzca esta orden (como root), se le preguntar� si quiere
o no separar la configuraci�n en m�ltiples ficheros
peque�os. Seleccione la opci�n marcada por omisi�n si no est� seguro
de qu� es mejor.

</para><para>

A continuaci�n, se le presentar�n distintos escenarios habituales.
Elija aqu�l que se parezca m�s a sus necesidades:

</para>

<!-- jfs: En exim4 se pone 'Internet site' cambiar aqu� o en po-debconf? -->
<variablelist>
<varlistentry>
<term>equipo en Internet</term>
<listitem><para>

Su equipo est� conectado a una red y env�a y recibe correo directamente
a trav�s de SMTP. En las pantallas mostradas posteriormente se le har�n algunas
preguntas b�sicas incluyendo el nombre de correo de su equipo, o la lista de
dominios para los que acepta o reenv�a correo.

</para></listitem>
</varlistentry>

<!-- jfs: En exim4 se pone 'smarthost' cambiar aqu� o en po-debconf? -->
<varlistentry>
<term>se env�a el correo a trav�s de una pasarela</term>
<listitem><para>

En este escenario su correo saliente se reenv�a a trav�s de otro
equipo llamado <quote>pasarela</quote> (<quote>smarthost</quote>, N. del T.)
que es el que se encarga de gestionar su correo. Las pasarelas generalmente
tambi�n almacenar�n el correo entrante dirigido a su equipo de forma que no es
necesario que est� permanentemente conectado. Esto tambi�n significa que tendr�
que descargar su correo de la pasarela con un programa como
<command>fetchmail</command>. 

</para><para>

En muchos casos la pasarela ser� el servidor de correo de su ISP, lo
que hace que esta opci�n sea la m�s apropiada para los usuarios que
utilicen acceso remoto a redes. Tambi�n puede ser que su pasarela sea
el servidor de correo interno de su empresa o incluso otro sistema en
su misma red.

</para></listitem>
</varlistentry>


<varlistentry>
<term>se env�a el correo a trav�s de una pasarela, no hay correo local</term>
<listitem><para>

Esta opci�n es b�sicamente igual que la anterior con la diferencia que
el sistema no se configurar� para utilizar correo de un dominio local
de correo electr�nico. Se seguir� gestionando el correo del propio
sistema (como por ejemplo, el del administrador del sistema).

</para></listitem>
</varlistentry>

<varlistentry>
<term>solamente entrega local</term>
<listitem><para>

Esta es la opci�n que se configura por omisi�n en su sistema.

</para><para>

<!-- TODO: This explanation text was lost in the chapter change? -->

Su sistema no est� conectado a ninguna red y se env�a o recibe el
correo s�lo entre los usuarios locales. Se le recomienda seleccionar
esta opci�n aunque no tenga pensado enviar ning�n mensaje de correo,
ya que algunas herramientas del sistema pueden enviar alertas de
cuando en cuando (como por ejemplo, los mensajes de <quote>Disk quota
exceeded</quote>).  Esta opci�n tambi�n es la m�s conveniente para los
nuevos usuarios ya que no se les har� m�s preguntas.

</para></listitem>
</varlistentry>

<!-- jfs: Esto es lo que pone en exim4 pero 'no configurar en este momento' parece mejor -->
<varlistentry>
<term>sin configuraci�n de momento</term>
<listitem><para>

Elija esta opci�n si est� absolutamente seguro de lo que est� haciendo.  Esto
dejar� su sistema de correo sin configurar hasta que vd. lo haga.  Lo que
significa que el sistema no podr� enviar o recibir correo y puede que no reciba
avisos enviados por correo de las herramientas del sistema.

</para></listitem>
</varlistentry>
</variablelist>

<para>

Tendr� que editar los archivos de configuraci�n en el directorio
<filename>/etc/exim4</filename> una vez haya terminado la instalaci�n
si ninguno de los escenarios arriba indicados se ajusta a sus
necesidades.  Podr� encontrar m�s informaci�n sobre
<classname>exim4</classname> bajo
<filename>/usr/share/doc/exim4</filename>. Encontrar� m�s informaci�n
sobre c�mo configurar <classname>exim4</classname> en el fichero
<filename>README.Debian.gz</filename>. En este documento tambi�n se
explica c�mo puede conseguir documentaci�n adicional.


</para><para>

Tenga en cuenta que el env�o de correos directamente a Internet sin
tener un nombre de dominio oficial puede hacer que su correo se
rechace por las medidas contra el correo basura implementadas en los
servidores que lo reciben. Lo preferible es utilizar el servidor de
correo de su ISP. En el caso de que s� desee enviar correo
directamente deber�a utilizar un nombre de direcci�n de correo
distinta de la que se genera por omisi�n. Si utiliza
<classname>exim4</classname> como su MTA puede hacer esto a�adiendo
una entrada en su fichero <filename>/etc/email-addresses</filename>.

</para>
  </sect2>
 </sect1>
