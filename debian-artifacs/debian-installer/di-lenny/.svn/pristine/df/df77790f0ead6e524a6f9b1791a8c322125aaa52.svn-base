# Hungarian translation of Debian Installation Guide bookinfo
# SZERVÁC Attila <sas@321.hu>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-05-30 00:09+0000\n"
"PO-Revision-Date: 2007-03-18 07:20+0100\n"
"Last-Translator: SZERVÁC Attila <sas@321.hu>\n"
"Language-Team: Hungarian <debian-l10n-hungarian@lists.debian>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-Language: Hungarian\n"
"X-Poedit-Country: HUNGARY\n"

#. Tag: title
#: bookinfo.xml:5
#, no-c-format
msgid "&debian; Installation Guide"
msgstr "&debian; Telepítési Útmutató"

#. Tag: para
#: bookinfo.xml:8
#, no-c-format
msgid ""
"This document contains installation instructions for the &debian; &release; "
"system (codename <quote>&releasename;</quote>), for the &arch-title; "
"(<quote>&architecture;</quote>) architecture. It also contains pointers to "
"more information and information on how to make the most of your new Debian "
"system."
msgstr ""
"E dokumentum tartalmazza a telepítő leírást a &debian; &release; rendszerhez "
"(kódnév: <quote>&releasename;</quote>) &arch-title; (<quote>&architecture;</"
"quote>) architektúrára. További leírásokra is mutat és szól egy új Debian "
"telepítés leghasznosabb beállítási lehetőségeiről is."

#. Tag: para
#: bookinfo.xml:17
#, no-c-format
msgid ""
"Because the &arch-title; port is not a release architecture for "
"&releasename;, there is no official version of this manual for &arch-title; "
"for &releasename;. However, because the port is still active and there is "
"hope that &arch-title; may be included again in future official releases, "
"this development version of the Installation Guide is still available."
msgstr ""
"Mivel ezen &arch-title; port nem kiadandó a jelen &releasename; kiadáshoz, "
"nincs e kézikönyvnek hivatalos &arch-title; &releasename; verziója. De mivel "
"e port még élő és van remény később újból hivatalos &arch-title; kiadásra, a "
"Telepítő Útmutató e fejlesztői változata még mindig elérhető."

#. Tag: para
#: bookinfo.xml:25
#, no-c-format
msgid ""
"Because &arch-title; is not an official architecture, some of the "
"information, and especially some links, in this manual may be incorrect. For "
"additional information, please check the <ulink url=\"&url-ports;"
"\">webpages</ulink> of the port or contact the <ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; mailing list</ulink>."
msgstr ""
"Mivel a jelen &arch-title; nem hivatalos architektúra, egyes adatok, főleg "
"hivatkozások pontatlanok lehetnek. További adatokért lásd a port <ulink url="
"\"&url-ports;\">oldalait</ulink> vagy írj a <ulink url=\"&url-list-subscribe;"
"\">debian-&arch-listname; lista</ulink> címére."

#. Tag: para
#: bookinfo.xml:36
#, no-c-format
msgid ""
"This installation guide is based on an earlier manual written for the old "
"Debian installation system (the <quote>boot-floppies</quote>), and has been "
"updated to document the new Debian installer. However, for &architecture;, "
"the manual has not been fully updated and fact checked for the new "
"installer. There may remain parts of the manual that are incomplete or "
"outdated or that still document the boot-floppies installer. A newer version "
"of this manual, possibly better documenting this architecture, may be found "
"on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</ulink>. You "
"may also be able to find additional translations there."
msgstr ""
"Ez a telepítő kézikönyv a régi (<quote>boot-floppies</quote> nevű) Debian "
"telepítő rendszerhez írt egy korábbi kézikönyvre épül, és az új Debian "
"telepítő leírására frissült. Ennek ellenére előfordulhat, hogy a jelen, "
"&architecture; architektúrához készült útmutató frissítése, vagy tényeinek "
"igazolása nem teljesen készült el az új telepítőhöz. Maradhattak a "
"kézikönyvnek nem teljes vagy elavult vagy még mindig a boot-floppies "
"telepítőt leíró részei. E kézikönyv egy újabb változata, mely pontosabban "
"leírja ezt az architektúrát a <ulink url=\"&url-d-i;\">&d-i; honlapon</"
"ulink> van. Itt további fordítások is találhatók."

#. Tag: para
#: bookinfo.xml:49
#, no-c-format
msgid ""
"Although this installation guide for &architecture; is mostly up-to-date, we "
"plan to make some changes and reorganize parts of the manual after the "
"official release of &releasename;. A newer version of this manual may be "
"found on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>. You may also be able to find additional translations there."
msgstr ""
"Bár a jelen, &architecture; architektúrához készült útmutató elég naprakész, "
"a kézikönyv egyes módosításait és részeinek átszervezését tervezzük a most "
"következő &releasename; kiadás hivatalos megjelenése után. E kézikönyv "
"legújabb változata mindig a <ulink url=\"&url-d-i;\">&d-i; honlapon</ulink> "
"van. Itt további fordítások is vannak."

#. Tag: para
#: bookinfo.xml:58
#, no-c-format
msgid ""
"Translators can use this paragraph to provide some information about the "
"status of the translation, for example if the translation is still being "
"worked on or if review is wanted (don't forget to mention where comments "
"should be sent!). See build/lang-options/README on how to enable this "
"paragraph. Its condition is \"translation-status\"."
msgstr ""
"E fordítás korai, reményeink szerint kevés tartalmi hibával bír. A fordítást "
"koordinálta: SZERVÁC Attila (sas @ wagner.d.o). A fordítást karbantartja: "
"Magyar Debian Alapítvány - 2006. december 31. napjától. Külön köszönet: Nagy "
"Zoltán - nagy.zoltan@szabadember - ARM fordítás."

#. Tag: holder
#: bookinfo.xml:75
#, no-c-format
msgid "the Debian Installer team"
msgstr "a Debian Telepítő csapat"

#. Tag: para
#: bookinfo.xml:79
#, no-c-format
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License. Please refer to the license in "
"<xref linkend=\"appendix-gpl\"/>."
msgstr ""
"E kézikönyv szabad szoftver; terjesztheted és/vagy módosíthatod a GNU "
"General Public License szerint. Ez a licenc itt található: <xref linkend="
"\"appendix-gpl\"/>."
