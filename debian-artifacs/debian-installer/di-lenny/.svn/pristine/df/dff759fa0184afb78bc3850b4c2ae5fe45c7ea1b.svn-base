msgid ""
msgstr ""
"Project-Id-Version: console-setup\n"
"Report-Msgid-Bugs-To: console-setup@packages.debian.org\n"
"POT-Creation-Date: 2008-07-11 20:12+0200\n"
"PO-Revision-Date: 2008-07-23 20:30+0200\n"
"Last-Translator: Esko Arajärvi <edu@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Country: FINLAND\n"

#. Type: text
#. Description
#. Main menu item. Please keep below 55 columns
#: ../console-setup.templates:1001
msgid "Configure the keyboard"
msgstr "Tee näppäimistön asetukset"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Arabic"
msgstr ". Arabialainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Armenian"
msgstr "# Armenialainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Cyrillic - KOI8-R and KOI8-U"
msgstr "# Kyrillinen - KOI8-R ja KOI8-U"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Cyrillic - non-Slavic languages"
msgstr "# Kyrillinen - ei-slaavilaiset kielet"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Cyrillic - Slavic languages (also Bosnian and Serbian Latin)"
msgstr "# Kyrillinen - slaavilaiset kielet (myös Bosnian ja Serbian Latin)"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Ethiopic"
msgstr ". Etiopialainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Georgian"
msgstr "# Georgialainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Greek"
msgstr "# Kreikkalainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Hebrew"
msgstr "# Heprealainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Lao"
msgstr "# Laolainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Latin1 and Latin5 - western Europe and Turkic languages"
msgstr "# Latin1 ja Latin5 - länsieurooppalaiset ja turkkilaiset kielet"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Latin2 - central Europe and Romanian"
msgstr "# Latin2 - keskieurooppalaiset kielet ja romania"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Latin3 and Latin8 - Chichewa; Esperanto; Irish; Maltese and Welsh"
msgstr "# Latin3 ja Latin8 - chichewa; esperanto; iiri; malta; kymri"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Latin7 - Lithuanian; Latvian; Maori and Marshallese"
msgstr "# Latin7 - liettua; latvia; maori; marshallin kieli"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Latin - Vietnamese"
msgstr ". Latin - vietnamilainen"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid "# Thai"
msgstr "# Thai"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Combined - Latin; Slavic Cyrillic; Hebrew; basic Arabic"
msgstr ""
". Yhdistelmä - Latin; slaavilainen kyrillinen; heprea; arabian perusversio"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Combined - Latin; Slavic Cyrillic; Greek"
msgstr ". Yhdistelmä - Latin; slaavilainen kyrillinen; kreikka"

#. Type: select
#. Choices
#: ../console-setup.templates:2001
msgid ". Combined - Latin; Slavic and non-Slavic Cyrillic"
msgstr ". Yhdistelmä - Latin; slaavilainen ja ei-slaavilainen kyrillinen"

#. Type: select
#. Description
#: ../console-setup.templates:2002
msgid "Set of characters that should be supported by the console font:"
msgstr "Merkkijoukko, joita konsolifontin tulisi tukea:"

#. Type: select
#. Description
#: ../console-setup.templates:2002
msgid ""
"If you don't use a framebuffer, the choices that start with \".\" will "
"reduce the number of available colors on the console."
msgstr ""
"Jos kehyspuskurointi ei ole käytössä, merkillä ”.” alkavat vaihtoehdot "
"rajoittavat konsolilla käytössä olevien värien määrää."

#. Type: select
#. Description
#: ../console-setup.templates:3001
msgid "Keyboard model:"
msgstr "Näppäimistön malli:"

#. Type: select
#. Description
#: ../console-setup.templates:4001
msgid "Origin of the keyboard:"
msgstr "Näppäimistön alkuperä:"

#. Type: select
#. Description
#: ../console-setup.templates:5001
msgid "Keyboard layout:"
msgstr "Näppäinasettelu:"

#. Type: select
#. Description
#: ../console-setup.templates:5001
msgid ""
"There is more than one keyboard layout with the origin you selected. Please "
"select the layout matching your keyboard."
msgstr ""
"Valittua näppäimistön alkuperää vastaa useampi näppäinasettelu. Valitse "
"oikea näppäinasettelu."

#. Type: error
#. Description
#: ../console-setup.templates:6001
msgid "Unsupported settings in configuration file"
msgstr "Ei-tuettuja asetuksia asetustiedostossa"

#. Type: error
#. Description
#: ../console-setup.templates:6001
msgid ""
"The configuration file /etc/default/console-setup specifies a keyboard "
"layout and variant that are not supported by the configuration program.  "
"Because of that, no questions about the keyboard layout will be asked and "
"your current configuration will be preserved."
msgstr ""
"Asetustiedostossa /etc/default/console-setup on määritelty näppäinasettelu "
"ja muunnelma, joita asetusohjelma ei tue. Tästä syystä näppäinasetteluun "
"liittyviä kysymyksiä ei kysytä ja nykyiset asetukset säilytetään."

#. Type: select
#. Description
#. The languages with many non-ASCII letters should not use formatted
#. lists.  If you decide to use formatted lists then keep the lines
#. relatively short.
#: ../console-setup.templates:7001
msgid "Font for the console:"
msgstr "Konsolissa käytettävä fontti:"

#. Type: select
#. Description
#. The languages with many non-ASCII letters should not use formatted
#. lists.  If you decide to use formatted lists then keep the lines
#. relatively short.
#: ../console-setup.templates:7001
msgid "Please choose the font face you would like to use on Linux console."
msgstr "Valitse fontti, jota Linux-konsolissa käytetään."

#. Type: select
#. Description
#. The languages with many non-ASCII letters should not use formatted
#. lists.  If you decide to use formatted lists then keep the lines
#. relatively short.
#: ../console-setup.templates:7001
msgid ""
" - VGA has a traditional appearance and has medium coverage\n"
"   of international scripts;\n"
" - Fixed has a simplistic appearance and has better coverage\n"
"   of international scripts;\n"
" - Terminus is aimed to reduce eye fatigue, though some symbols\n"
"   have a similar aspect which may be a problem for programmers."
msgstr ""
" - VGA:      Perinteinen ulkoasu, kohtuullinen tuki eri kielien merkeille.\n"
" - Fixed:    Yksinkertaisempi ulkoasu, parempi tuki eri kielien merkeille.\n"
" - Terminus: Suunniteltu vähentämään silmien väsymistä. Tietyt merkit\n"
"             näyttävät toisiltaan, mikä saattaa haitata ohjelmoitaessa."

#. Type: select
#. Description
#. The languages with many non-ASCII letters should not use formatted
#. lists.  If you decide to use formatted lists then keep the lines
#. relatively short.
#: ../console-setup.templates:7001
msgid ""
"If you prefer a bold version of the Terminus font, then choose TerminusBold "
"if you use a framebuffer, otherwise TerminusBoldVGA."
msgstr ""
"Jos käyttöön halutaan Terminus-fontin lihavoitu versio ja kehyspuskurointi "
"on käytössä, valitse TerminusBold, muussa tapauksessa valitse "
"TerminusBoldVGA."

#. Type: select
#. Description
#. Type: select
#. Description
#: ../console-setup.templates:8001 ../console-setup.templates:9001
msgid "Font size:"
msgstr "Fontin koko:"

#. Type: select
#. Description
#: ../console-setup.templates:8001
msgid ""
"Please select the size of the font for the Linux console.  For reference, "
"the font your computer starts with has size 16."
msgstr ""
"Valitse Linux-konsolilla käytettävän fontin koko. Vertailukohtana voidaan "
"käyttää tietokoneen käynnistyessä käytettävää koon 16 fonttia."

#. Type: select
#. Description
#: ../console-setup.templates:9001
msgid ""
"Please select the size of the font for the Linux console.  When the size is "
"represented as a plain number then the corresponding font can be used with "
"all console drivers and the number measures the height of the symbols (in "
"number of scan lines).  Otherwise the size has the format HEIGHTxWIDTH and "
"the corresponding fonts can be used only if you use framebuffer and the kbd "
"console package (console-tools doesn't work for such fonts).  Currently "
"these fonts cannot be used if the framebuffer you use is based on the "
"RadeonFB kernel module."
msgstr ""
"Valitse Linux-konsolin fontin koko. Kun koko annetaan pelkkänä numerona, "
"sitä vastaavaa fonttia voidaan käyttää kaikkien konsoliajurien kanssa ja "
"numero mittaa symbolien korkeutta (piirtorivien määränä). Vaihtoehtoisesti "
"koko annetaan muodossa KORKEUSxLEVEYS ja vastaavia fontteja voidaan käyttää "
"vain käytettäessä kehyspuskurointia ja konsolipakettia kbd (console-tools ei "
"toimi tällaisten fonttien kanssa). Tällä hetkellä näitä fontteja ei voida "
"käyttää, jos käytössä on RadeonFB-ydinmoduuliin perustuva kehyspuskuri."

#. Type: select
#. Description
#: ../console-setup.templates:9001
msgid ""
"You can use the height of the fonts in order to figure out the real size of "
"the symbols on the console.  For reference, the font your computer starts "
"with has height 16."
msgstr ""
"Fonttien korkeuksia voidaan käyttää apuna määritettäessä konsolilla näkyvien "
"symbolien kokoa. Vertailukohtana voidaan käyttää tietokoneen käynnistyessä "
"käytettävää koon 16 fonttia."

#. Type: select
#. Description
#: ../console-setup.templates:10001
msgid "Encoding on the console:"
msgstr "Konsolilla käytettävä koodaus:"

#. Type: string
#. Description
#: ../console-setup.templates:11001
msgid "Virtual consoles in use:"
msgstr "Käytössä olevat virtuaalikonsolit:"

#. Type: string
#. Description
#: ../console-setup.templates:11001
msgid ""
"Please enter a space-delimited list of virtual consoles you use. The usual "
"Unix filename wildcards are allowed (*, ? and [...])."
msgstr ""
"Anna välilyönnein eroteltu luettelo käytettävistä virtuaalikonsoleista. "
"Tavallisia Unixin tiedostonimien korvausmerkkejä voidaan käyttää (*, ? ja "
"[...])."

#. Type: string
#. Description
#: ../console-setup.templates:11001
msgid ""
"If you are unsure, then use the default /dev/tty[1-6] which stands for six "
"virtual consoles. If you use devfs, then enter /dev/vc/[1-6] instead."
msgstr ""
"Jos olet epävarma, käytä oletusta /dev/tty[1-6], joka tarkoittaa kuutta "
"virtuaalikonsolia. Käytettäessä devfs:ää, syötä edellisen sijaan /dev/vc/[1-"
"6]."

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:15001
msgid "Caps Lock"
msgstr "Caps Lock"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:13001
#: ../console-setup.templates:14001 ../console-setup.templates:15001
msgid "Right Alt"
msgstr "Oikea Alt"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:14001
#: ../console-setup.templates:15001
msgid "Right Control"
msgstr "Oikea Control"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Right Shift"
msgstr "Oikea Vaihto"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:13001
#: ../console-setup.templates:14001 ../console-setup.templates:15001
msgid "Right Logo key"
msgstr "Oikea Logo-näppäin"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:14001
#: ../console-setup.templates:15001
msgid "Menu key"
msgstr "Menu-näppäin"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Alt+Shift"
msgstr "Alt+Vaihto"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Control+Shift"
msgstr "Control+Vaihto"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Control+Alt"
msgstr "Control+Alt"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Alt+Caps Lock"
msgstr "Alt+Caps Lock"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Left Control+Left Shift"
msgstr "Vasen Control+Vasen Vaihto"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:13001
#: ../console-setup.templates:14001
msgid "Left Alt"
msgstr "Vasen Alt"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Left Control"
msgstr "Vasen Control"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Left Shift"
msgstr "Vasen Vaihto"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:12001 ../console-setup.templates:13001
#: ../console-setup.templates:14001 ../console-setup.templates:15001
msgid "Left Logo key"
msgstr "Vasen Logo-näppäin"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "Scroll Lock key"
msgstr "Scroll Lock -näppäin"

#. Type: select
#. Choices
#: ../console-setup.templates:12001
msgid "No toggling"
msgstr "Ei tapaa vaihtamiseen"

#. Type: select
#. Description
#: ../console-setup.templates:12002
msgid "Method for toggling between national and Latin mode:"
msgstr "Tapa paikallisen ja Latin-tilan välillä vaihtamiseen:"

#. Type: select
#. Description
#: ../console-setup.templates:12002
msgid ""
"You will need a way to toggle the keyboard between the national layout and "
"the standard Latin layout.  Several options are available."
msgstr ""
"Näppäinasettelua halutaan usein vaihtaa paikallisen ja standardin Latin-"
"näppäinasettelun välillä. Tämä voidaan tehdä usealla tavalla."

#. Type: select
#. Description
#: ../console-setup.templates:12002
msgid ""
"The most ergonomic choices seem to be the right Alt and the Caps Lock keys "
"(in the latter case, use the combination Shift+Caps Lock for normal Caps "
"toggle).  Another popular choice is the Alt+Shift combination; note however "
"that in this case the combination Alt+Shift (or Control+Shift if you choose "
"it) will lose its usual meaning in Emacs and other programs using it."
msgstr ""
"Helpoin näistä vaihtoehdoista on ehkä oikea Alt tai Caps Lock (käytä "
"jälkimmäisessä tapauksessa yhdistelmää Vaihto+Caps Lock normaaliin "
"kirjainkoon vaihtoon). Toinen suosittu vaihtoehto on yhdistelmä Alt+Vaihto. "
"Tällöin kuitenkin on huomattava, että tämä yhdistelmä (tai vastaavasti "
"Control+Vaihto) ei toimi Emacsissa tai sitä käyttävissä ohjelmissa."

#. Type: select
#. Description
#: ../console-setup.templates:12002
msgid "Note that the listed keys are not present on all keyboards."
msgstr "Kaikkia lueteltuja näppäimiä ei ole kaikissa näppäimistöissä."

#. Type: select
#. Choices
#: ../console-setup.templates:13001
msgid "No temporary switch"
msgstr "Ei väliaikaista muuntajaa"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../console-setup.templates:13001 ../console-setup.templates:14001
msgid "Both Logo keys"
msgstr "Molemmat Logo-näppäimet"

#. Type: select
#. Description
#: ../console-setup.templates:13002
msgid "Method for temporarily toggling between national and Latin input:"
msgstr "Tapa paikallisen ja Latin-merkistön välillä vaihtamiseen:"

#. Type: select
#. Description
#: ../console-setup.templates:13002
msgid ""
"Sometimes the keyboard is in national mode and you want to type only a few "
"Latin letters. In this case it may be desirable to have a key for "
"temporarily switching between national and Latin symbols.  While this key is "
"pressed in national mode, the keyboard types Latin letters.  Conversely, "
"when the keyboard is in Latin mode and this key is pressed, the keyboard "
"will type national letters."
msgstr ""
"Joskus näppäimistö on paikallisessa tilassa ja halutaan kirjoittaa vain "
"muutamia Latin-merkkejä. Tällaisessa tilanteessa on kätevää, jos paikallinen "
"näppäimistö voidaan väliaikaisesti muuntaa Latin-näppäimistöksi yhdellä "
"näppäimellä. Kun tätä näppäintä painetaan paikallisessa tilassa, näppäimistö "
"kirjoittaa Latin-merkkejä. Vastaavasti, kun näppäimistö on Latin-tilassa ja "
"tätä näppäintä painetaan, näppäimistö kirjoittaa paikallisia merkkejä."

#. Type: select
#. Description
#: ../console-setup.templates:13002
msgid ""
"If you don't like this feature, choose the option \"No temporary switch\"."
msgstr ""
"Jos tätä ominaisuutta ei haluta käyttää, valitse ”Ei väliaikaista muuntajaa”."

#. Type: select
#. Choices
#: ../console-setup.templates:14001
msgid "No AltGr key"
msgstr "Ei AltGr-näppäintä"

#. Type: select
#. Choices
#: ../console-setup.templates:14001
msgid "Keypad Enter key"
msgstr "Numeronäppäimistön Enter-näppäin"

#. Type: select
#. Choices
#: ../console-setup.templates:14001
msgid "Both Alt keys"
msgstr "Molemmat Alt-näppäimet"

#. Type: select
#. Description
#: ../console-setup.templates:14002
msgid "AltGr key replacement:"
msgstr "AltGr-näppäimen korvaaja:"

#. Type: select
#. Description
#: ../console-setup.templates:14002
msgid ""
"With some keyboard layouts, AltGr is a modifier key used to input some "
"characters, primarily ones that are unusual for the language of the keyboard "
"layout, such as foreign currency symbols and accented letters.  If a key has "
"a third symbol on it (on the front vertical face or the bottom right of the "
"key top, sometimes in a different color), then AltGr is often the means of "
"eliciting that symbol."
msgstr ""
"Joissain näppäinasetteluissa AltGr on muuntonäppäin, jonka avulla voidaan "
"syöttää joitain merkkejä. Nämä on yleensä näppäinasettelun kielelle "
"epätyypillisiä, kuten ulkomaisia valuuttasymboleita ja aksentoituja "
"kirjaimia. Jos näppäimessä on kolmas symboli, se saadaan yleensä syötettyä "
"kyseisen näppäimen ja AltGr-näppäimen yhdistelmällä."

#. Type: select
#. Choices
#: ../console-setup.templates:15001
msgid "No compose key"
msgstr "Ei yhdistelmänäppäintä"

#. Type: select
#. Description
#: ../console-setup.templates:15002
msgid "Compose key:"
msgstr "Yhdistelmänäppäin:"

#. Type: select
#. Description
#: ../console-setup.templates:15002
msgid ""
"The Compose key (known also as Multi_key) causes the computer to interpret "
"the next few keystrokes as a combination in order to produce a character not "
"found on the keyboard."
msgstr ""
"Yhdistelmänäppäin (Compose key tai Multi_key) käskee tietokonetta "
"tulkitsemaan pari seuraavaa näppäilyä yhdistelmäksi. Näin voidaan tuottaa "
"merkkejä, joita ei löydy näppäimistöltä."

#. Type: select
#. Description
#: ../console-setup.templates:15002
msgid ""
"On the text console the Compose key does not work in Unicode mode. If not in "
"Unicode mode, regardless of what you choose here, you can always also use "
"the Alt+period combination as a Compose key."
msgstr ""
"Tekstikonsolilla yhdistelmänäppäin ei toimi Unicode-tilassa. Muussa kuin "
"Unicode-tilassa voidaan tästä valinnasta riippumatta aina käyttää "
"yhdistelmää Alt+piste yhdistelmänäppäimenä."
