<!-- original version: 50620 -->

<para>

Afhankelijk van de eerder geselecteerde locatie, kan een keuzelijst met
tijdzones relevant voor die lokatie worden getoond. Als voor uw locatie
slechts één tijdzone relevant is, zal niets worden gevraagd maar wordt
de betreffende tijdzone automatisch ingesteld.

</para><para>

Als u, om wat voor reden dan ook, een tijdzone wilt instellen die
<emphasis>niet</emphasis> past bij de geselecteerde locatie, dan zijn er
twee opties.

</para>

<orderedlist>
<listitem>

<para>

De meest eenvoudige optie is om gewoon een andere tijdzone in te stellen
nadat de installatie is afgerond en u uw nieuwe systeem heeft opgestart.
Het juiste commando hiervoor is:

<informalexample><screen>
# dpkg-reconfigure tzdata
</screen></informalexample>

</para>

</listitem><listitem>

<para>

Een alternatieve mogelijkheid is om de tijdzone helemaal aan het begin van
de installatie op te geven door de parameter
<userinput>time/zone=<replaceable>waarde</replaceable></userinput>
mee te geven bij het opstarten van het installatiesysteem.
De waarde dient uiteraard een geldige tijdzone te zijn, zoals
<userinput>Europe/Amsterdam</userinput> of <userinput>UTC</userinput>.

</para>

</listitem>
</orderedlist>

<para>

Voor geautomatiseerde installaties kan de tijdzone ook worden ingesteld door
gebruikmaking van voorconfiguratie.

</para>
