<!-- original version: 56425 -->

<!-- This section is heavily outdated. It only really describes
     older BIOSes and not the current situation. Most of it is not
     really relevant for AMD64, but a general description would be.
     File should be renamed to x86.xml if a more general text is
     written. -->


  <sect2 arch="x86" id="bios-setup">
<title>Le menu de configuration du BIOS</title>

<para>

Le BIOS fournit les fonctions de base nécessaires à l'amorçage de votre
machine pour permettre au système d'exploitation d'accéder au matériel. Le 
système fournit probablement un menu destiné à la configuration du BIOS. 
Avant l'installation, vous <emphasis>devez</emphasis> vous assurer que votre 
BIOS est configuré correctement&nbsp;; ne pas le faire peut mener à des 
arrêts intermittents ou à l'impossibilité d'installer Debian.

</para><para>

La suite de cette section provient de la 
<ulink url="&url-pc-hw-faq;">FAQ sur le matériel PC</ulink> et répond à la 
question&nbsp;:
<quote>comment entrer dans le menu de configuration CMOS&nbsp;?</quote>.
La manière d'accéder au menu de configuration du BIOS (ou <quote>CMOS</quote>)
dépend du concepteur du BIOS&nbsp;:

</para>

<!-- From: burnesa@cat.com (Shaun Burnet) -->
<variablelist>

<varlistentry>
 <term>AMI BIOS</term>
 <listitem><para>

<keycap>Suppr</keycap> pendant l'auto-test de démarrage.

</para></listitem>
</varlistentry>

<varlistentry>
 <term>Award BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>Esc</keycap>
</keycombo>, ou <keycap>Suppr</keycap> pendant l'auto-test de démarrage

</para></listitem>
</varlistentry>

<varlistentry><term>DTK BIOS</term>
 <listitem><para>

<keycap>Esc</keycap> pendant l'auto-test de démarrage
</para></listitem>
</varlistentry>

<varlistentry><term>IBM PS/2 BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>Insert</keycap>
</keycombo>
après
<keycombo>
  <keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>Suppr</keycap>
</keycombo>

</para></listitem>
</varlistentry>

<varlistentry>
 <term>Phoenix BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>Esc</keycap>
</keycombo>
ou
<keycombo>
  <keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>S</keycap>
</keycombo>
ou
<keycap>F1</keycap>

</para></listitem>
</varlistentry>
</variablelist>

<para>

Des informations pour appeler d'autres routines du BIOS se trouvent dans
<ulink url="&url-invoking-bios-info;"></ulink>.

</para><para>

Certaines machines &arch-title; n'ont pas de menu de configuration CMOS
dans le BIOS. Elles ont besoin d'un programme de configuration CMOS au
niveau logiciel. Si vous n'avez pas les disquettes d'installation ou de
diagnostics, vous pouvez essayer un programme gratuit ou un partagiciel.
Consultez <ulink url="&url-simtel;"></ulink>.

</para>
  </sect2>


  <sect2 arch="x86" id="boot-dev-select"><title>Sélection du périphérique d'amorçage</title>

<para>

De nombreux menus de configuration des BIOS permettent de choisir les
périphériques utilisés pour l'amorçage du système. Réglez votre BIOS
pour rechercher un système d'exploitation amorçable sur 
<filename>A:</filename> (le premier lecteur de disquettes), puis en option 
sur le premier lecteur de cédérom (qui apparaît peut-être comme 
<filename>D:</filename> ou <filename>E:</filename>), et ensuite
sur <filename>C:</filename> (le premier disque dur). Cette configuration  
permet de démarrer soit à partir d'une disquette, soit d'un cédérom, qui sont 
les deux périphériques de démarrage les plus courants pour installer Debian.

</para><para>

Si vous possédez un contrôleur SCSI récent et un lecteur de cédérom qui lui
est attaché, vous pourrez normalement amorcer depuis le lecteur de cédérom. 
Tout ce que vous avez à faire est de permettre l'amorçage depuis un cédérom 
dans le BIOS SCSI de votre contrôleur.

</para><para>

Une autre option est d'amorcer à partir d'un périphérique USB, appelé aussi
mémoire USB ou clé USB. Certain BIOS savent amorcer directement les 
périphériques USB, d'autres non. Il vous faudra peut-être configurer votre
BIOS et choisir <quote>disque extractible</quote> ou même 
<quote>USB-ZIP</quote> pour qu'il puisse amorcer le périphérique USB.

</para><para>

Voici quelques précisions permettant de choisir l'ordre d'amorçage.
Il faudra remettre l'ordre initial après avoir installé Linux, afin de
pouvoir réamorcer la machine à partir du disque dur.

</para>

   <sect3 id="ctbooi">
   <title>Changer l'ordre d'amorçage sur les ordinateurs IDE</title>

<orderedlist>
<listitem><para>

Au démarrage de l'ordinateur, appuyez sur les touches qui
permettent d'entrer dans le menu du BIOS. Souvent, il s'agit de la
touche <keycap>Suppr</keycap>. Consultez le manuel fourni 
avec votre matériel pour connaître la séquence de touches exacte&nbsp;;

</para></listitem>
<listitem><para>

Trouvez la séquence d'amorçage dans l'utilitaire de configuration.
Son emplacement dépend de votre BIOS, mais cherchez un champ qui
liste les disques durs.

</para><para>

Les entrées habituelles sur des machines IDE sont C, A, cdrom ou A, C, cdrom.

</para><para>

Le disque dur est représenté par C et le lecteur de disquettes par A.

</para></listitem>
<listitem><para>

Modifiez l'ordre d'amorçage afin de mettre le cédérom ou la disquette en 
premier. Souvent, les touches <keycap>Page Up</keycap>
et <keycap>Page Down</keycap> permettent de se déplacer dans les
différents choix&nbsp;;

</para></listitem>
<listitem><para>

Enregistrez vos modifications. Des instructions à l'écran vous
indiquent comment effectuer cette opération sur votre ordinateur.

</para></listitem>
</orderedlist>
   </sect3>

   <sect3 id="ctboos">
   <title>Changer l'ordre d'amorçage sur les ordinateurs SCSI</title>
<para>

<orderedlist>
<listitem><para>

Au démarrage de l'ordinateur, appuyez sur les touches qui
    permettent d'entrer dans le menu de configuration SCSI.

</para><para>

Vous pouvez lancer l'utilitaire de configuration SCSI après la
    phase de vérification de la mémoire et l'affichage du message
    indiquant comment entrer dans la configuration du BIOS, lors du
    démarrage de l'ordinateur.

</para><para>

Les touches sur lesquelles appuyer dépendent de l'utilitaire.
Souvent, il s'agit de 
<keycombo><keycap>Ctrl</keycap><keycap>F2</keycap></keycombo>.
Vérifiez dans le manuel fourni avec votre matériel
la séquence de touches exacte&nbsp;;

</para></listitem>
<listitem><para>

Trouvez l'utilitaire qui permet de changer l'ordre d'amorçage&nbsp;;

</para></listitem>
<listitem><para>

Configurez l'utilitaire afin de mettre l'identifiant du disque
    SCSI en premier dans la liste&nbsp;;

</para></listitem>
<listitem><para>

Enregistrez vos modifications. Des instructions à l'écran vous indiquent 
comment effectuer cette opération sur votre ordinateur.
Souvent, vous devez appuyer sur <keycap>F10</keycap>.

</para></listitem>
</orderedlist>

</para>
   </sect3>
</sect2>
<sect2 arch="x86">
  <title>Autres réglages du BIOS</title>


   <sect3 id="cd-settings"><title>Paramètres du cédérom</title>
<para>

Certains BIOS (tels les BIOS Award) permettent de régler automatiquement
la vitesse du cédérom. Vous devriez éviter cela et, à la place, utiliser la 
vitesse la plus basse possible. Quand vous obtenez un message d'erreur du 
type <userinput>seek failed</userinput>, le problème vient sans doute de là.

</para>
   </sect3>

   <sect3><title>Mémoire étendue (Extended) et mémoire paginée (Expanded)</title>
<para>

Si vous pouvez utiliser sur votre système à la fois de la mémoire étendue
(<emphasis>extended</emphasis>) et de la mémoire paginée 
(<emphasis>expanded</emphasis>), configurez-le pour utiliser un maximum de 
mémoire étendue et un minimum de mémoire paginée. Linux a besoin de mémoire 
étendue et ne peut pas utiliser la mémoire paginée.

</para>
   </sect3>

   <sect3><title>Protection contre les virus</title>
<para>

Désactivez tout système de détection de virus fourni par le BIOS.
Si vous avez une carte ou tout autre matériel de protection contre les
virus, vérifiez qu'ils sont désactivés ou retirés physiquement
pendant que Linux fonctionne. Ils ne sont pas compatibles avec GNU/Linux&nbsp;;
de plus, à cause des permissions sur le système de fichiers et de la mémoire
protégée du noyau Linux, on n'entend quasiment jamais parler de virus
<footnote>
<para>

Après l'installation vous pouvez activer la protection du secteur de
démarrage si vous le désirez. Cela n'offre pas de sécurité
supplémentaire pour Linux, mais si vous utilisez aussi Windows, cela peut
éviter une catastrophe. Il n'y a pas besoin de modifier le secteur
d'amorçage principal (MBR) après l'installation du gestionnaire d'amorçage.
</para>
</footnote>.

</para>
   </sect3>
   <sect3><title>Cache mémoire (Shadow RAM)</title>
<para>

Votre carte mère permet sûrement d'utiliser de la <emphasis>Shadow
RAM</emphasis> ou <quote>BIOS caching</quote>. Vous pouvez voir des
options pour <quote>Vidéo BIOS Shadow</quote>, <quote>C800-CBFF
Shadow</quote>, etc. <emphasis>Désactivez</emphasis> toute utilisation de
<quote>Shadow</quote> RAM. La <quote>Shadow</quote> RAM est utilisée pour
accélérer l'accès à la mémoire morte (ROM) sur votre carte mère et sur
certaines cartes contrôleurs.
Linux n'utilise pas ces mémoires mortes une fois amorcé car il fournit
ses propres logiciels 32 bits plus rapides, à la place des logiciels 16
bits des mémoires mortes. La désactivation de ces <quote>Shadow</quote>
RAM peut vous permettre de libérer de la mémoire pour les logiciels.
L'activation de ces <quote>Shadow</quote> RAM pourrait interférer avec
les accès de Linux aux périphériques matériels.

</para>
   </sect3>

   <sect3><title>Trou dans la mémoire</title>
<para>

Si votre BIOS propose quelque chose comme <quote>15-16 MB Memory
Hole</quote>, désactivez cette option. Linux s'attend à trouver de la
mémoire à cet endroit, si vous avez autant de RAM.

</para><para>

Sur les cartes mère Intel Endeavor, il existe une option appelée
<quote>LFB</quote> ou <quote>Linear Frame Buffer</quote>. Deux choix sont
possibles&nbsp;: <quote>Disabled</quote> et <quote>1 Megabyte</quote>.
Choisissez <quote>1 Megabyte</quote>. Lorsque cette option est
désactivée, la disquette d'installation n'est pas lue correctement et le
système peut s'arrêter. À ce jour, nous ne
comprenons pas l'origine du problème avec ce périphérique, nous savons juste
que cela fonctionne avec ce choix et pas avec l'autre.

</para>
   </sect3>


<!-- no other platforms other than x86 provide this sort of thing, AFAIK -->

   <sect3><title>Gestion de l'énergie (APM)</title>
<para>

Si votre carte mère possède une fonction d'économie d'énergie par le
système APM, configurez-la pour que l'énergie soit contrôlée par ce
système APM. Désactivez les modes <quote>doze</quote>,
<quote>standby</quote>, <quote>suspend</quote>, <quote>nap</quote> et
<quote>sleep</quote> ainsi que la minuterie de mise en veille du disque
dur. Linux peut se charger de contrôler ces modes et fera un meilleur
travail d'économie d'énergie que le BIOS. 

</para>
   </sect3>
  </sect2>
