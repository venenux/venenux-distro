<!-- retain these comments for translator revision tracking -->
<!-- original version: 56423 -->


   <sect3 id="localechooser">
   <title>Lokalisierungs-Optionen auswählen (localechooser)</title>

<para>

In den meisten Fällen betreffen die ersten Fragen, die Ihnen gestellt
werden, die Auswahl von Lokalisierungs-Optionen, die sowohl für die
Installation als auch für das installierte System genutzt werden.
Diese Lokalisierungs-Optionen bestehen aus der Sprache, dem Land, in dem
Sie leben und dem Locale-Code.

</para><para>

Die Sprache, die Sie wählen, wird für den Rest des Installationsprozesses
genutzt, vorausgesetzt, eine Übersetzung der verschiedenen Dialoge ist
vorhanden. Falls keine passende Übersetzung für die gewählte Sprache
verfügbar ist, nutzt der Installer die Standardeinstellung Englisch.

</para><para>

Das ausgewählte Land wird später im Installationsprozess verwendet, um
die passende Zeitzone und einen Debian-Spiegelserver passend zu Ihrer
geographischen Position auszuwählen. Sprache und Land zusammen werden
genutzt, um die Standard-Locale für Ihr System festzulegen und bei
der Auswahl der Tastatur zu helfen.

</para><para>

Als erstes werden Sie aufgefordert, die gewünschte Sprache auszuwählen.
Die Sprachen sind sowohl in Englisch (links) wie auch in der Sprache
selbst (rechts) angegeben; die Namen auf der rechten Seite werden direkt
in einer passende Schriftart für die Sprache dargestellt. Die Liste ist
sortiert nach den englischen Namen. Am Anfang der Liste gibt es eine
extra Option, mit der Sie die <quote>C</quote>-Locale wählen können
statt einer Sprache. Die Auswahl der <quote>C</quote>-Locale führt zu
einem Installationsprozess in englischer Sprache; das installierte
System hat keine Unterstützung für unterschiedliche Lokalisierungen,
da das Paket <classname>locales</classname> nicht installiert wird.

</para><para>

Falls sie eine Sprache wählen, die offizielle Sprache in mehr als
einem Land ist<footnote>

<para>

Technisch gesehen: wenn verschiedene Locales für diese Sprache
mit unterschiedlichen Landeskennungen existieren.

</para>

</footnote>, wird Ihnen eine Liste mit diesen Ländern angezeigt. Falls Sie
ein Land wählen möchten, das in dieser Liste nicht enthalten ist, wählen Sie
<guimenuitem>Anderes</guimenuitem> (der letzte Eintrag). Es wird dann eine
Liste der Kontinente angezeigt; bei Auswahl eines Kontinents erscheint eine
Liste mit zu diesem Kontinent zugehörigen Ländern.

</para><para>

Falls es für die von Ihnen gewählte Sprache nur ein zugehöriges Land gibt,
wird dieses Land automatisch ausgewählt. In diesem Fall ist es nur möglich,
ein anderes Land auszuwählen, indem vorher die Debconf-Priorität auf Medium
herabgesetzt wird; danach muss erneut der Dialog zur Auswahl der Sprache
aufgerufen werden (mit dem entsprechenden Eintrag im Hauptmenü des Installers).

</para><para>

Basierend auf der Auswahl von Sprache und Land wird eine passende
Standard-Locale installiert. Wenn Sie mit Priorität medium oder low installieren,
haben Sie die Möglichkeit, eine andere Standard-Locale zu wählen sowie
zusätzliche Locales für das installierte System generieren zu lassen.

</para>
   </sect3>
