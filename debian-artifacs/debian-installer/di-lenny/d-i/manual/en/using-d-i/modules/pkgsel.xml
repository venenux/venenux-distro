<!-- retain these comments for translator revision tracking -->
<!-- $Id: pkgsel.xml 56511 2008-10-24 21:00:37Z fjp $ -->

   <sect3 id="pkgsel">
   <title>Selecting and Installing Software</title>

<para>

During the installation process, you are given the opportunity to select
additional software to install. Rather than picking individual software
packages from the &num-of-distrib-pkgs; available packages, this stage of
the installation process focuses on selecting and installing predefined
collections of software to quickly set up your computer to perform various
tasks.

</para><para>

<!-- TODO: Should explain popcon first -->

So, you have the ability to choose <emphasis>tasks</emphasis> first,
and then add on more individual packages later.  These tasks loosely
represent a number of different jobs or things you want to do with
your computer, such as <quote>Desktop environment</quote>,
<quote>Web server</quote>, or <quote>Print server</quote><footnote>

<para>

You should know that to present this list, the installer is merely
invoking the <command>tasksel</command> program. It can be run at any
time after installation to install more packages (or remove them), or
you can use a more fine-grained tool such as <command>aptitude</command>.
If you are looking for a specific single package, after
installation is complete, simply run <userinput>aptitude install
<replaceable>package</replaceable></userinput>, where
<replaceable>package</replaceable> is the name of the package you are
looking for.

</para>

</footnote>. <xref linkend="tasksel-size-list"/> lists the space
requirements for the available tasks.

</para><para>

Some tasks may be pre-selected based on the characteristics of the
computer you are installing. If you disagree with these selections you can
deselect them. You can even opt to install no tasks at all at this point.

</para>
<note><para>

<!-- TODO: Explain the "Standard system" task first -->

Unless you are using the special KDE or Xfce CDs, the <quote>Desktop
environment</quote> task will install the GNOME desktop environment.

</para><para>

It is not possible to interactively select a different desktop during
the installation. However, it <emphasis>is</emphasis> possible to get &d-i;
to install a KDE desktop environment instead of GNOME by using preseeding
(see <xref linkend="preseed-pkgsel"/>) or by adding the parameter
<literal>desktop=kde</literal> at the boot prompt when starting the
installer. Alternatively the more lightweight Xfce desktop environment can
be selected by using <literal>desktop=xfce</literal>.

</para><para>

Note that this will only work if the packages needed for KDE or Xfce
are actually available. If you are installing using a single full CD image,
they will need to be downloaded from a mirror as most needed packages are
only included on later CDs; installing KDE or Xfce this way should work
fine if you are using a DVD image or any other installation method.

</para><para>

The various server tasks will install software roughly as follows.
DNS server: <classname>bind9</classname>;
File server: <classname>samba</classname>, <classname>nfs</classname>;
Mail server: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Print server: <classname>cups</classname>;
SQL database: <classname>postgresql</classname>;
Web server: <classname>apache2</classname>.

</para></note>
<para>

Once you've selected your tasks, select &BTN-CONT;. At this point,
<command>aptitude</command> will install the packages that are part
of the selected tasks. If a particular program needs more information
from the user, it will prompt you during this process.

</para>
<note><para>

In the standard user interface of the installer, you can use the space bar
to toggle selection of a task.

</para></note>
<para>

You should be aware that especially the Desktop task is very large.
Especially when installing from a normal CD-ROM in combination with a
mirror for packages not on the CD-ROM, the installer may want to retrieve
a lot of packages over the network. If you have a relatively slow
Internet connection, this can take a long time. There is no option to
cancel the installation of packages once it has started.

</para><para>

Even when packages are included on the CD-ROM, the installer may still
retrieve them from the mirror if the version available on the mirror is
more recent than the one included on the CD-ROM. If you are installing
the stable distribution, this can happen after a point release (an update
of the original stable release); if you are installing the testing
distribution this will happen if you are using an older image.

</para>
   </sect3>
