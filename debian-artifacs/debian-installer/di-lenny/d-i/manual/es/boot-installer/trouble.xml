<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56669 -->
<!-- revisado jfs, 21 noviembre 2004 -->
<!-- revisado Rudy Godoy, 22 feb. 2005 -->

 <sect1 id="boot-troubleshooting">
 <title>Resoluci�n de problemas en el proceso de instalaci�n</title>
<para>
</para>

  <sect2 arch="not-s390" id="unreliable-cd">
  <title>Fiabilidad de la unidad de CD-ROM</title>
<para>

Algunas veces, especialmente en el caso de las unidades antiguas de
CD-ROM, el instalador puede fallar y no poder arrancar del CD-ROM. El
instalador puede que (a�n despu�s de haber arrancado con �xito del
CD-ROM) falle al reconocer el CD-ROM o devuelva errores mientras lee
de �ste durante la instalaci�n.

</para><para>

Hay muchas causas posibles para estos problemas. Aqu� s�lo se pueden
listar algunos problemas comunes y dar sugerencias generales de c�mo
tratarlos. Lo dem�s depende de usted.

</para><para>

Hay dos cosas muy sencillas que deber�a intentar primero.

<itemizedlist>
<listitem><para>

Si el CD-ROM no arranca, compruebe que se introdujo correctamente y
que no est� sucio.

</para></listitem>
<listitem><para>

Si el instalador no consigue reconocer el CD-ROM, intente ejecutarlo
de nuevo s�lo con la opci�n <menuchoice> <guimenuitem>Detectar y
montar el CD-ROM</guimenuitem> </menuchoice>. Se sabe que algunos
problemas relacionados con DMA en unidades de CD-ROM antiguas se
resuelven de esta forma.

</para></listitem>
</itemizedlist>

</para><para>

Pruebe las sugerencias que se muestran en las subsecciones a
continuaci�n si no funciona ninguna de estas opciones. Algunas, pero
no todas, las opciones que aqu� se discuten son v�lidas tanto para las
unidades de CD-ROM como las unidades de DVD. Se utilizar� s�lo el
t�rmino CD-ROM con el objetivo de simplificar el texto.

</para><para>

En cualquier caso, intente cualquiera de los otros m�todos de instalaci�n
disponible si no consigue que la instalaci�n funcione mediante CD-ROM.

</para>

  <sect3>
  <title>Problemas habituales</title>

<itemizedlist>
  <listitem><para>

Algunas unidades antiguas de CD-ROM no pueden leer discos que se
grabaron a altas velocidades con grabadores de CD modernos.

  </para></listitem>
  <listitem><para>

Si su sistema arranca correctamente desde CD-ROM, no significa
necesariamente que Linux tambi�n tenga soporte para CD-ROM (o, m�s
concretamente, la controladora a la que est� conectada su unidad de
CD-ROM).

  </para></listitem>
  <listitem><para>

Algunas unidades antiguas de CD-ROM no funcionan correctamente si se
activa la funci�n <quote>direct memory access</quote> (DMA).

  </para></listitem>
</itemizedlist>

  </sect3>

  <sect3>
  <title>C�mo investigar y, quiz�s, solucionar problemas</title>
<para>

Si el CD-ROM no puede arrancar, intente las sugerencias descritas a
continuaci�n.

<itemizedlist>
  <listitem><para>

Check that your BIOS actually supports booting from CD-ROM (older systems
possibly don't) and that your CD-ROM drive supports the media you are using.

  </para></listitem>
  <listitem><para>

Compruebe que la suma md5 de la imagen coincide con el valor listado 
en el fichero <filename>MD5SUMS</filename> si ha descargado la imagen
ISO. Este fichero deber�a estar disponibles en la misma ubicaci�n de
la que descargo la imagen.

<informalexample><screen>
$ md5sum <replaceable>debian-testing-i386-netinst.iso</replaceable>
a20391b12f7ff22ef705cee4059c6b92  <replaceable>debian-testing-i386-netinst.iso</replaceable>
</screen></informalexample>

A continuaci�n, compruebe que la suma md5 del CD-ROM grabado tambi�n
coincide. La orden mostrada a continuaci�n deber�a ser suficiente para
poder hacer esto. Utiliza el tama�o de la imagen ppara leer el n�mero
de bytes correcto del CD-ROM.

<informalexample><screen>
$ dd if=/dev/cdrom | \
> head -c `stat --format=%s <replaceable>debian-testing-i386-netinst.iso</replaceable>` | \
> md5sum
a20391b12f7ff22ef705cee4059c6b92  -
262668+0 records in
262668+0 records out
134486016 bytes (134 MB) copied, 97.474 seconds, 1.4 MB/s
</screen></informalexample>

  </para></listitem>
</itemizedlist>

</para><para>

Puede volver a intentar la instalaci�n si no se detecta el CD-ROM
despu�s de que el instalador haya arrancado con �xito, ya que esto
algunas veces resuelve el problema. Intente cambiar el CD-ROM a la
otra unidad si tiene m�s de una unidad de CD-ROM. Intente algunas de
las sugerencias mostradas a continuaci�n si no funciona ninguna de
�stas o si se dan errores cuando se intenta leer de �ste. Para
llevarlas a cabo es necesario tener algunos conocimientos b�sicos de
Linux. Debe cambiar primero a la segunda consola virtual (VT2) y
activar el int�rprete de �rdenes para ejecutar cualquiera de estas �rdenes.

<itemizedlist>
  <listitem><para>

Cambie al terminal virtual VT4 o consulte los contenidos de
<filename>/var/log/syslog</filename> (utilice <command>nano</command>
como editor) para comprobar si hay alg�n mensaje de error
espec�fico. Una vez lo haya hecho, compruebe tambi�n la salida de
ejecutar <command>dmesg</command>.

  </para></listitem>
  <listitem><para>

Puede ver si se ha reconocido su unidad de CD-ROM comprobando la
salida de <command>dmesg</command>. Deber�a ver algo como esto (las
l�neas no tienen por qu� ser consecutivas):

<informalexample><screen>
Probing IDE interface ide1...
hdc: TOSHIBA DVD-ROM SD-R6112, ATAPI CD/DVD-ROM drive
ide1 at 0x170-0x177,0x376 on irq 15
hdc: ATAPI 24X DVD-ROM DVD-R CD-R/RW drive, 2048kB Cache, UDMA(33)
Uniform CD-ROM driver Revision: 3.20
</screen></informalexample>

Es posible que su unidad de CD-ROM est� conectada pero no se haya
reconocido o puede no estar soportada, si no ve ninguna de estas
l�neas. Si sabe qu� controlador es necesario para su controladora
puede intentar cargarla de forma manual ejecutando la orden
<command>modprobe</command>.

  </para></listitem>
  <listitem><para>

Compruebe que hay un nodo de dispositivo para su unidad de CD-ROM en
<filename>/dev/</filename>. En el ejemplo anterior, el nodo se
llamar�a <filename>/dev/hdc</filename>.  Tambi�n deber�a existir
<filename>/dev/cdrom</filename>.

  </para></listitem>
  <listitem><para>

Utilice la orden <command>mount</command> para comprobar si el CD-ROM
est� ya montado. Si no lo est� puede intentar montarlo manualmente
con:


<informalexample><screen>
$ mount /dev/<replaceable>hdc</replaceable> /cdrom
</screen></informalexample>

Compruebe si se produce alg�n mensaje de error despu�s de ejecutar
esta orden.

  </para></listitem>
  <listitem><para>

Compruebe si est� activo la funci�n de DMA:

<informalexample><screen>
$ cd /proc/<replaceable>ide</replaceable>/<replaceable>hdc</replaceable>
$ grep using_dma settings
using_dma      1       0       1       rw
</screen></informalexample>

Un valor de <quote>1</quote> en la primera columna despu�s de
<literal>using_dma</literal> significa que est� activa. Si lo est�,
intente desactivarla:

<informalexample><screen>
$ echo -n "using_dma:0" >settings
</screen></informalexample>

Aseg�rese que est� en el directorio del dispositivo que corresponde a
su unidad de CD-ROM.

  </para></listitem>
  <listitem><para>

Intente comprobar la integridad del CD-ROM con la opci�n que
encontrar� al final del men� principal del instalador si se producen
problemas durante la instalaci�n. Esta opci�n puede utilizarse como
una prueba general para determinar si el CD-ROM se puede leer
con fiabilidad.


  </para></listitem>
</itemizedlist>

</para>
  </sect3>
  </sect2>

  <sect2 condition="supports-floppy-boot" id="unreliable-floppies">
  <title>Fiabilidad de los disquetes</title>

<para>

El problema m�s habitual para las personas que utilizan disquetes para instalar
Debian por primera vez suele ser la fiabilidad de los disquetes.

</para><para>

El disquete de arranque es el que puede dar mayores problemas, debido
a que es le�do directamente por el hardware, antes de que arranque
Linux. A menudo, el hardware no lee tan confiablemente como lo hace el
controlador de disquetes de Linux, y podr�a detenerse sin mostrar ning�n
mensaje de error si lee datos incorrectos. Tambi�n pueden
producirse problemas en los disquetes de controladores, f�cilmente
observables porque se producen
una gran cantidad de mensajes sobre errores E/S del disquete.

</para><para>

Si su instalaci�n se bloquea en un disquete en particular,
lo primero que debe hacer es volver a descargar la imagen de disquete
y escribirla en un disquete <emphasis>distinto</emphasis>. No es suficiente
con simplemente formatear de nuevo el antiguo, incluso aunque el
proceso de formateo parezca haberse producido correctamente y no ha
mostrado ning�n fallo. Muchas veces puede ser
�til intentar escribir en el disquete en un sistema diferente.

</para><para>

Un usuario inform� que tuvo que escribir las im�genes a un disquete
<emphasis>tres</emphasis> veces antes que uno funcionara, todo
funcion� correctamente con el tercer disquete.

</para><para>

Otros usuarios han informado que simplemente el reiniciar algunas
veces con el mismo disquete en la unidad de disquete puede llevar
a un arranque con �xito. Todos estos problemas se deben a
hardware o controladores firmware defectuosos.


</para>
  </sect2>

  <sect2><title>Configuraci�n del arranque</title>

<para>

En caso de que tenga problemas, el n�cleo se bloquee durante el proceso de
arranque, no reconozca los dispositivos que tiene o no se reconozcan
correctamente las unidades, lo primero que debe
verificar son los par�metros de arranque, como se explica en
<xref linkend="boot-parms"/>.

</para><para>

A menudo, se pueden solventar los problemas desconectando
algunos perif�ricos y elementos a�adidos e intentando de nuevo
el arranque.
<phrase arch="x86">Algunos m�dems internos, tarjetas de sonido, y
dispositivos �Plug-n-Play� pueden ser especialmente problem�ticos.
</phrase>

</para><para>

Puede que tenga que incluir un argumento de arranque para limitar la
cantidad de memoria que reconocer� el n�cleo si tiene una gran
cantidad de memoria en su m�quina, m�s de 512 MB, y el instalador se
bloquea cuando arranca el n�cleo, utilice, por ejemplo,
<userinput>mem=512m</userinput>.

</para>
  </sect2>

  <sect2 arch="x86" id="i386-boot-problems">
  <title>Problemas comunes durante la instalaci�n en &arch-title;</title>
<para>

Hay algunos problemas comunes que se producen en la instalaci�n y que pueden
resolverse o evitarse pasando ciertos par�metros de arranque al instalador.

</para><para>

Algunos sistemas tienen disquetes con <quote>DCLs invertidos</quote>. Si
obtiene errores en la lectura del disquete y est� seguro de que el disquete
es bueno y est� bien grabado intente utilizar el par�metro
<userinput>floppy=thinkpad</userinput>.

</para><para>

Puede no reconocerse la unidad IDE en algunos sistemas, como es el caso del IBM
PS/1 o ValuePoint (que tienen una unidad de disco ST-506). Intente primero
arrancar la instalaci�n sin introducir ning�n par�metro para ver si la unidad
IDE se detecta correctamente. Si no se detecta, debe obtener la informaci�n de
geometr�a de la unidad (cilindros, cabezas y sectores) y utilizar el par�metro
<userinput>hd=<replaceable>cilindros</replaceable>,<replaceable>cabezas</replaceable>,<replaceable>sectores</replaceable></userinput>.

</para><para>

Si el n�cleo se queda parado despu�s de decir <computeroutput>Checking 'hlt'
instruction...</computeroutput> y tiene un sistema muy antiguo deber�a probar a
deshabilitar esta prueba con el argumento de arranque
<userinput>no-hlt</userinput>.

</para><para>

Es posible que algunos sistemas (sobre todo port�tiles) que tengan una resoluci�n
nativa que no tenga una relaci�n de aspecto 4:3 (no sea, por ejemplo, 800x600 �
1024x768) presenten una pantalla en blanco una vez el instalador haya
arrancado. Puede arrancar el instalador con el par�metro
<userinput>vga=788</userinput>.<footnote>

<para>Este par�metro <userinput>vga=788</userinput> activar� el framebuffer con
una resoluci�n de 800x600. Aunque �sta no sea la resoluci�n �ptima para su
sistema probablemente funcione. Puede obtener una lista de las resoluciones
soportadas utilizando el par�metro <userinput>vga=ask</userinput>, pero ha de
tener en cuenta que la lista puede no ser completa.
</para>

</footnote> para resolver este problema. Si eso no funciona puede intentar
utilizar el par�metro de arranque <userinput>fb=false</userinput>.

</para><para>

Si su pantalla empieza a mostrar una imagen rara cuando arranca el n�cleo, como
pudiera ser mostrarse todo en blanco, o todo en negro, o con algunos pixels
coloreados al azar mal, puede que tenga una tarjeta de v�deo problem�tica que
no es capaz de cambiar al modo <quote>framebuffer</quote> correctamente. Puede
utilizar el par�metro del arranque
<userinput>fb=false</userinput>
para deshabilitar el framebuffer en
consola. Si hace esto, s�lo podr� ver la instalaci�n en un conjunto reducido
de idiomas 
debido a las funcionalidades limitadas de la consola. Si quiere los detalles,
consulte <xref linkend="boot-parms"/>.

</para>

  <sect3>
  <title>Parada del sistema durante la fase de configuraci�n de PCMCIA</title>
<para>

Se sabe de algunos modelos de ordenador port�til de Dell que se quedan parados
cuando la detecci�n de dispositivos PCMCIA intenta acceder a algunas
direcciones hardware. Es posible que otros ordenadores port�tiles sufran
problemas parecidos. Si tiene este problema y no necesita soporte de PCMCIA
durante la instalaci�n puede deshabilitar PCMCIA con el par�metro de arranque 
<userinput>hw-detect/start_pcmcia=false</userinput>. Podr� configurar PCMCIA
una vez termine la instalaci�n y excluir el rango de recursos que causa el problema.

</para><para>

Tambi�n puede intentar arrancar el instalador en modo experto. Si lo hace,
se le preguntar� las opciones del rango de recursos para sus necesidades
hardware. Por ejemplo, si tiene alguno de los ordenadores port�tiles Dell
mencionados anteriormente podr�a introducir aqu� 
<userinput>exclude port 0x800-0x8ff</userinput>. Encontrar� un listado 
de opciones de configuraci�n de recursos habituales en
<ulink
url="http://pcmcia-cs.sourceforge.net/ftp/doc/PCMCIA-HOWTO-1.html#ss1.12">System
resource settings section of the PCMCIA HOWTO</ulink>. Tenga en cuenta que ha
de omitir cualquier coma, si la hay, cuando introduzca este valor en el instalador.

</para>
   </sect3>

   <sect3>
   <title>Parada del sistema durante la carga de m�dulos USB</title>
<para>

El n�cleo intenta generalmente cargar los m�dulos USB y el controlador de teclado
USB para poder operar con algunos teclados USB no est�ndar. Hay, sin embargo, algunos
sistemas USB mal hechos donde el controlador cuelga el sistema al arrancar. Una
opci�n para evitar esta parada es deshabilitar el controlador USB en la
configuraci�n de la BIOS de su placa base. Otra opci�n es pasar el par�metro
<userinput>nousb</userinput> en el arranque.

</para>
   </sect3>
  </sect2>

  <sect2 arch="sparc" id="sparc-boot-problems">
      <title>Problemas de instalaci�n comunes en &arch-title;</title>
<para>

    Existen algunos problemas comunes de instalaci�n que merece la pena
    mencionar.
</para>

 <sect3>
   <title>Salida de video mal dirigida</title>
   <para>

    Es relativamente com�n tener dos tarjetas de video en un sistema
    &arch-title;, por ejemplo, una tarjeta ATI y una tarjeta Sun Creator 3D.
    En algunos casos esto origina que la salida de video se dirija 
    incorrectamente una vez el sistema haya arrancado. En estos
    casos habituales la salida s�lo mostrar�:

<informalexample><screen>
Remapping the kernel... done
Booting Linux...
</screen></informalexample>

Para evitar esto puede simplemente desconectar una de las tarjetas de video o
deshabilitar la que no utiliza en la fase de arranque OpenProm utilizando un
par�metro del n�cleo. Por ejemplo, si quiere deshabilitar una tarjeta ATI
deber�a arrancar el instalador con <userinput>video=atyfb:off</userinput>.

</para><para>

Tenga en cuenta que es posible que tenga que a�adir este par�metro manualmente
a la configuraci�n de silo (edite el fichero
<filename>/target/etc/silo.conf</filename> antes de reiniciar) y, si
ha instalado X11, modifique la controladora de video en 
<filename>/etc/X11/xorg.conf</filename>.

</para>
</sect3>

         <sect3>
             <title>Fallo en el arranque o la instalaci�n de CD-ROM</title>
<para>

Algunos sistemas Sparc son dif�ciles de arrancar del CD-ROM y, a�n cuando
arrancan, pueden producirse fallos inexplicables durante la instalaci�n. La
mayor�a de los problemas de este tipo se han reportado en sistemas SunBlade.

</para><para>

Le recomendamos instalar estos sistemas con el arranque de red del instalador.

</para>
   </sect3>
     </sect2>

  <sect2 id="kernel-msgs">
  <title>Interpretar los mensajes de inicio del n�cleo</title>

<para>

Durante la secuencia de arranque podr�a ver muchos mensajes
de la forma
<computeroutput>can't find <replaceable>algo</replaceable></computeroutput>, 
o <computeroutput><replaceable>algo</replaceable> not present</computeroutput>,
<computeroutput>can't initialize <replaceable>algo</replaceable></computeroutput>,
o incluso <computeroutput>this driver release depends
on <replaceable>algo</replaceable> </computeroutput>.
Muchos de estos mensajes son inocuos. Los ve porque
el sistema de instalaci�n est� programado para ejecutarse en ordenadores
con diversos dispositivos. Obviamente, ning�n ordenador tendr� todos y cada uno
de los posibles dispositivos, de modo que el sistema operativo
emite algunos mensajes de advertencia mientras intenta buscar dispositivos
que usted no tiene. Tambi�n podr�a observar que el sistema se detiene durante
algunos momentos. Esto ocurre cuando se est� esperando que un dispositivo
responda y �ste no est� presente en su sistema.
Podr� crear m�s adelante un n�cleo a medida (lea <xref
linkend="kernel-baking"/>) si piensa que el tiempo que tarda en
arrancar el sistema es muy largo.

</para>
  </sect2>


  <sect2 id="problem-report">
  <title>Informar de fallos</title>
<para>

Podr�a serle �til la opci�n de informe de fallos del men�
si ha pasado la fase inicial de arranque pero no puede completar
la instalaci�n.  Esta opci�n le permite copiar los registros de fallos de
sistema y la informaci�n de configuraci�n a un disquete, o descargarlas con un
navegador web.

Esta informaci�n puede darle pistas sobre lo que ha fallado y c�mo
solucionarlo. Podr�a tambi�n querer adjuntar esta informaci�n si va a
enviar un informe de fallos.

</para><para>

Puede encontrar otros mensajes de instalaci�n pertinentes
en <filename>/var/log/</filename> durante la instalaci�n,
y en <filename>/var/log/installer/</filename> despu�s de
que el ordenador ha sido arrancado con el sistema instalado.

</para>
  </sect2>

  <sect2 id="submit-bug">
  <title>Enviar los informes de la instalaci�n</title>
<para>

Por favor, env�e un informe de su instalaci�n si tiene problemas.
Le animamos a hacerlo incluso si la instalaci�n tiene �xito, de
esta forma podremos obtener la mayor informaci�n posible sobre la
mayor cantidad de configuraciones de hardware.

</para><para>

Tenga en cuenta que el informe de instalaci�n se publicar� en el sistema de
seguimiento de fallos de Debian y que se reenviar� a una lista de correo
p�blica. Aseg�rese que utiliza una direcci�n de correo electr�nico que no le
importa que se haga p�blico.

</para><para>

La forma m�s sencilla de enviar un informe de instalaci�n si tiene un sistema
Debian funcionando es instalar los paquetes <quote>installation-report</quote>
y <quote>reportbug</quote> (<command>apt-get install installation-report
reportbug</command>), configurar <classname>reportbug</classname> 
como se describe en <xref linkend="mail-outgoing"/>,
y ejecutar la orden <command>reportbug installation-reports</command>.

</para><para>

Haga uso de la plantilla mostrada a continuaci�n cuando haga un informe de
instalaci�n, y env�elo (en ingl�s) como un informe de fallo para el pseudo paquete
<classname>installation-reports</classname> a la direcci�n
<email>submit@bugs.debian.org</email>.

<informalexample><screen>
Package: installation-reports

Boot method: &lt;C�mo arranc� la instalaci�n? �Con un CD? �Con un disquete? �Desde la red?&gt;
Image version: &lt;Escriba la fecha y desde donde obtuvo la imagen&gt;
Date: &lt;Fecha y hora de la instalaci�n&gt;

Machine: &lt;Descripci�n de la m�quina (p. ej., IBM Thinkpad R32)&gt;
Processor: &lt;Tipo de procesador&gt;
Memory: &lt;Cantidad de memoria RAM&gt;
Partitions: &lt;Basta con la salida de �df -Tl�, es preferible la tabla de particiones sin editar&gt;

Output of lspci -knn (o lspci -nn): &lt;Salida de la orden �lspci -knn� (o �lspci -nn�)&gt;

Base System Installation Checklist: &lt;Marque seg�n sea su caso: Escriba �O�
     si dicha fase funcion�, �E� si present� alg�n fallo y d�jela en blanco si
     no intent� o no us� esta opci�n.&gt;
[O] = OK, [E] = Error (descr�balo a continuaci�n), [ ] = didn't try it

Initial boot:           [ ] &lt;�Funcion� el arranque inicial?&gt;
Detect network card:    [ ] &lt;�Se configur� el hardware de red?&gt;
Configure network:      [ ] &lt;�Se configur� la red?&gt;
Detect CD:              [ ] &lt;�Se detect� la unidad de CD?&gt;
Load installer modules: [ ] &lt;�Se cargaron los m�dulos del instalador?&gt;
Detect hard drives:     [ ] &lt;�Se detectaron los discos duros?&gt;
Partition hard drives:  [ ] &lt;�Se particion� el disco duro?&gt;
Install base system:    [ ] &lt;�Se instal� el sistema base?&gt;
Clock/timezone setup:   [ ] &lt;�Se configur� bien la zona horaria?&gt;
User/password setup:    [ ] &lt;�Se configur� correctamente el usuario?&gt;
Install tasks:          [ ] &lt;�Se instalaron bien las tareas?&gt;
Install boot loader:    [ ] &lt;�Se instal� el gestor de arranque?&gt;
Overall install:        [ ] &lt;�Reinici� correctamente?&gt;

Comments/Problems:

&lt;Describa la instalaci�n en detalle, e incluya cualquier idea o
comentario que tuvo durante la instalaci�n.&gt;
</screen></informalexample>

Nota del traductor: Los comentarios (todo lo que va entre &lt; &gt;) en espa�ol
deben ser eliminados en el momento de llenar el informe. Env�e el
informe en ingl�s, no debe traducir o escribir frases en espa�ol en las
opciones especificadas.

</para><para>

En el informe de fallo describa cu�l es el problema, incluya los
�ltimos mensajes visibles del n�cleo en el caso de que �ste
se bloquee. Describa los pasos que llev� a cabo y que condujeron al
sistema a la situaci�n del fallo.

</para>
  </sect2>
 </sect1>
