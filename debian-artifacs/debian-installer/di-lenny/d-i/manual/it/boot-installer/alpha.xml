<!-- retain these comments for translator revision tracking -->
<!-- original version: 43789 -->


  <sect2 arch="alpha" id="alpha-firmware">
  <!-- <title>Alpha Console Firmware</title> -->
  <title>Console firmware su alpha</title>
<para>

<!--
Console firmware is stored in a flash ROM and started when an Alpha
system is powered up or reset.  There are two different console
specifications used on Alpha systems, and hence two classes of console
firmware available:
-->

Il <quote>Console firmware</quote> è memorizzato all'interno di una
memoria flash ed è eseguito quando un sistema Alpha viene acceso o
riavviato. Esistono due diverse specifiche per la console dei sistemi
Alpha e quindi ci sono due diverse classi di firmware disponibili:

</para>

<itemizedlist>
<listitem><para>

<!--
  <emphasis>SRM console</emphasis>, based on the Alpha Console Subsystem
  specification, which provides an operating environment for OpenVMS, Tru64
  UNIX, and Linux operating systems.
-->

  <emphasis>Console SRM</emphasis>, basata sulla specifica
  Alpha Console Subsystem, fornisce un ambiente operativo per i sistemi
  operativi OpenVMS, Tru64 UNIX e Linux.

</para></listitem>
<listitem><para>

<!--
  <emphasis>ARC, AlphaBIOS, or ARCSBIOS console</emphasis>, based on the
  Advanced RISC Computing (ARC) specification, which provides an operating
  environment for Windows NT.
-->

  <emphasis>Console ARC, AlphaBIOS o ARCSBIOS</emphasis>, basate sulla
  specifica Advanced RISC Computing (ARC), fornisce un ambiente
  operativo per Windows NT.

</para></listitem>
</itemizedlist>

<para>

<!--
From the user's perspective, the most important difference between SRM
and ARC is that the choice of console constrains the possible
disk-partitioning scheme for the hard disk which you wish to boot off
of.
-->

Dal punto di vista dell'utente, la più importante differenza tra
SRM e ARC è che la scelta della console limita i possibili schemi di
partizionamento per il disco dal quale si avvia la macchina.

</para><para>

<!--
ARC requires that you use an MS-DOS partition table (as created by
<command>cfdisk</command>) for the boot disk.  Therefore MS-DOS partition
tables are the <quote>native</quote> partition format when booting from
ARC.  In fact, since AlphaBIOS contains a disk partitioning utility, you may
prefer to partition your disks from the firmware menus before
installing Linux.
-->

ARC richiede che si usi una tabella della partizioni MS-DOS (così
come la crea <command>cfdisk</command>) per il disco di avvio. Per questo
motivo le tabelle delle partizioni MS-DOS sono il formato <quote>nativo</quote>
quando si avvia utilizzando ARC. Difatti, poiché AlphaBIOS
contiene uno strumento per il partizionamento dei dischi, è
possibile partizionare i dischi direttamente dal firmware prima di
installare Linux.

</para><para>

<!--
Conversely, SRM is <emphasis>incompatible</emphasis><footnote>
-->

D'altro canto, SRM è <emphasis>incompatibile</emphasis><footnote>

<para>

<!--
Specifically, the bootsector format required by the Console Subsystem
Specification conflicts with the placement of the DOS partition table.
-->

In particolare, il formato del settore di avvio richiesto dalla Console
Subsystem Specification non permette la memorizzazione di una tabella
delle partizioni di tipo DOS.

</para>

<!--
</footnote> with MS-DOS partition tables. Since Tru64 Unix uses the BSD
disklabel format, this is the <quote>native</quote> partition format for
SRM installations.
-->


</footnote> con la tabella delle partizioni in formato MS-DOS. Poiché
Tru64 Unix usa il formato BSD disklabel, questo è quello
<quote>nativo</quote> per le installazioni SRM.

</para><para>

<!--
GNU/Linux is the only operating system on Alpha that can be booted from
both console types, but &debian; &release; only supports booting on
SRM-based systems.  If you have an Alpha for which no version of SRM is
available, if you will be dual-booting the system with Windows NT, or if
your boot device requires ARC console support for BIOS initialization,
you will not be able to use the &debian; &release; installer.  You can
still run &debian; &release; on such systems by using other install
media; for instance, you can install Debian woody with MILO and upgrade.
-->

GNU/Linux è il solo sistema operativo per Alpha che può essere avviato da
entrambi i tipi di console, purtroppo &debian; &release; può essere avviato
solo da sistemi SRM. Se si possiede un Alpha per cui non esiste una versione
di SRM, se il sistema dovrà essere in dual-boot con Windows-NT oppure se
il dispositivo di avvio richiede una console ARC per l'inizializzazione
del BIOS allora non è possibile usare il sistema di installazione di &debian;
&release;. Si può comunque usare &debian; &release; su questi sistemi usando
un supporto diverso per l'installazione; per esempio si può installare Debian
Woody con MILO e poi fare un aggiornamento.

</para><para>

<!--
Because <command>MILO</command> is not available for any of the Alpha
systems currently in production (as of February 2000), and because it
is no longer necessary to buy an OpenVMS or Tru64 Unix license to have
SRM firmware on your older Alpha, it is recommended that you use SRM
when possible.
-->

Poiché <command>MILO</command> non è disponibile per tutti i sistemi Alpha
attualmente in produzioni (al febbraio 2000) e poiché non è più necessario
comprare una licenza OpenVMS o Tru64 per avere il firmware SRM su gli Alpha
più vecchi, si raccomanda l'uso di SRM quando possibile.

</para><para>

<!--
The following table summarizes available and supported system
type/console combinations (see <xref linkend="alpha-cpus"/> for the
system type names).  The word <quote>ARC</quote> below denotes any of the
ARC-compliant consoles.
-->

La tabella seguente riassume le combinazioni possibili di tipi di
sistema/console (vedere <xref linkend="alpha-cpus"/> per maggiori
informazioni sui nomi dei tipi di sistema). La parola <quote>ARC</quote>
denota le console aderenti alla specifica ARC.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry><!-- System Type -->Tipo di sistema</entry>
  <entry><!-- Console Type Supported -->Tipo di console supportata</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry><!-- ARC (see motherboard manual) or SRM -->
  ARC (vedere il manuale della scheda madre) o SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>Solo ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>Solo SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>Solo ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>Solo ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

<!--
Generally, none of these consoles can boot Linux directly, so the
assistance of an intermediary bootloader is required.  For the SRM
console, <command>aboot</command>, a small, platform-independent
bootloader, is used.  See the (unfortunately outdated) <ulink
url="&url-srm-howto;">SRM HOWTO</ulink> for more information on
<command>aboot</command>.
-->

Normalmente nessuna di queste console è in grado di avviare Linux
direttamente, quindi è necessario l'utilizzo di un bootloader
intermedio. Con la console SRM si usa <command>aboot</command>, un
bootloader piccolo e indipendente dalla piattaforma. Si consulti
(l'ormai vecchio) <ulink url="&url-srm-howto;">SRM HOWTO</ulink> per
maggiori informazioni su <command>aboot</command>.

</para><para condition="FIXME">

<!--
The following paragraphs are from the woody install manual, and are
included here for reference; they may be useful to someone at a later
date when Debian supports MILO-based installs again.
-->

I prossimi paragrafi sono presi dal manuale di installazione di woody e
sono stati riportati qui come riferimento; possono risultare utili a
qualcuno almeno fino a quando non saranno nuovamente supportate le
installazioni con MILO.

</para><para condition="FIXME">

<!--
Generally, none of these consoles can boot Linux directly, so the
assistance of an intermediary bootloader is required.  There are two
mainstream Linux loaders: <command>MILO</command> and <command>aboot</command>.
-->

Normalmente nessuna di queste console è in grado di avviare Linux
direttamente, quindi è necessario l'utilizzo di un bootloader
intermedio. Ci sono due «loader» principali di Linux:
<command>MILO</command> e <command>aboot</command>.

</para><para condition="FIXME">

<!--
<command>MILO</command> is itself a console, which replaces ARC or SRM in
memory.  <command>MILO</command> can be booted from both ARC and SRM and is
the only way to bootstrap Linux from the ARC console.
<command>MILO</command> is platform-specific (a different <command>MILO</command>
is needed for each system type) and exist only for those systems, for
which ARC support is shown in the table above.  See also the
(unfortunately outdated) <ulink url="&url-milo-howto;">MILO HOWTO</ulink>.
-->

<command>MILO</command> è esso stesso una console che sostituisce ARC o
SRM in memoria. <command>MILO</command> può essere avviato sia da ARC sia
da SRM ed è l'unico modo per avviare Linux sulle console ARC.
<command>MILO</command> dipende dalla piattaforma (cioè esiste un
diverso <command>MILO</command> per ogni tipo di sistema) ed esiste solo
per quei sistemi per i quali è indicato il supporto di ARC nella tabella
precedente. Vedere anche il (purtroppo) datato
<ulink url="&url-milo-howto;">MILO HOWTO</ulink>.

</para><para condition="FIXME">

<!--
<command>aboot</command> is a small, platform-independent bootloader, which
runs from SRM only.  See the (also unfortunately outdated) <ulink
url="&url-srm-howto;">SRM HOWTO</ulink> for more information on
<command>aboot</command>.
-->

<command>aboot</command> è un piccolo bootloader, indipendente dalla
piattaforma, che può essere eseguito solo da SRM. Vedere anche
il (purtroppo) datato <ulink url="&url-srm-howto;">SRM HOWTO</ulink> per
maggiore informazioni su <command>aboot</command>.

</para><para condition="FIXME">

<!--
Thus, three scenarios are generally possible, depending on the
system's console firmware and whether or not <command>MILO</command> is
available:
-->

Si presentano quindi tre possibili scenari, a seconda dalla console
firmware presente e della disponibilità di <command>MILO</command>.

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

<!--
Because <command>MILO</command> is not available for any of the Alpha
systems currently in production (as of February 2000), and because it
is no longer necessary to buy an OpenVMS or Tru64 Unix license to have
SRM firmware on your older Alpha, it is recommended that you use SRM and
<command>aboot</command> on new installations of GNU/Linux, unless you wish
to dual-boot with Windows NT.
-->

Poiché <command>MILO</command> non è disponibile per
nessuno dei sistemi Alpha attualmente in produzione (febbraio 2000)
e poiché non è più necessario acquistare una
licenza OpenVMS o Tru64 Unix per ottenere il firmware SRM per un
vecchio sistema Alpha, si consiglia di utilizzare SRM e <command>aboot</command>
per le nuove installazioni di GNU/Linux a meno che non si debba
fare il dual-boot con Windows NT.

</para><para>

<!--
The majority of AlphaServers and all current server and workstation
products contain both SRM and AlphaBIOS in their firmware.  For
<quote>half-flash</quote> machines such as the various evaluation boards,
it is possible to switch from one version to another by reflashing the
firmware.  Also, once SRM is installed, it is possible to run
ARC/AlphaBIOS from a floppy disk (using the <command>arc</command>
command).  For the reasons mentioned above, we recommend switching to
SRM before installing &debian;.
-->

La maggior parte degli AlphaServer e tutti gli attuali server e
workstation contengono i firmware SRM e AlphaBIOS. Per le
macchine <quote>half-flash</quote> come le schede per prototipazione, è
possibile passare da uno all'altro semplicemente riscrivendo
il firmware nella memoria flash. Inoltre, una volta installato SRM,
è possibile eseguire ARC/AlphaBIOS da un
dischetto (usando il comando<command>arc</command>). Per le
ragioni appena elencate si raccomanda il passaggio a SRM
prima di installare &debian;.

</para><para>

<!--
As on other architectures, you should install the newest available
revision of the firmware<footnote>
-->

Come per le altre architetture si dovrebbe installare l'ultima revisione
del firmware<footnote>

<para>

<!--
Except on Jensen, where Linux is not supported on firmware versions
newer than 1.7 &mdash; see <ulink url="&url-jensen-howto;"></ulink>
for more information.
-->

Con l'eccezione di Jensen, per il quale Linux non è supportato
nelle versioni successive alla 1.7 &mdash; vedere
<ulink url="&url-jensen-howto;"></ulink> per altre informazioni.

</para>

<!--
</footnote> before installing &debian;.
For Alpha, firmware updates can be obtained from
<ulink url="&url-alpha-firmware;">Alpha Firmware Updates</ulink>.
-->

</footnote> prima di installare &debian;. Per Alpha è possibile ottenere
gli aggiornamenti del firmware da <ulink url="&url-alpha-firmware;">Alpha
Firmware Updates</ulink>.

</para>
  </sect2>

  <sect2 arch="alpha">
  <!-- <title>Booting with TFTP</title> -->
  <title>Avvio con TFTP</title>
<para>

<!--
In SRM, Ethernet interfaces are named with the <userinput>ewa</userinput>
prefix, and will be listed in the output of the <userinput>show dev</userinput> command,
like this (edited slightly):
-->

In SRM, le interfacce di rete ethernet sono chiamate con il
prefisso <userinput>ewa</userinput> e sono elencate con il comando
<userinput>show dev</userinput>, in maniera simile a quanto
mostrato di seguito:

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

<!--
You first need to set the boot protocol:
-->

Per prima cosa si deve specificare il protocollo per l'avvio:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

<!--
Then check the medium type is correct:
-->

Successivamente si deve verificare che il tipo di supporto sia corretto:

<!--
<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>
-->

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>modalità</replaceable>
</screen></informalexample>

<!--
You can get a listing of valid modes with <userinput>&gt;&gt;&gt;set ewa0_mode</userinput>.
-->

Si può ottenere un elenco delle modalità valide con
<userinput>&gt;&gt;&gt;set ewa0_mode</userinput>.

</para><para>

<!--
Then, to boot from the first Ethernet interface, you would type:
-->

Infine, per avviare dalla prima interfaccia ethernet, si scriverà:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags ""
</screen></informalexample>

<!--
This will boot using the default kernel parameters as included in the
netboot image.
-->

Questo avvierà utilizzando i parametri predefiniti per il kernel
così come specificati nell'immagine netboot.

</para><para>

<!--
If you wish to use a serial console, you <emphasis>must</emphasis>
pass the <userinput>console=</userinput> parameter to the kernel.
This can be done using the <userinput>-flags</userinput> argument to
the SRM <userinput>boot</userinput> command.  The serial ports are
named the same as their corresponding files in
<userinput>/dev</userinput>.  Also, when specifying additional kernel
parameters, you must repeat certain default options that are needed by
the &d-i; images.  For example, to boot from <userinput>ewa0</userinput>
and use a console on the first serial port, you would type:
-->

Se si vuole utilizzare una console seriale, si <emphasis>deve</emphasis>
passare il parametro <userinput>console=</userinput> al kernel.
Questo non può essere fatto con argomenti <userinput>-flags</userinput>
al comando <userinput>boot</userinput> di SRM. I nomi delle porte
seriali sono gli stessi dei corrispondenti device in
<userinput>/dev</userinput>. Inoltre, quando si specificano dei parametri
aggiuntivi del kernel, si devono ripetere alcune delle opzioni predefinite
perché necessarie alle immagini &d-i;. Ad esempio per avviare da
<userinput>ewa0</userinput> e utilizzare una console sulla prima porta
seriale, si scriverà:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags &quot;root=/dev/ram ramdisk_size=16384 console=ttyS0&quot;
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha">
  <!-- <title>Booting from CD-ROM with the SRM Console</title> -->
  <title>Avvio da CD-ROM tramite console SRM</title>
<para>

<!--
The &debian; install CDs include several preconfigured boot options for
VGA and serial consoles.  Type
-->

Nei CD d'installazione di &debian; sono incluse alcune opzioni d'avvio
per le console VGA e seriali. Usare

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

<!--
to boot using VGA console, where <replaceable>xxxx</replaceable> is your
CD-ROM drive in SRM notation.  To use serial console on the first 
serial device, type
-->

per l'avvio con una console VGA, dove <replaceable>xxxx</replaceable> è
il lettore CD-ROM nella notazione di SRM. Per usare una console seriale
collegata al primo device seriale, usare

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

<!--
and for console on the second serial port, type
-->

oppure, se la console è collegata alla seconda porta seriale, usare

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <!-- <title>Booting from CD-ROM with the ARC or AlphaBIOS Console</title> -->
  <title>Avvio da CD-ROM tramite la console ARC o AlphaBIOS</title>
<para>

<!--
To boot a CD-ROM from the ARC console, find your sub-architecture code
name (see <xref linkend="alpha-cpus"/>), then enter
<filename>\milo\linload.exe</filename> as the boot loader and
<filename>\milo\<replaceable>subarch</replaceable></filename> (where
<replaceable>subarch</replaceable> is the proper subarchitecture name)
as the OS Path in the `OS Selection Setup' menu. Ruffians make an
exception: You need to use <filename>\milo\ldmilo.exe</filename> as
boot loader.
-->

Per l'avvio da CD-ROM tramite console ARC si deve trovare il nome
in codice della propria sottoarchitettura (vedere
<xref linkend="alpha-cpus"/>), poi
scrivere <filename>\milo\linload.exe</filename> come boot loader e
<filename>\milo\<replaceable>subarch</replaceable></filename> (dove
<replaceable>subarch</replaceable> è il nome corretto della
sotto architettura) come OS Path nel menu <quote>OS Selection Setup</quote>.
Fanno eccezione le Ruffian: si deve usare <filename>\milo\ldmilo.exe</filename>
come bootloader.

</para>
  </sect2>

  <sect2 arch="alpha" condition="supports-floppy-boot">
  <!-- <title>Booting from Floppies with the SRM Console</title> -->
  <title>Avvio da dischetto tramite la console SRM</title>
<para>

<!--
At the SRM prompt (<prompt>&gt;&gt;&gt;</prompt>), issue the following
command:
-->

Al prompt di SRM (<prompt>&gt;&gt;&gt;</prompt>), eseguire il comando
seguente:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

<!--
possibly replacing <filename>dva0</filename> with the actual device
name.  Usually, <filename>dva0</filename> is the floppy; type
-->

sostituendo eventualmente <filename>dva0</filename> con il nome del device
corretto. Di norma <filename>dva0</filename> è il lettore di dischetti;
scrivere

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

<!--
to see the list of devices (e.g., if you want to boot from a CD).
Note that if you are booting via MILO, <command>-flags</command> argument
is ignored, so you can just type <command>boot dva0</command>.
If everything works OK, you will eventually see the Linux kernel boot.
-->

per vedere la lista dei device (per esempio se si vuole avviare da CD).
Se si sta utilizzando MILO, gli argomenti nella forma
<command>-flags</command> vengono ignorati, quindi si può
semplicemente scrivere <command>boot dva0</command>.
Se tutto funziona correttamente si dovrà vedere prima o poi
l'avvio del kernel Linux.

</para><para>

<!--
If you want to specify kernel parameters when booting via
<command>aboot</command>, use the following command:
-->

Se si vuole specificare qualche parametro del kernel durante
l'avvio tramite <command>aboot</command>, usare il seguente
comando:

<!--
<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 arguments"
</screen></informalexample>
-->

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 argomenti"
</screen></informalexample>

<!--
(typed on one line), substituting, if necessary, the actual SRM boot
device name for <filename>dva0</filename>, the Linux boot device name for
<filename>fd0</filename>, and the desired kernel parameters for
<filename>arguments</filename>.
-->

(scritto su una sola riga), sostituendo, se necessario, il nome
del device SRM corrente al posto di <filename>dva0</filename>, il nome
del boot device di linux al posto di <filename>fd0</filename> e i
parametri del kernel al posto di <filename>argomenti</filename>.

</para><para>

<!--
If you want to specify kernel parameters when booting via
<command>MILO</command>, you will have to interrupt bootstrap once you get
into MILO.  See <xref linkend="booting-from-milo"/>.
-->

Se si vogliono specificare degli argomenti del kernel durante l'avvio
con <command>MILO</command>, si dovrà interrompere l'avvio
una volta che MILO è in esecuzione. Vedere <xref linkend="booting-from-milo"/>.

</para>
  </sect2>

  <sect2 arch="alpha" condition="supports-floppy-boot">
  <!-- <title>Booting from Floppies with the ARC or AlphaBIOS Console</title> -->
  <title>Avvio da dischetto tramite la console ARC o AlphaBIOS</title>
<para>

<!--
In the OS Selection menu, set <command>linload.exe</command> as the boot
loader, and <command>milo</command> as the OS Path.  Bootstrap using the
newly created entry.
-->

Nel menu OS Selection, impostare <command>linload.exe</command> come
boot loader e <command>milo</command> come OS Path. Avviare con tali
impostazioni.

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME" id="booting-from-milo">
  <!-- <title>Booting with MILO</title> -->
  <title>Avvio con MILO</title>
<para>

<!--
MILO contained on the bootstrap media is configured to proceed straight
to Linux automatically.  Should you wish to intervene, all you need is to
press space during MILO countdown.
-->

La versione di MILO inclusa nel supporto d'avvio è configurata per procedere
direttamente all'avvio di Linux in maniera automatica. Se fosse necessario
intervenire si deve semplicemente premere la barra spaziatrice durante il
conto alla rovescia.

</para><para>

<!--
If you want to specify all the bits explicitly (for example, to supply
additional parameters), you can use a command like this:
-->

Se si vuole specificare ogni cosa manualmente (per esempio, per fornire
parametri aggiuntivi), è possibile usare un comando come
il seguente:

<informalexample><screen>
MILO&gt; boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1 <!-- arguments -->
</screen></informalexample>

<!--
If you are booting from something other than a floppy, substitute
<filename>fd0</filename> in the above example with the appropriate device name
in Linux notation.  The <command>help</command> command would give you a brief
MILO command reference.
-->

Se si sta avviando da una sorgente diversa dal dischetto, sostituire
<filename>fd0</filename> nell'esempio con il nome appropriato del device
nella notazione di Linux. Il comando <command>help</command> fa un
breve resoconto dell'interfaccia MILO.

</para>
  </sect2>
