<!-- retain these comments for translator revision tracking -->
<!-- original version: 39920 untranslated -->


  <sect2 arch="alpha"><title>Partitioning for &arch-title;</title>
<para>

Booting Debian from the SRM console (the only disk boot method supported
by &releasename;) requires you to have a BSD disk label, not a DOS
partition table, on your boot disk.  (Remember, the SRM boot block is
incompatible with MS-DOS partition tables &mdash; see
<xref linkend="alpha-firmware"/>.)  As a result, <command>partman</command>
creates BSD disk labels when running on &architecture;, but if your disk
has an existing DOS partition table the existing partitions will need to be
deleted before <command>partman</command> can convert it to use a disk label.

</para><para>

If you have chosen to use <command>fdisk</command> to partition your
disk, and the disk that you have selected for partitioning does not
already contain a BSD disk label, you must use the <quote>b</quote>
command to enter disk label mode.

</para><para>

Unless you wish to use the disk you are partitioning from Tru64 Unix
or one of the free 4.4BSD-Lite derived operating systems (FreeBSD,
OpenBSD, or NetBSD), you should <emphasis>not</emphasis> create the
third partition as a <quote>whole disk</quote> partition (i.e. with
start and end sectors to span the whole disk), as this renders the
disk incompatible with the tools used to make it bootable with aboot.
This means that the disk configured by the installer for use as the
Debian boot disk will be inaccessible to the operating systems mentioned
earlier.

</para><para>

Also, because <command>aboot</command> is written to the first few
sectors of the disk (currently it occupies about 70 kilobytes, or 150
sectors), you <emphasis>must</emphasis> leave enough empty space at
the beginning of the disk for it.  In the past, it was suggested that
you make a small partition at the beginning of the disk, to be left
unformatted.  For the same reason mentioned above, we now suggest that
you do not do this on disks that will only be used by GNU/Linux.  When
using <command>partman</command>, a small partition will still be
created for <command>aboot</command> for convenience reasons.

</para><para condition="FIXME">

For ARC installations, you should make a small FAT partition at the
beginning of the disk to contain <command>MILO</command> and
<command>linload.exe</command> &mdash; 5 megabytes should be sufficient, see
<xref linkend="non-debian-partitioning"/>. Unfortunately, making FAT
file systems from the menu is not yet supported, so you'll have to do
it manually from the shell using <command>mkdosfs</command> before
attempting to install the boot loader.

</para>
  </sect2>
