# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_lv.po to Latvian
# Aigars Mahinovs <aigarius@debian.org>, 2006.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Peteris Krisjanis <pecisk@gmail.com>, 2008.
# Latvian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_lv\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-09-19 00:00+0300\n"
"Last-Translator: Peteris Krisjanis <pecisk@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:1001
msgid "Aboot installation failed.  Continue anyway?"
msgstr "Aboot instalācija neizdevās.  Turpināt?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:1001
msgid ""
"The aboot package failed to install into /target/.  Installing aboot as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to aboot, so continuing the installation may be possible."
msgstr ""
"Aboot pakas instalācija beidzās neveiksmīgi. Aboot kā sāknētāja instalācija "
"ir nepieciešams solis.  Taču instalācijas problēma varētu būt nesaistīta ar "
"aboot un tāpēc ir iespējams, ka instalāciju var turpināt."

#. Type: select
#. Description
#. :sl5:
#: ../aboot-installer.templates:2001
msgid "Device for boot loader installation:"
msgstr "Ierīce, uz kuras instalēt palaidēju:"

#. Type: select
#. Description
#. :sl5:
#: ../aboot-installer.templates:2001
msgid ""
"Aboot needs to install the boot loader on a bootable device containing an "
"ext2 partition.  Please select the ext2 partition that you wish aboot to "
"use.  If this is not the root file system, your kernel image and the "
"configuration file /etc/aboot.conf will be copied to that partition."
msgstr ""
"Aboot jāuzstāda sāknētājs uz sānējamas ierīces ar ext2 tipapartīciju.  Lūdzu "
"izvēlēties, kuru ext2 partīciju jūs gribat lai aboot lieto. Ja tā nav saknes "
"partīcija, tad jūsu sistēmas kodols un /etc/aboot.conf konfiguracijas fails "
"tiks nokopēti uz šo partīciju."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:3001
msgid "Install /boot on a disk with an unsupported partition table?"
msgstr "Instalēt /boot uz diska ar neatbalstītu partīciju tabulu?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:3001
msgid ""
"To be bootable from the SRM console, aboot and the kernel it loads must be "
"installed on a disk which uses BSD disklabels for its partition table.  "
"Your /boot directory is not located on such a disk.  If you proceed, you "
"will not be able to boot your system using aboot, and will need to boot it "
"some other way."
msgstr ""
"Lai varētu Sāknēties no SRM konsoles, aboot un kodolam jāatrodas uz diska ar "
"BSD disklabels tipa partīciju tabulu.  jūsu /boot mape neatrodas uz šāda "
"diska. Ja jūs turpināsiet instalāciju, tad jūs nevarēsiet izmantot aboot, "
"lai ielādētos un jums būs jāizmanto kāda cita metode."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid "Use unsupported file system type for /boot?"
msgstr "Lietot neatbalstītu failu sistēmas tipu priekš /boot?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid ""
"Aboot requires /boot to be located on an ext2 partition on a bootable "
"device.  This means that either the root partition must be an ext2 file "
"system, or you must have a separate ext2 partition mounted at /boot."
msgstr ""
"Aboot pieprasa lai /boot atrastos uz ext2 tipa partīcijas uz sāknējama "
"diska.  Tas nozīmē, ka vai nu jūsu saknes partīcijai ir jābūt ext2, vai arī "
"jums ir jābūt atsevišķai ext2 partīcijai piemontētai uz /boot."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid ""
"Currently, /boot is located on a partition of type ${PARTTYPE}.  If you keep "
"this setting, you will not be able to boot your Debian system using aboot."
msgstr ""
"Pašlaik /boot atrodas uz ${PARTTYPE} tipa partīcijas.  Ja jūs to "
"neizmainīsiet, jūs nevarēsiet ielādēt savu Debian sistēmu ar aboot."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid "Install without aboot?"
msgstr "Instalēt bez aboot?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid ""
"Your /boot directory is located on a disk that has no space for the aboot "
"boot loader.  To install aboot, you must have a special aboot partition at "
"the beginning of your disk."
msgstr ""
"Jūsu /boot mape atrodas uz diska, kur nav vietas aboot sāknētājam. Lai "
"instalētu aboot, jums ir jāizveido speciāla aboot partīcija diska sākumā."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid ""
"If you continue without correcting this problem, aboot will not be installed "
"and you will not be able to boot your new system from the SRM console."
msgstr ""
"Ja jūs turpināsiet, neizlabojot šo problēmu, tad aboot netiks uzinstalēts un "
"Jūs, visticamāk, nevarēsiet sāknēt savu jauno sistēmu no SRM konsoles."

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../aboot-installer.templates:6001
msgid "Install aboot on a hard disk"
msgstr "Aboot sāknētāja installēšana uz cietā diska"

#. Type: error
#. Description
#. :sl5:
#: ../aboot-installer.templates:7001
msgid "No ext2 partitions found"
msgstr "Ext2 tipa partīcijas nav atrastas"

#. Type: error
#. Description
#. :sl5:
#: ../aboot-installer.templates:7001
msgid ""
"No ext2 file systems were found on your computer.  To load the Linux kernel "
"from the SRM console, aboot needs an ext2 partition on a disk that has been "
"partitioned with BSD disklabels.  You will need to configure at least one "
"ext2 partition on your system before continuing."
msgstr ""
"Jūsu datorā nav atrasta neviena partīcija ar ext2 failu sistemu. Lai lādētu "
"Linux no SRM konsoles, aboot ir nepieciešama ext partīcija uz diska ar BSD "
"disklabels tipa partīcijām. Jums ir jāizveido vismaz viena šāda partīcija "
"lai turpinātu instalāciju."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:8001
msgid "Installing the aboot boot loader"
msgstr "Aboot sāknētāja instalēšana"

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:9001
msgid "Installing the 'aboot' package..."
msgstr "Instalē 'aboot' paku..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:10001
msgid "Determining aboot boot device..."
msgstr "Nosaku aboot sāknēšanas ierīci..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:11001
msgid "Installing aboot on ${BOOTDISK}..."
msgstr "Instalē aboot uz ${BOOTDISK}..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:12001
msgid "Configuring aboot to use partition ${PARTNUM}..."
msgstr "Konfigurē aboot lai lietotu ${PARTNUM} partīciju..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:13001
msgid "Copying kernel images to ${BOOTDEV}..."
msgstr "Kopē kodolu uz ${BOOTDEV}..."

#. Type: text
#. Description
#. :sl5:
#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:14001 ../aboot-installer.templates:15001
msgid "Aboot boot partition"
msgstr "Aboot sāknēšanas partīcija"

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:16001
msgid "aboot"
msgstr "aboot"
