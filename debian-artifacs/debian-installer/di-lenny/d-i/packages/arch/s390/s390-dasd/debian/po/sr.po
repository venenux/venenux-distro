# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Serbian/Cyrillic translation of cp6Linux.
# Copyright (C) 2008 THE cp6Linux'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cp6Linux package.
# Veselin Mijušković <veselin.mijuskovic@gmail.com>, 2008.
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: cp6Linux 2008\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2008-09-19 22:27+0100\n"
"Last-Translator: Veselin Mijušković <panzer@cp6linux.org>\n"
"Language-Team: Serbian/Cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl5:
#. TRANSLATORS, please translate "Finish" the same way you translate it in
#. the " Select "Finish" at the bottom of the list when you are done" string
#: ../s390-dasd.templates:1001
msgid "Finish"
msgstr "Заврши"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available disks:"
msgstr "Доступни дискови:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following disk access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"Следећи диск сториџ уређаји (disk access storage devices - DASD) су "
"доступни. Молим, одаберите сваки уређај који желите да користите, један по "
"један."

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "Одаберите \"Заврши\" на дну листе када будете готови."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose disk:"
msgstr "Одаберите диск:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a disk. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"Молим, одаберите диск. Морате да наведете цео број уређаја, укључујући и "
"водеће нуле."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid disk"
msgstr "Неисправан диск"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "Одабран је неисправан број диска."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "Format the disk?"
msgstr "Да форматирам диск?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"The installer is unable to detect if the device ${device} has already been "
"formatted or not. Disks need to be formatted before you can create "
"partitions."
msgstr ""
"Инсталер не може да пронађе да ли је уређај ${device} већ форматиран или "
"није. Дискови треба да буду форматирани пре него што креирате партиције."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"If you are sure the disk has already been correctly formatted, you don't "
"need to do so again."
msgstr ""
"Ако сте сигурни да је диск већ исправно форматиран, није потребно да га "
"поново форматирате."

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid "Formatting ${device}..."
msgstr "Форматирам ${device}..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "Configure disk access storage devices (DASD)"
msgstr "Конфигуриши DASD-ове"
