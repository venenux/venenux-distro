# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/gu.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# translation of d-i.po to Gujarati
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Contributor:
# Kartik Mistry <kartik.mistry@gmail.com>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: d-i\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2006-07-10 15:53+0530\n"
"Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>\n"
"Language-Team: Gujarati <team@utkarsh.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../save-logs.templates:1001
msgid "Save debug logs"
msgstr "ડિબગ લૉગ સંગ્રહો"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "floppy"
msgstr "ફ્લોપી"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "web"
msgstr "વેબ"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "mounted file system"
msgstr "માઉન્ટ કરેલ ફાઈલ સિસ્ટમ"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid "How should the debug logs be saved or transferred?"
msgstr "ડિબગ કરેલ ફાઇલો કેવી રીત સંગ્રહ કરવામાં અથવા ફેરવવામાં આવશે?"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid ""
"Debugging log files for the installer can be saved to floppy, served up over "
"the web, or saved to a mounted file system."
msgstr ""
"સ્થાપન માટેની ડિબગીંગ લોગ ફાઇલો ફ્લોપી પર સંગ્રહી શકાશે, વેબ પર મૂકી શકાશે, અથવા માઉન્ટ "
"કરેલ ફાઇલ સિસ્ટમમાં સંગ્રહી શકાશે."

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid "Directory in which to save debug logs:"
msgstr "ડિરેક્ટરી કે જેમાં ડિબગ લૉગ સંગ્રહવવામાં આવશે:"

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid ""
"Please make sure the file system you want to save debug logs on is mounted "
"before you continue."
msgstr ""
"મહેરબાની કરી ખાતરી કરો કે જ્યાં ડિબગ લોગ સંગ્રહવાનાં છે તે ફાઇલ સિસ્ટમ તમે આગળ વધો તે "
"પહેલાં માઉન્ટ કરવામાં આવેલ છે."

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "Cannot save logs"
msgstr "લૉગ સંગ્રહી શકાયું નહી"

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "The directory \"${DIR}\" does not exist."
msgstr "ડિરેક્ટરી \"${DIR}\" અસ્તિત્વ ધરાવતી નથી."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid "Web server started, but network not running"
msgstr "વેબ સર્વર શરૂ થયું છે, પણ નેટવર્ક ચાલતું નથી"

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. However, the network is not set up yet. The web server will be "
"left running, and will be accessible once the network is configured."
msgstr ""
"સરળ વેબ સર્વર આ કમ્પ્યુટરમાં લોગ ફાઇલો અને ડીબગ માહિતી આપવા માટે શરુ કરવામાં આવ્યું છે. "
"જોકે, નેટવર્ક હજી સુધી ગોઠવવામાં આવ્યું નથી. વેબસર્વર ચાલુ રાખવામાં આવશે, અને જ્યારે નેટવર્ક "
"ગોઠવાય ત્યારે ઉપયોગમાં લઇ શકાશે."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid "Web server started"
msgstr "વેબ સર્વર શરૂ થયું"

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. An index of all the available log files can be found at http://"
"${ADDRESS}/"
msgstr ""
"સરળ વેબ સર્વર આ કમ્પ્યુટરમાં લોગ ફાઇલો અને ડીબગ માહિતી આપવા માટે શરુ કરવામાં આવ્યું છે. "
"બધી પ્રાપ્ત લોગ ફાઇલોની અનુક્રમણિકા http://${ADDRESS}/ પર મળી શકશે."

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Insert formatted floppy in drive"
msgstr "ડ્રાઇવમાં ફોર્મેટ કરેલ ફ્લોપી નાંખો"

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Log files and debug info will be copied into this floppy."
msgstr "લૉગ ફાઇલો અને ડિબગ માહિતી આ ફ્લોપીમાં નકલ કરવામાં આવશે."

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid ""
"The information will also be stored in /var/log/installer/ on the installed "
"system."
msgstr "માહિતી સ્થાપિત સિસ્ટમ પર /var/log/installer/ માં સંગ્રહ કરવામાં આવશે."

#. Type: error
#. Description
#: ../save-logs.templates:8001
msgid "Failed to mount the floppy"
msgstr "ફ્લોપી માઉન્ટ કરવામાં નિષ્ફળ"

#. Type: error
#. Description
#: ../save-logs.templates:8001
msgid ""
"Either the floppy device cannot be found, or a formatted floppy is not in "
"the drive."
msgstr "ફ્લોપી ઉપકરણ મળી શક્યું નથી અથવા, ડ્રાઇવમાં ફોર્મેટ કરેલ ફ્લોપી નથી."

#. Type: text
#. Description
#. :sl1:
#. finish-install progress bar item
#: ../save-logs.templates:9001
msgid "Gathering information for installation report..."
msgstr "સ્થાપન રીપોર્ટ માટે માહિતી ભેગી કરે છે..."
