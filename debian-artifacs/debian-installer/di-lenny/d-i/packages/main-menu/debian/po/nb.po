# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/nb.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# translation of nb.po to Norwegian Bokmål
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Knut Yrvin <knuty@skolelinux.no>, 2004.
# Klaus Ade Johnstad <klaus@skolelinux.no>, 2004.
# Axel Bojer <axelb@skolelinux.no>, 2004.
# Hans Fredrik Nordhaug <hans@nordhaug.priv.no>, 2005.
# Bjørn Steensrud <bjornst@powertech.no>, 2004,2005, 2006, 2007.
msgid ""
msgstr ""
"Project-Id-Version: nb\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2007-04-18 18:15+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@powertech.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: text
#. Description
#. :sl1:
#: ../main-menu.templates:1001
msgid "Debian installer main menu"
msgstr "Hovedmenyen for installeringen av Debian"

#. Type: select
#. Description
#. :sl1:
#: ../main-menu.templates:2001
msgid "Choose the next step in the install process:"
msgstr "Velg neste trinn i installasjonen:"

#. Type: error
#. Description
#. :sl2:
#: ../main-menu.templates:3001
msgid "Installation step failed"
msgstr "Installasjonstrinnet mislyktes"

#. Type: error
#. Description
#. :sl2:
#: ../main-menu.templates:3001
msgid ""
"An installation step failed. You can try to run the failing item again from "
"the menu, or skip it and choose something else. The failing step is: ${ITEM}"
msgstr ""
"Et trinn i installasjonen mislyktes. Du kan forsøke å kjøre dette trinnet om "
"igjen fra menyen, eller hoppe over det og gjøre noe annet. Det som mislyktes "
"var: ${ITEM}"

#. Type: select
#. Description
#. :sl2:
#: ../main-menu.templates:4001
msgid "Choose an installation step:"
msgstr "Velg et installasjonstrinn:"

#. Type: select
#. Description
#. :sl2:
#: ../main-menu.templates:4001
msgid ""
"This installation step depends on one or more other steps that have not yet "
"been performed."
msgstr ""
"Dette trinnet av installasjonen forutsetter ett eller flere trinn som ennå "
"ikke er blitt utført."
