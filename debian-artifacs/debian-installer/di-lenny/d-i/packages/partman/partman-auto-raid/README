This allows preseeding of a fully configured RAIDed system by setting
partman-auto/method to "raid" and using partman-auto-raid/recipe to tell
it how to construct the RAID array(s).

The preseedable parameter partman-auto-raid/recipe should consist of comma
separated recipes and recipes are of the form:

<raidtype> <devcount> <sparecount> <fstype> <mountpoint> <devices> <sparedevices>

where <devices> is a hash separated list of the devices to make up the RAID
and <sparedevices> is a hash separated list of the spare devices in the
array.

e.g.
1 2 0 ext3 / /dev/discs/disc0/part1#/dev/discs/disc1/part1 .
1 2 0 swap / /dev/discs/disc0/part5#/dev/discs/disc1/part5 .
(but without the line break)

This makes the first RAID device that was created by this udeb be a RAID1 of
two disks disc0/part1 and disc1/part1 and be formatted as ext3 and mounted
on /.  The second is also a RAID1 and used as swap (and the mountpoint
ignored).

-- Simon Huggins <huggie@earth.li>
sponsored by Black Cat Networks http://www.blackcatnetworks.co.uk/
