# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005-2007.
# Felipe Castro <fefcas@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: eo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2008-09-14 18:32-0300\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid "Error while setting up RAID"
msgstr "La 'RAID'-akomodado eraras"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid ""
"An unexpected error occurred while setting up a preseeded RAID configuration."
msgstr "Eraro okazis dum la enmetado de la antaŭagorda 'RAID'-skemo."

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:1001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Vidu '/var/log/syslog' aŭ la kvara virtuala konzolo por akiri detalojn."

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:4001
msgid "Not enough RAID partitions specified"
msgstr "Nesufiĉe indikitaj 'RAID'-diskpartoj"

#. Type: error
#. Description
#. :sl3:
#: ../partman-auto-raid.templates:4001
msgid ""
"There are not enough RAID partitions specified for your preseeded "
"configuration. You need at least 3 devices for a RAID5 array."
msgstr ""
"Nesufiĉe indikitaj 'RAID'-diskpartoj por via elektita agordo. Vi almenaŭ "
"bezonas po tri aparatojn por ĉiu 'RAID5'-disktabelo."
