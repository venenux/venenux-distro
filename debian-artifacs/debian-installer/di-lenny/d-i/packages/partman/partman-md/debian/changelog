partman-md (44) unstable; urgency=low

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Bosnian (bs.po) by Armin Besirovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po)
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Felipe Castro
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hebrew (he.po) by Omer Zak
  * Hindi (hi.po) by Kumar Appaiah
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Georgian (ka.po) by Aiet Kolkhi
  * Central Khmer (km.po) by KHOEM Sokhem
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Arangel Angov
  * Malayalam (ml.po) by പ്രവീണണ്‍ അരിമ്പ്രത്തൊടിയിലല്‍
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Vanja Cvelbar
  * Albanian (sq.po) by Elian Myftiu
  * Serbian (sr.po) by Veselin Mijušković
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Ukrainian (uk.po) by Євгеній Мещеряков
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Deng Xiyue

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 21:19:37 -0300

partman-md (43) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Do not ask confirmation on further calls to device_remove_md() when
    partman-md/device_remove_md is preseed.
  * Use 255 as return code when user backs up in device_remove_md().
  * Use 99 as return code when partman needs to be restarted in
    device_remove_md() instead of doing it directly.
    Breaks: partman-partitioning (<< 62)
  * Handle device_remove_lvm() returning 99 to indicate that partman needs to
    be restarted.
    Depends: partman-lvm (>= 64)

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Dzongkha (dz.po) by Jurmey Rabgay(Bongop) (DIT,BHUTAN)
  * Greek, Modern (el.po) by Emmanuel Galatoulas
  * Basque (eu.po) by Piarres Beobide
  * Italian (it.po) by Milo Casagrande
  * Kurdish (ku.po) by Erdal Ronahi
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 25 Aug 2008 21:12:23 +0200

partman-md (42) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Clean up the initialization of MD devices.  Together with the changes
    introduced in partman-base (>= 124), setup of RAID devices won't be lost
    across partman restarts anymore.
    (Closes: #391479, #391483, #393728, #398668)
  * Load the necessary modules and scan RAID arrays during partman
    initialization.  (Closes: #391474)
  * Add device_remove_md() in lib/md-remove.sh: it is necessary to allow
    proper removal of MD devices when we create a new label on a disk
    (for example, during automatic partitioning).

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Czech (cs.po) by Miroslav Kure
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Tenzin Dendup
  * Esperanto (eo.po) by Felipe Castro
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Esko Arajärvi
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Gujarati (gu.po) by Kartik Mistry
  * Croatian (hr.po) by Josip Rodin
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Milo Casagrande
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Changwoo Ryu
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malayalam (ml.po) by Praveen|പ്രവീണണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Norwegian Bokmål (nb.po) by Hans Fredrik Nordhaug
  * Dutch (nl.po) by Frans Pop
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Ivan Masár
  * Slovenian (sl.po) by Matej Kovacic
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Mert Dirik
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Kov Chai
	
 -- Otavio Salvador <otavio@debian.org>  Tue, 05 Aug 2008 14:01:00 -0300

partman-md (41) unstable; urgency=low

  [ Frans Pop ]
  * Use /dev/mdX instead of /dev/md/X while version 0 superblocks are default.
    No actual change as only commented code is modified.

  [ Updated translations ]
  * Kurdish (ku.po) by Erdal Ronahi
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Portuguese (pt.po) by Miguel Figueiredo
  
 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 13:38:04 -0300

partman-md (40) unstable; urgency=low

  [ Updated translations ]
  * Finnish (fi.po) by Esko Arajärvi
  * Hindi (hi.po) by Kumar Appaiah
  * Indonesian (id.po) by Arief S Fitrianto
  * Central Khmer (km.po) by Khoem Sokhem
  * Latvian (lv.po) by Viesturs Zarins
  * Nepali (ne.po) by Shyam Krishna Bal
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Slovenian (sl.po) by Matej Kovacic
  * Turkish (tr.po) by Recai Oktaş

 -- Otavio Salvador <otavio@ossystems.com.br>  Fri, 15 Feb 2008 09:38:42 -0200

partman-md (39) unstable; urgency=low

  * Moved definitions.sh to ./lib/base.sh. Requires partman-base (>= 114).
  * Use new function library ./lib/commit.sh. Requires partman-base (>= 115).
  * Whitespace cleanup and a few minor coding style fixes.
  * md_sync_flag: use single assignment for adding flags.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Korean (ko.po) by Changwoo Ryu
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Panjabi (pa.po) by A S Alam

 -- Frans Pop <fjp@debian.org>  Sat, 29 Dec 2007 22:14:53 +0100

partman-md (38) unstable; urgency=low

  [ Max Vozeler ]
  * Replace code to commit partman changes to disk with call to commit_changes
    from partman-base (>= 113) and version the dependency accordingly.

  [ Updated translations ]
  * Belarusian (be.po) by Hleb Rubanau
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Esperanto (eo.po) by Serge Leblanc
  * Korean (ko.po) by Sunjae Park
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Norwegian Bokmål (nb.po) by Hans Fredrik Nordhaug
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Wed, 05 Dec 2007 13:22:19 +0100

partman-md (37) unstable; urgency=low

  [ Christian Perrier ]
  * Consistent writing in templates: use "has been aborted" instead
    of "is aborted"

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bengali (bn.po) by Jamil Ahmed
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Tshewang Norbu
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Nepali (ne.po) by Nabin Gautam
  * Dutch (nl.po) by Frans Pop
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Ukrainian (uk.po)
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Colin Watson <cjwatson@debian.org>  Tue, 02 Oct 2007 17:13:36 +0100

partman-md (36) unstable; urgency=low

  [ Colin Watson ]
  * init.d/md-devices: Simplify code to calculate RAID device names.

  [ Frans Pop ]
  * Move deletion of SVN directories to install-rc script.
  * Improve the way install-rc is called.

  [ Updated translations ]
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Mon, 21 May 2007 16:58:45 +0200

partman-md (35) unstable; urgency=low

  * init.d/md-devices: Handle /dev/md* as well as /dev/md/*.

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud

 -- Colin Watson <cjwatson@debian.org>  Fri, 27 Apr 2007 10:15:58 +0100

partman-md (34) unstable; urgency=low

  * Fix syntax error resulting in failure to set partition type.
    Closes: #414203.

 -- Frans Pop <fjp@debian.org>  Sat, 10 Mar 2007 17:53:26 +0100

partman-md (33) unstable; urgency=low

  [ David Härdeman ]
  * Make sure that the lvm, raid and swap flags are used in a mutually
    exclusive manner. Closes: #397973.

 -- Frans Pop <fjp@debian.org>  Wed,  7 Mar 2007 22:49:26 +0100

partman-md (32) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 18:24:05 +0100

partman-md (31) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 12:26:02 +0100

partman-md (30) unstable; urgency=low

  [ Colin Watson ]
  * Mark ${ITEMS} in partman-md/confirm as untranslatable.

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 16:04:57 +0200

partman-md (29) unstable; urgency=low

  * Do not run init.d script if we don't have RAID support.
  * Remove Standards-Version as it is not relevant for udebs.
  * Add dependency on cdebconf.

 -- Frans Pop <fjp@debian.org>  Wed, 12 Jul 2006 18:52:43 +0200

partman-md (28) unstable; urgency=low

  * Avoid error if device directory already exists.
  * Set maintainer to d-i team and add myself to uploaders.

 -- Frans Pop <fjp@debian.org>  Wed, 12 Jul 2006 14:21:06 +0200

partman-md (27) unstable; urgency=low

  * During partman initialization, check if RAID device is already opened to
    avoid opening it twice which results in a warning. Closes: #377291.

 -- Frans Pop <fjp@debian.org>  Sat,  8 Jul 2006 15:48:57 +0200

partman-md (26) unstable; urgency=low

  [ David Härdeman ]
  * Use the generic confirm_changes from partman-base.
    (needs partman-base 87)

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş

 -- Frans Pop <fjp@debian.org>  Fri, 23 Jun 2006 19:26:15 +0200

partman-md (25) unstable; urgency=low

  [ David Härdeman ]
  * Make the partman-md choose_partition menu entry dynamic to match the
    behaviour of partman-lvm and partman-crypto.

 -- Joey Hess <joeyh@debian.org>  Mon,  5 Jun 2006 12:39:32 -0400

partman-md (24) unstable; urgency=low

  [ Fabio M. Di Nitto ]
  * RAID cannot be selected if we are on sparc AND the partiton starts at 0
    otherwise there will partition table corruption happening in the first
    512 bytes of the disk.
    (NOTE: this will work only once patch for sparc/raid support will make it
           in parted)

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bosnian (bs.po) by Safir Secerovic
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Sonam Rinchen
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hungarian (hu.po) by SZERVÑC Attila
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Sat, 29 Apr 2006 03:30:04 +0200

partman-md (23) unstable; urgency=low

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.
  * Use stop_parted_server and restart_partman functions from partman-base
    73 instead of duplicating the code.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Baishampayan Ghose
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Icelandic (is.po) by David Steinn Geirsson
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Norwegian Nynorsk (nn.po)
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrişor
  * Slovenian (sl.po) by Jure Cuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 24 Jan 2006 22:11:05 +0100

partman-md (22) unstable; urgency=low

  [ Colin Watson ]
  * Depend on partman-base rather than partman.

  [ Updated translations ]
  * German (de.po) by Holger Wansing
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Romanian (ro.po) by Eddy Petrisor
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 18:17:21 +0200

partman-md (21) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Giuseppe Sacco
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagasy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 17:25:20 +0300

partman-md (20) unstable; urgency=low

  * Includes fixes for variable substitutions in translated templates.
  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Finnish (fi.po) by Tapio Lehtonen
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Hungarian (hu.po) by VEROK Istvan
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 17:40:33 -0500

partman-md (19) unstable; urgency=low

  * Joey Hess
    - Remove unused raid_root_boot template.
    - Fix prompting before writing partition table. Closes: #272514, #260698
    - Rename templates file.
  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon, 18 Oct 2004 15:28:37 -0400

partman-md (18) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Oct 2004 15:33:10 -0400

partman-md (17) unstable; urgency=low

  * Joey Hess
    - raid1 works for root (or /boot) with raid1, of course does not with
      raid0. Remove the big scarey warning, though we really need a new one
      about raid0 and some other raid levels.

 -- Joey Hess <joeyh@debian.org>  Tue, 28 Sep 2004 21:41:57 -0400

partman-md (16) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Bøkmal, Norwegian (nb.po) by Axel Bojer
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Russian L10N Team
    - Slovenian (sl.po) by Jure Čuhalev
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 21:29:58 -0400

partman-md (15) unstable; urgency=low

  * Joey Hess
    - Remove seen flag setting and fix reset calls for preseeding.
  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj

 -- Joey Hess <joeyh@debian.org>  Wed,  1 Sep 2004 16:16:33 -0400

partman-md (14) unstable; urgency=low

  * Anton Zinoviev
    - update.d/lvm_sync_flag: do not create "format" files in the
      partition directories.  Look at #248062.
  * Updated translations: 
    - Arabic (ar.po) by Christian Perrier
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Yuri Kozlov
    - Slovenian (sl.po) by Matjaz Horvat
    - Turkish (tr.po) by Recai Oktaş

 -- Joey Hess <joeyh@debian.org>  Mon, 30 Aug 2004 12:23:24 -0400

partman-md (13) unstable; urgency=low

  * Anton Zinoviev
    - update.d/md_sync_flag: autoset raid-method for partitions with raid
      flag and no method instead of unsetting the raid flag.  Thanks to
      Joey Hess, see #250033. 
    - templates: remove partman-md/text/method
    - choose_method/md/choices: use partman/method_long/raid instead of
      partman-md/text/method
    - choose_method/md/do_option: do not create a `format' file in the
      partition directory.  Thanks to Martin Michlmayr, closes: #248062
  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Persian (fa.po) by Arash Bijanzadeh
    - Croatian (hr.po) by Krunoslav Gernhard
    - Italian (it.po) by Stefano Canepa
    - Polish (pl.po) by Bartosz Fenski
    - Swedish (sv.po) by Per Olofsson
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Sun, 25 Jul 2004 19:24:45 -0400

partman-md (12) unstable; urgency=low

  * Martin Michlmayr
    - Sync changes from Anton Zinoviev made in partman-lvm (15), namely:
      - in the confirmation dialog say which partitions are going to be
        formatted.  (See #247343)
      - init.d/lvm: inform parted_server for whether the newly created
        partition contains some file system; use the command "open_dialog
        DISK_UNCHANGED" to inform parted_server that the newly created loop
        "partition table" and partition do not exist only in the internal
        data structures of libparted, but are real. (See #247780)
       closes: #249153
    - Only prompt about writing changes to disk when there actually
      are any changes.  Closes: #250971
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by I Gede Wijaya S
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Marius Gedminas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Recai Oktas
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Ming Hua

 -- Joey Hess <joeyh@debian.org>  Sat, 17 Jul 2004 14:27:36 -0400

partman-md (11) unstable; urgency=low

  * Joey Hess
    - Add a warning if / or /boot is on md, as the debian initrd does not
      (yet) support that.
  * Updated translations:
    - French (fr.po) by Christian Perrier
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 28 May 2004 19:00:41 -0300

partman-md (10) unstable; urgency=medium

  * Martin Michlmayr
    - Under 2.6, /proc/mdstat doesn't mention the sector size; don't assume
      it's listed because this will lead to a syntax error under 2.6.
      Closes: #251133.
  * Updated translations:
    - Korean (ko.po) by Changwoo Ryu

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 27 May 2004 01:57:13 -0300

partman-md (9) unstable; urgency=low

  * Martin Michlmayr
    - Correctly display the size of an MD device.
  * Stephen R. Marenka
    - Don't try to init inactive devices (at least on 2.2 kernels).
  * Colin Watson
    - Remove useless use of cat.
    - Depend on partman.
  * Anton Zinoviev
    - move the item in the main partitioning menu up.
  * Updated translations:
    - German (de.po) by Dennis Stampfer
    - Basque (eu.po) by Piarres Beobide Egaña
    - Hungarian (hu.po) by VERÓK István
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Marius Gedminas
    - Dutch (nl.po) by Bart Cornelis
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Tue, 25 May 2004 12:38:26 -0300

partman-md (8) unstable; urgency=low

  * Joey Hess
    - Add a po/output file to get the template encoding to be utf-8.
  * Martin Michlmayr
    - Add searching for MD devices more reliable.
    - Add myself as uploader.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by I Gede Wijaya S
    - Japanese (ja.po) by Kenshi Muto
    - Norwegian (nb.po) by Bjørn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Recai Oktas
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 05 May 2004 03:48:38 +0100

partman-md (7) never-released; urgency=low

  * Templates are now being used properly.

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  21 Apr 2004 01:32:46 +0200

partman-md (6) never-released; urgency=low

  * Filesystems on a RAID-device partition should now be detected properly

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  21 Apr 2004 01:32:46 +0200

partman-md (5) never-released; urgency=low

  * Should now detect RAID partitions and add them as devices

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  20 Apr 2004 23:20:49 +0200

partman-md (4) never-released; urgency=low

  * Took some of the templates out
  * Fixed template references
  * now depends on mdcfg-utils instead of mdcfg

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  20 Apr 2004 20:29:30 +0200

partman-md (3) never-released; urgency=low

  * Put the templates back in
  * Should be able to run the mdcfg program now

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  10 Apr 2004 21:54:52 +0200

partman-md (2) never-released; urgency=low

  * Removed the usage of templates for now...

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  10 Apr 2004 19:49:04 +0200

partman-md (1) never-released; urgency=low

  * Took parts of the partman-lvm package, and created a base for the MD
    package

 -- Paul Fleischer <proguy@proguy.dk>  Sat,  10 Apr 2004 14:11:50 +0200

