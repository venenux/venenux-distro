# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Simplified Chinese translation for Debian Installer.
#
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translated by Yijun Yuan (2004), Carlos Z.F. Liu (2004,2005,2006),
# Ming Hua (2005,2006,2007,2008), Xiyue Deng (2008), Kov Chai (2008).
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-09 23:25+0800\n"
"Last-Translator: Deng Xiyue <manphiz@gmail.com>\n"
"Language-Team: Simplified Chinese <debian-chinese-gb@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:1001
msgid "Software RAID device"
msgstr "软件 RAID 设备"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:2001
msgid "Configure software RAID"
msgstr "软件 RAID 设置"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001
msgid "Write the changes to the storage devices and configure RAID?"
msgstr "将修改内容写入存储设备并配置 RAID 吗？"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001
msgid ""
"Before RAID can be configured, the changes have to be written to the storage "
"devices.  These changes cannot be undone."
msgstr "在设置 RAID 之前，所有改动都必须被写入存储设备。这些改动将无法被撤消。"

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001 ../partman-md.templates:4001
msgid ""
"When RAID is configured, no additional changes to the partitions in the "
"disks containing physical volumes are allowed.  Please convince yourself "
"that you are satisfied with the current partitioning scheme in these disks."
msgstr ""
"RAID 设置完成后，就不能再对含有物理卷的磁盘上的分区进行修改。因此，请您确定真"
"的满意这些磁盘上当前的分区方案。"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:4001
msgid "Keep current partition layout and configure RAID?"
msgstr "保留现有的分区设置并配置 RAID 吗？"

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "RAID configuration failure"
msgstr "配置 RAID 失败"

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "An error occurred while writing the changes to the storage devices."
msgstr "将修改内容写入存储设备时出现一个错误。"

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "RAID configuration has been aborted."
msgstr "配置 RAID 已被中止。"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:6001
msgid "physical volume for RAID"
msgstr "RAID 物理卷"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:7001
msgid "raid"
msgstr "raid"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Remove existing software RAID partitions?"
msgstr "移除现有软件 RAID 分区吗？"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid ""
"The selected device contains partitions used for software RAID devices. The "
"following devices and partitions are about to be removed:"
msgstr "您所选择的设备包含用于软件 RAID 设备的分区、以下设备和分区将会被移除："

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Software RAID devices about to be removed: ${REMOVED_DEVICES}"
msgstr "将要移除软件 RAID 设备: ${REMOVED_DEVICES}"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Partitions used by these RAID devices: ${REMOVED_PARTITIONS}"
msgstr "这些 RAID 设备使用的分区: ${REMOVED_PARTITIONS}"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid ""
"Note that this will also permanently erase any data currently on the "
"software RAID devices."
msgstr "注意这也将会把这些软件 RAID 设备上现有的数据全都永久删除。"
