# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of nn.po to Norwegian Nynorsk
# translation of d-i_nn.po to
# Norwegian Nynorsk messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2004, 2005, 2006, 2008.
# Håvard Korsvoll <korsvoll@gmail.com>, 2006, 2007.
msgid ""
msgstr ""
"Project-Id-Version: nn\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-10 21:17+0200\n"
"Last-Translator: Håvard Korsvoll\n"
"Language-Team: Norwegian Nynorsk <i18n-no@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"nynorsk@lists.debian.org>\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Naudmodus"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Gå inn i naudmodus"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Fann inga partisjonar"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Installasjonsprogrammet fann ingen partisjonar så du kan ikkje montere noko "
"rotfilsystem. Grunnen kan vere at kjerna ikkje klarte å finne harddisken "
"din, ikkje klarte å lese partisjonstabellen eller harddisken ikkje er "
"partisjonert. Viss du ynskjer, kan du undersøkje dette frå eit skal i "
"installasjonsmiljøet."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Einig som skal brukast som rotfilsystem:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Oppgje ei eining som du vil bruke som rot-filsystem. Du vil kunne velje "
"mellom ulike redningshandlingar som kan utførast på dette filsystemet."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Inga slik eining"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Eininga du oppgav som ditt rotfilsystem (${DEVICE}) eksisterer ikkje. Prøv "
"igjen."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Montering feila"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Ein feil oppstod under montering av eininga du oppgav som ditt rotfilsystem "
"(${DEVICE}) på /target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Sjå i syslog etter meir informasjon."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Redningsoperasjonar"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Redningsoperasjon mislukkast"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "Redningsoperasjonen «${OPERATION}» mislukkast med returkoden ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Køyr eit skal i ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Køyr eit skal i installeringensmiljøet"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Vel eit anna rot-filsystem"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Start systemet om att"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Køyrer eit skal"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Etter denne meldinga vil du få eit skal med dette ${DEVICE} montert på «/». "
"Viss du treng andre filsystem (som «/usr»), så må du montere desse sjølv."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Feil ved køyring av skal i /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Eit skal (${SHELL}) blei funne på rotfilsystemet ditt (${DEVICE}), men det "
"oppstod ein feil under køyring."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Ingen skal funne i /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr "Ingen brukbare skal blei funne i rotfilsystemet ditt (${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Interaktivt skal på ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Etter denne meldinga vil du få eit skal med dette ${DEVICE} montert på «/"
"target». Du kan arbeide i dette skalet med dei verktøya som finst "
"tilgjengeleg i installasjonsmiljøet. Viss du treng å gjere filsystemet til "
"rot-filsystem, så køyrer du «chroot /target». Viss du treng andre filsystem "
"(som «/usr»), så må du montere desse sjølv."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Etter denne meldinga vil du få eit skal i installasjonsmiljøet. Sidan "
"installasjonsprogrammet ikkje klarte å finne nokon partisjonar, så er ingen "
"filsystem montert for deg."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Interaktivt skal i installasjonsmiljøet"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Passordstreng for ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Skriv inn passordstrengen for det krypterte dataområdet ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Viss du ikkje skriv inn noko, vil dataområdet ikkje vere tilgjengeleg under "
"redningsoperasjonar."
