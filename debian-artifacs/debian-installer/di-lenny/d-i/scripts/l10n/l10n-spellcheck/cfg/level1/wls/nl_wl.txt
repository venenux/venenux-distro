## This file is iso-8859-1 encoded!

## Normal words that really should have been in aspell's dictionary
��n
gaat
herbruiken
herstart
letten
namen
taalkeuze
wenst
zoniet

## General ICT terminology
KB
GB
MB
TB
gigabyte
terabyte

aanhechtpunt
aanhechtpunten
aankoppellen
aankoppelopties
apparaatbestand
apparaatbestanden
apparaatnaam
apparaatnummer
apparaatnummers
bestandssysteem
bestandssystemen
CD-station
CD-stations
computernaam
computernamen
desktopmachine
domeinnaam
encryptie
firewalls
firmware
foutcode
foutcontrole
gescand
hardwareherkenning
hardwarematig
Internet
Internetadres
kabelmodem
logbestanden
loggend
naamserver
naamserveradressen
naamservers
netwerkadressen
netwerkapparaat
netwerkapparaten
netwerkbeheerder
netwerkconfiguratie
netwerkinstallatie
netwerkinterface
netwerkkaart
onoffici�le
opstartbaar
opstartlader
partitiegrootte
partitiegroottes
partitienaam
partitietabel
poortnaam
poortnummer
reservekopie
schijfruimte
spiegelserver
superblok
systeemlog
systeemlogboek
toegangspunt
toegangspunten
toetsenbordindeling
versleuteld
versleutelde
versleutelen
wisselgeheugen
wisselgeheugenruimte

# LVM and RAID related
volumebeheerder
volumegroep
volumegroepen
volumegroepnaam


## English ICT terminology we've decided not to translate
## (or should not be translated in some situations)
device
devices
discs
exclude
false
framebuffer
kernels
kernelversie
local
messages
microcode
news
random
router
shell
slave
standard
storage
syslog
token
true
windows


## Commands, devices, etc.
ls
max
MD
MD-apparaat
MD-apparaten
VG
VM


## Debian specific terminology
stable
unstable
testing


# Special strings that we don't want to appear in the check
# Some of these should maybe be moved to the general WL
a-z
choose
com
CTC-apparaat
iucv
language
lcs
mcdx
NbPart
nl
nn
nnnn-nnnn-nn
nnnnnnnn
non-QDIO
O-poort
onbruikb
opt
opts
org
OSA
sync
Unix


## Geographical and timezone names not in aspell's dictionary
Afrika
Argentini�
Arizona
Australi�
Azi�
Brazili�
Broken
Bulgarije
Cara�bische
Centraal-Amerika
China
Colombia
#Costa Rica
Denemarken
Duitsland
Estland
Europa
Finland
Frankrijk
Griekenland
Groot-Brittanni�
#Hong Kong
Hongarije
Ierland
IJsland
Indi�
Indonesi�
Isra�l
Itali�
Japan
Kroati�
Letland
Litouwen
Luxemburg
Marokko
Nederland
Nicaragua
Nieuw-Zeeland
Noord-Amerika
Noorwegen
Oceani�
Oekra�ne
Oostenrijk
Portugal
Roemeni�
Rusland
Singapore
Slovakije
Sloveni�
Spanje
Taiwan
Thailand
Tsjechi�
Vostok
Wit-Rusland
Zwitserland

Adelaide
Alaska
Amur
Aqtobe
Araguaina
Atlantic
Atyrau
Auckland
Bahia
Baikal
Baja
Belem
Branco
Brisbane
California
Campo
Casey
Central
Ceuta
Chatham-eilanden
Choibalsan
Cuiaba
d'Urville
Danmarkshavn
Darwin
Dumont
East
Eastern
Eirunepe
Enderbury
Fortaleza
Galapagos
Gambier-eilanden
Gilbert-eilanden
Godthab
Guayaquil
Hawaii
Hill
Hobart
Hora
Hovd
Howe
Indiana
Jayapura
Johnston
Kaliningrad
Kiritimati
Kosrae
Lena
Lindeman
Line
Lubumbashi
Maceio
Madeira-eilanden
Magadan
Manaus
Marquesas-eilanden
Mawson
McMurdo
Melbourne
Midway-eilanden
Montana
Mountain
Netherlands
Newfoundland
Noronha
Novosibirsk
Oral
Pacific
Pacifico
Palmer
Paulo
Perth
Phoenix-eilanden
Ponape
Pontianak
Qyzylorda
Recife
Rio
Rothera
Sachalin
Sao
Saskatchewan
Scoresbysund
Society-eilanden
Syowa
Tahiti
Tarawa
Thule
Truk
Velho
West-Rusland
West-Siberi�
Yap
Yenisei
Yukon
Zuid-Amerika

# These should be here but words with dots and hyphens have to go 
# in individual languages lists
a-z
# aboot.conf
Ad-Hoc
ad-hoc
auto-boot
auto-boot-timeout
base-config
boot-command
boot-device
boot-file
Bourne-shell
CD-R
CD-RW
Command-Option-P-R
Device-mapper
dhcp-client
dm-crypt
EFI-fat
Fat-lib
grub-install
# initrd.img
Inter-User
# kernel-img.conf
# lilo.conf
loop-AES
lvm-mod
nnnn-nnnn-nn
OSA-Express
partman-crypt
partman-crypto
save-all
# sources.list
update-grub
