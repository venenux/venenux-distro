# Words with hyphens and dots which should be here
# but can't because of a limitation in the spellchecker
# They should be copied to languages lists
# and uncommented there
#aliases.0
app-defaults
apt-get
base-dir
conf.d
create-home
Debian.gz
double-buffering
dpkg-reconfigure
exim-tls
home-dir
init.d
keep-tokens
lock-time
login.defs
move-home
multi-head
non-unique
Non-interactive
PCI-Express
preserve-environment
Reply-To
Return-Path
sources.list
X.Org
xkb-data
xorg.conf
xkeyboard-config
xserver-xorg
